<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnAttrToAddonDishTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('addon_dish', function (Blueprint $table) {
            $table->string('min')->after('dish_id')->nullable();
            $table->string('max')->after('min')->nullable();
            $table->string('condition')->after('max')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('addon_dish', function (Blueprint $table) {
            //
        });
    }
}
