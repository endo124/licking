'use strict'

// start owl-carousel


$(document).ready(function(){ 
	$('.owl-carousel').owlCarousel({
		autoplayHoverPause:true,
		autoplay: true,
		autoplayTimeout: 5000,
		navigation: false,
		loop:true,
		margin:10,
		responsiveClass:true,
		responsive:{
			0:{
				items:1,
				nav:true
			},
			600:{
				items:3,
				nav:false
			},
			1000:{
				items:3,
				nav:true,
				loop:false
			}
		}
	})
}); // End OWL CAROUSELE 

// Start Shop-Button 
//Get the button
    var mybutton = document.getElementById("myBtn");

    // When the user scrolls down 20px from the top of the document, show the button
    window.onscroll = function() {scrollFunction()};

    function scrollFunction() {
      if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
        mybutton.style.display = "block";
      } else {
        mybutton.style.display = "none";
      }
    }

    // When the user clicks on the button, scroll to the top of the document
    function topFunction() {
      document.body.scrollTop = 0;
      document.documentElement.scrollTop = 0;
    }

   // END SHOP BUTTON 


   // START LOADER 
   var myVar;

    function loadFunction() {
      myVar = setTimeout(showPage, 3000);
    }

    function showPage() {
      document.getElementById("loader").style.display = "none";
      document.getElementById("loaderDiv").style.display = "block";
    }