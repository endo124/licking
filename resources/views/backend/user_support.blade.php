@extends('backend.layouts.app')

@push('style')

<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">

<script src=" https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>



<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://www.gstatic.com/firebasejs/7.8.1/firebase-app.js"></script>
<script src="https://www.gstatic.com/firebasejs/7.8.1/firebase-auth.js"></script>
<script src="https://www.gstatic.com/firebasejs/7.8.1/firebase-database.js"></script>
<script src="https://www.gstatic.com/firebasejs/7.8.1/firebase-storage.js"></script>
<script src="https://cdn.firebase.com/libs/firebaseui/3.5.2/firebaseui.js"></script>
<script src = "https://cdnjs.cloudflare.com/ajax/libs/node-uuid/1.4.8/uuid.min.js"></script>
<!------ Include the above in your HEAD tag ---------->




<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" type="text/css" rel="stylesheet">
<style>
    .container{max-width:1170px; margin:auto;}
img{ max-width:100%;}
.inbox_people {
  background: #f8f8f8 none repeat scroll 0 0;
  float: left;
  overflow: hidden;
  width: 40%; border-right:1px solid #c4c4c4;
}
.inbox_msg {
  border: 1px solid #c4c4c4;
  clear: both;
  overflow: hidden;
}
.top_spac{ margin: 20px 0 0;}


.recent_heading {float: left; width:40%;}
.srch_bar {
  display: inline-block;
  text-align: right;
  width: 60%;
}
.headind_srch{ padding:10px 29px 10px 20px; overflow:hidden; border-bottom:1px solid #c4c4c4;}

.recent_heading h4 {
  color: #05728f;
  font-size: 21px;
  margin: auto;
}
.srch_bar input{ border:1px solid #cdcdcd; border-width:0 0 1px 0; width:80%; padding:2px 0 4px 6px; background:none;}
.srch_bar .input-group-addon button {
  background: rgba(0, 0, 0, 0) none repeat scroll 0 0;
  border: medium none;
  padding: 0;
  color: #707070;
  font-size: 18px;
}
.srch_bar .input-group-addon { margin: 0 0 0 -27px;}

.chat_ib h5{ font-size:15px; color:#464646; margin:0 0 8px 0;}
.chat_ib h5 span{ font-size:13px; float:right;}
.chat_ib p{ font-size:14px; color:#989898; margin:auto}
.chat_img {
  float: left;
  width: 11%;
}
.chat_ib {
  float: left;
  padding: 0 0 0 15px;
  width: 88%;
}

.chat_people{ overflow:hidden; clear:both;}
.chat_list {
  border-bottom: 1px solid #c4c4c4;
  margin: 0;
  padding: 18px 16px 10px;
}
.inbox_chat { height: 550px; overflow-y: scroll;}

.active_chat{ background:#ebebeb;}

.incoming_msg_img {
  display: inline-block;
  width: 6%;
}
.received_msg {
  display: inline-block;
  padding: 0 0 0 10px;
  vertical-align: top;
  width: 92%;
 }
 .received_withd_msg p {
  background: #ebebeb none repeat scroll 0 0;
  border-radius: 3px;
  color: #646464;
  font-size: 14px;
  margin: 0;
  padding: 5px 10px 5px 12px;
  width: 100%;
}
.time_date {
  color: #747474;
  display: block;
  font-size: 12px;
  margin: 8px 0 0;
}
.received_withd_msg { width: 57%;}
.mesgs {
  float: left;
  padding: 30px 15px 0 25px;
  width: 100%;
}

 .sent_msg p {
  background: #05728f none repeat scroll 0 0;
  border-radius: 3px;
  font-size: 14px;
  margin: 0; color:#fff;
  padding: 5px 10px 5px 12px;
  width:100%;
}
.outgoing_msg{ overflow:hidden; margin:26px 0 26px;}
.sent_msg {
  float: right;
  width: 46%;
}
.input_msg_write input {
  background: rgba(0, 0, 0, 0) none repeat scroll 0 0;
  border: medium none;
  color: #4c4c4c;
  font-size: 15px;
  min-height: 48px;
  width: 100%;
}

.type_msg {border-top: 1px solid #c4c4c4;position: relative;}
.msg_send_btn {
  background: #05728f none repeat scroll 0 0;
  border: medium none;
  border-radius: 50%;
  color: #fff;
  cursor: pointer;
  font-size: 17px;
  height: 33px;
  position: absolute;
  right: 0;
  top: 11px;
  width: 33px;
}
.messaging { padding: 0 0 50px 0;}
.msg_history {
  height: 516px;
  overflow-y: auto;
}
</style>
@section('content')

 <div class="page-wrapper">
    <div class="content container-fluid">
       <!-- Page Header -->
      <div class="page-header" >
          <div class="row">
              <div class="col-sm-12">
                  <h3 class="page-title"> @lang('site.support') </h3>
                  <ul class="breadcrumb">
                      <li class="breadcrumb-item"><a href="/dashboard">@lang('site.Dashboard')</a></li>
                      <li class="breadcrumb-item active">@lang('site.support')</li>
                  </ul>
              </div>
          {{-- @if (!Auth::guard('order')) --}}
              {{-- <div class="col-sm-12 col">
                  {{-- <a href="#Add_orders" data-toggle="modal" class="btn btn-otbokhly float-right mt-2 {{ auth()->guard('order') ? 'disabled' : '' }}" >Add</a> --}}
                  {{-- <a href="#Add_orders" data-toggle="modal" class="btn btn-otbokhly float-right mt-2   " >Add</a>
              </div>  --}}
          {{-- @endif --}}
          </div>

      </div>
      <!-- /Page Header -->
      <div class="row" >
        <div class="col-md-12 d-flex">


            <div class="messaging w-100">
              <div class="inbox_msg">
              
                <div class="mesgs">
                  <div class="msg_history"  id = "messages">
                
                  
                  
                  </div>
                  <div class="type_msg">
                    <div class="input_msg_write">
                        <form id = "messageForm" autocomplete="off">
                      <input  id = "msg-input" type="text" class="write_msg" placeholder="@lang('site.Type a message')" />
                      <button   id = "msg-btn" class="msg_send_btn" type="submit"><i class="fas fa-location-arrow"></i></button>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
        </div>
      </div>
    </div>

</div>
   
@endsection
   
@push('scripts')

    <script src='/chat/js/app.js'></script>    
    <script>

let email = "";
let name = "";
let uid=""
const msgScreen = document.getElementById("messages");
const msgForm = document.getElementById("messageForm");
const msgInput = document.getElementById("msg-input");
const msgBtn = document.getElementById("msg-btn");
const userName = document.getElementById("user-name");
const db = firebase.database();
const msgRef = db.ref("/msgs"); //save in msgs folder in database

function init(){

    
          // User is signed in. Get their name.
          email =<?php echo json_encode(auth('admin')->user()->email); ?>;
           uid =<?php echo json_encode(auth('admin')->user()->id); ?>;
     
           db.ref(`/msgs/${uid}`).on('child_added', updateMsgs);
     
         
        
    

  
    msgForm.addEventListener('submit', sendMessage);
}



const updateMsgs = data =>{
  const {email: userEmail , name, text,date} = data.val();

  //Check the encrypting mode
  var encryptMode = fetchJson();
  var outputText = text;
  
  if(encryptMode == "nr"){
    outputText = normalEncrypt(outputText);
  }else if(encryptMode == "cr"){
    outputText = crazyEncrypt(outputText);
  }
  
  //load messages
//   const msg = `<li class="${email == userEmail ? "msg my": "msg"}"><span class = "msg-span">
//     <i class = "name">${name}: </i>${outputText}
//     </span>
//   </li>`
//   msgScreen.innerHTML += msg;
//   document.getElementById("chat-window").scrollTop = document.getElementById("chat-window").scrollHeight;
  //auto scroll to bottom

  if(email == userEmail)
  {
        var msg = ` <div class="incoming_msg">
            
        <div class="received_msg">
          <div class="received_withd_msg">
          <i class = "name">${email}: </i>
            <p>${outputText}</p>
            <span class="time_date"> ${new Date(date).toLocaleString()}   </span>
           </div>
        </div>
      </div>`
  }
  else
  {
    var msg = ` <div class="outgoing_msg">
            
    <div class="received_msg">
      <div class="sent_msg">
      <i class = "name">Admin </i>
        <p>${outputText}</p>
        <span class="time_date"> ${new Date(date).toLocaleString()}   </span>
       </div>
    </div>
  </div>`

  }

  msgScreen.innerHTML += msg;
  document.getElementById("messages").scrollTop = document.getElementById("messages").scrollHeight;


}

function sendMessage(e){
  e.preventDefault();
  const text = msgInput.value;

    if(!text.trim()) return alert('Please type your message.'); //no msg submitted
    const msg = {
        email,
        name:email,
        text: text,
        date:Date.now()
    };

    db.ref(`/msgs/${uid}`).push(msg);
 
    db.ref(`/new_user/${uid}`).update({'name':email,'email':email, 'text': text, 'date':Date.now(),'uid':uid})
    msgInput.value = "";
}

//Get encryption settings
function fetchJson(){
  var settings = JSON.parse(localStorage.getItem('settings'));
  return settings;
}


function crazyEncrypt(text){
  var words = text.replace(/[\r\n]/g, '').toLowerCase().split(' ');
  var newWord = '';
  var newArr =[];

  words.map(function(w) {
    if(w.length > 1){
      w.split('').map(function() {
        var hash = Math.floor(Math.random() * w.length);
        newWord += w[hash];
        w = w.replace(w.charAt(hash), '');
      });
      newArr.push(newWord);
      newWord = '';
    
    }else{
      newArr.push(w);
    }
  });
  text = newArr.join(' ');
  return text;
}

//Normal encryption - first and last letter fixed position
function normalEncrypt(text){
  var words = text.replace(/[\r\n]/g, '').toLowerCase().split(' ');
  var newWord = '';
  var newArr =[];

  words.map(function(w) {
    if(w.length > 1){
      var lastIndex = w.length-1;
      var lastLetter = w[lastIndex];

      //add the first letter
      newWord += w[0];
      w = w.slice(1,lastIndex);

      //scramble only letters in between the first and last letter
      w.split('').map(function(x) { 
          var hash = Math.floor(Math.random() * w.length);
          newWord += w[hash];
          w = w.replace(w.charAt(hash), '');
      });

      //add the last letter
      newWord+=lastLetter;
      newArr.push(newWord);
      newWord = '';
    }else{
      newArr.push(w);
    }
  });
  text = newArr.join(' ');
  return text;
}
document.addEventListener('DOMContentLoaded',init);
</script>
@endpush

