@extends('backend.layouts.app')


@section('content')
 <!-- Page Wrapper -->
 <div class="page-wrapper">
    <div class="content container-fluid">

        <!-- Page Header -->
        <div class="page-header">
            <div class="row">
                <div class="col-sm-12">
                    <h3 class="page-title">List of sections</h3>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
                        <li class="breadcrumb-item active">section</li>
                    </ul>
                </div>
               {{-- @if (!Auth::guard('cook')) --}}
                <div class="col-sm-12 col">
                    <a href="#Add_section" data-toggle="modal" class="btn btn-otbokhly float-right mt-2 {{ auth()->guard('admin')->user()->parent?'disabled' : '' }}" >Add</a>
                </div>
               {{-- @endif --}}
            </div>

        </div>
        <!-- /Page Header -->


        <div class="row" >
            <div class="col-md-12 d-flex">

                <!-- Recent Orders -->
                <div class="card card-table flex-fill">
                    <div class="card-header">
                        <h4 class="card-title">section List</h4>

                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-hover table-center mb-0">
                                <thead>
                                    <tr>
                                        <th> #</th>
                                        <th> Name</th>
                                        @if (auth()->guard('admin')->user()->roles[0]->name !='cook')
                                        <th>Vendor Name</th>
                                        @endif
                                        <th> From</th>
                                        <th> To</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @isset ($sections)
                                        @foreach ($sections as $index=>$section)
                                        <tr>
                                            <td>
                                                {{ $index }}
                                            </td>

                                            <td>{{ $section->name }}</td>
                                            @if (auth()->guard('admin')->user()->roles[0]->name !='cook')
                                            <td>{{ $section->users[0]->name ?? '' }}</td>
                                            @endif
                                            <td>{{ $section['available_from'] }}</td>
                                            <td>{{ $section['available_to']  }}</td>

                                            <td >
                                                <div class="actions">
                                                    <a id="{{ $section->id }}" class="link_update bg-success-light mr-2  {{ auth()->guard('admin')->user()->parent?'disabled' : '' }}" data-toggle="modal"  href="#edit_sections_details{{ $section->id }}" >
                                                        <i class="fe fe-pencil"></i> Edit
                                                    </a>
                                                    <a id="{{ $section->id }}" class="link_delete  {{ auth()->guard('admin')->user()->parent?'disabled' : '' }}" data-toggle="modal" href="#delete_modal" data-url=""  class="btn btn-sm btn-danger" style="color: #f00">
                                                        <i class="fe fe-trash"></i> Delete
                                                    </a>
                                                </div>
                                            </td>
                                        </tr>
                                         <!-- Edit Details Modal -->
                                        <div class="modal fade" id="edit_sections_details{{ $section->id }}" aria-hidden="true" role="dialog">
                                            <div class="modal-dialog modal-dialog-centered" role="document" >
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title">Edit sections</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="form-group errors" style="display: none" >
                                                            <div class="alert alert-danger">
                                                              <ul class="list">

                                                              </ul>
                                                            </div>
                                                        </div>
                                                        <form id="formUpdate" method="post" action="{{ route('section.update',$section->id) }}" enctype="multipart/form-data">
                                                            @csrf
                                                            @method('put')
                                                            <div class="row form-row">
                                                                @if (auth()->guard('admin')->user()->roles[0]->name !='cook')
                                                                <div class="col-12 col-sm-12">
                                                                    <label>Vendors : </label>

                                                                    <select id="Vendors" name="Vendor"   style="padding: 10px;background: #E0E0E0;color: #000;border: none;margin-left: 20px;width: 64%;">
                                                                        <option value="">select vendor</option>
                                                                        @foreach ($cooks as $cook)
                                                                            <option value="{{ $cook->id }}"  {{ $cook->id == $section->users->first()->id ? 'selected' : '' }}>{{ $cook->name }}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            @else
                                                            <input name="Vendor" value="{{ auth()->guard('admin')->user()->id }}" type="text" hidden>
                                                            @endif
                                                                @foreach (config('translatable.locales') as $locale)
                                                                <div class="col-12 col-sm-12">
                                                                    <div class="form-group">
                                                                        <label>@lang('site.'.$locale.'.name')</label>
                                                                        @isset($section)
                                                                        <input type="text" name="{{ $locale }}[name]" class="form-control" value="{{ $section->translate($locale)->name }}">
                                                                        @endisset
                                                                    </div>
                                                                </div>
                                                                @endforeach
                                                            </div>
                                                            <div class="col-12 col-sm-12">
                                                                <div class="form-group">
                                                                    <label>From</label>
                                                                    <input type="time" name="from" class="form-control"  style="display: inline-block; width:43%" value="{{$section['available_from'] ?? old('from') }}">
                                                                    <label>To</label>
                                                                    <input type="time" name="to" class="form-control"  style="display: inline-block; width:43%" value="{{ $section['available_to'] ??old('to') }}">
                                                                </div>
                                                            </div>
                                                            <button type="submit" class="btn btn-primary btn-block">Save Changes</button>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- /Edit Details Modal -->
                                        @endforeach
                                    @endisset

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- /Recent Orders -->

            </div>
        </div>

    </div>
</div>
<!-- /Page Wrapper -->

  <!-- Add Modal -->
  <div class="modal fade" id="Add_section" aria-hidden="true" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document" >
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Add section</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                @include('backend.partials.errors')
                <div class="form-group errors" style="display: none" >
                    <div class="alert alert-danger">
                      <ul class="list">

                      </ul>
                    </div>
                </div>
                <form method="POST" action="{{ route('section.store') }}">
                    @csrf
                    <div class="row form-row">
                        @if (auth()->guard('admin')->user()->roles[0]->name !='cook')
                        <div class="col-12 col-sm-12">
                            <label>Vendors : </label>
                            <select id="Vendors" name="Vendor"   style="padding: 10px;background: #E0E0E0;color: #000;border: none;margin-left: 20px;width: 64%;">
                                <option value="">select vendor</option>

                                @foreach ($cooks as $cook)

                                    <option value="{{ $cook->id }}">{{ $cook->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    @else
                    <input name="Vendor" value="{{ auth()->guard('admin')->user()->id }}" type="text" hidden>
                    @endif
                        @foreach (config('translatable.locales') as $locale)
                        <div class="col-12 col-sm-12">
                            <div class="form-group">
                                <label>{{ trans('site.'.$locale.'.name') }}</label>
                                <input type="text" name="{{ $locale }}[name]" class="form-control" value="{{ old($locale.'.name') }}">
                            </div>
                        </div>

                        @endforeach
                        <div class="col-12 col-sm-12">
                            <div class="form-group">
                                <label>From</label>
                                <input type="time" name="from" class="form-control"  style="display: inline-block; width:43%" value="{{ old('from') }}">
                                <label>To</label>
                                <input type="time" name="to" class="form-control"  style="display: inline-block; width:43%" value="{{ old('to') }}">
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-otbokhly btn-block">Save</button>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- /ADD Modal -->

      <!-- Delete Modal -->

      <div class="modal fade" id="delete_modal" aria-hidden="true" role="dialog">
        <div class="modal-dialog modal-dialog-centered" role="document" >
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Delete</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-content p-2">
                        <h4 class="modal-title">Delete</h4>
                        <p class="mb-4">Are you sure want to delete?</p>

                        <form  id="formDelete" action="" method="POST" style="display: inline">
                            @csrf
                            @method('delete')
                            <button class="btn btn-primary btn-delete" type="submit" class="btn btn-primary">Delete </button>
                        </form>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Delete Modal -->


@endsection


@push('scripts')
    <script>
       var err= <?php echo json_encode(session()->get('message_update')); ?>;
                var errors_add= <?php echo json_encode(session()->get('message_add')); ?>;

                if(err !=null) {
                    let id= <?php echo json_encode(session()->get('section_id')); ?>;
                    let modal_edit='#edit_sections_details'+id;
                    for (x in err) {
                        var err='<li>'+ err[x]+'</li>';
                        $('.list').append(err);
                    }
                    $('.errors').css('display','block');
                    $(modal_edit).modal('show');
                }

                else if(errors_add != null)
                {
                    for (x in errors_add) {
                        var err='<li>'+ errors_add[x]+'</li>';
                        $('.list').append(err);
                    }
                    $('.errors').css('display','block');

                    $('#Add_section').modal('show');

                }

                $(".modal").on("hidden.bs.modal", () => {
                    $('.errors').css('display','none');
                });

        // $('.link_update').on('click',function(){
        //     var section_id=$(this).attr('id');

        //     $('#formUpdate').attr('action',')

        // })
        $('.link_delete').on('click',function(){
            var section_id=$(this).attr('id');

            $('#formDelete').attr('action','/{{ Config::get('app.locale') }}/dashboard/section/'+section_id)

        })
    </script>
@endpush
