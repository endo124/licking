@extends('backend.layouts.app')

<style>
    .profile
{
    min-height: 355px;

    }
figcaption.ratings
{
    margin-top:20px;
    }
figcaption.ratings a
{
    color:#f1c40f;
    font-size:11px;
    }
figcaption.ratings a:hover
{
    color:#f39c12;
    text-decoration:none;
    }
.divider
{
    border-top:1px solid rgba(0,0,0,0.1);
    }
.emphasis
{
    border-top: 4px solid transparent;
    }
.emphasis:hover
{
    border-top: 4px solid #1abc9c;
    }
.emphasis h2
{
    margin-bottom:0;
    }
span.tags
{
    background: #1abc9c;
    border-radius: 2px;
    color: #f5f5f5;
    font-weight: bold;
    padding: 2px 4px;
    }
.dropdown-menu
{
    background-color: #34495e;
    box-shadow: none;
    -webkit-box-shadow: none;
    width: 250px;
    margin-left: -125px;
    left: 50%;
    }
.dropdown-menu .divider
{
    background:none;
    }
.dropdown-menu>li>a
{
    color:#f5f5f5;
    }
.dropup .dropdown-menu
{
    margin-bottom:10px;
    }
.dropup .dropdown-menu:before
{
    content: "";
    border-top: 10px solid #34495e;
    border-right: 10px solid transparent;
    border-left: 10px solid transparent;
    position: absolute;
    bottom: -10px;
    left: 50%;
    margin-left: -10px;
    z-index: 10;
    }
    /* -----------------------------------------
  =CSS3 Loading animations
-------------------------------------------- */

/* =Elements style
---------------------- */
.load-wrapp {
  float: left;
  width: 100px;
  height: 100px;
  margin: 0 10px 10px 0;
  padding: 20px 20px 20px;
  border-radius: 5px;
  text-align: center;
  background-color: #d8d8d8;
}

.load-wrapp p {
  padding: 0 0 20px;
}
.load-wrapp:last-child {
  margin-right: 0;
}

.line {
  display: inline-block;
  width: 15px;
  height: 15px;
  border-radius: 15px;
  background-color: #4b9cdb;
}

.ring-1 {
  width: 10px;
  height: 10px;
  margin: 0 auto;
  padding: 10px;
  border: 7px dashed #4b9cdb;
  border-radius: 100%;
}

.ring-2 {
  position: relative;
  width: 45px;
  height: 45px;
  margin: 0 auto;
  border: 4px solid #4b9cdb;
  border-radius: 100%;
}

.ball-holder {
  position: absolute;
  width: 12px;
  height: 45px;
  left: 17px;
  top: 0px;
}

.ball {
  position: absolute;
  top: -11px;
  left: 0;
  width: 16px;
  height: 16px;
  border-radius: 100%;
  background: #4282b3;
}

.letter-holder {
  padding: 16px;
}

.letter {
  float: left;
  font-size: 14px;
  color: #777;
}

.square {
  width: 12px;
  height: 12px;
  border-radius: 4px;
  background-color: #4b9cdb;
}

.spinner {
  position: relative;
  width: 45px;
  height: 45px;
  margin: 0 auto;
}

.bubble-1,
.bubble-2 {
  position: absolute;
  top: 0;
  width: 25px;
  height: 25px;
  border-radius: 100%;
  background-color: #4b9cdb;
}

.bubble-2 {
  top: auto;
  bottom: 0;
}

.bar {
  float: left;
  width: 15px;
  height: 6px;
  border-radius: 2px;
  background-color: #4b9cdb;
}

/* =Animate the stuff
------------------------ */
.load-1 .line:nth-last-child(1) {
  animation: loadingA 1.5s 1s infinite;
}
.load-1 .line:nth-last-child(2) {
  animation: loadingA 1.5s 0.5s infinite;
}
.load-1 .line:nth-last-child(3) {
  animation: loadingA 1.5s 0s infinite;
}

.load-2 .line:nth-last-child(1) {
  animation: loadingB 1.5s 1s infinite;
}
.load-2 .line:nth-last-child(2) {
  animation: loadingB 1.5s 0.5s infinite;
}
.load-2 .line:nth-last-child(3) {
  animation: loadingB 1.5s 0s infinite;
}

.load-3 .line:nth-last-child(1) {
  animation: loadingC 0.6s 0.1s linear infinite;
}
.load-3 .line:nth-last-child(2) {
  animation: loadingC 0.6s 0.2s linear infinite;
}
.load-3 .line:nth-last-child(3) {
  animation: loadingC 0.6s 0.3s linear infinite;
}

.load-4 .ring-1 {
  animation: loadingD 1.5s 0.3s cubic-bezier(0.17, 0.37, 0.43, 0.67) infinite;
}

.load-5 .ball-holder {
  animation: loadingE 1.3s linear infinite;
}

.load-6 .letter {
  animation-name: loadingF;
  animation-duration: 1.6s;
  animation-iteration-count: infinite;
  animation-direction: linear;
}

.l-1 {
  animation-delay: 0.48s;
}
.l-2 {
  animation-delay: 0.6s;
}
.l-3 {
  animation-delay: 0.72s;
}
.l-4 {
  animation-delay: 0.84s;
}
.l-5 {
  animation-delay: 0.96s;
}
.l-6 {
  animation-delay: 1.08s;
}
.l-7 {
  animation-delay: 1.2s;
}
.l-8 {
  animation-delay: 1.32s;
}
.l-9 {
  animation-delay: 1.44s;
}
.l-10 {
  animation-delay: 1.56s;
}

.load-7 .square {
  animation: loadingG 1.5s cubic-bezier(0.17, 0.37, 0.43, 0.67) infinite;
}

.load-8 .line {
  animation: loadingH 1.5s cubic-bezier(0.17, 0.37, 0.43, 0.67) infinite;
}

.load-9 .spinner {
  animation: loadingI 2s linear infinite;
}
.load-9 .bubble-1,
.load-9 .bubble-2 {
  animation: bounce 2s ease-in-out infinite;
}
.load-9 .bubble-2 {
  animation-delay: -1s;
}

.load-10 .bar {
  animation: loadingJ 2s cubic-bezier(0.17, 0.37, 0.43, 0.67) infinite;
}
/* Preloader */
.preloader {
  position: fixed;
  top: 0;
  left: 0;
  z-index: 999999999;
  width: 100%;
  height: 100%;
  background-color: #fff;
  overflow: hidden;
}
.preloader-inner {
  position: absolute;
  top: 50%;
  left: 50%;
  -webkit-transform: translate(-50%,-50%);
  -moz-transform: translate(-50%,-50%);
  transform: translate(-50%,-50%);
}
.preloader-icon {
  width: 100px;
  height: 100px;
  display: inline-block;
  padding: 0px;
}
.preloader-icon span {
  position: absolute;
  display: inline-block;
  width: 100px;
  height: 100px;
  border-radius: 100%;
  background:#C5171C;
  -webkit-animation: preloader-fx 1.6s linear infinite;
  animation: preloader-fx 1.6s linear infinite;
}
.preloader-icon span:last-child {
  animation-delay: -0.8s;
  -webkit-animation-delay: -0.8s;
}
@keyframes preloader-fx {
  0% {transform: scale(0, 0); opacity:0.5;}
  100% {transform: scale(1, 1); opacity:0;}
}
@-webkit-keyframes preloader-fx {
  0% {-webkit-transform: scale(0, 0); opacity:0.5;}
  100% {-webkit-transform: scale(1, 1); opacity:0;}
}
/* End Preloader */
</style>
@section('content')

<div class="page-wrapper">
    <div class="content container-fluid">

        <!-- Page Header -->
        <div class="page-header">
            <div class="row">
                <label for="cars" >Select Drivers</label>
                <meta name="csrf-token" content="{{ csrf_token() }}">

                <select name="drivers" id="drivers" style="max-height: 23px;">

                @foreach($drivers as $key=>$driver)

                  <option value="{{$key}}">{{$driver['driverFirstName'] .' '.$driver['driverLastName']}}</option>
                @endforeach
                </select>
                {{-- <div class="load-wrapp" id="loader" style="display: none;">
                      <div class="load-3">
                        <p>Loading 3</p>
                        <div class="line"></div>
                        <div class="line"></div>
                        <div class="line"></div>
                      </div>
                </div> --}}
                <!-- Preloader -->
	<div class="preloader" id="loader"  style="display: none;">
		<div class="preloader-inner">
			<div class="preloader-icon">
				<span></span>
				<span></span>
			</div>
		</div>
	</div>
	<!-- End Preloader -->
                <div class="row" id="showData" style="display: none;">
                        <div class="col-md-offset-2 col-md-8 col-lg-offset-3 col-lg-12">
                         <div class="well profile">
                            <div class="col-sm-12">
                                <div class="col-xs-12 col-sm-12">
                                    <h2 id="name"></h2>
                                    <strong>Email: </strong> <p id="email">< </p>
                                    <strong>Phone: </strong> <p id="phone"> </p>
                                    <strong>Team: </strong> <p id="team"> </p>
                                    <strong>Status: </strong> <p id="status"> </p>
                                    <strong>Transportation: </strong> <p id="transportation"> </p>
                                    <div id="tasks">
                                    </div>

                                </div>

                            </div>

                         </div>
                        </div>
                        <div class="noMap" style="display: none">
                            <h1 style="color: red">No Location Found</h1>
                        </div>
                </div>

                    <div id='map' style='height: 400px;background-color:#08c;width: 100%; display:none;'></div>

            </div>

        </div>
        <!-- /Page Header -->
    </div>
</div>




@endsection


@push('scripts')

<script>
$(document).ready(function(){
  $("#drivers").change(function(){
    var selectedDriver = $(this).children("option:selected").val();
    var id = { id : selectedDriver }

console.log(id,'id')
    $.ajaxSetup({
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  }
});
document.getElementById('showData').style.display="none"
document.getElementById('map').style.display="none"
document.getElementById('name').innerHTML=""
document.getElementById('email').innerHTML=""
document.getElementById('phone').innerHTML=""
document.getElementById('status').innerHTML=""
document.getElementById('transportation').innerHTML=""
document.getElementById('team').innerHTML=""
document.getElementById('loader').style.display="block"
var getData = $.ajax({
    headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
      type: 'POST',
      url: "/{{ Config::get('app.locale') }}/dashboard/getDataDriver",
      data: id,
      dataType : 'json',

      success: function(resultData) {

document.getElementById('name').innerHTML=resultData.data.driverFirstName +' ' +resultData.data.driverLastName
document.getElementById('email').innerHTML=resultData.data.driverEmail
document.getElementById('phone').innerHTML=resultData.data.driverPhoneNumber
document.getElementById('transportation').innerHTML=resultData.data.driverTransportation
document.getElementById('team').innerHTML=resultData.data.driverTeam.name

if(resultData.data.driverStatus ==1)
{
    document.getElementById('status').innerHTML="Inactive"

}
else if(resultData.data.driverStatus ==0)
{
    document.getElementById('status').innerHTML="Free"

}
else if(resultData.data.driverStatus ==-1)
{
    document.getElementById('status').innerHTML="Busy"

}
$('.noMap').hide();
if(typeof resultData.data.currantLatitude!=='undefined'){
    initialize(resultData.data.currantLatitude,resultData.data.currantLongitude);
}else{
    $('.noMap').show();
}
document.getElementById('loader').style.display="none"

document.getElementById('showData').style.display="block"


       }


});
  });

  function initialize(lat,lng) {
    document.getElementById('map').style.display="block"
        var mapOptions = {
            center: new google.maps.LatLng(parseFloat(lat),parseFloat(lng)),
            zoom: 16
        };
        var map = new google.maps.Map(document.getElementById("map"), mapOptions);
        const marker3 = new google.maps.Marker({
                        position: { lat:parseFloat(lat),lng:parseFloat(lng)},
                        map: map,
                        label: "Driver",

                      });

    }
});
</script>
@endpush


