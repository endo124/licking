@extends('backend.layouts.app')

@push('style')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.23/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/fixedheader/3.1.8/css/fixedHeader.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.5/css/buttons.dataTables.min.css    ">
@endpush

<style>
    thead input {
        width: 100%;
    }
</style>
@section('content')
 <!-- Page Wrapper -->
 <div class="page-wrapper">
    <div class="content container-fluid">

        <!-- Page Header -->
        <div class="page-header">
            <div class="row">
                <div class="col-sm-12">
                    <h3 class="page-title">@lang('site.List of reports')</h3>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('/dashboard') }}">@lang('site.Dashboard')</a></li>
                        <li class="breadcrumb-item active">@lang('site.reports')</li>
                    </ul>
                </div>
               {{-- @if (!Auth::guard('cook')) --}}
                {{-- <div class="col-sm-12 col">
                    <a href="#Add_feast" data-toggle="modal" class="btn btn-otbokhly float-right mt-2 " >Add</a>
                </div> --}}
               {{-- @endif --}}
            </div>

        </div>
        <!-- /Page Header -->


        <div class="row" >
            <div class="col-md-12 d-flex">

                <!-- Recent Orders -->
                <div class="card card-table flex-fill">
                    <div class="card-header">
                        <h4 class="card-title">@lang('site.Reports List')</h4>

                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="example" class="display" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>@lang('site.Restaurant')</th>
                                        <th>@lang('site.Customer')</th>
                                        <th>@lang('site.Date')</th>
                                        <th>@lang('site.Notes')</th>
                                        {{-- <th>Commission</th> --}}
                                        <th>@lang('site.Tax')</th>
                                        <th>@lang('site.Delivery Fees')</th>
                                        <th>@lang('site.Delivery Type')</th>
                                        {{-- <th>Discount</th> --}}
                                        <th>@lang('site.Total price')</th>
                                        <th>@lang('site.Driver Name')</th>
                                        <th>@lang('site.Admin notes')</th>
                                        {{-- <th>Total profit</th>
                                        <th>Revenue</th> --}}
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($orders as $index=> $order)
                                    @php
                                        $cook=\App\Models\User::where('id',$order->cook_id)->first();
                                        $cook_name=$cook->name;
                                        $tax=$cook->taxes;
                                        $taxtype=$cook->taxes_type;
                                        if($taxtype == '%'){
                                            $taxes=$order->total_price - ($tax*$order->total_price /100)  ;
                                        }else{
                                            $taxes=  $tax ;

                                        }
                                        $customer=\App\Models\User::where('id',$order->user_id)->first();
                                        $customer_name=$customer->name;
                                        if ($cook->commission_type == '%') {
                                            $commission=($order->total_price *$cook->commission)/100 ;
                                        }else{
                                        $commission=$cook->commission . " " .$cook->commission_type ;
                                        }

                                    $fees=0;
                                    $discount=0;
                                    if($order->delivery_fees){
                                        $fees=$order->delivery_fees;
                                    }

                                    if(isset($order->coupon['discount_type'])){
                                        if ($order->coupon['discount_type'] =="%") {
                                            // $discount= ($order->coupon['total']*$order->total_price)/100;
                                        }
                                        else if ($order->coupon['discount_type'] == $currency) {
                                            $discount= $order->coupon['total'];
                                        }
                                    }
                                    if ($order->total_price) {
                                        $total= $order->total_price;
                                    }
                                    $revenue =$fees+$discount+ $total;
                                    $profit =floatVal($commission) -(intVal($discount)/2) .' '.$currency;

                                    @endphp
                                    <tr>
                                        <td>{{ $order->id }}</td>
                                        <td>{{ $cook_name }}</td>
                                        <td>{{ $customer_name }}</td>
                                        <td>{{ $order->date }}</td>
                                        <td>{{ $order->note ?? '' }}</td>
                                        <td>{{ $taxes ?? $taxes .' ' }} @lang('site.'.$currency )</td>
                                        {{-- <td>{{ $commission ?? '0'}} {{ $currency }}</td> --}}
                                        <td class="fees">{{ $order->delivery_fees ?? '0' }} @lang('site.'.$currency )</td>
                                        <td>{{ $order->delivery_type == '1' ? 'delivery' : 'pickup' }}</td>

                                        {{-- @if($order->coupon)
                                          <td class="discount">{{ $order->coupon['discount_type'] =="%" ? ($order->coupon['total']*$order->total_price)/100 : $order->coupon['total'] }} {{ $currency }}</td>
                                        @else
                                        <td class="discount">0 {{ $currency }}</td>

                                        @endif --}}
                                        <td class="total">{{ $order->total_price }} @lang('site.'.$currency )</td>
                                        <td class="profit">{{ $order->adminnote?? '' }}</td>
                                        <td class="revenue">{{ $order->drivername??' '}} </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- /Recent Orders -->

            </div>
        </div>

    </div>
</div>
<!-- /Page Wrapper -->


      <!-- Delete Modal -->
{{--
      <div class="modal fade" id="delete_modal" aria-hidden="true" role="dialog">
        <div class="modal-dialog modal-dialog-centered" role="document" >
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Delete</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-content p-2">
                        <h4 class="modal-title">Delete</h4>
                        <p class="mb-4">Are you sure want to delete?</p>

                        <form  id="formDelete" action="" method="POST" style="display: inline">
                            @csrf
                            @method('delete')
                            <button class="btn btn-primary btn-delete" type="submit" class="btn btn-primary">Delete </button>
                        </form>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </div> --}}
    <!-- /Delete Modal -->


@endsection


@push('scripts')
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.5/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.print.min.js"></script>
<script src="https://cdn.datatables.net/fixedheader/3.1.8/js/dataTables.fixedHeader.min.js"></script>
    <script>

        $( document ).ready(function() {
            $('.revenue').html()
        });


        // @if (count($errors) > 0)
        //         $('#Add_feast').modal('show');
        // @endif

        // $('.link_update').on('click',function(){
        //     var feast_id=$(this).attr('id');

        //     $('#formUpdate').attr('action','/{{ Config::get('app.locale') }}/dashboard/feast/'+feast_id)

        // })
        // $('.link_delete').on('click',function(){
        //     var feast_id=$(this).attr('id');

        //     $('#formDelete').attr('action','/{{ Config::get('app.locale') }}/dashboard/feast/'+feast_id)

        // })
        $(document).ready(function() {
    // Setup - add a text input to each footer cell
    $('#example thead tr').clone(true).appendTo( '#example thead' );
    $('#example thead tr:eq(1) th').each( function (i) {
        var title = $(this).text();
        $(this).html( '<input type="text" placeholder=" '+title+'" />' );

        $( 'input', this ).on( 'keyup change', function () {
            if ( table.column(i).search() !== this.value ) {
                table
                    .column(i)
                    .search( this.value )
                    .draw();
            }
        });
    });
    var lang='{{ Config::get('app.locale') }}';
    if (lang == 'ar') {
        var url = '//cdn.datatables.net/plug-ins/1.10.24/i18n/Arabic.json';
    }
    var table = $('#example').DataTable( {

        language: {
            url: url
        },
        orderCellsTop: true,
        fixedHeader: true,
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    } );

} );

    </script>
@endpush
