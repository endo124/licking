@extends('backend.layouts.app')

@push('style')

    <style>
        .avatar-sm {
         width: 4.5rem !important;
        }
        .sub_table{
            width: 100%
        }
        /* .disabled{

        }         */
        .modal {
    display:    none;}
        /* When the body has the loading class, we turn
   the scrollbar off with overflow:hidden */
body.loading .modal {
    overflow: hidden;
}

/* Anytime the body has the loading class, our
   modal element will be visible */
body.loading .modal {
    display: block;
}
.past-order{
    padding:3px 5px;
    border:1px solid green;
    margin:0 3px !important
}
    </style>
@endpush

@section('content')

 <!-- Page Wrapper -->
 <div class="page-wrapper">
    <div class="content container-fluid">

       
        <div class="card card-table">
            <div class="card-header">
                <h4 class="card-title">@lang('site.Current Activities List')</h4>
            </div>
            <div class="card-body scroll">
                <div class="table-responsive">
                    <table class="table table-hover table-center mb-0">
                        <thead>
                            <tr>
                                <th>@lang('site.Order Number')</th>
                                <th>@lang('site.Order Time')</th>
                                <th>@lang('site.Order Date')</th>
                                <th>@lang('site.Customer Name')</th>
                                @if (!auth()->guard('admin')->user()->hasRole('cook'))
                                    <th>@lang('site.Restaurant Name')</th>
                                @endif

                                {{-- <th>
                                    <table>
                                        <tr>
                                            <th>Dish Name</th>
                                            <th>Dish Section</th>
                                            <th>Dish price</th>
                                            <th>Amount</th>
                                        </tr>
                                    </table>
                                </th> --}}
                                <th>@lang('site.Quantity')</th>
                                @if (!auth()->guard('admin')->user()->hasRole('cook'))
                                    <th>@lang('site.Location')</th>
                                @endif
                                @if (!auth()->guard('admin')->user()->hasRole('cook'))
                                    <th>@lang('site.Total Price')</th>
                                    <th>@lang('site.Payment Method')</th>
                                    <th>@lang('site.Payment Status')</th>
                                    <th>@lang('site.Delivery Status')</th>
                                    <th>@lang('site.Order Status')</th>
                                    <th>@lang('site.Customer service')</th>
                                @endif
                                {{-- <th>Operations service</th> --}}
                                <th>@lang('site.Delivery Type')</th>
                                {{-- <th>@lang('site.Admin Note')</th> --}}

                                <th>@lang('site.Action')</th>
                            </tr>
                        </thead>
                        <tbody>
                            @isset($orders)
                                @php
                                        $qty=0;
                                        $address=[];
                                @endphp
                                @foreach ($currentActivities as $index=>$order)
                                    @php
                                        $cook=\App\Models\User::where('id',$order->cook_id)->first();
                                        $cook_name=$cook->name;
                                    @endphp
                                    <tr>
                                        <td>{{ $index +1 }}</td>
                                        <td>{{date('h:i A', strtotime($order->time ))  }}</td>
                                        <td>{{$order->date  }}</td>
                                        <td>
                                            <h2 class="table-avatar">
                                                {{-- <a href="profile.html" class="avatar avatar-sm mr-2"><img class="avatar-img rounded-circle" src="" alt="User Image"></a> --}}
                                                <span  id="customer_{{ $order->id }}">{{ $order->users->name }}   </span>
                                            </h2>
                                        </td>
                                        @if (!auth()->guard('admin')->user()->hasRole('cook'))
                                            <td>
                                                <h2 class="table-avatar">
                                                    {{-- <a href="profile.html" class="avatar avatar-sm mr-2"><img class="avatar-img rounded-circle" src="" alt="User Image"></a> --}}
                                                    <span  id="cook_{{ $order->id }}">{{ $cook_name }}</span>
                                                </h2>
                                            </td>
                                        @endif

                                        @php

                                            $orderEntry=$order->orderEntry;
                                            foreach ($orderEntry as $item){
                                                $qty+=$item['quantity'];
                                            }
                                        @endphp
                                        {{-- <td>
                                            <table class="sub_table">
                                                @foreach ($orderEntry as $item)
                                                    <td>
                                                    <h2 class="table-avatar">

                                                    <a href="#" class="avatar avatar-sm mr-2"><img class="avatar-img rounded-circle" src="{{ asset('backend/img/dishes/'.explode('__',$item['orderEntry']['images'] )[0]) }}" ></a>
                                                    <a href="#">{{ $item['orderEntry']['name'] }}</a>
                                                    </h2>
                                                    </td>
                                                    </td>
                                                    <td>{{ $item['orderEntry']['section'] }}</td>
                                                    <td>{{ $item['orderEntry']['price'] }} EGP</td>
                                                    <td>{{ $item['orderEntry']['qty'] }}</td>
                                                    <td>{{ $item['orderEntry']['price'] * $item['qty']}}</td>
                                                    <td>{{ $item['orderEntry']['notes'] }}</td>
                                                    <td >
                                                        <div class="actions">
                                                            <a class="link_delete" data-toggle="modal" href="#delete_modal" data-url=""  class="btn btn-sm btn-danger" style="color: #f00">
                                                                <i class="fe fe-trash"></i> Delete
                                                            </a>
                                                        </div>
                                                    </td>
                                                @endforeach
                                            </table>
                                            </td> --}}
                                            <td>{{ $qty }}</td>
                                            @if (!auth()->guard('admin')->user()->hasRole('cook'))

                                                <td>
                                                    @if (in_array($order['address'], $address))
                                                    @lang('site.old')
                                                        @else
                                                        @lang('site.new')
                                                    @endif
                                                    @php
                                                        array_push($address,$order['address']);
                                                    @endphp
                                                </td>
                                                <td>{{ $order['total_price'] }} SAR</td>

                                                <td id="payment_method_{{ $order->id }}">
                                                    @if ($order['payment_method'] == 0 )
                                                    @lang('site.Cash')
                                                    @elseif($order['payment_method'] == 1)
                                                    @lang('site.Credit')
                                                    @endif
                                                </td>

                                                <td id="payment_status_{{ $order->id }}">
                                                    @if ($order['payment_status'] == 0 )
                                                    @lang('site.Pending')
                                                    {{-- @elseif($order['payment_status'] == 1) --}}

                                                    @endif
                                                    </td>
                                                    <td id="delivery_status_{{ $order->id }}">@lang('site.out of delivery')</td>
                                                    <td>
                                                        @if ($order['status'] == 1 )
                                                        @lang('site.Pending')
                                                        @elseif($order['status'] == 2)
                                                        @lang('site.Accepted')
                                                        @elseif($order['status'] == 3)
                                                        @lang('site.In progress')
                                                        @elseif($order['status'] == 4)
                                                        @lang('site.Shipped')
                                                        @elseif($order['status'] == 5)
                                                        @lang('site.Delivered')
                                                        @elseif($order['status'] == 6)
                                                        @lang('site.Completed')
                                                        @elseif($order['status'] == 0)
                                                        @lang('site.Rejected')
                                                        @endif
                                                    </td>
                                                    <td>
                                                        {{-- @php
                                                            $customer_service=\App\Models\User::whereHas('roles',function($q){
                                                                $q->where('name','admin');
                                                            })->where('city_id',$cook->city_id)->first();
                                                        @endphp
                                                        {{ $customer_service->name }} --}}

                                                        @if (auth()->guard('admin')->user()->roles[0]->name == 'admin')
                                                            <button id="{{  $order->id }}" class="assign">@lang('site.unAssigned')</button>
                                                        @else
                                                            {{ $order->assign ?? ' unAssigned yet' }}
                                                        @endif
                                                </td>
                                            @endif
                                            <td id="order_status_{{ $order->id }}">
                                                {{-- @if ($order['payment_method'] == 0 )
                                                @lang('site.Cache')
                                                @elseif($order['payment_method'] == 1)
                                                @lang('site.Mada')
                                                @elseif($order['payment_method'] == 2)
                                                @lang('site.Apple Pay')
                                                @endif --}}
                                                @if ($order['delivery_type'] == 0 )
                                                @lang('site.Pickup')
                                                @elseif($order['delivery_type'] == 1)
                                                @lang('site.Delivery')
                                                
                                                @endif        
                                            </td>
                                            {{-- <td></td> --}}
                                            {{-- <td>
                                                @if (auth()->guard('admin')->user()->hasRole('cook') && $order->status == -1)
                                                    <a id="{{ $order->id }}" class="link_show bg-success-light mr-2"  href="{{ url( Config::get('app.locale') .'/dashboard/order/accept/'.$order->id) }}" >
                                                        <i class="fa fa-check" aria-hidden="true"></i>
                                                        Accept
                                                    </a>
                                                    <a id="{{ $order->id }}" class="link_show bg-danger-light mr-2" href="{{ url( Config::get('app.locale') .'/dashboard/order/reject/'.$order->id) }}" >
                                                        <i class="fa fa-ban" aria-hidden="true"></i>
                                                        Reject
                                                    </a>
                                                @endif
                                                @if (!auth()->guard('admin')->user()->hasRole('cook'))

                                                <a id="{{ $order->id }}" class="link_show bg-success-light mr-2" data-toggle="modal"  href="#edit_orders_details" >
                                                    <i class="fe fe-eye"></i> View Details
                                                </a>
                                                @endif

                                            </td>
                                            @php
                                                $qty=0;
                                            @endphp --}}

                                            {{-- <td>
                                                <div class="actions">
                                                    @if ($order['status'] == 2)
                                                        <a style="color: rgb(118, 196, 83)"  onclick="status({{ $order->id }})" href="#"  id="{{  $order->id }}">In Progress</a>
                                                    @elseif($order['status'] == 3)
                                                    <a style="color: rgb(118, 196, 83)"  onclick="status({{ $order->id }})" href="#"  id="{{  $order->id }}">Shipped</a>
                                                    @endif
                                                    @if ($order['status'] == 0)
                                                    <a class="link_delete" id="{{ $order->id }}" data-toggle="modal" href="#delete_modal" data-url=""  class="btn btn-sm btn-danger" style="color: #f00"> Delete</a>
                                                    @endif

                                                </div>
                                            </td> --}}
                                            {{-- <td>{{ $order->admin_note ?? ' ' }}</td> --}}
                                            <td>
                                                <div class="actions">
                                                    <a  href="#"  style="color: #f00;  border: 1px solid;  padding: 2px 5px;  margin: 0 10px 0 0;"  onclick="reject({{ $order->id }})" href="#"  id="{{  $order->id }}"> @lang('site.Reject') </a>
                                                    <a style="color: rgb(118, 196, 83); border: 1px solid;  padding: 2px 5px; "  onclick="status({{ $order->id }})" href="#"  id="{{  $order->id }}">@lang('site.Accept')</a>
                                                </div>
                                            </td>

                                    </tr>

                                @endforeach
                            @endisset

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- /Recent Orders -->
        <div class="row" >

            <div class="col-md-12 d-flex">

                <!-- Recent Orders -->
                <div class="card card-table flex-fill">
                    <div class="card-header">
                        <h4 class="card-title">@lang('site.orders List')</h4>

                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-hover table-center mb-0">
                                <thead>
                                    <tr>
                                        <th>@lang('site.Order Number')</th>
                                        <th>@lang('site.Order Time')</th>
                                        <th>@lang('site.Customer Name')</th>
                                        @if (!auth()->guard('admin')->user()->hasRole('cook'))
                                            <th>@lang('site.Restaurant Name')</th>
                                        @endif

                                        {{-- <th>
                                            <table>
                                                <tr>
                                                    <th>Dish Name</th>
                                                    <th>Dish Section</th>
                                                    <th>Dish price</th>
                                                    <th>Amount</th>
                                                </tr>
                                            </table>
                                        </th> --}}
                                        <th>@lang('site.Quantity')</th>
                                        @if (!auth()->guard('admin')->user()->hasRole('cook'))
                                            <th>@lang('site.Location')</th>
                                        @endif
                                        @if (!auth()->guard('admin')->user()->hasRole('cook'))
                                            <th>@lang('site.Total Price')</th>
                                            <th>@lang('site.Payment Method')</th>
                                            <th>@lang('site.Payment Status')</th>
                                            <th>@lang('site.Delivery Status')</th>
                                            <th>@lang('site.Order Status')</th>
                                            <th>@lang('site.Customer service')</th>
                                        @endif
                                        {{-- <th>Operations service</th> --}}
                                        <th>@lang('site.Delivery Type')</th>

                                        <th>@lang('site.Status')</th>
                                        @if (!auth()->guard('admin')->user()->hasRole('cook'))
                                            <th>@lang('site.Admin Note')</th>
                                        @endif

                                        <th>@lang('site.Action')</th>
                                    </tr>
                                </thead>
                                <tbody>
                                   @isset($orders)
                                   @php
                                         $qty=0;
                                         $address=[];
                                   @endphp
                                    @foreach ($orders as $index=>$order)
                                    @php
                                        $cook=\App\Models\User::where('id',$order->cook_id)->first();
                                        $cook_name=$cook->name;
                                    @endphp
                                    <tr>
                                        <td>{{ $index+1 }}</td>
                                        <td>{{date('h:i A', strtotime($order->time ))  }}</td>
                                            <td>
                                                <h2 class="table-avatar">
                                                    {{-- <a href="profile.html" class="avatar avatar-sm mr-2"><img class="avatar-img rounded-circle" src="" alt="User Image"></a> --}}
                                                    <a href="#" id="customer_{{ $order->id }}">{{ $order->users->name }}   </a>
                                                </h2>
                                            </td>
                                        @if (!auth()->guard('admin')->user()->hasRole('cook'))

                                        <td>
                                            <h2 class="table-avatar">
                                                {{-- <a href="profile.html" class="avatar avatar-sm mr-2"><img class="avatar-img rounded-circle" src="" alt="User Image"></a> --}}
                                                <a href="#"  id="cook_{{ $order->id }}">{{ $cook_name }}</a>
                                            </h2>
                                        </td>
                                        @endif

                                        @php

                                            $orderEntry=$order->orderEntry;
                                            foreach ($orderEntry as $item){
                                                $qty+=$item['quantity'];
                                            }

                                        @endphp
                                         {{-- <td>
                                             <table class="sub_table">
                                                @foreach ($orderEntry as $item)
                                                    <td>
                                                    <h2 class="table-avatar">

                                                    <a href="#" class="avatar avatar-sm mr-2"><img class="avatar-img rounded-circle" src="{{ asset('backend/img/dishes/'.explode('__',$item['orderEntry']['images'] )[0]) }}" ></a>
                                                    <a href="#">{{ $item['orderEntry']['name'] }}</a>
                                                    </h2>
                                                    </td>
                                                    </td>
                                                    <td>{{ $item['orderEntry']['section'] }}</td>
                                                    <td>{{ $item['orderEntry']['price'] }} EGP</td>
                                                    <td>{{ $item['orderEntry']['qty'] }}</td>
                                                    <td>{{ $item['orderEntry']['price'] * $item['qty']}}</td>
                                                    <td>{{ $item['orderEntry']['notes'] }}</td>
                                                    <td >
                                                        <div class="actions">
                                                            <a class="link_delete" data-toggle="modal" href="#delete_modal" data-url=""  class="btn btn-sm btn-danger" style="color: #f00">
                                                                <i class="fe fe-trash"></i> Delete
                                                            </a>
                                                        </div>
                                                    </td>
                                                 @endforeach
                                             </table>
                                            </td> --}}
                                            <td>{{ $qty }}</td>
                                            @if (!auth()->guard('admin')->user()->hasRole('cook'))

                                                <td>
                                                    @if (in_array($order['address'], $address))
                                                    @lang('site.old')
                                                        @else
                                                        @lang('site.new')
                                                    @endif
                                                    @php
                                                        array_push($address,$order['address']);
                                                    @endphp
                                                </td>


                                             <td>{{ $order['total_price'] }} {{ $currency }}</td>
                                             <td id="payment_method_{{ $order->id }}">
                                                @if ($order['payment_method'] == 0 )
                                                @lang('site.Cash')
                                                @elseif($order['payment_method'] == 1)
                                                @lang('site.Credit')
                                                @endif
                                             </td>
                                             <td id="payment_status_{{ $order->id }}">
                                                @if ($order['payment_status'] == 0 )
                                                @lang('site.Pending')
                                                {{-- @elseif($order['payment_status'] == 1) --}}

                                                @endif
                                            </td>
                                             <td id="delivery_status_{{ $order->id }}">@lang('site.out of delivery')</td>
                                             <td>
                                                 @if ($order['status'] == 1 )
                                                 @lang('site.Pending')
                                                 @elseif($order['status'] == 2)
                                                 @lang('site.Accepted')
                                                 @elseif($order['status'] == 3)
                                                 @lang('site.In progress')
                                                 @elseif($order['status'] == 4)
                                                 @lang('site.Shipped')
                                                 @elseif($order['status'] == 5)
                                                 @lang('site.Delivered')
                                                 @elseif($order['status'] == 6)
                                                 @lang('site.Completed')
                                                 @elseif($order['status'] == 0)
                                                 @lang('site.Rejected')
                                                 @elseif($order['status'] == 9)
                                                 @lang('site.Cancelled')
                                                 @endif
                                             </td>
                                             <td>
                                                {{-- @php
                                                    $customer_service=\App\Models\User::whereHas('roles',function($q){
                                                        $q->where('name','admin');
                                                    })->where('city_id',$cook->city_id)->first();
                                                @endphp
                                                {{ $customer_service->name }} --}}
                                                @if (auth()->guard('admin')->user()->roles[0]->name == 'admin')
                                                    @if ( $order->assign == auth()->guard('admin')->user()->name)
                                                    @lang('site.Assigned')
                                                    @else
                                                    <button id="{{  $order->id }}" class="assign">@lang('site.unAssigned')</button>
                                                    @endif

                                                @else
                                                    {{ $order->assign ?? ' unAssigned yet' }}
                                                @endif
                                             </td>
                                             @endif
                                             {{-- <td id="order_status_{{ $order->id }}">
                                                @if ($order['payment_method'] == 0 )
                                                @lang('site.Cache')
                                                @elseif($order['payment_method'] == 1)
                                                @lang('site.Mada')
                                                @elseif($order['payment_method'] == 2)
                                                @lang('site.Apple Pay')
                                                @endif

                                             </td> --}}
                                               <td id="order_status_{{ $order->id }}">
                                                @if ($order['delivery_type'] == 0 )
                                                @lang('site.Pickup')
                                                @elseif($order['delivery_type'] == 1)
                                                @lang('site.Delivery')
                                                
                                                @endif

                                             </td>
                                             <td id="{{$order->logista_order_id ?? ''}}" status="{{$order['status']}}">
                                                @if ($order['status'] == 1 )
                                                @lang('site.Pending')
                                                @elseif($order['status'] == 2)
                                                @lang('site.Accepted')
                                                @elseif($order['status'] == 3)
                                                @lang('site.In progress')
                                                @elseif($order['status'] == 4)
                                                @lang('site.Shipped')
                                                @elseif($order['status'] == 5)
                                                @lang('site.Delivered')
                                                @elseif($order['status'] == 6)
                                                @lang('site.Completed')
                                                @elseif($order['status'] == 0)
                                                @lang('site.Rejected')
                                                @elseif($order['status'] == 9)
                                                @lang('site.Cancelled')
                                                @endif

                                            </td>
                                            @if (!auth()->guard('admin')->user()->hasRole('cook'))
                                                <td>{{ $order->admin_note ?? ' ' }}</td>
                                            @endif
                                            <td>
                                                {{-- @if (auth()->guard('admin')->user()->hasRole('cook') && $order->status == -1)
                                                    <a id="{{ $order->id }}" class="link_show bg-success-light mr-2"  href="{{ url( Config::get('app.locale') .'/dashboard/order/accept/'.$order->id) }}" >
                                                        <i class="fa fa-check" aria-hidden="true"></i>
                                                        Accept
                                                    </a>
                                                    <a id="{{ $order->id }}" class="link_show bg-danger-light mr-2" href="{{ url( Config::get('app.locale') .'/dashboard/order/reject/'.$order->id) }}" >
                                                        <i class="fa fa-ban" aria-hidden="true"></i>
                                                        Reject
                                                    </a>
                                                @endif --}}


                                                <div class="actions">

                                                    @if (!auth()->guard('admin')->user()->hasRole('cook'))
                                                        <a id="{{ $order->id }}" class="link_update bg-success-light mr-2" data-toggle="modal"  href="#edit_order_details{{ $order->id }}" >
                                                            <i class="fe fe-pencil"></i> @lang('site.Edit')
                                                        </a>
                                                        <a id="{{ $order->id }}" class="link_show bg-success-light mr-2 mb-1" data-toggle="modal"  href="#edit_orders_details" >
                                                            <i class="fe fe-eye"></i> @lang('site.View Details')
                                                        </a>
                                                    @endif
                                                    @if ($order['status'] == 2)
                                                        <a style="color: rgb(118, 196, 83);border: 1px solid;  padding: 2px 5px;"  onclick="status({{ $order->id }})" href="#"  id="{{  $order->id }}">@lang('site.In progress')</a>
                                                    @elseif($order['status'] == 3)
                                                    <a  style="color: rgb(118, 196, 83); border: 1px solid;  padding: 2px 5px; "  onclick="status({{ $order->id }})" href="#"  id="{{  $order->id }}">@lang('site.Shipped')</a>
                                                    @endif
                                                    @if ($order['status'] == 2 || $order['status'] == 3)
                                                    <a class="mx-1" style="color: rgb(230, 23, 40); border: 1px solid ;  padding: 2px 5px; "  onclick="cancel({{ $order->id }})" href="#"  id="{{  $order->id }}">@lang('site.Cancel')</a>

                                                    @endif
                                                    @if ($order['status'] == 0 || $order['status'] == 9)
                                                    <a class="link_delete" id="{{ $order->id }}" data-toggle="modal" href="#delete_modal" data-url=""  class="btn btn-sm btn-danger" style="color: #f00; border: 1px solid;  padding: 2px 5px; "> @lang('site.Delete')</a>
                                                    @endif

                                                </div>


                                                {{-- @switch($order->status)
                                                    @case(0)
                                                        <span class="bg-danger-light">rejected</span>
                                                        @break
                                                    @case(1)
                                                        <span class="bg-success-light">accepted</span>
                                                        @break
                                                    @default

                                                @endswitch --}}

                                             </td>
                                             @php
                                                 $qty=0;
                                             @endphp

                                    </tr>

                                    <!-- Edit Details Modal -->
                                    <div class="modal fade" id="edit_order_details{{ $order->id }}" aria-hidden="true" role="dialog">
                                        <div class="modal-dialog modal-dialog-centered" role="document" >
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title">Edit Order</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <form id="formUpdate" method="post" action="{{ route('order.updatedata',$order->id) }}" enctype="multipart/form-data">
                                                        @csrf
                                                        @method('put')
                                                        <div class="row form-row">
                                                            <div class="col-12 col-sm-12">
                                                                <div class="form-group">
                                                                    <label>Total Price</label>
                                                                    <input type="text" name="total_price" class="form-control" value="{{$order->total_price }}">
                                                                </div>
                                                            </div>
                                                            <div class="col-12 col-sm-12">
                                                                <div class="form-group">
                                                                    <label>Admin Note</label>
                                                                    <input type="text" name="admin_note" class="form-control" value="{{$order->admin_note }}">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <button type="submit" class="btn btn-primary btn-block">Save Changes</button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /Edit Details Modal -->
                                    @endforeach
                                   @endisset

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- /Recent Orders -->

            </div>
        </div>


    </div>
</div>
<!-- /Page Wrapper -->
  <!-- view order Modal -->
  @if (!auth()->guard('admin')->user()->hasRole('cook'))

  <div class="modal fade" id="edit_orders_details" aria-hidden="true" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document" >
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"> @lang('site.Order Details')</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body" id="view">

            </div>
        </div>
    </div>
</div>
@endif

@endsection

@push('scripts')

   <script>
        // $('.modal').hide();

        @if (count($errors) > 0)
                $('#Add_orders').modal('show');
        @endif


        $('.assign').on('click',function(){

            var order=$(this).attr('id');
            $.ajax({
                type: 'GET',
                url: '/{{ Config::get('app.locale') }}/dashboard/order/assign/'+order,

                success:function(data){


                }

            });
            $(this).text('Assigned');
            $(this).prop("disabled", true);

        });

//         $('#edit_orders_details').on('hidden.bs.modal', function () {
//  location.reload();
// })
        $('.link_show').on('click',function(){
            $('#view').html('');
            $('#body').html('');
            var order_id=$(this).attr('id');
            var customer=$('#customer_'+order_id).html()
            var cook=$('#cook_'+order_id).html()
            var payment_method=$('#payment_method_'+order_id).html()
            var payment_status=$('#payment_status_'+order_id).html()
            var delivery_status=$('#delivery_status_'+order_id).html()
            var order_status=$('#order_status_'+order_id).html()

            $.ajax(
                {url: '/{{ Config::get('app.locale') }}/dashboard/order/orderdetails/'+order_id,
                 success: function(result){
                    var data= result.order.orderEntry;
                    var details='<div class="row form-row">'+
                                    '<div class="col-12 col-sm-12">'+
                                        '<div class="form-group">'+
                                            '<label>Number : '+result.order.id+'</label>'+
                                        '</div>'+
                                    '</div>'+
                                    '<div class="col-12 col-sm-12">'+
                                        '<div class="form-group">'+
                                            '<label>Customer Name : '+customer+'</label>'+
                                        '</div>'+
                                    '</div>'+
                                    '<div class="col-12 col-sm-12">'+
                                        '<div class="form-group">'+
                                            '<label>Restaurant Name : '+cook+'</label>'+
                                        '</div>'+
                                    '</div>'+
                                    '<div class="col-12 col-sm-12">'+
                                        '<div class="form-group">'+
                                            '<table class="table table-borderd table-hover">'+
                                                '<thead>'+
                                                    '<tr>'+
                                                        '<th>Dish Name</th>'+
                                                        // '<th>Dish Section</th>'+
                                                        '<th>Dish price</th>'+
                                                        '<th>Amount</th>'+
                                                    '</tr>'+
                                                '</thead>'+
                                                '<tbody id="body">'+

                                                '</tbody>'+
                                            '</table>'+
                                        '</div>'+
                                    '</div>'+
                                    '<div class="col-12 col-sm-12">'+
                                        '<div class="form-group">'+
                                            '<label>Location : '+result.order.address+'</label>'+
                                        '</div>'+
                                    '</div>'+
                                    '<div class="col-12 col-sm-12">'+
                                        '<div class="form-group">'+
                                            '<label>Total Price : '+result.order.total_price+'</label>'+
                                        '</div>'+
                                    '</div>'+
                                    '<div class="col-12 col-sm-12">'+
                                        '<div class="form-group">'+
                                            '<label>payment Method : '+payment_method+'</label>'+
                                        '</div>'+
                                    '</div>'+
                                    '<div class="col-12 col-sm-12">'+
                                        '<div class="form-group">'+
                                            '<label>payment Status : '+payment_status+'</label>'+
                                        '</div>'+
                                    '</div>'+
                                    '<div class="col-12 col-sm-12">'+
                                        '<div class="form-group">'+
                                            '<label>Delivery Status : '+delivery_status+'</label>'+
                                        '</div>'+
                                    '</div>'+
                                    '<div class="col-12 col-sm-12">'+
                                        '<div class="form-group">'+
                                            '<label>Order Status : '+order_status+'</label>'+
                                        '</div>'+
                                    '</div>'+
                                '</div>';
                    $('#view').append(details);

                      for (let i = 0; i < data.length; i++) {
                       var body= '<tr>'+
                            '<td>'+data[i].name_{{ Config::get('app.locale') }}+'</td>'+
                            // '<td>'+data[i]['orderEntry']['section']+'</td>'+
                            '<td>'+data[i].price+'</td>'+
                            '<td>'+data[i].quantity+'</td>'+

                        '</tr>';

                      }
                    $('#body').append(body)  ;
                }
            });
        });


        function status(order_id){


                $.ajax({
                    type: 'GET',
                    url: '/{{ Config::get('app.locale') }}/dashboard/order/'+order_id,

                    success:function(data){
                        //    document.getElementById(order_id).innerHTML= data.status;

                        if (data.status == 2) {
                            $('.alert-success').text('Order Accepted ')
                        }else if (data.status == 3){
                            $('.alert-success').text('Order In Progressive ')
                        }
                        $('.alert-success').css('display','block');

                        setTimeout(function() {
                            $('.alert').fadeOut('fast');
                            location.reload();
                        }, 2000);
                    },
                    error:function(data){
                    }
                });
                // location.reload();
        };

        function cancel(order_id){
            $.ajax({
                    type: 'GET',
                    url: '/{{ Config::get('app.locale') }}/dashboard/order/cancel/'+order_id,

                    success:function(data){
                       document.getElementById(order_id).innerHTML= data.status;
                    if (data.status == 'cancelled') {
                        $('.alert-danger').css('display','block');
                        $('.alert-danger').text('Order Cancelled ');

                        setTimeout(function() {
                            $('.alert').fadeOut('fast');
                            location.reload();
                        }, 2000);
                    }
                    },
                    error:function(data){
                    }
                });
        }

        function reject(order_id){

                $.ajax({
                    type: 'GET',
                    url: '/{{ Config::get('app.locale') }}/dashboard/order/reject/'+order_id,

                    success:function(data){
                    //    document.getElementById(order_id).innerHTML= data.status;
                    if (data.status == 0) {
                        $('.alert-danger').css('display','block');
                        $('.alert-danger').text('Order Rejected ');

                        setTimeout(function() {
                            $('.alert').fadeOut('fast');
                            location.reload();
                        }, 2000);
                    }
                    },
                    error:function(data){
                    }
                });
            };


    </script>
@endpush
