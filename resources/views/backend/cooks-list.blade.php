@extends('backend.layouts.app')
@push('style')

<style>


    input[type=checkbox] + label {
      display: block;
      margin: 0.2em;
      cursor: pointer;
      padding: 0.2em;
    }

    input[type=checkbox] {
      display: none;
    }

    input[type=checkbox] + label:before {
      content: "\2714";
      border: 0.1em solid #000;
      border-radius: 0.2em;
      display: inline-table;
      width: 10px;
      height: 10px;
      padding-left: 0.2em;
      padding-bottom: 0.3em;
      margin-right: 0.2em;
      vertical-align: bottom;
      color: transparent;
      transition: .2s;
    }

    input[type=checkbox] + label:active:before {
      transform: scale(0);
    }

    input[type=checkbox]:checked + label:before {
      background-color: gold;
      border-color: gold;
      color: #fff;
    }

    input[type=checkbox]:disabled + label:before {
      transform: scale(1);
      border-color: #aaa;
    }

    input[type=checkbox]:checked:disabled + label:before {
      transform: scale(1);
      background-color: #bfb;
      border-color: #bfb;
    }
    </style>
    @endpush
@section('content')
 <!-- Page Wrapper -->
 <div class="page-wrapper">
    <div class="content container-fluid">

        <!-- Page Header -->
        <div class="page-header">
            <div class="row">
                <div class="col-sm-12">
                    <h3 class="page-title">List of Vendors</h3>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
                        <li class="breadcrumb-item active">Vendor</li>
                    </ul>
                </div>
               {{-- @if (!Auth::guard('cook')) --}}
                <div class="col-sm-12 col">
                    {{-- <a href="#Add_Cooks" data-toggle="modal" class="btn btn-otbokhly float-right mt-2 {{ auth()->guard('cook') ? 'disabled' : '' }}" >Add</a> --}}
                    <a href="#Add_Cooks" data-toggle="modal" class="btn btn-otbokhly float-right mt-2   " >Add</a>
                </div>
               {{-- @endif --}}
            </div>

        </div>
        <!-- /Page Header -->


        <div class="row" >
            <div class="col-md-12 d-flex">

                <!-- Recent Orders -->
                <div class="card card-table flex-fill">
                    <div class="card-header">
                        <h4 class="card-title">Vendors List</h4>

                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-hover table-center mb-0">
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Phone</th>
                                        <th>Bank</th>
                                        <th>Credit Number</th>
                                        <th>Card Holder Name</th>
                                        <th>Contract</th>
                                        <th>City</th>
                                        <th>Commission</th>
                                        <th>Commission Type</th>
                                        {{-- <th>Reviews</th> --}}
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @isset($cooks)
                                        @foreach ($cooks as $cook)
                                        <tr>
                                            <td>
                                                <h2 class="table-avatar">


                                                    <input type="checkbox" class="vip_{{ $cook->id }}" id="{{ $cook->id }}" name="vip{{ $cook->id }}" value="{{ $cook->VIP =='1' ? $cook->VIP :'0'  }}"{{ $cook->VIP =='1' ? 'checked' :' '  }} onchange="vip({{ $cook->id }},this)">
                                                    <label for="{{ $cook->id }}">
                                                    <a href="#" style="color:{{ $cook->VIP =='1' ? 'gold' :' '  }}">{{ $cook->name }}</a>

                                                </h2>
                                            </td>
                                            <td>{{ $cook->email }}</td>
                                            <td>{{  $cook->phone  }}</td>

                                            @php
                                                $bank =json_decode( $cook->bank);
                                                $credit_number=json_decode($cook->credit_number);
                                                $card_holder_name=json_decode($cook->card_holder_name);
                                            @endphp
                                            @if ( is_array($bank))
                                                <td>{{ $bank ? implode(' , ',$bank) : ''  }}</td>
                                            @else
                                                 <td>{{$cook->bank }}</td>
                                            @endif
                                            @if ( is_array($credit_number))
                                                <td>{{ $credit_number ? implode(' , ',$credit_number) : ''  }}</td>
                                            @else
                                                <td>{{$cook->credit_number }}</td>
                                            @endif
                                            @if ( is_array($credit_number))
                                            <td>{{ $card_holder_name ? implode(' , ',$card_holder_name) : ''  }}</td>
                                            @else
                                            <td>{{  $cook->card_holder_name ?? ''   }}</td>
                                            @endif
                                            <td>{{  $cook->contract  }}</td>
                                            <td>{{  $cook->cities->name ?? ' '}}</td>
                                            <td>{{  $cook->commission  }}</td>
                                            <td>{{  $cook->commission_type  }}</td>
                                            {{-- <td>
                                                <i class="fe fe-star-o text-secondary"></i>
                                                <i class="fe fe-star-o text-secondary"></i>
                                                <i class="fe fe-star-o text-secondary"></i>
                                                <i class="fe fe-star-o text-secondary"></i>
                                                <i class="fe fe-star-o text-secondary"></i>
                                            </td> --}}
                                            <td>{{  $cook->active ==0 ?'Deactive' : 'Active'  }}</td>
                                            <td >
                                                <div class="actions">
                                                    <a id="{{ $cook->id }}" class="link_update bg-success-light mr-2" href="{{ url('/dashboard/cookaccount/'.$cook->id) }}">
                                                        <i class="fe fe-pencil"></i> Edit
                                                    </a>
                                                    <a id="{{ $cook->id }}" class="link_edit"  class="btn btn-sm btn-success" href="{{ $cook->active == 0 ?  '/dashboard/cook/active/'.$cook->id  : '/dashboard/cook/deactive/'.$cook->id }} " >
                                                        <i class="fe fe-edit"></i> {{ $cook->active == 0 ?'Active' : 'Deactive' }}
                                                    </a>
                                                    <a id="{{ $cook->id }}" class="link_delete" data-toggle="modal" href="#delete_modal" data-url=""  class="btn btn-sm btn-danger" style="color: #f00">
                                                        <i class="fe fe-trash"></i> Delete
                                                    </a>
                                                </div>
                                            </td>
                                        </tr>
                                        {{-- <div class="modal fade" id="edit_cooks_details_{{ $cook->id }}" aria-hidden="true" role="dialog">
                                            <div class="modal-dialog modal-dialog-centered" role="document" >
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title">Edit Vendor</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="form-group errors" style="display: none" >
                                                            <div class="alert alert-danger">
                                                              <ul class="list">

                                                              </ul>
                                                            </div>
                                                          </div>
                                                        <form  method="post" action="{{ route('cook.update',$cook->id) }}" enctype="multipart/form-data">
                                                            @csrf
                                                            @method('put')

                                                            <div class="row form-row">
                                                                @foreach (config('translatable.locales') as $locale)
                                                                <div class="col-12 col-sm-12">
                                                                    <div class="form-group">
                                                                        <label>{{ trans('site.'.$locale.'.name') }}</label>
                                                                        <input type="text" name="{{ $locale }}[name]" class="form-control" value="{{$cook->translate($locale)->name ??old($locale.'.name') }}">
                                                                    </div>
                                                                </div>
                                                                @endforeach
                                                                <div class="col-12 col-sm-12">
                                                                    <div class="form-group">
                                                                        <label>phone</label>
                                                                        <input type="text" name="phone" class="form-control" value="{{$cook->phone ??old('phone') }}">
                                                                    </div>
                                                                </div>
                                                                <div class="col-12 col-sm-12">
                                                                    <div class="form-group">
                                                                        <label>E-mail</label>
                                                                        <input type="text" name="email" class="form-control" value="{{ $cook->email??old('email') }}">
                                                                    </div>
                                                                </div>
                                                                <div class="col-12 col-sm-12">
                                                                    <div class="form-group">
                                                                        <label>password</label>
                                                                        <input type="password" name="password" class="form-control" >
                                                                    </div>
                                                                </div>
                                                                @php
                                                                    $bank =json_decode( $cook->bank);
                                                                    $credit_number=json_decode($cook->credit_number);
                                                                @endphp
                                                                @if (is_array($bank))
                                                                    @for ($i = 0; $i < count($bank); $i++)
                                                                        <div class="col-12 col-sm-12">
                                                                            <label>Bank : </label>
                                                                            <select id="bankedit" name="bankedit[]"  style="padding: 10px;background: #E0E0E0;color: #000;border: none;margin-left: 20px;width: 64%;">
                                                                                <option value="The National Commercial Bank" {{ 'The National Commercial Bank' == $bank[$i]? 'selected' : ''}} >The National Commercial Bank </option>
                                                                                <option value="The Saudi British Bank"{{ 'The Saudi British Bank' == $bank[$i]? 'selected' : ''}}>The Saudi British Bank</option>
                                                                                <option value="Saudi Investment Bank"{{ 'Saudi Investment Bank' == $bank[$i]? 'selected' : ''}}>Saudi Investment Bank</option>
                                                                                <option value="Alinma Bank"{{ 'Alinma Bank' == $bank[$i]? 'selected' : ''}}>Alinma Bank</option>
                                                                                <option value="Banque Saudi Fransi"{{ 'Banque Saudi Fransi' == $bank[$i]? 'selected' : ''}}>Banque Saudi Fransi</option>
                                                                                <option value="Riyad Bank"{{ 'Riyad Bank' == $bank[$i]? 'selected' : ''}}>Riyad Bank</option>
                                                                                <option value="Samba Financial Group (Samba)"{{ 'Samba Financial Group (Samba)' == $bank[$i]? 'selected' : ''}}>Samba Financial Group (Samba)</option>
                                                                                <option value="Alawwal Bank"{{ 'Alawwal Bank' == $bank[$i]? 'selected' : ''}}>Alawwal Bank</option>
                                                                                <option value="Al Rajhi Bank"{{ 'Al Rajhi Bank' == $bank[$i]? 'selected' : ''}}>Al Rajhi Bank</option>
                                                                                <option value="Arab National Bank"{{ 'Arab National Bank' == $bank[$i]? 'selected' : ''}}>Arab National Bank</option>
                                                                                <option value="Bank AlBilad"{{ 'Bank AlBilad' == $bank[$i]? 'selected' : ''}}>Bank AlBilad</option>
                                                                                <option value="Bank AlJazira"{{ 'Bank AlJazira' == $bank[$i]? 'selected' : ''}}>Bank AlJazira</option>
                                                                                <option value="Gulf International Bank Saudi Aribia (GIB-SA)"{{ 'Gulf International Bank Saudi Aribia (GIB-SA)' == $bank[$i]? 'selected' : ''}}>Gulf International Bank Saudi Aribia (GIB-SA)</option>
                                                                            </select>
                                                                            @if ($i == 0)
                                                                                <a  class="btn " onclick="add(this)"  style="float: right;color:blue;font-size:20px;padding:5px 10px">+</a>
                                                                            @else
                                                                                <a  class="btn" onclick="removeedit(this)" style="float: right;color:blue;font-size:20px;padding:5px 10px">-</a>
                                                                            @endif
                                                                        </div>
                                                                        <div class="col-12 col-sm-12 mt-3">
                                                                            <div class="form-group">
                                                                                <label>Credit Number</label>
                                                                                <input type="text" name="credit_numberedit[]" value="{{ $credit_number[$i] ?? '' }}" class="form-control"  required>
                                                                            </div>
                                                                        </div>

                                                                    @endfor
                                                                @else
                                                                    <div class="col-12 col-sm-12">
                                                                        <label>Bank : </label>
                                                                        <select id="bankedit" name="bankedit[]"  style="padding: 10px;background: #E0E0E0;color: #000;border: none;margin-left: 20px;width: 64%;">
                                                                            <option value="The National Commercial Bank" {{ 'The National Commercial Bank' == $cook->bank? 'selected' : ''}} >The National Commercial Bank </option>
                                                                            <option value="The Saudi British Bank"{{ 'The Saudi British Bank' == $cook->bank? 'selected' : ''}}>The Saudi British Bank</option>
                                                                            <option value="Saudi Investment Bank"{{ 'Saudi Investment Bank' == $cook->bank? 'selected' : ''}}>Saudi Investment Bank</option>
                                                                            <option value="Alinma Bank"{{ 'Alinma Bank' == $cook->bank? 'selected' : ''}}>Alinma Bank</option>
                                                                            <option value="Banque Saudi Fransi"{{ 'Banque Saudi Fransi' == $cook->bank? 'selected' : ''}}>Banque Saudi Fransi</option>
                                                                            <option value="Riyad Bank"{{ 'Riyad Bank' == $cook->bank? 'selected' : ''}}>Riyad Bank</option>
                                                                            <option value="Samba Financial Group (Samba)"{{ 'Samba Financial Group (Samba)' == $cook->bank? 'selected' : ''}}>Samba Financial Group (Samba)</option>
                                                                            <option value="Alawwal Bank"{{ 'Alawwal Bank' == $cook->bank? 'selected' : ''}}>Alawwal Bank</option>
                                                                            <option value="Al Rajhi Bank"{{ 'Al Rajhi Bank' == $cook->bank? 'selected' : ''}}>Al Rajhi Bank</option>
                                                                            <option value="Arab National Bank"{{ 'Arab National Bank' == $cook->bank? 'selected' : ''}}>Arab National Bank</option>
                                                                            <option value="Bank AlBilad"{{ 'Bank AlBilad' == $cook->bank? 'selected' : ''}}>Bank AlBilad</option>
                                                                            <option value="Bank AlJazira"{{ 'Bank AlJazira' == $cook->bank? 'selected' : ''}}>Bank AlJazira</option>
                                                                            <option value="Gulf International Bank Saudi Aribia (GIB-SA)"{{ 'Gulf International Bank Saudi Aribia (GIB-SA)' == $cook->bank? 'selected' : ''}}>Gulf International Bank Saudi Aribia (GIB-SA)</option>
                                                                        </select>

                                                                        <a  class="btn " onclick="add(this)"   style="float: right;color:blue;font-size:20px;padding:5px 10px">+</a>

                                                                    </div>
                                                                    <div class="col-12 col-sm-12 mt-3">
                                                                        <div class="form-group">
                                                                            <label>Credit Number</label>
                                                                            <input type="text" name="credit_numberedit[]" value="{{ $cook->credit_number ?? '' }}" class="form-control"  required>
                                                                        </div>
                                                                    </div>
                                                                @endif

                                                                <div class="banksectionedit" >

                                                                </div>
                                                                <div class="col-12 col-sm-12">
                                                                    <label>City : </label>
                                                                    <select  name="city" class="form-control"     style="width:50%;paddind:2px">
                                                                        @foreach ($cities as $city)
                                                                        @if ($cook->cities)
                                                                            <option value="{{ $city->id ?? ' ' }}" {{ (in_array($city->id, $cook->cities->pluck('id')->toArray()))? 'selected' :' ' }}>{{ $city->name ?? ' ' }}</option>
                                                                        @else
                                                                        <option value="{{ $city->id }}" >{{ $city->name  }}</option>

                                                                        @endif
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                                <div class="col-12 col-sm-12 mt-3">
                                                                    <div class="form-group">
                                                                        <label>Contract Number</label>
                                                                        <input type="text" name="contract" class="form-control" value="{{$cook->contract ??old('contract') }}" >
                                                                    </div>
                                                                </div>
                                                                <div class="col-12 col-sm-12">
                                                                    <div class="form-group">
                                                                        <label>Commission</label>
                                                                        <input type="number" name="commission" class="" style="width: 50%;dislay:inline !important" value="{{ $cook->commission ??old('commission') }}">
                                                                        <select class="form-control" name="commission_type"  id="commission_type" title="EGP" placeholder="EGP"  data-live-search="true"   style="width:20%!important;display: inline !important;paddind:2px" >
                                                                            <option value="EGP" {{ $cook->commission_type =='EGP' ? 'selected':'' }}>EGP </option>
                                                                            <option value="%" {{ $cook->commission_type =='%' ? 'selected':'' }}>% </option>

                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <button type="submit" class="btn btn-primary btn-block">Save Changes</button>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div> --}}

                                        <!-- /Edit Details Modal -->
                                        @endforeach
                                    @endisset

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- /Recent Orders -->

            </div>
        </div>

    </div>
</div>
<div id="addRow" style="display: none" >
    <div class="col-12 col-sm-12">
        <label>Bank : </label>
        <select id="bank" name="bank[]"  style="padding: 10px;background: #E0E0E0;color: #000;border: none;margin-left: 20px;width: 64%;">
            <option value="The National Commercial Bank"  >The National Commercial Bank </option>
            <option value="The Saudi British Bank">The Saudi British Bank</option>
            <option value="Saudi Investment Bank">Saudi Investment Bank</option>
            <option value="Alinma Bank">Alinma Bank</option>
            <option value="Banque Saudi Fransi">Banque Saudi Fransi</option>
            <option value="Riyad Bank">Riyad Bank</option>
            <option value="Samba Financial Group (Samba)">Samba Financial Group (Samba)</option>
            <option value="Alawwal Bank">Alawwal Bank</option>
            <option value="Al Rajhi Bank">Al Rajhi Bank</option>
            <option value="Arab National Bank">Arab National Bank</option>
            <option value="Bank AlBilad">Bank AlBilad</option>
            <option value="Bank AlJazira">Bank AlJazira</option>
            <option value="Gulf International Bank Saudi Aribia (GIB-SA)">Gulf International Bank Saudi Aribia (GIB-SA)</option>
        </select>
        <a  class="btn" onclick="remove(this)" style="float: right;color:blue;font-size:20px;padding:5px 10px">-</a>
    </div>
    <div class="col-12 col-sm-12 mt-3">
        <div class="form-group">
            <label>Credit Number</label>
            <input type="text" name="credit_number[]" value="" class="form-control" required>
        </div>
        <div class="form-group">
            <label>Card Holder Name</label>
            <input type="text" name="card_holder_name[]" value="{{ old('card_holder_name') }}" class="form-control" >
        </div>
    </div>
</div>
{{-- <div id="addRowedit" style="display: none" >
    <div class="col-12 col-sm-12">
        <label>Bank : </label>
        <select id="bank" name="bankedit[]"  style="padding: 10px;background: #E0E0E0;color: #000;border: none;margin-left: 20px;width: 64%;">
            <option value="The National Commercial Bank"  >The National Commercial Bank </option>
            <option value="The Saudi British Bank">The Saudi British Bank</option>
            <option value="Saudi Investment Bank">Saudi Investment Bank</option>
            <option value="Alinma Bank">Alinma Bank</option>
            <option value="Banque Saudi Fransi">Banque Saudi Fransi</option>
            <option value="Riyad Bank">Riyad Bank</option>
            <option value="Samba Financial Group (Samba)">Samba Financial Group (Samba)</option>
            <option value="Alawwal Bank">Alawwal Bank</option>
            <option value="Al Rajhi Bank">Al Rajhi Bank</option>
            <option value="Arab National Bank">Arab National Bank</option>
            <option value="Bank AlBilad">Bank AlBilad</option>
            <option value="Bank AlJazira">Bank AlJazira</option>
            <option value="Gulf International Bank Saudi Aribia (GIB-SA)">Gulf International Bank Saudi Aribia (GIB-SA)</option>
        </select>
        <a  class="btn" onclick="remove(this)" style="float: right;color:blue;font-size:20px;padding:5px 10px">-</a>
    </div>
    <div class="col-12 col-sm-12 mt-3">
        <div class="form-group">
            <label>Credit Number</label>
            <input type="text" name="credit_numberedit[]" value="" class="form-control" required>
        </div>
        <div class="form-group">
            <label>Card Holder Name</label>
            <input type="text" name="card_holder_name[]" value="{{ old('card_holder_name') }}" class="form-control" >
        </div>
    </div>

</div> --}}
<!-- /Page Wrapper -->
  <!-- Add Modal -->
  <div class="modal fade" id="Add_Cooks" aria-hidden="true" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document" >
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Add Vendor</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                @include('backend.partials.errors')
                <div class="form-group errors" style="display: none" >
                    <div class="alert alert-danger">
                      <ul class="list">

                      </ul>
                    </div>
                  </div>
                <form method="POST" action="{{ route('cook.store') }}" enctype="multipart/form-data">
                    @csrf
                    <div class="row form-row">
                        @foreach (config('translatable.locales') as $locale)
                        <div class="col-12 col-sm-12">
                            <div class="form-group">
                                <label>{{ trans('site.'.$locale.'.name') }}</label>
                                <input type="text" name="{{ $locale }}[name]" class="form-control" value="{{ old($locale.'.name') }}">
                            </div>
                        </div>
                        @endforeach
                        <div class="col-12 col-sm-12">
                            <div class="form-group">
                                <label>phone</label>
                                <input type="text" name="phone" class="form-control" value="{{ old('phone') }}">
                            </div>
                        </div>
                        <div class="col-12 col-sm-12">
                            <div class="form-group">
                                <label>E-mail</label>
                                <input type="text" name="email" class="form-control" value="{{ old('email') }}">
                            </div>
                        </div>
                        <div class="col-12 col-sm-12">
                            <div class="form-group">
                                <label>password</label>
                                <input type="password" name="password" class="form-control" >
                            </div>
                        </div>
                        <div class="col-12 col-sm-12">
                            <label>Bank : </label>
                            <select id="bank" name="bank[]" class="selectpicker"  data-live-search="true"   style="width:50%;paddind:2px">
                                <option value="The National Commercial Bank ">The National Commercial Bank </option>
                                <option value="The Saudi British Bank">The Saudi British Bank</option>
                                <option value="Saudi Investment Bank">Saudi Investment Bank</option>
                                <option value="Alinma Bank">Alinma Bank</option>
                                <option value="Banque Saudi Fransi">Banque Saudi Fransi</option>
                                <option value="Riyad Bank">Riyad Bank</option>
                                <option value="Samba Financial Group (Samba)">Samba Financial Group (Samba)</option>
                                <option value="Alawwal Bank">Alawwal Bank</option>
                                <option value="Al Rajhi Bank">Al Rajhi Bank</option>
                                <option value="Arab National Bank">Arab National Bank</option>
                                <option value="Bank AlBilad">Bank AlBilad</option>
                                <option value="Bank AlJazira">Bank AlJazira</option>
                                <option value="Gulf International Bank Saudi Aribia (GIB-SA)">Gulf International Bank Saudi Aribia (GIB-SA)</option>
                            </select>

                            <a id="add" class="btn" style="float: right;color:blue;font-size:20px;padding:5px 10px">+</a>
                        </div>

                        <div class="col-12 col-sm-12 mt-3">
                            <div class="form-group">
                                <label>Credit Number</label>
                                <input type="text" name="credit_number[]" value="{{ old('credit_number') }}" class="form-control" >
                            </div>
                            <div class="form-group">
                                <label>Card Holder Name</label>
                                <input type="text" name="card_holder_name[]" value="{{ old('card_holder_name') }}" class="form-control" >
                            </div>
                        </div>

                        <div  id="banksection">

                        </div>
                        <div class="col-12 col-sm-12">
                            <label>City : </label>
                            <select id="cooks" name="city" class="selectpicker"  data-live-search="true"   style="width:50%;paddind:2px">
                                @foreach ($cities as $city)
                                <option value="{{ $city->id }}">{{ $city->name }}</option>
                            @endforeach
                            </select>
                        </div>
                        <div class="col-12 col-sm-12 mt-3">
                            <div class="form-group">
                                <label>Contract Number</label>
                                <input type="text" name="contract" class="form-control" >
                            </div>
                        </div>
                        <div class="col-12 col-sm-12">
                            <div class="form-group">
                                <label>Commission</label>
                                <input type="number" name="commission" class="" style="width: 50%;dislay:inline !important">
                                <select class="form-control" name="commission_type"  id="commission_type" title="EGP" placeholder="EGP"  data-live-search="true"   style="width:20%!important;display: inline !important;paddind:2px" >
                                    <option value="EGP">EGP </option>
                                    <option value="%">% </option>

                                </select>
                            </div>
                        </div>

                    </div>
                    <button type="submit" class="btn btn-otbokhly btn-block">Save Changes</button>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- /ADD Modal -->

      <!-- Delete Modal -->

      <div class="modal fade" id="delete_modal" aria-hidden="true" role="dialog">
        <div class="modal-dialog modal-dialog-centered" role="document" >
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Delete</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-content p-2">
                        <h4 class="modal-title">Delete</h4>
                        <p class="mb-4">Are you sure want to delete?</p>

                        <form  id="formDelete" action="" method="POST" style="display: inline">
                            @csrf
                            @method('delete')
                            <button class="btn btn-primary btn-delete" type="submit" class="btn btn-primary">Delete </button>
                        </form>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Delete Modal -->


@endsection


@push('scripts')
    <script>
        function  remove(elem){
            $(elem).parent().parent().remove();
        }

        function removeedit(elem){
            $(elem).parent().next().remove();
            $(elem).parent().remove();
        }

        $('#add').click(function(){

            let newbank =$( "#addRow" ).clone();

            $('#banksection').append(newbank);
            $(this).parent().parent().find('#addRow').css('display','block')

        });

        // function add(elem){
        //     let newbank =$( "#addRowedit" ).clone();

        //     $(elem).parent().parent().find('.banksectionedit').append(newbank);
        //     $(elem).parent().parent().find('#addRowedit').css('display','block')
        // };

</script>
    <script>



        var errors= <?php echo json_encode(session()->get('message_update')); ?>;
        var errors_add= <?php echo json_encode(session()->get('message_add')); ?>;

        if (errors !=null) {
            let id= <?php echo json_encode(session()->get('cook_id')); ?>;
            let modal_edit='#edit_cooks_details_'+id;
            for (x in errors) {
                var err='<li>'+ errors[x]+'</li>';
                $('.list').append(err);
            }
             $('.errors').css('display','block');
            $(modal_edit).modal('show');
        }
        else if(errors_add != null)
        {
            for (x in errors_add) {
                var err='<li>'+ errors_add[x]+'</li>';
                $('.list').append(err);
            }
             $('.errors').css('display','block');

            $('#Add_Cooks').modal('show');

        }

        $(".modal").on("hidden.bs.modal", () => {
            $('.errors').css('display','none');
        });

        $('.link_delete').on('click',function(){
            var cook_id=$(this).attr('id');

            $('#formDelete').attr('action','/{{ Config::get('app.locale') }}/dashboard/cook/'+cook_id)

        });


        // function vip(cook_id,checkbox){
        //     var value=document.getElementById(cook_id);
        //     if(value.checked == true){

        //         $.ajax({
        //             type: 'GET',
        //             url: '/{{ Config::get('app.locale') }}/dashboard/cook/vip/'+cook_id,

        //             success:function(data){
        //                 value='1';

        //             },
        //             error:function(data){
        //             }
        //         });


        //     }else{
        //         $.ajax({
        //             type: 'GET',
        //             url: '/{{ Config::get('app.locale') }}/dashboard/cook/reset/'+cook_id,

        //             success:function(data){
        //                 value='0';

        //             },
        //             error:function(data){
        //             }
        //         });
        //     }
        // };
    </script>
@endpush
