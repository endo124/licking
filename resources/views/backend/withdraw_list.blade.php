@extends('backend.layouts.app')


@section('content')
 <!-- Page Wrapper -->
 <div class="page-wrapper">
    <div class="content container-fluid">

        <!-- Page Header -->
        <div class="page-header">
            <div class="row">
                <div class="col-sm-12">
                    <h3 class="page-title">@lang('site.List of withdraws')</h3>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('/dashboard') }}">@lang('site.Dashboard')</a></li>
                        <li class="breadcrumb-item active">@lang('site.withdraw')</li>
                    </ul>
                </div>
                @if (auth()->guard('admin')->user()->hasRole('cook'))
                <div class="col-sm-12 col">
                    <a href="#Add_withdraw" data-toggle="modal" class="btn btn-otbokhly float-right mt-2 " >@lang('site.Add')</a>
                </div>
               @endif
            </div>

        </div>
        <!-- /Page Header -->


        <div class="row" >
            <div class="col-md-12 d-flex">

                <!-- Recent Orders -->
                <div class="card card-table flex-fill">
                    <div class="card-header">
                        <h4 class="card-title">@lang('site.withdraws List')</h4>

                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-hover table-center mb-0">
                                <thead>
                                    <tr>
                                        <th> #</th>
                                       @if (auth()->guard('admin')->user()->hasRole('super_admin'))
                                         <th> @lang('site.Restaurant')</th>
                                       @endif
                                        <th> @lang('site.Amount')</th>
                                        <th> @lang('site.Status')</th>
                                        <th>@lang('site.date and Time')</th>
                                        <th>@lang('site.Action')</th>
                                    </tr>
                                </thead>
                                <tbody>
                                   @isset($withdraws)
                                   @foreach ($withdraws as $index=>$withdraw)
                                   <tr>
                                       <td>
                                           {{ $index+1 }}
                                       </td>
                                       @if (auth()->guard('admin')->user()->hasRole('super_admin'))
                                        <td>{{ $withdraw->cooks->name ?? ''}}</td>
                                       @endif
                                       <td>{{ $withdraw->amount }}</td>
                                       <td>
                                           @if ($withdraw->status ==0)
                                           @lang('site.Pending')
                                           @elseif($withdraw->status ==-1)
                                           @lang('site.Declined')
                                           @else
                                           @lang('site.Accepted')
                                           @endif
                                        </td>
                                       <td>{{ $withdraw->created_at }}</td>

                                       <td >
                                           <div class="actions">
                                                @if (auth()->guard('admin')->user()->hasRole('super_admin') ||auth()->guard('admin')->user()->hasRole('admin'))
                                                    @if($withdraw->status == 0 )
                                                        <a id="" class="link_decline" href="{{ url('dashboard/withdraw/decline/'. $withdraw->id ) }}"  class="btn btn-sm btn-danger" style="color: #f00">
                                                            @lang('site.Decline')
                                                        </a>
                                                        <a id="" class="link_accept ml-2" href="{{ url('dashboard/withdraw/accept/'.$withdraw->id ) }}" class="btn btn-sm btn-danger" style="color: Green">
                                                            @lang('site.Accept')
                                                        </a>
                                                    @else
                                                    <a class="link_delete" id="{{ $withdraw->id }}" data-toggle="modal" href="#delete_modal" data-url=""  class="btn btn-sm btn-danger" style="color: #f00"> @lang('site.Delete')</a>

                                                    @endif
                                                @elseif(auth()->guard('admin')->user()->hasRole('cook'))
                                                @if ($withdraw->status == 0)
                                                    <a id="" href="{{ url('dashboard/withdraw/cancel/'.$withdraw->id ) }}" class="link_delete" href=""  class="btn btn-sm btn-danger" style="color: #f00">
                                                        @lang('site.Cancel')
                                                    </a>
                                                @else
                                                <a class="link_delete" id="{{ $withdraw->id }}" data-toggle="modal" href="#delete_modal" data-url=""  class="btn btn-sm btn-danger" style="color: #f00"> @lang('site.Delete')</a>

                                                @endif


                                                @endif
                                           </div>
                                       </td>
                                   </tr>
                                   @endforeach
                                   @endisset

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- /Recent Orders -->

            </div>
        </div>

    </div>
</div>
<!-- /Page Wrapper -->
  <!-- Add Modal -->
  <div class="modal fade" id="Add_withdraw" aria-hidden="true" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document" >
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">@lang('site.Add withdraw')  </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span id="total">{{$total_profit??' '}}</span> @lang('site.'.$currency)
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                @include('backend.partials.errors')
                @if ($total_profit >=1)

                <form method="POST" action="{{ route('withdraw.store') }}">
                    @csrf
                    <div class="row form-row">
                        <div class="col-12 col-sm-12">
                            <div class="form-group">
                                <label>@lang('site.Amount')</label>
                                <input id="withdraw" type="number" name="amount" min="1" max="{{$total_profit <=1? 1:$total_profit }}" class="form-control" value="{{ old('amount') }}">
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-otbokhly btn-block">@lang('site.Save')</button>
                </form>
                @else
                    <p class="text-center"> @lang('site.you have no money to make withdraw request')</p>
                @endif
            </div>
        </div>
    </div>
</div>
<!-- /ADD Modal -->

      <!-- Delete Modal -->

      <div class="modal fade" id="delete_modal" aria-hidden="true" role="dialog">
        <div class="modal-dialog modal-dialog-centered" role="document" >
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">@lang('site.Delete')</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-content p-2">
                        <h4 class="modal-title">@lang('site.Delete')</h4>
                        <p class="mb-4">@lang('site.Are you sure want to delete?')</p>

                        <form  id="formDelete" action="" method="POST" style="display: inline">
                            @csrf
                            @method('delete')
                            <button class="btn btn-primary btn-delete" type="submit" class="btn btn-primary">@lang('site.Delete') </button>
                        </form>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">@lang('site.Close')</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Delete Modal -->


@endsection


@push('scripts')
    <script>
        @if (count($errors) > 0)
                $('#Add_withdraw').modal('show');
        @endif


        $('.link_delete').on('click',function(){
            var withdraw_id=$(this).attr('id');

            $('#formDelete').attr('action','/{{ Config::get('app.locale') }}/dashboard/withdraw/'+withdraw_id)

        });

        // var total_profit=$('#total').html();
        // $('#withdraw').attr("max",total_profit)

        // $('#withdraw').keydown(function(){
        //     var total=$('#total').html();
        //     var subtotal=total-$(this).val();
        //     $('#total').html(subtotal);
        // });


    </script>
@endpush
