<!-- Sidebar -->
<div class="sidebar" id="sidebar" style="overflow: auto;">
    <div class="sidebar-inner slimscroll">
        <div id="sidebar-menu" class="sidebar-menu">
            <ul>
                {{-- <li class="menu-title">
                    <span>Main</span>
                </li> --}}
                <li class="">
                    <a href="/dashboard"><i class="fe fe-home"></i> <span>Dashboard</span></a>
                </li>
                {{-- @if (auth()->guard('admin')->user()->hasRole('super_admin') ) --}}
                    @if (is_null(auth()->guard('admin')->user()->parent) )

                        <li class="{{ Request::segment(3) == 'branches' ? 'active' : null }}">
                            <a href="{{ url('/dashboard/branches') }}"><i class="fas fa-code-branch"></i><span>Branches</span></a>
                        </li>
                    {{-- @endif --}}
                    {{-- <li class="{{ Request::segment(3) == 'category' ? 'active' : null }}">
                        <a href="{{ url('/dashboard/category') }}"><i class="fa fa-dot-circle-o" aria-hidden="true"></i><span>Categories</span></a>
                    </li> --}}
                @endif
                {{-- @if (auth()->guard('admin')->user()->hasRole('cook'))
                    <li class="{{ Request::segment(3) == 'section' ? 'active' : null }}">
                        <a href="{{ url('/dashboard/section') }}"><i class="fa fa-dot-circle-o" aria-hidden="true"></i><span>Sections</span></a>
                    </li>
                @endif --}}
                {{-- @if (auth()->guard('admin')->user()->hasRole('super_admin') ||auth()->guard('admin')->user()->hasRole('admin')) --}}
                    @if (auth()->guard('admin')->user()->hasPermission('admins_read'))
                    <li class="{{ Request::segment(3) == 'admin' ? 'active' : null }}">
                        <a href="{{ url('/dashboard/admin') }}"><i class="fe fe-users"></i><span>Admins</span></a>
                    </li>
                    @endif
                    @if (!isset(auth()->guard('admin')->user()->parent))
                    <li class="submenu">
                        <a href="#"><i class="fa fa-database " aria-hidden="true"></i><span> Catalog </span> <span class="menu-arrow"></span></a>
                        <ul style="display: none;">
                            {{-- @if (auth()->guard('admin')->user()->hasPermission('cusines_read')) --}}
                                <li class="{{ Request::segment(3) == 'cusine' ? 'active' : null }}">
                                    <a href="{{ url('/dashboard/cusine') }}"><i class="fa fa-dot-circle-o" aria-hidden="true"></i><span>Cusine</span></a>
                                </li>
                            {{-- @endif --}}
                            {{-- @if (auth()->guard('admin')->user()->hasPermission('sections_read')) --}}
                                <li class="{{ Request::segment(3) == 'section' ? 'active' : null }}">
                                    <a href="{{ url('/dashboard/section') }}"><i class="fa fa-dot-circle-o" aria-hidden="true"></i><span>Sections</span></a>
                                </li>
                            {{-- @endif --}}

                            {{-- @if (auth()->guard('admin')->user()->hasPermission('allergenes_read')) --}}
                                <li class="{{ Request::segment(3) == 'allergen' ? 'active' : null }}">
                                    <a href="{{ url('/dashboard/allergen') }}"><i class="fa fa-dot-circle-o" aria-hidden="true"></i><span>Allergen</span></a>
                                </li>
                            {{-- @endif
                            @if (auth()->guard('admin')->user()->hasPermission('categories_read')) --}}
                                <li class="{{ Request::segment(3) == 'category' ? 'active' : null }}">
                                    <a href="{{ url('/dashboard/category') }}"><i class="fa fa-dot-circle-o" aria-hidden="true"></i><span>Categories</span></a>
                                </li>
                            {{-- @endif --}}
                        </ul>
                    </li>
                    @endif




                    {{-- @if (auth()->guard('admin')->user()->hasPermission('cooks_read'))
                    <li class="{{ Request::segment(3) == 'cook' ? 'active' : null }}">
                        <a href="{{ url('/dashboard/cook') }}"><i class="fe fe-user-plus"></i> <span>Vendors</span></a>
                    </li>
                    @endif --}}
                    @if (!isset(auth()->guard('admin')->user()->parent))

                    {{-- @if (auth()->guard('admin')->user()->hasPermission('coupons_read')) --}}
                    <li class="{{ Request::segment(3) == 'discount' ? 'active' : null }}">
                        <a href="{{ url('/dashboard/discount') }}"><i class="fa fa-gift" aria-hidden="true"></i><span>Coupons</span></a>
                    </li>
                    @endif
                    {{-- @if (auth()->guard('admin')->user()->hasPermission('customers_read')) --}}
                    <li class="{{ Request::segment(3) == 'customer' ? 'active' : null }}">
                        <a href="{{ url('/dashboard/customer') }}"><i class="fa fa-users" aria-hidden="true"></i><span>Customers</span></a>
                    </li>
                    {{-- @endif --}}
                    {{-- @if (auth()->guard('admin')->user()->hasPermission('cities_read')) --}}
                    @if (!isset(auth()->guard('admin')->user()->parent))

                    {{-- <li class="{{ Request::segment(3) == 'city' ? 'active' : null }}">
                        <a href="{{ url('/dashboard/city') }}"><i class="fa fa-university" aria-hidden="true"></i><span>Cities</span></a>
                    </li> --}}
                    {{-- @endif
                    @if (auth()->guard('admin')->user()->hasPermission('settings_read')) --}}
                    <li class="{{ Request::segment(3) == 'setting' ? 'active' : null }}">
                        <a href="{{ url('/dashboard/setting') }}"><i class="fe fe-vector"></i> <span>Settings</span></a>
                    </li>
                    {{-- @endif
                    @if (auth()->guard('admin')->user()->hasPermission('VIP_read'))
                    <li class="{{ Request::segment(3) == 'vipCustomers' ? 'active' : null }}">
                        <a href="{{ url('/dashboard/vipCustomers') }}"><i class="fe fe-user"></i> <span>VIP Customer</span></a>
                    </li>
                    @endif
                    {{-- @if (auth()->guard('admin')->user()->hasPermission('VIP_read'))
                    <li class="{{ Request::segment(1) == 'vipCooks' ? 'active' : null }}">
                        <a href="{{ url('/dashboard/vipCooks') }}"><i class="fe fe-user"></i> <span>VIP Cooks</span></a>
                    </li>
                    @endif --}}
                    {{-- <li class="{{ Request::segment(3) == 'drivers' ? 'active' : null }}">
                        <a href="{{ url('/dashboard/drivers') }}"><i class="fa fa-motorcycle" aria-hidden="true"></i><span> Driver Filter</span></a>
                    </li>
                    <li class="{{ Request::segment(3) == 'drivers' ? 'active' : null }}">
                        <a href="{{ url('/dashboard/tracking-drivers') }}"><i class="fa fa-motorcycle" aria-hidden="true"></i><span>Tracking Drivers</span></a>
                    </li> --}}
                    <li class="{{ Request::segment(3) == 'account' ? 'active' : null }}">
                        <a href="{{ url('/dashboard/account') }}"><i class="fe fe-money" aria-hidden="true" style="font-size: 25px;"></i><span>Account</span></a>
                    </li>
                @endif
                @if (auth()->guard('admin')->user()->hasRole('cook') || auth()->guard('admin')->user()->hasRole('super_admin'))
                    <li class="{{ Request::segment(3) == 'addons' ? 'active' : null }}">
                        <a href="{{ url('/dashboard/addons') }}"><i class="fa fa-plus-square" aria-hidden="true"></i><span>Addons</span></a>
                    </li>
                    <li class="{{ Request::segment(3) == 'addons_section' ? 'active' : null }}">
                        <a href="{{ url('/dashboard/addons_section') }}"><i class="fa fa-plus-square" aria-hidden="true"></i><span>Addons Section</span></a>
                    </li>
                @endif

                {{-- <li class="{{ Request::segment(3) == 'support' ? 'active' : null }}">
                    <a href="{{ url('/dashboard/support') }}"><i class="far fa-comments"></i><span>Support</span></a>
                </li> --}}


                {{-- <li class="{{ Request::segment(3) == 'withdraw' ? 'active' : null }}">
                    <a href="{{ url('/dashboard/withdraw') }}"><i class="fa fa-money" aria-hidden="true"></i> <span>Withdraw Request</span></a>
                </li> --}}
                @if (auth()->guard('admin')->user())
                <li class="{{ Request::segment(3) == 'order' ? 'active' : null }}">
                    <a href="{{ url('/dashboard/order') }}"><i class="fa fa-bars" aria-hidden="true"></i><span>Orders</span></a>
                </li>
                @endif
                @if (auth()->guard('admin')->user())
                <li class="{{ Request::segment(3) == 'dish' ? 'active' : null }}">
                    <a href="{{ url('/dashboard/dish') }}"><i class="fas fa-pizza-slice" style="font-size: 15px"></i> <span>Dishes</span></a>
                </li>
                @endif
                <li class="{{ Request::segment(3) == 'changepass' ? 'active' : null }}">
                    <a href="{{ url('/dashboard/changepass') }}"><i class="fas fa-user-cog" style="font-size: 15px"></i> <span>change pass</span></a>
                </li>



                {{-- <li>
                    <a href="#"><i class="fe fe-star-o"></i> <span>Reviews</span></a>
                </li>
                <li>
                    <a href="#"><i class="fe fe-activity"></i> <span>Transactions</span></a>
                </li>

                <li class="submenu">
                    <a href="#"><i class="fe fe-document"></i> <span> Reports</span> <span class="menu-arrow"></span></a>
                    <ul style="display: none;">
                        <li><a href="invoice-report.html">Invoice Reports</a></li>
                    </ul>
                </li> --}}

            </ul>
        </div>
    </div>
</div>
<!-- /Sidebar -->
