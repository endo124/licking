
@extends('backend.layouts.app')
<link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">


<style>
    .dropzone.dz-clickable{
    cursor:pointer;
    text-align: center;
}
.dropzone {
    background-color: #fbfbfb;
    border: 2px dashed rgba(0, 0, 0, 0.1);
    min-height: 150px;
    border: 2px solid rgba(0,0,0,0.3);
    background: white;
    padding: 20px 20px;
}
.dropzone .dz-message{
    margin: 3em 0;

}
.upload-image{
position: relative;
    width: 80px;
    margin-right: 20px;
    display: none
}
.upload-image img {
    border-radius: 4px;
    height: 80px;
    width: 80px;
}





</style>


@section('content')
<!-- Page Wrapper -->
<div class="page-wrapper">
    <div class="content container-fluid">

        <!-- Page Header -->
        <div class="page-header">
            <div class="row">
                <div class="col-sm-12">
                    <h3 class="page-title">Profile of Cooks</h3>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
                        <li class="breadcrumb-item active">Cook profile</li>
                    </ul>
                </div>

            </div>

        </div>
        <!-- /Page Header -->

    <!-- Page Content -->
    <div class="content mt-5">
        <div class="container">

            <div class="row">
                <div class="col-md-7 col-lg-8 col-xl-9">

                    <form action="{{ route('cook.update',$cook->id) }}" method="post" enctype="multipart/form-data">
                        @csrf
                        @method('put')
                        <!-- Basic Information -->
                        <div class="card">
                            <div class="card-body">
                                @include('backend.partials.errors')

                               <div class="row">
                                <div class="col-md-6">
                                    <h4 class="card-title">Basic Information</h4>
                                </div>
                                <div class="col-md-6">
                                    <div class="custom-control custom-switch">
                                        <input name="availability" type="checkbox" class="custom-control-input" id="customSwitches" value="{{ $cook->availability }}" {{ $cook->availability == 1 ? 'checked' : ''}} >
                                        <label class="custom-control-label" for="customSwitches">Status</label>
                                    </div>
                                </div>
                               </div>
                                <div class="row form-row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="change-avatar">
                                                <div class="profile-img mb-3">
                                                    <img id="hidden_img_src" src="{{ asset('/backend/img/cook/'.$cook->images) }}" alt="Cook Image" width="150" style="border-radius: 25%;" >
                                                </div>
                                                <div class="upload-img">
                                                    <div class="change-photo-btn">
                                                        <span><i class="fa fa-upload"></i> Upload Photo</span>
                                                        <input type="file" name="profile_image" class="upload" onchange="hidden_img_src.src=window.URL.createObjectURL(this.files[0])">

                                                    </div>
                                                    <small class="form-text text-muted">Allowed JPG, GIF or PNG. Max size of 2MB</small>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Username </label>
                                            <input type="text" name="name" class="form-control" value="{{ $cook->name }}">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Email </label>
                                            <input type="email" name="email" class="form-control"value="{{ $cook->email }}" >
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Phone Number</label>
                                            <input type="text" name="phone" value="{{ $cook->phone }}" class="form-control">
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group mb-0">
                                            <label>Date of Birth</label>
                                            <input name="birth_date"  type="date" value="{{$cook->date_of_birth ?? old('birth_date') }}" class="form-control">
                                        </div>
                                    </div>
                                    <div class="row">
                                        Address :
                                        <div class="col-sm-12 mb-3">
                                            <div class="row pb-3 map-location pt-3">
                                                <input type="text" value=""  class="form-control "  name="coordinates" id="coordinates"   hidden>
                                                <input type="text"  class="form-control "  name="address"  value="{{ $cook->address[0]->address ?? ' ' }}"  >
                                                <div id="map"  style="width: 800px;height:200px "></div>
                                                <div id="location" class="marker"><i class="fa fa-arrow"></i></div>

                                            </div>
                                        </div>
                                        {{-- <div class="col-md-6">
                                            <div class="form-group mb-0">
                                                <label>Title</label>
                                                <input name="title"  type="text" value="{{ $cook->address[0]->title ?? old('title') }}" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group mb-0">
                                                <label>State</label>
                                                <input name="state"  type="text" value="{{ $cook->address[0]->state ?? old('state') }}" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group mb-0">
                                                <label>City</label>
                                                <input name="city"  type="text" value="{{ $cook->address[0]->city ?? old('city')  }}" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group mb-0">
                                                <label>Area</label>
                                                <input name="area"  type="text" value="{{$cook->address[0]->area ?? old('area') }}" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group mb-0">
                                                <label>Floor</label>
                                                <input name="floor"  type="text" value="{{$cook->address[0]->floor ?? old('floor') }}" class="form-control">
                                            </div>
                                        </div> --}}
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 mt-3 ">
                                            Availability :
                                        </div>
                                        <div class="col-md-12 mt-3 ">
                                            <div class="table-responsive">
                                                <table class="table table-hover table-center mb-0">
                                                    @php
                                                        $from=json_decode($cook->work_from,true);
                                                        $to=json_decode($cook->work_to,true);
                                                        $days=['Sat','Sun','Mon','Tue','Wed','Thu','Fri']
                                                    @endphp
                                                    <thead>
                                                        <tr>
                                                            <th> #</th>
                                                            <th> From</th>
                                                            <th> To</th>
                                                            <th> Holiday</th>
                                                        </tr>

                                                    </thead>
                                                    <tbody>
                                                        @for ($i = 0; $i < count($days); $i++)
                                                        <tr>
                                                            <td>{{ $days[$i] }}</td>
                                                            @if ($from[$i] != null  )
                                                                <td class="from"> <input class="from_input" name="work_from[]"  type="time" value="{{ date('H:i:s', strtotime($from[$i])) }}"   ></td>
                                                            @else
                                                                <td class="from"> <input class="from_input" name="work_from[]"  type="time" value=""   ></td>
                                                            @endif
                                                            @if ($to[$i] != null )
                                                                <td class="to"> <input class="to_input" name="work_to[]"   type="time" value="{{date('H:i:s', strtotime($to[$i])) ?? old('work_to') }}"  > </td>
                                                            @else
                                                                <td class="to"> <input class="to_input" name="work_to[]"   type="time" value=""  > </td>
                                                            @endif

                                                            <td class="text-center" onclick="reset(this)">
                                                                <input  name="holiday[]" type="checkbox" id="holiday{{ $i }}"   value="{{ $i }}"  {{ $from[$i] == null ? 'checked':' ' }} >
                                                            </td>
                                                        </tr>
                                                        @endfor


                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>


                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /Basic Information -->

                        <!-- About Me -->
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">About Me</h4>
                                <div class="form-group mb-0">
                                    <label>info</label>
                                    <textarea name="info" class="form-control"  rows="5" >{{$cook->info ?? old('info') }}</textarea>
                                </div>
                            </div>
                        </div>
                        <!-- /About Me -->

                        <div class="submit-section submit-btn-bottom">
                            <button type="submit" class="btn btn-primary submit-btn"> <i class="fas fa-edit"></i> Save Changes</button>
                        </div>
                    </form>
                </div>
            </div>

        </div>

    </div>
    <!-- /Page Content -->
    </div>
</div>
<script src="https://code.jquery.com/jquery-1.11.2.min.js"></script>
{{-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css"> --}}
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ol3/3.6.0/ol.css" type="text/css">
<link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">

<script src="https://cdnjs.cloudflare.com/ajax/libs/ol3/3.6.0/ol.js"></script>

    <script>
     // create a style to display our position history (track)
     var trackStyle = new ol.style.Style({
        stroke: new ol.style.Stroke({
          color: 'rgba(0,0,255,1.0)',
          width: 3,
          lineCap: 'round'
        })
      });
      // use a single feature with a linestring geometry to display our track
      var trackFeature = new ol.Feature({
        geometry: new ol.geom.LineString([])
      });
      // we'll need a vector layer to render it
      var trackLayer = new ol.layer.Vector({
        source: new ol.source.Vector({
          features: [trackFeature]
        }),
        style: trackStyle
      });
      var baseLayer = new ol.layer.Tile({
        source: new ol.source.OSM()
      });
      var london = ol.proj.transform([-0.12755, 51.507222], 'EPSG:4326', 'EPSG:3857');
      var view = new ol.View({
        center: london,
        zoom: 6
      });
      var map = new ol.Map({
        target: 'map',
        layers: [baseLayer, trackLayer],
        view: view
      });
      // set up the geolocation api to track our position
      var geolocation = new ol.Geolocation({
        tracking: true
      });
      // bind the view's projection
      geolocation.bindTo('projection', view);
      // when we get a position update, add the coordinate to the track's
      // geometry and recenter the view
      geolocation.on('change:position', function() {
        var coordinate = geolocation.getPosition();
        view.setCenter(coordinate);
        trackFeature.getGeometry().appendCoordinate(coordinate);
      });
      // put a marker at our current position
      var marker = new ol.Overlay({
        element: document.getElementById('location'),
        positioning: 'center-center'
      });
      map.addOverlay(marker);
      marker.bindTo('position', geolocation);
      // rotate the view to match the device orientation
      var deviceOrientation = new ol.DeviceOrientation({
        tracking: true
      });
      deviceOrientation.on('change:heading', onChangeHeading);
      function onChangeHeading(event) {
        var heading = event.target.getHeading();
        view.setRotation(-heading);
      }
    </script>



@endsection




