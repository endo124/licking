@extends('backend.layouts.app')
<style>
.dish-type  li{
    list-style: none;
    float: left;
    margin-left:10px
}

/* #addRow{
    display: hidden;
}
#addRow:first-child {
display: block;

} */
#new_addon2{
    margin-left: 61px;
    margin-top: 6px;
    width: 100%;
}
.fa-trash-alt{
    margin-left: 5px
}
</style>

@section('content')
 <!-- Page Wrapper -->
 <div class="page-wrapper">
    <div class="content container">
        <div class="row" style="justify-content: center" >
            <div class="col-md-8 d-flex">

                <!-- Recent Orders -->
                <div class="card card-table flex-fill" style="padding:20px">
                    <div class="card-header">
                        <h4 class="card-title">@lang('site.Edit Vendor')</h4>
                    </div>
                    <div class="card-body">
                    @include('backend.partials.errors')
                    <div class="form-group errors" style="display: none" >
                        <div class="alert alert-danger">
                          <ul class="list">

                          </ul>
                        </div>
                      </div>
                    <form  method="post" action="{{ route('cook.update',$cook->id) }}">
                        @csrf
                        @method('put')

                        <div class="row form-row">
                            @foreach (config('translatable.locales') as $locale)
                            <div class="col-12 col-sm-12">
                                <div class="form-group">
                                    <label>{{ trans('site.'.$locale.'.name') }}</label>
                                    <input type="text" name="{{ $locale }}[name]" class="form-control" value="{{$cook->translate($locale)->name ??old($locale.'.name') }}">
                                </div>
                            </div>
                            @endforeach
                            <div class="col-12 col-sm-12">
                                <div class="form-group">
                                    <label>@lang('site.Phone')</label>
                                    <input type="text" name="phone" class="form-control" value="{{$cook->phone ??old('phone') }}">
                                </div>
                            </div>
                            <div class="col-12 col-sm-12">
                                <div class="form-group">
                                    <label>@lang('site.E-mail')</label>
                                    <input type="text" name="email" class="form-control" value="{{ $cook->email??old('email') }}">
                                </div>
                            </div>
                            <div class="col-12 col-sm-12">
                                <div class="form-group">
                                    <label>@lang('site.password')</label>
                                    <input type="password" name="password" class="form-control" >
                                </div>
                            </div>
                            @php
                                $bank =json_decode( $cook->bank);
                                $credit_number=json_decode($cook->credit_number);
                                $card_holder_name=json_decode($cook->card_holder_name);

                            @endphp
                            @if($cook->parent == null)

                                    @if (is_array($bank))
                                        @for ($i = 0; $i < count($bank); $i++)
                                            <div class="col-12 col-sm-12">
                                                <label>@lang('site.Bank') : </label>
                                                <select id="bankedit" name="bankedit[]"  style="padding: 10px;background: #E0E0E0;color: #000;border: none;margin-left: 20px;width: 64%;">
                                                    <option value="The National Commercial Bank" {{ 'The National Commercial Bank' == $bank[$i]? 'selected' : ''}} >@lang('site.The National Commercial Bank') </option>
                                                    <option value="The Saudi British Bank"{{ 'The Saudi British Bank' == $bank[$i]? 'selected' : ''}}>@lang('site.The Saudi British Bank')</option>
                                                    <option value="Saudi Investment Bank"{{ 'Saudi Investment Bank' == $bank[$i]? 'selected' : ''}}>@lang('site.Saudi Investment Bank')</option>
                                                    <option value="Alinma Bank"{{ 'Alinma Bank' == $bank[$i]? 'selected' : ''}}>@lang('site.Alinma Bank')</option>
                                                    <option value="Banque Saudi Fransi"{{ 'Banque Saudi Fransi' == $bank[$i]? 'selected' : ''}}>@lang('site.Banque Saudi Fransi')</option>
                                                    <option value="Riyad Bank"{{ 'Riyad Bank' == $bank[$i]? 'selected' : ''}}>@lang('site.Riyad Bank')</option>
                                                    <option value="Samba Financial Group (Samba)"{{ 'Samba Financial Group (Samba)' == $bank[$i]? 'selected' : ''}}>@lang('site.Samba Financial Group (Samba)')</option>
                                                    <option value="Alawwal Bank"{{ 'Alawwal Bank' == $bank[$i]? 'selected' : ''}}>@lang('site.Alawwal Bank')</option>
                                                    <option value="Al Rajhi Bank"{{ 'Al Rajhi Bank' == $bank[$i]? 'selected' : ''}}>@lang('site.Al Rajhi Bank')</option>
                                                    <option value="Arab National Bank"{{ 'Arab National Bank' == $bank[$i]? 'selected' : ''}}>@lang('site.Arab National Bank')</option>
                                                    <option value="Bank AlBilad"{{ 'Bank AlBilad' == $bank[$i]? 'selected' : ''}}>@lang('site.Bank AlBilad')</option>
                                                    <option value="Bank AlJazira"{{ 'Bank AlJazira' == $bank[$i]? 'selected' : ''}}>@lang('site.Bank AlJazira')</option>
                                                    <option value="Gulf International Bank Saudi Aribia (GIB-SA)"{{ 'Gulf International Bank Saudi Aribia (GIB-SA)' == $bank[$i]? 'selected' : ''}}>@lang('site.Gulf International Bank Saudi Aribia (GIB-SA)')</option>
                                                </select>
                                                @if ($i == 0)
                                                    <a  class="btn " onclick="add(this)"  style="float: right;color:blue;font-size:20px;padding:5px 10px">+</a>
                                                @else
                                                    <a  class="btn" onclick="removeedit(this)" style="float: right;color:blue;font-size:20px;padding:5px 10px">-</a>
                                                @endif
                                            </div>
                                            <div class="col-12 col-sm-12 mt-3">
                                                <div class="form-group">
                                                    <label>@lang('site.Credit Number')</label>

                                                    <input type="text" name="credit_numberedit[]" value="{{ $credit_number[$i] ?? '' }}" class="form-control"  required>
                                                </div>
                                                <div class="form-group">
                                                    <label>@lang('site.Card Holder Name')</label>
                                                    <input type="text" name="card_holder_name[]" value="{{$card_holder_name[$i] }}" class="form-control" >
                                                </div>
                                            </div>

                                        @endfor
                                    @else
                                        <div class="col-12 col-sm-12">
                                            <label>@lang('site.Bank')  : </label>
                                            <select id="bankedit" name="bankedit[]"  style="padding: 10px;background: #E0E0E0;color: #000;border: none;margin-left: 20px;width: 64%;">
                                                <option value="The National Commercial Bank" {{ 'The National Commercial Bank' == $cook->bank? 'selected' : ''}} >@lang('site.The National Commercial Bank') </option>
                                                <option value="The Saudi British Bank"{{ 'The Saudi British Bank' == $cook->bank? 'selected' : ''}}>@lang('site.The Saudi British Bank')</option>
                                                <option value="Saudi Investment Bank"{{ 'Saudi Investment Bank' == $cook->bank? 'selected' : ''}}>@lang('site.Saudi Investment Bank')</option>
                                                <option value="Alinma Bank"{{ 'Alinma Bank' == $cook->bank? 'selected' : ''}}>@lang('site.Alinma Bank')</option>
                                                <option value="Banque Saudi Fransi"{{ 'Banque Saudi Fransi' == $cook->bank? 'selected' : ''}}>@lang('site.Banque Saudi Fransi')</option>
                                                <option value="Riyad Bank"{{ 'Riyad Bank' == $cook->bank? 'selected' : ''}}>@lang('site.Riyad Bank')</option>
                                                <option value="Samba Financial Group (Samba)"{{ 'Samba Financial Group (Samba)' == $cook->bank? 'selected' : ''}}>@lang('site.Samba Financial Group (Samba)')</option>
                                                <option value="Alawwal Bank"{{ 'Alawwal Bank' == $cook->bank? 'selected' : ''}}>@lang('site.Alawwal Bank')</option>
                                                <option value="Al Rajhi Bank"{{ 'Al Rajhi Bank' == $cook->bank? 'selected' : ''}}>@lang('site.Al Rajhi Bank')</option>
                                                <option value="Arab National Bank"{{ 'Arab National Bank' == $cook->bank? 'selected' : ''}}>@lang('site.Arab National Bank')</option>
                                                <option value="Bank AlBilad"{{ 'Bank AlBilad' == $cook->bank? 'selected' : ''}}>@lang('site.Bank AlBilad')</option>
                                                <option value="Bank AlJazira"{{ 'Bank AlJazira' == $cook->bank? 'selected' : ''}}>@lang('site.Bank AlJazira')</option>
                                                <option value="Gulf International Bank Saudi Aribia (GIB-SA)"{{ 'Gulf International Bank Saudi Aribia (GIB-SA)' == $cook->bank? 'selected' : ''}}>@lang('site.Gulf International Bank Saudi Aribia (GIB-SA)')</option>
                                            </select>

                                            <a  class="btn " onclick="add(this)"   style="float: right;color:blue;font-size:20px;padding:5px 10px">+</a>

                                        </div>
                                        <div class="col-12 col-sm-12 mt-3">
                                            <div class="form-group">
                                                <label>@lang('site.Credit Number')</label>
                                                <input type="text" name="credit_numberedit[]" value="{{ $cook->credit_number ?? '' }}" class="form-control"  required>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-12 mt-3">

                                            <div class="form-group">
                                                <label>@lang('site.Card Holder Name')</label>
                                                <input type="text" name="card_holder_name[]" value="{{ old('card_holder_name') }}" class="form-control" >
                                            </div>
                                        </div>

                                    @endif

                                    <div class="banksectionedit" >

                                    </div>
                                    <div class="col-12 col-sm-12">
                                        <label>@lang('site.City') : </label>
                                        <select  name="city" class="form-control"     style="width:50%;paddind:2px">
                                            @foreach ($cities as $city)
                                            @if ($cook->cities)
                                                <option value="{{ $city->id ?? ' ' }}" {{ (in_array($city->id, $cook->cities->pluck('id')->toArray()))? 'selected' :' ' }}>{{ $city->name ?? ' ' }}</option>
                                            @else
                                            <option value="{{ $city->id }}" >{{ $city->name  }}</option>

                                            @endif
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-12 col-sm-12 mt-3">
                                        <div class="form-group">
                                            <label>@lang('site.Contract Number')</label>
                                            <input type="text" name="contract" class="form-control" value="{{$cook->contract ??old('contract') }}" >
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-12">
                                        <div class="form-group">
                                            <label>@lang('site.Commission')</label>
                                            <input type="number" name="commission" class="" style="width: 50%;dislay:inline !important" value="{{ $cook->commission ??old('commission') }}">
                                            <select class="form-control" name="commission_type"  id="commission_type" title="{{$currency ?? '' }}" placeholder="{{$currency ?? '' }}"  data-live-search="true"   style="width:20%!important;display: inline !important;paddind:2px" >
                                                <option value="{{$currency}}" {{ $cook->commission_type == $currency   ? 'selected':' ' }}>{{$currency }} </option>
                                                <option value="%" {{ $cook->commission_type =='%' ? 'selected':'' }}>% </option>

                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-12">
                                        <div class="form-group">
                                            <label>@lang('site.Pickup Commission')</label>
                                            <input type="number" name="pickup_commission" class="" style="width: 50%;dislay:inline !important" value="{{ $cook->pickup_commission ??old('pickup_commission') }}">
                                            <select class="form-control" name="pickup_commission_type"  id="pickup_commission_type" title="{{$currency ?? '' }}" placeholder="{{$currency ?? '' }}"  data-live-search="true"   style="width:20%!important;display: inline !important;paddind:2px" >
                                                <option value="{{$currency  }}"  {{ $cook->pickup_commission_type ==$currency  ? 'selected':'' }}>{{$currency ?? '' }} </option>
                                                <option value="%" {{ $cook->pickup_commission_type =='%' ? 'selected':'' }}>% </option>

                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-12">
                                        <div class="form-group">
                                            <label>@lang('site.Taxes')</label>
                                            <input type="number" name="taxes" value="{{ $cook->taxes ??old('taxes') }}" class="" style="width: 50%;dislay:inline !important">
                                            <select class="form-control" name="taxes_type"  id="taxes_type" title="{{$currency }}" placeholder="{{$currency}}"  data-live-search="true"   style="width:20%!important;display: inline !important;paddind:2px" >
                                                <option value="{{$currency }}" {{ $cook->taxes_type ==$currency  ? 'selected':'' }}>{{$currency ?? '' }} </option>
                                                <option value="%" {{ $cook->taxes_type =='%' ? 'selected':'' }}>% </option>

                                            </select>
                                        </div>
                                    </div>
                            @endif
                        </div>

                        <button type="submit" class="btn btn-primary btn-block">@lang('site.Save Changes')</button>
                    </form>
                </div>
                </div>
            </div>
            <!-- /Recent Orders -->
        </div>
    </div>
</div>

{{-- <div id="addRowedit" style="display: none" >
    <div class="col-12 col-sm-12">
        <label>@lang('site.Bank') : </label>
        <select id="bank" name="bankedit[]"  style="padding: 10px;background: #E0E0E0;color: #000;border: none;margin-left: 20px;width: 64%;">
            <option value="The National Commercial Bank"  >@lang('site.The National Commercial Bank') </option>
            <option value="The Saudi British Bank">@lang('site.The Saudi British Bank')</option>
            <option value="Saudi Investment Bank">@lang('site.Saudi Investment Bank')</option>
            <option value="Alinma Bank">@lang('site.Alinma Bank')</option>
            <option value="Banque Saudi Fransi">@lang('site.Banque Saudi Fransi')</option>
            <option value="Riyad Bank">@lang('site.Riyad Bank')</option>
            <option value="Samba Financial Group (Samba)">@lang('site.Samba Financial Group (Samba)')</option>
            <option value="Alawwal Bank">@lang('site.Alawwal Bank')</option>
            <option value="Al Rajhi Bank">@lang('site.Al Rajhi Bank')</option>
            <option value="Arab National Bank">@lang('site.Arab National Bank')</option>
            <option value="Bank AlBilad">@lang('site.Bank AlBilad')</option>
            <option value="Bank AlJazira">@lang('site.Bank AlJazira')</option>
            <option value="Gulf International Bank Saudi Aribia (GIB-SA)">@lang('site.Gulf International Bank Saudi Aribia (GIB-SA)')</option>
        </select>
        <a  class="btn" onclick="remove(this)" style="float: right;color:blue;font-size:20px;padding:5px 10px">-</a>
    </div>
    <div class="col-12 col-sm-12 mt-3">
        <div class="form-group">
            <label>@lang('site.Credit Number')</label>
            <input type="text" name="credit_numberedit[]" value="" class="form-control" required>
        </div>
        <div class="form-group">
            <label>@lang('site.Card Holder Name')</label>
            <input type="text" name="card_holder_name[]" value="{{ old('card_holder_name') }}" class="form-control" >
        </div>
    </div>
</div> --}}
@push('scripts')
    <script>
        function  remove(elem){
            $(elem).parent().parent().remove();
        }

        function removeedit(elem){
            $(elem).parent().next().remove();
            $(elem).parent().remove();
        }

        $('#add').click(function(){

            let newbank =$( "#addRow" ).clone();

            $('#banksection').append(newbank);
            $(this).parent().parent().find('#addRow').css('display','block')

        });

        function add(elem){
            let newbank =$( "#addRowedit" ).clone();

            $(elem).parent().parent().find('.banksectionedit').append(newbank);
            $(elem).parent().parent().find('#addRowedit').css('display','block')
        };

</script>
    <script>



        var errors= <?php echo json_encode(session()->get('message_update')); ?>;
        var errors_add= <?php echo json_encode(session()->get('message_add')); ?>;

        if (errors !=null) {
            let id= <?php echo json_encode(session()->get('cook_id')); ?>;
            let modal_edit='#edit_cooks_details_'+id;
            for (x in errors) {
                var err='<li>'+ errors[x]+'</li>';
                $('.list').append(err);
            }
             $('.errors').css('display','block');
            $(modal_edit).modal('show');
        }
        else if(errors_add != null)
        {
            for (x in errors_add) {
                var err='<li>'+ errors_add[x]+'</li>';
                $('.list').append(err);
            }
             $('.errors').css('display','block');

            $('#Add_Cooks').modal('show');

        }

        $(".modal").on("hidden.bs.modal", () => {
            $('.errors').css('display','none');
        });

        $('.link_delete').on('click',function(){
            var cook_id=$(this).attr('id');

            $('#formDelete').attr('action','/{{ Config::get('app.locale') }}/dashboard/cook/'+cook_id)

        });


        // function vip(cook_id,checkbox){
        //     var value=document.getElementById(cook_id);
        //     if(value.checked == true){

        //         $.ajax({
        //             type: 'GET',
        //             url: '/{{ Config::get('app.locale') }}/dashboard/cook/vip/'+cook_id,

        //             success:function(data){
        //                 value='1';

        //             },
        //             error:function(data){
        //             }
        //         });


        //     }else{
        //         $.ajax({
        //             type: 'GET',
        //             url: '/{{ Config::get('app.locale') }}/dashboard/cook/reset/'+cook_id,

        //             success:function(data){
        //                 value='0';

        //             },
        //             error:function(data){
        //             }
        //         });
        //     }
        // };
    </script>
@endpush

