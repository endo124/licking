@extends('backend.layouts.app')


@category('content')
 <!-- Page Wrapper -->
 <div class="page-wrapper">
    <div class="content container-fluid">

        <!-- Page Header -->
        <div class="page-header">
            <div class="row">
                <div class="col-sm-12">
                    <h3 class="page-title">delivery charges list</h3>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
                        <li class="breadcrumb-item active">delivery charges </li>
                    </ul>
                </div>
               {{-- @if (!Auth::guard('cook')) --}}
                <div class="col-sm-12 col">
                    <a href="#Add_category" data-toggle="modal" class="btn btn-otbokhly float-right mt-2 {{ auth()->guard('admin')->user()->parent?'disabled' : '' }}"  >Add</a>
                </div>
               {{-- @endif --}}
            </div>

        </div>
        <!-- /Page Header -->


        <div class="row" >
            <div class="col-md-12 d-flex">

                <!-- Recent Orders -->
                <div class="card card-table flex-fill">
                    <div class="card-header">
                        <h4 class="card-title">delivery charges list</h4>

                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-hover table-center mb-0">
                                <thead>
                                    <tr>
                                        <th> #</th>
                                        {{-- <th> Name</th> --}}
                                        <th>Vendor Name</th>
                                        <th>From</th>
                                        <th>To</th>
                                        <th>Orders</th>
                                        <th>Type of Delivery Charges</th>
                                        <th>Amount</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @isset ($cooks)
                                        @foreach ($cooks as $index=>$cook)
                                        <tr>
                                            <td>
                                                {{ $index+1 }}
                                            </td>

                                            <td>{{ $cook->name }}</td>
                                            <td>{{ $cook->deliveryCharges->from ?? ' ' }}</td>
                                            <td>{{ $cook->deliveryCharges->to ?? ' ' }}</td>
                                            <td>{{ $cook->deliveryCharges->orders ?? ' ' }}</td>
                                            <td>{{ $cook->deliveryCharges->delivery_type == 0 ? 'free' :'fixed' }}</td>
                                            <td>{{ $cook->deliveryCharges->amount ?? '' }}</td>
                                            <td >
                                                <div class="actions">
                                                    <a id="{{ $cook->deliveryCharges->id }}" class="link_update bg-success-light mr-2 {{ auth()->guard('admin')->user()->parent?'disabled' : '' }}" data-toggle="modal"  href="#edit_categories_details{{ $cook->deliveryCharges->id }}" >
                                                        <i class="fe fe-pencil"></i> Edit
                                                    </a>
                                                    <a id="{{ $cook->deliveryCharges->id }}"  class="link_delete  {{ auth()->guard('admin')->user()->parent?'disabled' : '' }}" data-toggle="modal" href="#delete_modal" data-url=""  class="btn btn-sm btn-danger" style="color: #f00">
                                                        <i class="fe fe-trash"></i> Delete
                                                    </a>
                                                </div>
                                            </td>
                                        </tr>
                                            <!-- Edit Details Modal -->
                                            <div class="modal fade" id="edit_categories_details{{ $cook->deliveryCharges->id }}" aria-hidden="true" role="dialog">
                                                <div class="modal-dialog modal-dialog-centered" role="document" >
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title">Edit Delivery Charges</h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="form-group errors" style="display: none" >
                                                                <div class="alert alert-danger">
                                                                  <ul class="list">

                                                                  </ul>
                                                                </div>
                                                            </div>
                                                            <form id="formUpdate" method="post" action="{{ route('delivery.update', $cook->deliveryCharges->id) }}" enctype="multipart/form-data">
                                                                @csrf
                                                                @method('put')
                                                                <div class="row form-row">
                                                                    <div class="col-12 col-sm-12">
                                                                        <label>Vendors : </label>
                                                                        <select id="Vendors" name="Vendor"  style="padding: 10px;background: #E0E0E0;color: #000;border: none;margin-left: 20px;width: 64%;">
                                                                            <option value="">select vendor</option>
                                                                            @foreach ($vendors as $vendor)
                                                                                <option {{ $vendor->id== $cook->deliveryCharges->cook_id? 'selected' :' ' }}  value="{{ $vendor->id }}">{{ $vendor->name }}</option>
                                                                            @endforeach
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-12 col-sm-12 my-3" style="display: flex;">
                                                                        <div class="form-check mr-3">
                                                                            <input class="form-check-input" type="radio" name="delivery_type"value="{{ old('delivery_type') ?? 0}}" {{ $cook->deliveryCharges->delivery_type == 0 ? 'checked' : '' }}>
                                                                            <label class="form-check-label" for="free">Free Delivery</label>
                                                                        </div>
                                                                        <div class="form-check">

                                                                            <input class="form-check-input" type="radio" name="delivery_type" value="{{ old('delivery_type') ?? 1}}" {{ $cook->deliveryCharges->delivery_type == 1 ? 'checked' : '' }}>
                                                                            <label class="form-check-label" for="fixed">Fixed Rate</label>
                                                                        </div>

                                                                    </div>
                                                                    <div class="col-12 col-sm-12 mb-3">

                                                                                    <label class="form-check-label" for="from">From</label>
                                                                                    <input class="from-control" type="date" name="from" id=""  style="border: 1px solid #ccc !important;" value="{{ $cook->deliveryCharges->from ?? '' }}">

                                                                                    <label class="form-check-label" for="to">To</label>
                                                                                    <input class="from-control" type="date" name="to" id=""  style="border: 1px solid #ccc !important;" value="{{ $cook->deliveryCharges->to ?? ''}}">

                                                                        </div>


                                                                    </div>

                                                                    <div class="col-12 col-sm-12  mb-3">
                                                                        <label class="form-check-label" for="from">Orders No.</label>
                                                                        <input class="from-control" type="text" name="orders" id=""  style="border: 1px solid #ccc !important;" value="{{ $cook->deliveryCharges->orders ?? '' }}">
                                                                    </div>
                                                                    <div class="col-12 col-sm-12  mb-3">
                                                                        <label class="form-check-label" for="from">Amount.</label>
                                                                        <input class="from-control" type="text" name="amount" id=""  style="border: 1px solid #ccc !important;" value="{{ $cook->deliveryCharges->amount ?? '' }}"><span class="ml-1">SAR</span>
                                                                    </div>
                                                                </div>
                                                                <button type="submit" class="btn btn-primary btn-block">Save Changes</button>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- /Edit Details Modal -->
                                        @endforeach
                                    @endisset

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- /Recent Orders -->

            </div>
        </div>

    </div>
</div>
<!-- /Page Wrapper -->

  <!-- Add Modal -->
  <div class="modal fade" id="Add_category" aria-hidden="true" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document" >
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Add delivery charges </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                @include('backend.partials.errors')
                <div class="form-group errors" style="display: none" >
                    <div class="alert alert-danger">
                      <ul class="list">

                      </ul>
                    </div>
                </div>
                <form method="POST" action="{{ route('delivery.store') }}">
                    @csrf
                    <div class="row form-row">
                            <div class="col-12 col-sm-12">
                                <label>Vendors : </label>
                                <select id="Vendors" name="Vendor" class="selectpicker"  data-live-search="true"   style="width:50%;paddind:2px">
                                    <option value="">select vendor</option>
                                    @foreach ($vendors as $cook)
                                         <option value="{{ $cook->id }}">{{ $cook->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-12 col-sm-12 my-3" style="display: flex;">
                                <div class="form-check mr-3">
                                    <input class="form-check-input" type="radio" name="delivery_type"value="{{ old('delivery_type') ?? 0}}">
                                    <label class="form-check-label" for="free">Free Delivery</label>
                                </div>
                                <div class="form-check">

                                    <input class="form-check-input" type="radio" name="delivery_type" value="{{ old('delivery_type') ?? 1}}">
                                    <label class="form-check-label" for="fixed">Fixed Rate</label>
                                </div>

                            </div>
                            <div class="col-12 col-sm-12 mb-3">
                                <table>
                                    <tr>

                                        <td><label class="form-check-label" for="from">From</label></td>
                                        <td><input class="from-control" type="date" name="from" id=""  style="border: 1px solid #ccc !important;" value="{{ old('from') }}"></td>
                                    </tr>
                                    <tr>
                                        <td><label class="form-check-label" for="to">To</label></td>
                                        <td><input class="from-control" type="date" name="to" id=""  style="border: 1px solid #ccc !important;" value="{{ old('to') }}"></td>
                                    </tr>
                                </table>

                            </div>

                            <div class="col-12 col-sm-12  mb-3">
                                <label class="form-check-label" for="from">Orders No.</label>
                                <input class="from-control" type="text" name="orders" id=""  style="border: 1px solid #ccc !important;" value="{{ old('orders') }}">
                            </div>
                            <div class="col-12 col-sm-12  mb-3">
                                <label class="form-check-label" for="from">Amount.</label>
                                <input class="from-control" type="text" name="amount" id=""  style="border: 1px solid #ccc !important;" value="{{ old('amount') }}"><span class="ml-1">SAR</span>
                            </div>
                    </div>
                    <button type="submit" class="btn btn-otbokhly btn-block">Save</button>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- /ADD Modal -->

      <!-- Delete Modal -->

      <div class="modal fade" id="delete_modal" aria-hidden="true" role="dialog">
        <div class="modal-dialog modal-dialog-centered" role="document" >
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Delete</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-content p-2">
                        <h4 class="modal-title">Delete</h4>
                        <p class="mb-4">Are you sure want to delete?</p>

                        <form  id="formDelete" action="" method="POST" style="display: inline">
                            @csrf
                            @method('delete')
                            <button class="btn btn-primary btn-delete" type="submit" class="btn btn-primary">Delete </button>
                        </form>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Delete Modal -->


@endcategory


@push('scripts')
    <script>
        var err= <?php echo json_encode(session()->get('message_update')); ?>;
                var errors_add= <?php echo json_encode(session()->get('message_add')); ?>;

                if(err !=null) {
                    let id= <?php echo json_encode(session()->get('category_id')); ?>;
                    let modal_edit='#edit_categories_details'+id;
                    for (x in err) {
                        var err='<li>'+ err[x]+'</li>';
                        $('.list').append(err);
                    }
                    $('.errors').css('display','block');
                    $(modal_edit).modal('show');
                }

                else if(errors_add != null)
                {
                    for (x in errors_add) {
                        var err='<li>'+ errors_add[x]+'</li>';
                        $('.list').append(err);
                    }
                    $('.errors').css('display','block');

                    $('#Add_category').modal('show');

                }

                $(".modal").on("hidden.bs.modal", () => {
                    $('.errors').css('display','none');
                });

        $('.link_update').on('click',function(){
            var delivery_id=$(this).attr('id');

            $('#formUpdate').attr('action','/{{ Config::get('app.locale') }}/dashboard/delivery/'+delivery_id)

        })
        $('.link_delete').on('click',function(){
            var delivery_id=$(this).attr('id');

            $('#formDelete').attr('action','/{{ Config::get('app.locale') }}/dashboard/delivery/'+delivery_id)

        })

    </script>
@endpush
