@extends('backend.layouts.app')

@push('style')

<style>
    .link_reject{
        border: 1px solid #f00;
    padding: 3px 5px ;
    margin: 0 8px 0 0 !important;
    }

</style>
<script src="https://www.gstatic.com/firebasejs/7.16.1/firebase-app.js"></script>
<script src="https://www.gstatic.com/firebasejs/7.16.1/firebase-auth.js"></script>
<script src="https://www.gstatic.com/firebasejs/7.16.1/firebase-database.js"></script>
<script src="https://js.pusher.com/7.0/pusher.min.js"></script>

<script>
    // Your web app's Firebase configuration
    // For Firebase JS SDK v7.20.0 and later, measurementId is optional
    var firebaseConfig = {
        apiKey: "AIzaSyCX54Ej_pzEV3lwwK9QNmpcbMUmc-mAxug",
        authDomain: "logista-282218.firebaseapp.com",
        databaseURL: "https://logista-282218.firebaseio.com",
        projectId: "logista-282218",
        storageBucket: "logista-282218.appspot.com",
        messagingSenderId: "747873953165",
        appId: "1:747873953165:web:ff0d3b01fa6dd2671c89c4",
        measurementId: "G-HE8VE9GSM3"
    };
    // Initialize Firebase
    firebase.initializeApp(firebaseConfig);
  </script>
    <style>
        .avatar-sm {
         width: 4.5rem !important;
        }
        .sub_table{
            width: 100%
        }
        /* .disabled{

        }         */
        .modal {
    display:    none;}
        /* When the body has the loading class, we turn
   the scrollbar off with overflow:hidden */
body.loading .modal {
    overflow: hidden;
}

/* Anytime the body has the loading class, our
   modal element will be visible */
body.loading .modal {
    display: block;
}
.past-order{
    padding:3px 5px;
    border:1px solid green;
    margin:0 3px !important
}
    </style>
@endpush

@section('content')

 <!-- Page Wrapper -->
 <div class="page-wrapper" id="page-wrapper">
    <div class="content container-fluid">

        @if (auth()->guard('admin')->user()->hasRole('cook'))
                <!-- Page Header -->
            <div class="page-header">
                <div class="row">
                    <div class="col-sm-12">
                        <h3 class="page-title"> @lang('site.Orders List') </h3>
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="/dashboard">@lang('site.Dashboard')</a></li>
                            <li class="breadcrumb-item active">@lang('site.order')</li>
                        </ul>
                    </div>
                {{-- @if (!Auth::guard('order')) --}}
                    {{-- <div class="col-sm-12 col">
                        {{-- <a href="#Add_orders" data-toggle="modal" class="btn btn-otbokhly float-right mt-2 {{ auth()->guard('order') ? 'disabled' : '' }}" >Add</a> --}}
                        {{-- <a href="#Add_orders" data-toggle="modal" class="btn btn-otbokhly float-right mt-2   " >Add</a>
                    </div>  --}}
                {{-- @endif --}}
                </div>

            </div>
            <!-- /Page Header -->

            <div class="row">
                <div class="col-xl-2 col-sm-6 col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="dash-widget-header">
                                <span class="dash-widget-icon text-primary border-primary">
                                </span>
                                <div class="dash-count">
                                    <h3>{{ count($accepted_orders) }}</h3>
                                </div>
                            </div>
                            <div class="dash-widget-info">
                                <h6 class="text-muted">@lang('site.Accepted Orders')</h6>
                                <div class="progress progress-sm">
                                    <div class="progress-bar bg-primary" style="width: {{ $accepted_orders ?? "0" }}%"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-2 col-sm-6 col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="dash-widget-header">
                                <span class="dash-widget-icon text-primary border-primary">
                                </span>
                                <div class="dash-count">
                                    <h3>0</h3>
                                </div>
                            </div>
                            <div class="dash-widget-info">
                                <h6 class="text-muted">@lang('site.late orders')</h6>
                                <div class="progress progress-sm">
                                    <div class="progress-bar bg-primary" style="width: 0%"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-2 col-sm-6 col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="dash-widget-header">
                                <span class="dash-widget-icon text-primary border-primary">
                                </span>
                                <div class="dash-count">
                                    @isset($onduty)
                                        <h3>{{ count($onduty)}}</h3>
                                    @endisset
                                </div>
                            </div>
                            <div class="dash-widget-info">
                                <h6 class="text-muted">@lang('site.on duty drivers')</h6>
                                <div class="progress progress-sm">
                                    @isset($onduty)
                                        <div class="progress-bar bg-primary" style="width: {{ count($onduty) }}%"></div>
                                    @endisset
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-2 col-sm-6 col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="dash-widget-header">
                                <span class="dash-widget-icon text-primary border-primary">
                                </span>
                                <div class="dash-count">
                                    @isset($available)
                                        <h3>{{ count($available) }}</h3>
                                    @endisset
                                </div>
                            </div>
                            <div class="dash-widget-info">
                                <h6 class="text-muted">@lang('site.available drivers')</h6>
                                <div class="progress progress-sm">
                                    @isset($available)
                                        <div class="progress-bar bg-primary" style="width: {{ count($available) }}%"></div>
                                    @endisset
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <!-- Recent Activity Orders -->

        @endif
        <div class="card card-table" >
            <div class="card-header">
                <h4 class="card-title">@lang('site.Current Activities List')</h4>
            </div>
            <div class="card-body scroll">
                <div class="table-responsive" >
                    <table class="table table-hover table-center mb-0" id="refresh">
                        <thead>
                            <tr>
                                <th>@lang('site.Order Number')</th>
                                <th>@lang('site.Order Time')</th>
                                <th>@lang('site.Order Date')</th>
                                <th>@lang('site.Customer Name')</th>
                                @if (auth()->guard('admin')->user()->hasRole('cook'))
                                    <th>Branch Name</th>
                                @endif

                                {{-- <th>
                                    <table>
                                        <tr>
                                            <th>Dish Name</th>
                                            <th>Dish Section</th>
                                            <th>Dish price</th>
                                            <th>Amount</th>
                                        </tr>
                                    </table>
                                </th> --}}
                                <th>@lang('site.Quantity')</th>
                                {{-- @if (auth()->guard('admin')->user()->hasRole('cook'))
                                    <th>@lang('site.Location')</th>
                                @endif --}}
                                @if (auth()->guard('admin')->user()->hasRole('cook'))
                                    <th>@lang('site.Total Price')</th>
                                    {{-- <th>@lang('site.Payment Method')</th> --}}
                                    <th>@lang('site.Payment Status')</th>
                                    {{-- <th>@lang('site.Delivery Status')</th> --}}
                                    <th>@lang('site.Order Status')</th>
                                    {{-- <th>@lang('site.Customer service')</th> --}}
                                @endif
                                {{-- <th>Operations service</th> --}}
                                <th>@lang('site.Delivery Type')</th>
                                {{-- <th>@lang('site.Admin Note')</th> --}}

                                <th>@lang('site.Action')</th>
                            </tr>
                        </thead>
                        <tbody>
                            @isset($orders)
                                @php
                                        $qty=0;
                                        $address=[];
                                @endphp
                                @foreach ($currentActivities as $index=>$order)
                                    @php
                                        $cook=\App\Models\User::where('id',$order->cook_id)->first();
                                        $cook_name=$cook->name;
                                    @endphp
                                    <tr>
                                        <td>{{ $index +1 }}</td>
                                        <td>{{date('h:i A', strtotime($order->time ))  }}</td>
                                        <td>{{$order->date  }}</td>
                                        <td>
                                            <h2 class="table-avatar">
                                                {{-- <a href="profile.html" class="avatar avatar-sm mr-2"><img class="avatar-img rounded-circle" src="" alt="User Image"></a> --}}
                                                <span  id="customer_{{ $order->id }}">{{ $order->users->name }}  <a class="past-order" target="_blank" href="{{url('dashboard/order/customerorders/'.$order->user_id)}}">@lang("site.Customer's  Orders")</a></span>
                                            </h2>
                                        </td>
                                        @if (auth()->guard('admin')->user()->hasRole('cook'))
                                            <td>
                                                <h2 class="table-avatar">
                                                    {{-- <a href="profile.html" class="avatar avatar-sm mr-2"><img class="avatar-img rounded-circle" src="" alt="User Image"></a> --}}
                                                    <span  id="cook_{{ $order->id }}">{{ $cook_name }}</span>
                                                </h2>
                                            </td>
                                        @endif

                                        @php

                                            $orderEntry=$order->orderEntry;
                                            foreach ($orderEntry as $item){
                                                $qty+=$item['quantity'];
                                            }
                                        @endphp
                                        {{-- <td>
                                            <table class="sub_table">
                                                @foreach ($orderEntry as $item)
                                                    <td>
                                                    <h2 class="table-avatar">

                                                    <a href="#" class="avatar avatar-sm mr-2"><img class="avatar-img rounded-circle" src="{{ asset('backend/img/dishes/'.explode('__',$item['orderEntry']['images'] )[0]) }}" ></a>
                                                    <a href="#">{{ $item['orderEntry']['name'] }}</a>
                                                    </h2>
                                                    </td>
                                                    </td>
                                                    <td>{{ $item['orderEntry']['section'] }}</td>
                                                    <td>{{ $item['orderEntry']['price'] }} EGP</td>
                                                    <td>{{ $item['orderEntry']['qty'] }}</td>
                                                    <td>{{ $item['orderEntry']['price'] * $item['qty']}}</td>
                                                    <td>{{ $item['orderEntry']['notes'] }}</td>
                                                    <td >
                                                        <div class="actions">
                                                            <a class="link_delete" data-toggle="modal" href="#delete_modal" data-url=""  class="btn btn-sm btn-danger" style="color: #f00">
                                                                <i class="fe fe-trash"></i> Delete
                                                            </a>
                                                        </div>
                                                    </td>
                                                @endforeach
                                            </table>
                                            </td> --}}
                                            <td>{{ $qty }}</td>
                                            @if (auth()->guard('admin')->user()->hasRole('cook'))

                                                {{-- <td>
                                                    @if (in_array($order['address'], $address))
                                                    @lang('site.old')
                                                        @else
                                                        @lang('site.new')
                                                    @endif
                                                    @php
                                                        array_push($address,$order['address']);
                                                    @endphp
                                                </td> --}}
                                                <td>{{ $order['total_price'] }} {{$currency}}</td>

                                                {{-- <td id="payment_method_{{ $order->id }}">
                                                    @if ($order['payment_method'] == 0 )
                                                    @lang('site.Cash')
                                                    @elseif($order['payment_method'] == 1)
                                                    @lang('site.Credit')
                                                    @endif
                                                </td> --}}

                                                <td id="payment_status_{{ $order->id }}">
                                                    @if ($order['paid'] == 0 )
                                                    @lang('site.Pending')
                                                    {{-- @elseif($order['payment_status'] == 1) --}}
                                                    @else
                                                    paid
                                                    @endif
                                                    </td>
                                                    {{-- <td id="delivery_status_{{ $order->id }}">@lang('site.out of delivery')</td> --}}
                                                    <td>
                                                        @if ($order['status'] == 1 )
                                                        @lang('site.Pending')
                                                        @elseif($order['status'] == 2)
                                                        @lang('site.Accepted')
                                                        @elseif($order['status'] == 3)
                                                        @lang('site.In progress')
                                                        @elseif($order['status'] == 4)
                                                        @lang('site.Shipped')
                                                        @elseif($order['status'] == 5)
                                                        @lang('site.Delivered')
                                                        @elseif($order['status'] == 6)
                                                        @lang('site.Completed')
                                                        @elseif($order['status'] == 0)
                                                        @lang('site.Rejected')
                                                        @endif
                                                    </td>
                                                    {{-- <td>
                                                        @php
                                                            $customer_service=\App\Models\User::whereHas('roles',function($q){
                                                                $q->where('name','admin');
                                                            })->where('city_id',$cook->city_id)->first();
                                                        @endphp
                                                        {{ $customer_service->name }}

                                                        @if (auth()->guard('admin')->user()->roles[0]->name == 'admin')
                                                            <button id="{{  $order->id }}" class="assign">@lang('site.unAssigned')</button>
                                                        @else
                                                            {{ $order->assign ?? ' unAssigned yet' }}
                                                        @endif
                                                </td> --}}
                                            @endif
                                            <td id="order_status_{{ $order->id }}">
                                                {{-- @if ($order['payment_method'] == 0 )
                                                @lang('site.Cache')
                                                @elseif($order['payment_method'] == 1)
                                                @lang('site.Mada')
                                                @elseif($order['payment_method'] == 2)
                                                @lang('site.Apple Pay')
                                                @endif --}}
                                                @if ($order['delivery_type'] == 0 )
                                                @lang('site.Pickup')
                                                @elseif($order['delivery_type'] == 1)
                                                @lang('site.Delivery')

                                                @endif
                                            </td>
                                            {{-- <td></td> --}}
                                            {{-- <td>
                                                @if (auth()->guard('admin')->user()->hasRole('cook') && $order->status == -1)
                                                    <a id="{{ $order->id }}" class="link_show bg-success-light mr-2"  href="{{ url( Config::get('app.locale') .'/dashboard/order/accept/'.$order->id) }}" >
                                                        <i class="fa fa-check" aria-hidden="true"></i>
                                                        Accept
                                                    </a>
                                                    <a id="{{ $order->id }}" class="link_show bg-danger-light mr-2" href="{{ url( Config::get('app.locale') .'/dashboard/order/reject/'.$order->id) }}" >
                                                        <i class="fa fa-ban" aria-hidden="true"></i>
                                                        Reject
                                                    </a>
                                                @endif
                                                @if (!auth()->guard('admin')->user()->hasRole('cook'))

                                                <a id="{{ $order->id }}" class="link_show bg-success-light mr-2" data-toggle="modal"  href="#edit_orders_details" >
                                                    <i class="fe fe-eye"></i> View Details
                                                </a>
                                                @endif

                                            </td>
                                            @php
                                                $qty=0;
                                            @endphp --}}

                                            {{-- <td>
                                                <div class="actions">
                                                    @if ($order['status'] == 2)
                                                        <a style="color: rgb(118, 196, 83)"  onclick="status({{ $order->id }})" href="#"  id="{{  $order->id }}">In Progress</a>
                                                    @elseif($order['status'] == 3)
                                                    <a style="color: rgb(118, 196, 83)"  onclick="status({{ $order->id }})" href="#"  id="{{  $order->id }}">Shipped</a>
                                                    @endif
                                                    @if ($order['status'] == 0)
                                                    <a class="link_delete" id="{{ $order->id }}" data-toggle="modal" href="#delete_modal" data-url=""  class="btn btn-sm btn-danger" style="color: #f00"> Delete</a>
                                                    @endif

                                                </div>
                                            </td> --}}
                                            {{-- <td>{{ $order->admin_note ?? ' ' }}</td> --}}
                                            <td>
                                                <div class="actions">
                                                    <a id="{{ $order->id }}" class="link_show bg-success-light mr-2 mb-1" data-toggle="modal"  href="#edit_orders_details" >
                                                        <i class="fe fe-eye"></i> @lang('site.View Details')
                                                    </a>
                                                    <a  class="action-btn link_reject" style="color: #f00" id="{{ $order->id }}" data-toggle="modal" href="#reject_modal" data-url=""   > reject </a>
                                                    <a style="color: rgb(118, 196, 83); border: 1px solid;  padding: 2px 5px; "  onclick="status({{ $order->id }})" href="#"  id="{{  $order->id }}">@lang('site.Accept')</a>
                                                </div>
                                            </td>

                                    </tr>

                                @endforeach
                            @endisset

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- /Recent Orders -->
        <div class="row" >

            <div class="col-md-12 d-flex">

                <!-- Recent Orders -->
                <div class="card card-table flex-fill">
                    <div class="card-header">
                        <h4 class="card-title">@lang('site.orders List')</h4>

                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-hover table-center mb-0">
                                <thead>
                                    <tr>
                                        <th>@lang('site.Order Number')</th>
                                        <th>@lang('site.Order Time')</th>
                                        <th>@lang('site.Customer Name')</th>
                                        @if (auth()->guard('admin')->user()->hasRole('cook'))
                                            <th>Branch Name</th>
                                        @endif

                                        {{-- <th>
                                            <table>
                                                <tr>
                                                    <th>Dish Name</th>
                                                    <th>Dish Section</th>
                                                    <th>Dish price</th>
                                                    <th>Amount</th>
                                                </tr>
                                            </table>
                                        </th> --}}
                                        <th>@lang('site.Quantity')</th>
                                        {{-- @if (auth()->guard('admin')->user()->hasRole('cook'))
                                            <th>@lang('site.Location')</th>
                                        @endif --}}
                                        @if (auth()->guard('admin')->user()->hasRole('cook'))
                                            <th>@lang('site.Total Price')</th>
                                            {{-- <th>@lang('site.Payment Method')</th> --}}
                                            <th>@lang('site.Payment Status')</th>
                                            {{-- <th>@lang('site.Delivery Status')</th> --}}
                                            <th>@lang('site.Notes')</th>
                                            <th>@lang('site.Order Status')</th>
                                            {{-- <th>@lang('site.Customer service')</th> --}}
                                        @endif
                                        {{-- <th>Operations service</th> --}}
                                        <th>@lang('site.Delivery Type')</th>

                                        <th>@lang('site.Status')</th>
                                        @if (auth()->guard('admin')->user()->hasRole('cook'))
                                            <th>@lang('site.Admin Note')</th>
                                        @endif

                                        <th>@lang('site.Action')</th>
                                    </tr>
                                </thead>
                                <tbody>
                                   @isset($orders)
                                   @php
                                         $qty=0;
                                         $address=[];
                                   @endphp
                                    @foreach ($orders as $index=>$order)
                                    @php
                                        $cook=\App\Models\User::where('id',$order->cook_id)->first();
                                        $cook_name=$cook->name;
                                    @endphp
                                    <tr>
                                        <td>{{ $index+1 }}</td>
                                        <td>{{date('h:i A', strtotime($order->time ))  }}</td>
                                            <td>
                                                <h2 class="table-avatar">
                                                    {{-- <a href="profile.html" class="avatar avatar-sm mr-2"><img class="avatar-img rounded-circle" src="" alt="User Image"></a> --}}
                                                    <a href="#" id="customer_{{ $order->id }}">{{ $order->users->name }}  <a class="past-order" target="_blank" href="{{url('dashboard/order/customerorders/'.$order->user_id)}}">@lang("site.Customer's  Orders")</a></a>
                                                </h2>
                                            </td>
                                        @if (auth()->guard('admin')->user()->hasRole('cook'))

                                        <td>
                                            <h2 class="table-avatar">
                                                {{-- <a href="profile.html" class="avatar avatar-sm mr-2"><img class="avatar-img rounded-circle" src="" alt="User Image"></a> --}}
                                                <a href="#"  id="cook_{{ $order->id }}">{{ $cook_name }}</a>
                                            </h2>
                                        </td>
                                        @endif

                                        @php

                                            $orderEntry=$order->orderEntry;
                                            foreach ($orderEntry as $item){
                                                $qty+=$item['quantity'];
                                            }

                                        @endphp
                                         {{-- <td>
                                             <table class="sub_table">
                                                @foreach ($orderEntry as $item)
                                                    <td>
                                                    <h2 class="table-avatar">

                                                    <a href="#" class="avatar avatar-sm mr-2"><img class="avatar-img rounded-circle" src="{{ asset('backend/img/dishes/'.explode('__',$item['orderEntry']['images'] )[0]) }}" ></a>
                                                    <a href="#">{{ $item['orderEntry']['name'] }}</a>
                                                    </h2>
                                                    </td>
                                                    </td>
                                                    <td>{{ $item['orderEntry']['section'] }}</td>
                                                    <td>{{ $item['orderEntry']['price'] }} EGP</td>
                                                    <td>{{ $item['orderEntry']['qty'] }}</td>
                                                    <td>{{ $item['orderEntry']['price'] * $item['qty']}}</td>
                                                    <td>{{ $item['orderEntry']['notes'] }}</td>
                                                    <td >
                                                        <div class="actions">
                                                            <a class="link_delete" data-toggle="modal" href="#delete_modal" data-url=""  class="btn btn-sm btn-danger" style="color: #f00">
                                                                <i class="fe fe-trash"></i> Delete
                                                            </a>
                                                        </div>
                                                    </td>
                                                 @endforeach
                                             </table>
                                            </td> --}}
                                            <td>{{ $qty }}</td>
                                            @if (auth()->guard('admin')->user()->hasRole('cook'))

                                                {{-- <td>
                                                    @if (in_array($order['address'], $address))
                                                    @lang('site.old')
                                                        @else
                                                        @lang('site.new')
                                                    @endif
                                                    @php
                                                        array_push($address,$order['address']);
                                                    @endphp
                                                </td> --}}


                                             <td>{{ $order['total_price'] }} {{ $currency }}</td>
                                             {{-- <td id="payment_method_{{ $order->id }}">
                                                @if ($order['payment_method'] == 0 )
                                                @lang('site.Cash')
                                                @elseif($order['payment_method'] == 1)
                                                @lang('site.Credit')
                                                @endif
                                             </td> --}}
                                             <td id="payment_status_{{ $order->id }}">
                                                @if ($order['payment_status'] == 0 )
                                                @lang('site.Pending')
                                                {{-- @elseif($order['payment_status'] == 1) --}}
                                                @else
                                                paid
                                                @endif
                                            </td>
                                             {{-- <td id="delivery_status_{{ $order->id }}">@lang('site.out of delivery')</td> --}}
                                             <td>{{ $order->note ?? ' ' }}</td>

                                             <td>
                                                 @if ($order['status'] == 1 )
                                                 @lang('site.Pending')
                                                 @elseif($order['status'] == 2)
                                                 @lang('site.Accepted')
                                                 @elseif($order['status'] == 3)
                                                 @lang('site.In progress')
                                                 @elseif($order['status'] == 4)
                                                 @lang('site.Shipped')
                                                 @elseif($order['status'] == 5)
                                                 @lang('site.Delivered')
                                                 @elseif($order['status'] == 6)
                                                 @lang('site.Completed')
                                                 @elseif($order['status'] == 0)
                                                 @lang('site.Rejected')
                                                 @elseif($order['status'] == 9)
                                                 @lang('site.Cancelled')
                                                 @endif
                                             </td>
                                             {{-- <td>
                                                @php
                                                    $customer_service=\App\Models\User::whereHas('roles',function($q){
                                                        $q->where('name','admin');
                                                    })->where('city_id',$cook->city_id)->first();
                                                @endphp
                                                {{ $customer_service->name }}
                                                @if (auth()->guard('admin')->user()->roles[0]->name == 'admin')
                                                    @if ( $order->assign == auth()->guard('admin')->user()->name)
                                                    @lang('site.Assigned')
                                                    @else
                                                    <button id="{{  $order->id }}" class="assign">@lang('site.unAssigned')</button>
                                                    @endif

                                                @else
                                                    {{ $order->assign ?? ' unAssigned yet' }}
                                                @endif
                                             </td> --}}
                                             @endif
                                             {{-- <td id="order_status_{{ $order->id }}">
                                                @if ($order['payment_method'] == 0 )
                                                @lang('site.Cache')
                                                @elseif($order['payment_method'] == 1)
                                                @lang('site.Mada')
                                                @elseif($order['payment_method'] == 2)
                                                @lang('site.Apple Pay')
                                                @endif

                                             </td> --}}
                                               <td id="order_status_{{ $order->id }}">
                                                @if ($order['delivery_type'] == 0 )
                                                @lang('site.Pickup')
                                                @elseif($order['delivery_type'] == 1)
                                                @lang('site.Delivery')

                                                @endif

                                             </td>

                                             <td id="{{$order->logista_order_id ?? ''}}" status="{{$order['status']}}">
                                                @if ($order['status'] == 1 )
                                                @lang('site.Pending')
                                                @elseif($order['status'] == 2)
                                                @lang('site.Accepted')
                                                @elseif($order['status'] == 3)
                                                @lang('site.In progress')
                                                @elseif($order['status'] == 4)
                                                @lang('site.Shipped')
                                                @elseif($order['status'] == 5)
                                                @lang('site.Delivered')
                                                @elseif($order['status'] == 6)
                                                @lang('site.Completed')
                                                @elseif($order['status'] == 0)
                                                @lang('site.Rejected')
                                                @elseif($order['status'] == 9)
                                                @lang('site.Cancelled')
                                                @endif

                                            </td>
                                            @if (auth()->guard('admin')->user()->hasRole('cook'))
                                                <td>{{ $order->admin_note ?? ' ' }}</td>
                                            @endif
                                            <td>
                                                {{-- @if (auth()->guard('admin')->user()->hasRole('cook') && $order->status == -1)
                                                    <a id="{{ $order->id }}" class="link_show bg-success-light mr-2"  href="{{ url( Config::get('app.locale') .'/dashboard/order/accept/'.$order->id) }}" >
                                                        <i class="fa fa-check" aria-hidden="true"></i>
                                                        Accept
                                                    </a>
                                                    <a id="{{ $order->id }}" class="link_show bg-danger-light mr-2" href="{{ url( Config::get('app.locale') .'/dashboard/order/reject/'.$order->id) }}" >
                                                        <i class="fa fa-ban" aria-hidden="true"></i>
                                                        Reject
                                                    </a>
                                                @endif --}}


                                                <div class="actions">
                                                    <a id="{{ $order->id }}" class="link_show bg-success-light mr-2 mb-1" data-toggle="modal"  href="#edit_orders_details" >
                                                        <i class="fe fe-eye"></i> @lang('site.View Details')
                                                    </a>
                                                    @if (auth()->guard('admin')->user()->hasRole('cook'))
                                                        <a id="{{ $order->id }}" class="link_update bg-success-light mr-2" data-toggle="modal"  href="#edit_order_details{{ $order->id }}" >
                                                            <i class="fe fe-pencil"></i> @lang('site.Edit')
                                                        </a>

                                                    @endif
                                                    @if ($order['status'] == 2)
                                                        <a style="color: rgb(118, 196, 83);border: 1px solid;  padding: 2px 5px;"  onclick="status({{ $order->id }})" href="#"  id="{{  $order->id }}">@lang('site.In progress')</a>
                                                    @elseif($order['status'] == 3)
                                                    <a  style="color: rgb(118, 196, 83); border: 1px solid;  padding: 2px 5px; "  onclick="status({{ $order->id }})" href="#"  id="{{  $order->id }}">@lang('site.Shipped')</a>
                                                    @endif
                                                    @if ($order['status'] == 2 || $order['status'] == 3)
                                                        <a class="mx-1" style="color: rgb(230, 23, 40); border: 1px solid ;  padding: 2px 5px; "   href="#"  id="{{  $order->id }}"><span id="cancel_order">@lang('site.Cancel')</span></a>
                                                    @endif
                                                    @if ($order['status'] == 0 || $order['status'] == 9)
                                                    <a class="link_delete" id="{{ $order->id }}" data-toggle="modal" href="#delete_modal" data-url=""  class="btn btn-sm btn-danger" style="color: #f00; border: 1px solid;  padding: 2px 5px; "> @lang('site.Delete')</a>
                                                    @endif

                                                </div>


                                                {{-- @switch($order->status)
                                                    @case(0)
                                                        <span class="bg-danger-light">rejected</span>
                                                        @break
                                                    @case(1)
                                                        <span class="bg-success-light">accepted</span>
                                                        @break
                                                    @default

                                                @endswitch --}}

                                             </td>
                                             @php
                                                 $qty=0;
                                             @endphp

                                    </tr>

                                    <!-- Edit Details Modal -->
                                    <div class="modal fade" id="edit_order_details{{ $order->id }}" aria-hidden="true" role="dialog">
                                        <div class="modal-dialog modal-dialog-centered" role="document" >
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title">Edit Order</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <form id="formUpdate" method="post" action="{{ route('order.updatedata',$order->id) }}" enctype="multipart/form-data">
                                                        @csrf
                                                        @method('put')
                                                        <div class="row form-row">
                                                            <div class="col-12 col-sm-12">
                                                                <div class="form-group">
                                                                    <label>Total Price</label>
                                                                    <input type="text" name="total_price" class="form-control" value="{{$order->total_price }}">
                                                                </div>
                                                            </div>
                                                            <div class="col-12 col-sm-12">
                                                                <div class="form-group">
                                                                    <label>Admin Note</label>
                                                                    <input type="text" name="admin_note" class="form-control" value="{{$order->admin_note }}">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <button type="submit" class="btn btn-primary btn-block">Save Changes</button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /Edit Details Modal -->
                                    @endforeach
                                   @endisset

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- /Recent Orders -->

            </div>
        </div>


    </div>
</div>
<!-- /Page Wrapper -->
  <!-- view order Modal -->
  @if (auth()->guard('admin')->user()->hasRole('cook'))

  <div class="modal fade" id="edit_orders_details" aria-hidden="true" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document" >
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"> @lang('site.Order Details')</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body" id="view">

            </div>
        </div>
    </div>
</div>
@endif


<!-- Reject Modal -->
<div class="modal fade" id="reject_modal" aria-hidden="true" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document" >
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Reject</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-content p-2">
                    <h4 class="modal-title">Reject</h4>
                    <p class="mb-4">Are you sure want to Reject?</p>

                    <div class="text-center">
                        <form  id="formReject" action="" method="post" style="display: inline">
                            @csrf
                            @method('post')
                            <input type="text" name="id" id="hiddenid" hidden>
                            <input class="form-control mb-2" type="text" name="note" placeholder="please enter the reason">
                            <button class="btn btn-primary btn-delete" type="submit" class="btn btn-primary">Yes </button>
                        </form>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /Reject Modal -->

@endsection

@push('scripts')



<script>

    // Enable pusher logging - don't include this in production
    Pusher.logToConsole = true;

    var pusher = new Pusher('e18a602fc8a8c1d5a939', {
      cluster: 'eu'
    });

    var channel = pusher.subscribe('my-channel');
    channel.bind('new-order', function(data) {
      location.reload();
    });
    </script>

    <script>
    const dbRef = firebase.database();
    const tasks=dbRef.ref('users/aVuMsFcc72VzayNmpBAeBr851uA2/tasks').on("value", (snapshot) => {

        snapshot.forEach((taskSnapshot) => {

            var value =taskSnapshot.val();
            var key =taskSnapshot.key;
            var order_status=document.getElementById(key).getAttribute("status") ;
            console.log(order_status != 5 && value.status == 4 ,order_status ,  value.status , 'lllllll')
                if(order_status != 5 && value.status == 4){

                    // console.log(value.status,order_status);
                    $.ajax({
                        type: 'GET',
                        url: '/api/order/delivered/'+key,
                        success:function(data){
                            location.reload();
                            // alert(document.getElementById(key).innerHTML)
                            // document.getElementById(key).innerHTML= <?php echo __('site.Delivered'); ?>;
                        }
                    });
                }

        });



        // location.reload();

    });

    // console.log(tasks,'llllllllllllllllllll');
    </script>
    <script>
        // $('.modal').hide();

        @if (count($errors) > 0)
                $('#Add_orders').modal('show');
        @endif


        $('.assign').on('click',function(){

            var order=$(this).attr('id');
            $.ajax({
                type: 'GET',
                url: '/{{ Config::get('app.locale') }}/dashboard/order/assign/'+order,

                success:function(data){


                }

            });
            $(this).text('Assigned');
            $(this).prop("disabled", true);

        });

//         $('#edit_orders_details').on('hidden.bs.modal', function () {
//  location.reload();
// })
$('.link_show').on('click',function(){
            $('#view').html('');
            $('#body').html('');
            var order_id=$(this).attr('id');
            var customer=$('#customer_'+order_id).html()
            var cook=$('#cook_'+order_id).html()
            // var payment_method=$('#payment_method_'+order_id).html()
            // var payment_status=$('#payment_status_'+order_id).html()
            // var delivery_status=$('#delivery_status_'+order_id).html()
            var order_status=$('#order_status_'+order_id).html()
            $.ajax(
                {url: '/{{ Config::get('app.locale') }}/dashboard/order/orderdetails/'+order_id,
                 success: function(result){
                    var data= result.order.orderEntry;
                    var details='<div class="row form-row">'+
                                    '<div class="col-12 col-sm-12">'+
                                        '<div class="form-group">'+
                                            '<label>Number : '+result.order.id+'</label>'+
                                        '</div>'+
                                    '</div>'+
                                    '<div class="col-12 col-sm-12">'+
                                        '<div class="form-group">'+
                                            '<label>Customer Name : '+customer+'</label>'+
                                        '</div>'+
                                    '</div>'+
                                    '<div class="col-12 col-sm-12">'+
                                        '<div class="form-group">'+
                                            '<label>Cook Name : '+cook+'</label>'+
                                        '</div>'+
                                    '</div>'+
                                    '<div class="col-12 col-sm-12">'+
                                        '<div class="form-group">'+
                                            '<table class="table table-borderd table-hover">'+
                                                '<thead>'+
                                                    '<tr>'+
                                                        '<th>Dish Name</th>'+
                                                        // '<th>Dish Section</th>'+
                                                        '<th>Dish price</th>'+
                                                        '<th>Amount</th>'+
                                                    '</tr>'+
                                                '</thead>'+
                                                '<tbody id="body">'+

                                                '</tbody>'+
                                            '</table>'+

                                            '<table class="table table-borderd table-hover">'+
                                                '<thead>'+
                                                    '<tr>'+
                                                        '<th>Addon Name</th>'+
                                                        // '<th>Dish Section</th>'+
                                                        '<th>Addon price</th>'+
                                                        // '<th>Amount</th>'+
                                                    '</tr>'+
                                                '</thead>'+
                                                '<tbody id="body_addon">'+

                                                '</tbody>'+
                                            '</table>'+


                                        '</div>'+
                                    '</div>'+
                                    '<div class="col-12 col-sm-12">'+
                                        '<div class="form-group">'+
                                            '<label>Location : '+result.order.address+'</label>'+
                                        '</div>'+
                                    '</div>'+
                                    '<div class="col-12 col-sm-12">'+
                                        '<div class="form-group">'+
                                            '<label>Total Price : '+result.order.total_price+'</label>'+
                                        '</div>'+
                                    '</div>'+

                                    // '<div class="col-12 col-sm-12">'+
                                    //     '<div class="form-group">'+
                                    //         '<label>Delivery Status : '+delivery_status+'</label>'+
                                    //     '</div>'+
                                    // '</div>'+
                                    '<div class="col-12 col-sm-12">'+
                                        '<div class="form-group">'+
                                            '<label>Order Status : '+order_status+'</label>'+
                                        '</div>'+
                                    '</div>'+
                                '</div>';
                    $('#view').append(details);


                      for (let i = 0; i < data.length; i++) {


                        for (let j = 0; j < data[i].addons.length; j++) {

                            var body_Addon= '<tr>'+
                                    '<td>'+data[i].addons[j].add+'</td>'+
                                    // '<td>'+data[i]['orderEntry']['section']+'</td>'+
                                    '<td>'+data[i].addons[j].add_price+'</td>'+
                                    // '<td>'+data[i].addons[j].qty+'</td>'+

                                '</tr>';
                            $('#body_addon').append(body_Addon)  ;

                        }


                       var body= '<tr>'+
                            '<td>'+data[i].name+'</td>'+
                            // '<td>'+data[i]['orderEntry']['section']+'</td>'+
                            '<td>'+data[i].price+'</td>'+
                            '<td>'+data[i].quantity+'</td>'+

                        '</tr>';
                    $('#body').append(body)  ;


                      }


                }
            });
        });





        function status(order_id){


                $.ajax({
                    type: 'GET',
                    url: '/{{ Config::get('app.locale') }}/dashboard/order/'+order_id,

                    success:function(data){
                        //    document.getElementById(order_id).innerHTML= data.status;

                        if (data.status == 2) {
                            $('.alert-success').text('Order Accepted ')
                        }else if (data.status == 3){
                            $('.alert-success').text('Order In Progressive ')
                        }
                        $('.alert-success').css('display','block');

                        setTimeout(function() {
                            $('.alert').fadeOut('fast');
                            location.reload();
                        }, 2000);
                    },
                    error:function(data){
                    }
                });
                // location.reload();
        };

        $('#cancel_order').click(function(e){
            e.preventDefault();
            var order_id =$(this).parent().attr('id');
            $.ajax({
                    type: 'GET',
                    url: '/{{ Config::get('app.locale') }}/dashboard/order/cancel/'+order_id,

                    success:function(data){
                        location.reload();
                       document.getElementById(order_id).innerHTML= data.status;
                    if (data.status == 'cancelled') {
                        $('.alert-danger').css('display','block');
                        $('.alert-danger').text('Order Cancelled ');

                        setTimeout(function() {
                            $('.alert').fadeOut('fast');
                            location.reload();
                        }, 2000);
                    }
                    },
                    error:function(data){
                    }
                });
        });



        $('.link_reject').on('click',function(){
             var order=$(this).attr('id');
             $('#hiddenid').val(order);
            $('#formReject').attr('action','/{{ Config::get('app.locale') }}/dashboard/order/reject')

        });

        // function reject(order_id){

        //         $.ajax({
        //             type: 'GET',
        //             url: '/{{ Config::get('app.locale') }}/dashboard/order/reject/'+order_id,

        //             success:function(data){
        //             //    document.getElementById(order_id).innerHTML= data.status;
        //             if (data.status == 0) {
        //                 $('.alert-danger').css('display','block');
        //                 $('.alert-danger').text('Order Rejected ');

        //                 setTimeout(function() {
        //                     $('.alert').fadeOut('fast');
        //                     location.reload();
        //                 }, 2000);
        //             }
        //             },
        //             error:function(data){
        //             }
        //         });
        //     };


        // function loadlink(){
        //     $('#page-wrapper').load('/{{ Config::get('app.locale') }}/dashboard/order',function () {
        //         $(this).unwrap();
        //     });
        // }

        // loadlink(); // This will run on page load
        // setInterval(function(){
        //     loadlink() // this will run after every 5 seconds
        // }, 5000);

//         setTimeout(function(){
//    window.location.reload(1);
// }, 5000);



    </script>
@endpush
