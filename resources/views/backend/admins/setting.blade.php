@extends('backend.layouts.app')


@section('content')

<!-- Page Wrapper -->
<div class="page-wrapper">
    <div class="content container-fluid">

        <!-- Page Header -->
        <div class="page-header">
            <div class="row">
                <div class="col-sm-12">
                    <h3 class="page-title">General Settings</h3>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="javascript:(0)">Settings</a></li>
                        <li class="breadcrumb-item active">General Settings</li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- /Page Header -->

        <div class="row">

            <div class="col-12">

                <!-- General -->

                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">General</h4>
                        </div>
                        <div class="card-body">
                            <form action="{{ route('setting.store') }}" method="post">
                                @csrf
                                <div class="form-group">
                                    <label>Google Map API</label>
                                    <input name="googleapikey" type="text" class="form-control" value="{{ $sett->googleapikey ?? old('googleapikey') }}">
                                </div>

                                @foreach (config('translatable.locales') as $locale)
                                    <div class="form-group">
                                        <label for="terms">@lang('site.'.$locale.'.termsandconditions')</label>
                                        <textarea class="form-control" name="{{ $locale }}[terms]" id="" cols="30" rows="10">{{$sett->translate($locale)->terms ??  '' }}</textarea>
                                    </div>
                                @endforeach

                                <div class="form-group">
                                    <label>Initial Delivery Fees</label>
                                    <input name="fees" type="text" class="form-control" value="{{ $sett->fees ?? old('fees') }}">
                                </div>

                                <select name="currency">
                                    <option value="USD"{{ $sett->currency == "USD" ? 'selected' : ' ' }}>United States Dollars</option>
                                    <option value="EUR"{{ $sett->currency == "EUR" ? 'selected' : ' ' }}>Euro</option>
                                    <option value="GBP"{{ $sett->currency == "GBP" ? 'selected' : ' ' }}>United Kingdom Pounds</option>
                                    <option value="DZD"{{ $sett->currency == "DZD" ? 'selected' : ' ' }}>Algeria Dinars</option>
                                    <option value="ARP"{{ $sett->currency == "ARP" ? 'selected' : ' ' }}>Argentina Pesos</option>
                                    <option value="AUD"{{ $sett->currency == "AUD" ? 'selected' : ' ' }}>Australia Dollars</option>
                                    <option value="ATS"{{ $sett->currency == "ATS" ? 'selected' : ' ' }}>Austria Schillings</option>
                                    <option value="BSD"{{ $sett->currency == "BSD" ? 'selected' : ' ' }}>Bahamas Dollars</option>
                                    <option value="BBD"{{ $sett->currency == "BBD" ? 'selected' : ' ' }}>Barbados Dollars</option>
                                    <option value="BEF"{{ $sett->currency == "BEF" ? 'selected' : ' ' }}>Belgium Francs</option>
                                    <option value="BMD"{{ $sett->currency == "BMD" ? 'selected' : ' ' }}>Bermuda Dollars</option>
                                    <option value="BRR"{{ $sett->currency == "BRR" ? 'selected' : ' ' }}>Brazil Real</option>
                                    <option value="BGL"{{ $sett->currency == "BGL" ? 'selected' : ' ' }}>Bulgaria Lev</option>
                                    <option value="CAD"{{ $sett->currency == "CAD" ? 'selected' : ' ' }}>Canada Dollars</option>
                                    <option value="CLP"{{ $sett->currency == "CLP" ? 'selected' : ' ' }}>Chile Pesos</option>
                                    <option value="CNY"{{ $sett->currency == "CNY" ? 'selected' : ' ' }}>China Yuan Renmimbi</option>
                                    <option value="CYP"{{ $sett->currency == "CYP" ? 'selected' : ' ' }}>Cyprus Pounds</option>
                                    <option value="CSK"{{ $sett->currency == "CSK" ? 'selected' : ' ' }}>Czech Republic Koruna</option>
                                    <option value="DKK"{{ $sett->currency == "DKK" ? 'selected' : ' ' }}>Denmark Kroner</option>
                                    <option value="NLG"{{ $sett->currency == "NLG" ? 'selected' : ' ' }}>Dutch Guilders</option>
                                    <option value="XCD"{{ $sett->currency == "XCD" ? 'selected' : ' ' }}>Eastern Caribbean Dollars</option>
                                    <option value="EGP"{{ $sett->currency == "EGP" ? 'selected' : ' ' }}>Egypt Pounds</option>
                                    <option value="FJD"{{ $sett->currency == "FJD" ? 'selected' : ' ' }}>Fiji Dollars</option>
                                    <option value="FIM"{{ $sett->currency == "FIM" ? 'selected' : ' ' }}>Finland Markka</option>
                                    <option value="FRF"{{ $sett->currency == "FRF" ? 'selected' : ' ' }}>France Francs</option>
                                    <option value="DEM"{{ $sett->currency == "DEM" ? 'selected' : ' ' }}>Germany Deutsche Marks</option>
                                    <option value="XAU"{{ $sett->currency == "XAU" ? 'selected' : ' ' }}>Gold Ounces</option>
                                    <option value="GRD"{{ $sett->currency == "GRD" ? 'selected' : ' ' }}>Greece Drachmas</option>
                                    <option value="HKD"{{ $sett->currency == "HKD" ? 'selected' : ' ' }}>Hong Kong Dollars</option>
                                    <option value="HUF"{{ $sett->currency == "HUF" ? 'selected' : ' ' }}>Hungary Forint</option>
                                    <option value="ISK"{{ $sett->currency == "ISK" ? 'selected' : ' ' }}>Iceland Krona</option>
                                    <option value="INR"{{ $sett->currency == "INR" ? 'selected' : ' ' }}>India Rupees</option>
                                    <option value="IDR"{{ $sett->currency == "IDR" ? 'selected' : ' ' }}>Indonesia Rupiah</option>
                                    <option value="IEP"{{ $sett->currency == "IEP" ? 'selected' : ' ' }}>Ireland Punt</option>
                                    <option value="ILS"{{ $sett->currency == "ILS" ? 'selected' : ' ' }}>Israel New Shekels</option>
                                    <option value="ITL"{{ $sett->currency == "ITL" ? 'selected' : ' ' }}>Italy Lira</option>
                                    <option value="JMD"{{ $sett->currency == "JMD" ? 'selected' : ' ' }}>Jamaica Dollars</option>
                                    <option value="JPY"{{ $sett->currency == "JPY" ? 'selected' : ' ' }}>Japan Yen</option>
                                    <option value="JOD"{{ $sett->currency == "JOD" ? 'selected' : ' ' }}>Jordan Dinar</option>
                                    <option value="KRW"{{ $sett->currency == "KRW" ? 'selected' : ' ' }}>Korea (South) Won</option>
                                    <option value="LBP"{{ $sett->currency == "LBP" ? 'selected' : ' ' }}>Lebanon Pounds</option>
                                    <option value="LUF"{{ $sett->currency == "LUF" ? 'selected' : ' ' }}>Luxembourg Francs</option>
                                    <option value="MYR"{{ $sett->currency == "MYR" ? 'selected' : ' ' }}>Malaysia Ringgit</option>
                                    <option value="MXP"{{ $sett->currency == "MXP" ? 'selected' : ' ' }}>Mexico Pesos</option>
                                    <option value="NLG"{{ $sett->currency == "NLG" ? 'selected' : ' ' }}>Netherlands Guilders</option>
                                    <option value="NZD"{{ $sett->currency == "NZD" ? 'selected' : ' ' }}>New Zealand Dollars</option>
                                    <option value="NOK"{{ $sett->currency == "NOK" ? 'selected' : ' ' }}>Norway Kroner</option>
                                    <option value="PKR"{{ $sett->currency == "PKR" ? 'selected' : ' ' }}>Pakistan Rupees</option>
                                    <option value="XPD"{{ $sett->currency == "XPD" ? 'selected' : ' ' }}>Palladium Ounces</option>
                                    <option value="PHP"{{ $sett->currency == "PHP" ? 'selected' : ' ' }}>Philippines Pesos</option>
                                    <option value="XPT"{{ $sett->currency == "XPT" ? 'selected' : ' ' }}>Platinum Ounces</option>
                                    <option value="PLZ"{{ $sett->currency == "PLZ" ? 'selected' : ' ' }}>Poland Zloty</option>
                                    <option value="PTE"{{ $sett->currency == "PTE" ? 'selected' : ' ' }}>Portugal Escudo</option>
                                    <option value="ROL"{{ $sett->currency == "ROL" ? 'selected' : ' ' }}>Romania Leu</option>
                                    <option value="RUR"{{ $sett->currency == "RUR" ? 'selected' : ' ' }}>Russia Rubles</option>
                                    <option value="SAR"{{ $sett->currency == "SAR" ? 'selected' : ' ' }}>Saudi Arabia Riyal</option>
                                    <option value="XAG"{{ $sett->currency == "XAG" ? 'selected' : ' ' }}>Silver Ounces</option>
                                    <option value="SGD"{{ $sett->currency == "SGD" ? 'selected' : ' ' }}>Singapore Dollars</option>
                                    <option value="SKK"{{ $sett->currency == "SKK" ? 'selected' : ' ' }}>Slovakia Koruna</option>
                                    <option value="ZAR"{{ $sett->currency == "ZAR" ? 'selected' : ' ' }}>South Africa Rand</option>
                                    <option value="KRW"{{ $sett->currency == "KRW" ? 'selected' : ' ' }}>South Korea Won</option>
                                    <option value="ESP"{{ $sett->currency == "ESP" ? 'selected' : ' ' }}>Spain Pesetas</option>
                                    <option value="XDR"{{ $sett->currency == "XDR" ? 'selected' : ' ' }}>Special Drawing Right (IMF)</option>
                                    <option value="SDD"{{ $sett->currency == "SDD" ? 'selected' : ' ' }}>Sudan Dinar</option>
                                    <option value="SEK"{{ $sett->currency == "SEK" ? 'selected' : ' ' }}>Sweden Krona</option>
                                    <option value="CHF"{{ $sett->currency == "CHF" ? 'selected' : ' ' }}>Switzerland Francs</option>
                                    <option value="TWD"{{ $sett->currency == "TWD" ? 'selected' : ' ' }}>Taiwan Dollars</option>
                                    <option value="THB"{{ $sett->currency == "THB" ? 'selected' : ' ' }}>Thailand Baht</option>
                                    <option value="TTD"{{ $sett->currency == "TTD" ? 'selected' : ' ' }}>Trinidad and Tobago Dollars</option>
                                    <option value="TRL"{{ $sett->currency == "TRL" ? 'selected' : ' ' }}>Turkey Lira</option>
                                    <option value="VEB"{{ $sett->currency == "VEB" ? 'selected' : ' ' }}>Venezuela Bolivar</option>
                                    <option value="ZMK"{{ $sett->currency == "ZMK" ? 'selected' : ' ' }}>Zambia Kwacha</option>
                                    <option value="EUR"{{ $sett->currency == "EUR" ? 'selected' : ' ' }}>Euro</option>
                                    <option value="XCD"{{ $sett->currency == "XCD" ? 'selected' : ' ' }}>Eastern Caribbean Dollars</option>
                                    <option value="XDR"{{ $sett->currency == "XDR" ? 'selected' : ' ' }}>Special Drawing Right (IMF)</option>
                                    <option value="XAG"{{ $sett->currency == "XAG" ? 'selected' : ' ' }}>Silver Ounces</option>
                                    <option value="XAU"{{ $sett->currency == "XAU" ? 'selected' : ' ' }}>Gold Ounces</option>
                                    <option value="XPD"{{ $sett->currency == "XPD" ? 'selected' : ' ' }}>Palladium Ounces</option>
                                    <option value="XPT"{{ $sett->currency == "XPT" ? 'selected' : ' ' }}>Platinum Ounces</option>
                                </select>
                                {{-- <div class="form-group">
                                    <label>Website Logo</label>
                                    <input type="file" class="form-control">
                                    <small class="text-secondary">Recommended image size is <b>150px x 150px</b></small>
                                </div>
                                <div class="form-group mb-0">
                                    <label>Favicon</label>
                                    <input type="file" class="form-control">
                                    <small class="text-secondary">Recommended image size is <b>16px x 16px</b> or <b>32px x 32px</b></small><br>
                                    <small class="text-secondary">Accepted formats : only png and ico</small>
                                </div> --}}
                                <button class="btn btn-success" type="submit">Submit</button>
                            </form>
                        </div>
                    </div>

                <!-- /General -->

            </div>
        </div>

    </div>
</div>
<!-- /Page Wrapper -->

@endsection


@push('scripts')

@endpush
