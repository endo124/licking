@extends('frontend.layouts.app')

@section('content')
<!-- wrapper-->
<div id="wrapper">
    <!-- content-->
    <div class="content">
         <!--  section  -->
        <section class="gray-bg" id="sec3">
            <div class="container">
                <div class="section-title">
                    <h2> FAQ</h2>
                    <div class="section-subtitle">Popular Questions</div>
                    <span class="section-separator"></span>
                    <p>Morbi varius, nulla sit amet rutrum elementum, est elit finibus tellus, ut tristique elit risus at metus.</p>
                </div>
                <div class="post-container fl-wrap">
                    <div class="row">
                        <!-- blog content-->
                        <div class="col-md-3">
                            <div class="faq-nav help-bar scroll-init">
                                <ul class="no-list-style">
                                    <li>
                                        <a href="#fq1">
                                        <i class="fal fa-space-shuttle"></i>
                                        <span>Getting Started</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#fq2">
                                        <i class="fal fa-cart-arrow-down"></i>
                                        <span>Pricing Plans</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#fq3">
                                        <i class="fal fa-barcode-read"></i>
                                        <span>Sales Questions</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#fq4">
                                        <i class="fal fa-user-headset"></i>
                                        <span>Usage Guides</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <!-- blog conten end-->
                        <!-- blog sidebar -->
                        <div class="col-md-9">
                            <!-- faq-section -->
                            <div class="faq-title faq-title_first fl-wrap">Getting Started</div>
                            <div class="faq-section fl-wrap" id="fq1">
                                <!-- accordion-->
                                <div class="accordion">
                                    <a class="toggle act-accordion" href="#">Suggestions<span></span></a>
                                    <div class="accordion-inner visible">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas in pulvinar neque. Nulla finibus lobortis pulvinar. Donec a consectetur nulla. Nulla posuere sapien vitae lectus suscipit, et pulvinar nisi tincidunt. Aliquam erat volutpat. Curabitur convallis fringilla diam sed aliquam. Sed tempor iaculis massa faucibus feugiat. In fermentum facilisis massa, a consequat purus viverra.</p>
                                    </div>
                                    <a class="toggle" href="#">Reccomendations <span></span></a>
                                    <div class="accordion-inner">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas in pulvinar neque. Nulla finibus lobortis pulvinar. Donec a consectetur nulla. Nulla posuere sapien vitae lectus suscipit, et pulvinar nisi tincidunt. Aliquam erat volutpat. Curabitur convallis fringilla diam sed aliquam. Sed tempor iaculis massa faucibus feugiat. In fermentum facilisis massa, a consequat purus viverra.</p>
                                    </div>
                                    <a class="toggle" href="#"> Listing  <span></span></a>
                                    <div class="accordion-inner">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas in pulvinar neque. Nulla finibus lobortis pulvinar. Donec a consectetur nulla. Nulla posuere sapien vitae lectus suscipit, et pulvinar nisi tincidunt. Aliquam erat volutpat. Curabitur convallis fringilla diam sed aliquam. Sed tempor iaculis massa faucibus feugiat. In fermentum facilisis massa, a consequat purus viverra.</p>
                                    </div>
                                </div>
                                <!-- accordion end -->
                            </div>
                            <!-- faq-section end -->
                            <!-- faq-section -->
                            <div class="faq-title fl-wrap">Pricing Plans</div>
                            <div class="faq-section fl-wrap" id="fq2">
                                <!-- accordion-->
                                <div class="accordion">
                                    <a class="toggle" href="#">Suggestions<span></span></a>
                                    <div class="accordion-inner">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas in pulvinar neque. Nulla finibus lobortis pulvinar. Donec a consectetur nulla. Nulla posuere sapien vitae lectus suscipit, et pulvinar nisi tincidunt. Aliquam erat volutpat. Curabitur convallis fringilla diam sed aliquam. Sed tempor iaculis massa faucibus feugiat. In fermentum facilisis massa, a consequat purus viverra.</p>
                                    </div>
                                    <a class="toggle" href="#">Reccomendations <span></span></a>
                                    <div class="accordion-inner">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas in pulvinar neque. Nulla finibus lobortis pulvinar. Donec a consectetur nulla. Nulla posuere sapien vitae lectus suscipit, et pulvinar nisi tincidunt. Aliquam erat volutpat. Curabitur convallis fringilla diam sed aliquam. Sed tempor iaculis massa faucibus feugiat. In fermentum facilisis massa, a consequat purus viverra.</p>
                                    </div>
                                </div>
                                <!-- accordion end -->
                            </div>
                            <!-- faq-section end -->
                            <!-- faq-section -->
                            <div class="faq-title fl-wrap">Sales Questions</div>
                            <div class="faq-section fl-wrap" id="fq3">
                                <!-- accordion-->
                                <div class="accordion">
                                    <a class="toggle" href="#">Suggestions<span></span></a>
                                    <div class="accordion-inner">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas in pulvinar neque. Nulla finibus lobortis pulvinar. Donec a consectetur nulla. Nulla posuere sapien vitae lectus suscipit, et pulvinar nisi tincidunt. Aliquam erat volutpat. Curabitur convallis fringilla diam sed aliquam. Sed tempor iaculis massa faucibus feugiat. In fermentum facilisis massa, a consequat purus viverra.</p>
                                    </div>
                                    <a class="toggle" href="#">Reccomendations <span></span></a>
                                    <div class="accordion-inner">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas in pulvinar neque. Nulla finibus lobortis pulvinar. Donec a consectetur nulla. Nulla posuere sapien vitae lectus suscipit, et pulvinar nisi tincidunt. Aliquam erat volutpat. Curabitur convallis fringilla diam sed aliquam. Sed tempor iaculis massa faucibus feugiat. In fermentum facilisis massa, a consequat purus viverra.</p>
                                    </div>
                                    <a class="toggle" href="#"> Listing  <span></span></a>
                                    <div class="accordion-inner">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas in pulvinar neque. Nulla finibus lobortis pulvinar. Donec a consectetur nulla. Nulla posuere sapien vitae lectus suscipit, et pulvinar nisi tincidunt. Aliquam erat volutpat. Curabitur convallis fringilla diam sed aliquam. Sed tempor iaculis massa faucibus feugiat. In fermentum facilisis massa, a consequat purus viverra.</p>
                                    </div>
                                </div>
                                <!-- accordion end -->
                            </div>
                            <!-- faq-section end -->
                            <!-- faq-section -->
                            <div class="faq-title fl-wrap">Usage Guides</div>
                            <div class="faq-section fl-wrap" id="fq4">
                                <!-- accordion-->
                                <div class="accordion">
                                    <a class="toggle" href="#">Suggestions<span></span></a>
                                    <div class="accordion-inner">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas in pulvinar neque. Nulla finibus lobortis pulvinar. Donec a consectetur nulla. Nulla posuere sapien vitae lectus suscipit, et pulvinar nisi tincidunt. Aliquam erat volutpat. Curabitur convallis fringilla diam sed aliquam. Sed tempor iaculis massa faucibus feugiat. In fermentum facilisis massa, a consequat purus viverra.</p>
                                    </div>
                                    <a class="toggle" href="#">Reccomendations <span></span></a>
                                    <div class="accordion-inner">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas in pulvinar neque. Nulla finibus lobortis pulvinar. Donec a consectetur nulla. Nulla posuere sapien vitae lectus suscipit, et pulvinar nisi tincidunt. Aliquam erat volutpat. Curabitur convallis fringilla diam sed aliquam. Sed tempor iaculis massa faucibus feugiat. In fermentum facilisis massa, a consequat purus viverra.</p>
                                    </div>
                                    <a class="toggle" href="#"> Listing  <span></span></a>
                                    <div class="accordion-inner">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas in pulvinar neque. Nulla finibus lobortis pulvinar. Donec a consectetur nulla. Nulla posuere sapien vitae lectus suscipit, et pulvinar nisi tincidunt. Aliquam erat volutpat. Curabitur convallis fringilla diam sed aliquam. Sed tempor iaculis massa faucibus feugiat. In fermentum facilisis massa, a consequat purus viverra.</p>
                                    </div>
                                    <a class="toggle" href="#"> Listing  <span></span></a>
                                    <div class="accordion-inner">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas in pulvinar neque. Nulla finibus lobortis pulvinar. Donec a consectetur nulla. Nulla posuere sapien vitae lectus suscipit, et pulvinar nisi tincidunt. Aliquam erat volutpat. Curabitur convallis fringilla diam sed aliquam. Sed tempor iaculis massa faucibus feugiat. In fermentum facilisis massa, a consequat purus viverra.</p>
                                    </div>
                                </div>
                                <!-- accordion end -->
                            </div>
                            <!-- faq-section end -->
                            <div class="faq-links fl-wrap">
                                <h3 class="faq-links-title">Still Need Help ?</h3>
                                <span class="section-separator"></span>
                                <!-- post nav -->
                                <div class="post-nav-wrap fl-wrap">
                                    <a class="post-nav post-nav-prev" href="tel:+966541587134"><span class="post-nav-img"><img src="{{ asset('frontend/images/phone.jpg') }}" alt=""></span><span class="post-nav-text"><strong>Call us</strong> <br>+966 54 158 7134</span></a>
                                    <a class="post-nav post-nav-next"  href="mailto:info@thejetter.com" ><span class="post-nav-img"><img src="{{ asset('frontend/images/envelope.png') }}" alt=""></span><span class="post-nav-text"><strong>Write to us</strong><br>info@thejetter.com</span></a>
                                </div>
                                <!-- post nav end -->
                            </div>
                        </div>
                        <!-- blog sidebar end -->
                    </div>
                </div>
            </div>
        </section>
        <div class="limit-box fl-wrap"></div>
    </div>
    <!--content end-->
</div>
<!-- wrapper end-->
@endsection
