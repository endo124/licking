@extends('frontend.partials.profile.app')


        <!--  section  -->
        <section class="gray-bg main-dashboard-sec" id="sec1">
            <div class="container">
                @section('profile-content')
                <form action="{{ route('change-password') }}" method="post" >
                    @csrf
                    @method('put')
                  <!-- dashboard content-->
                  <div class="col-md-8 my-5">
                        <div class="dashboard-title   fl-wrap">
                            <h3>Change Password</h3>
                        </div>
                        <!-- profile-edit-container-->
                        <div class="profile-edit-container fl-wrap block_box">
                            <div class="custom-form">
                                @include('frontend.partials.errors')
                                <div class="pass-input-wrap fl-wrap">
                                    <label>Current Password</label>
                                    <input type="password" name="current_password" class="pass-input" placeholder="" value=""/>
                                    <span class="eye"><i class="far fa-eye" aria-hidden="true"></i> </span>
                                </div>
                                <div class="pass-input-wrap fl-wrap">
                                    <label>New Password</label>
                                    <input type="password" name="password" class="pass-input" placeholder="" value=""/>
                                    <span class="eye"><i class="far fa-eye" aria-hidden="true"></i> </span>
                                </div>
                                <div class="pass-input-wrap fl-wrap">
                                    <label>Confirm New Password</label>
                                    <input type="password" name="password_confirmation" class="pass-input" placeholder="" value=""/>
                                    <span class="eye"><i class="far fa-eye" aria-hidden="true"></i> </span>
                                </div>
                                <button type="submit" class="btn    color2-bg  float-btn">Save Changes<i class="fal fa-save"></i></button>
                            </div>
                        </div>
                        <!-- profile-edit-container end-->
                    </div>
                <!-- dashboard content end-->
                </form>

                @endsection
            </div>
        </section>
        <!--  section  end-->
        <div class="limit-box fl-wrap"></div>
    </div>
    <!--content end-->
</div>
<!-- wrapper end-->

