@extends('frontend.layouts.app')

@section('content')
<!-- wrapper-->
<div id="wrapper">
    <!-- content-->
    <div class="content">

         <!--  section  -->
         <section class="parallax-section single-par" data-scrollax-parent="true">
            <video autoplay muted loop id="myVideo">
                <source src="{{ asset('frontend/images/video_preview_h264 (1).mp4') }}" type="video/mp4">
              </video>
            {{-- <div class="bg par-elem "  data-bg="images/bg/1.jpg" data-scrollax="properties: { translateY: '30%' }"></div> --}}
            <div class="overlay op7"></div>
            <div class="container">
                <div class="section-title center-align big-title">
                    <h2><span>Help Center</span></h2>
                    <span class="section-separator"></span>
                    <div class="breadcrumbs fl-wrap"><a href="{{ route('home') }}">Home</a><span>Help Center</span></div>
                </div>
            </div>
            <div class="header-sec-link">
                <a href="#overview" class="custom-scroll-link"><i class="fal fa-angle-double-down"></i></a>
            </div>
        </section>
        <!--  section  end-->
        <!--section  -->
        <section    id="overview"  data-scrollax-parent="true">
            <div class="container">
                <div class="section-title">
                    <h2>How it works</h2>
                    <div class="section-subtitle">How it works </div>
                    <span class="section-separator"></span>
                    <p>Morbi varius, nulla sit amet rutrum elementum, est elit finibus tellus, ut tristique elit risus at metus.</p>
                </div>
                <div class="process-wrap fl-wrap">
                    <ul class="no-list-style">
                        <li>
                            <div class="process-item">
                                <span class="process-count">01 </span>
                                <div class="time-line-icon"><i class="fal fa-map-marker-alt"></i></div>
                                <h4> Find Interesting Restaurant</h4>
                                <p>Proin dapibus nisl ornare diam varius tempus. Aenean a quam luctus, finibus tellus ut, convallis eros sollicitudin turpis.</p>
                            </div>
                            <span class="pr-dec"></span>
                        </li>
                        <li>
                            <div class="process-item">
                                <span class="process-count">02</span>
                                <div class="time-line-icon"><i class="fas fa-hamburger"></i></div>
                                <h4> Make Order</h4>
                                <p>Faucibus ante, in porttitor tellus blandit et. Phasellus tincidunt metus lectus sollicitudin feugiat pharetra consectetur.</p>
                            </div>
                            <span class="pr-dec"></span>
                        </li>
                        <li>
                            <div class="process-item">
                                <span class="process-count">03</span>
                                <div class="time-line-icon"><i class="fas fa-motorcycle"></i></div>
                                <h4> Receive Order</h4>
                                <p>Maecenas pulvinar, risus in facilisis dignissim, quam nisi hendrerit nulla, id vestibulum metus nullam viverra porta.</p>
                            </div>
                        </li>
                    </ul>
                    <div class="process-end"><i class="fal fa-check"></i></div>
                </div>
            </div>
        </section>
        <!--section end-->
        <!--section  -->
        {{-- <section class="gradient-bg hidden-section" data-scrollax-parent="true">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <div class="colomn-text  pad-top-column-text fl-wrap">
                            <div class="colomn-text-title">
                                <h3>Our Video Tutorials</h3>
                                <p>In ut odio libero, at vulputate urna. Nulla tristique mi a massa convallis cursus. Nulla eu mi magna. Etiam suscipit commodo gravida. Lorem ipsum dolor sit amet, conse ctetuer adipiscing elit .</p>
                                <a href="#" class=" down-btn color3-bg"><i class="fab fa-vimeo-v"></i> Vimeo Chanel</a>
                                <a href="#" class=" down-btn color3-bg"><i class="fab fa-youtube"></i>  Youtube Chanel </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="list-single-main-media fl-wrap">
                            <img src="images/all/1.jpg" class="respimg" alt="">
                            <a href="https://vimeo.com/70851162" class="promo-link   image-popup"><i class="fal fa-video"></i><span>How Townhub Works</span></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="gradient-bg-figure" style="right:-30px;top:10px;"></div>
            <div class="gradient-bg-figure" style="left:-20px;bottom:30px;"></div>
            <div class="circle-wrap" style="left:270px;top:120px;" data-scrollax="properties: { translateY: '-200px' }">
                <div class="circle_bg-bal circle_bg-bal_small"></div>
            </div>
            <div class="circle-wrap" style="right:420px;bottom:-70px;" data-scrollax="properties: { translateY: '150px' }">
                <div class="circle_bg-bal circle_bg-bal_big"></div>
            </div>
            <div class="circle-wrap" style="left:420px;top:-70px;" data-scrollax="properties: { translateY: '100px' }">
                <div class="circle_bg-bal circle_bg-bal_big"></div>
            </div>
            <div class="circle-wrap" style="left:40%;bottom:-70px;"  >
                <div class="circle_bg-bal circle_bg-bal_middle"></div>
            </div>
            <div class="circle-wrap" style="right:40%;top:-10px;"  >
                <div class="circle_bg-bal circle_bg-bal_versmall" data-scrollax="properties: { translateY: '-350px' }"></div>
            </div>
            <div class="circle-wrap" style="right:55%;top:90px;"  >
                <div class="circle_bg-bal circle_bg-bal_versmall" data-scrollax="properties: { translateY: '-350px' }"></div>
            </div>
        </section> --}}
        <!--section end-->

    </div>
    <!--content end-->
</div>
<!-- wrapper end-->
@endsection
