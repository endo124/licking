<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8" />
            <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
            <meta name="theme-color" content="#f40000"/>	<link rel="profile" href="https://gmpg.org/xfn/11" />
                <script type="text/javascript">
                if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
                    var originalAddEventListener = EventTarget.prototype.addEventListener,
                        oldWidth = window.innerWidth;

                    EventTarget.prototype.addEventListener = function (eventName, eventHandler, useCapture) {
                        if (eventName === "resize") {
                            originalAddEventListener.call(this, eventName, function (event) {
                                if (oldWidth === window.innerWidth) {
                                    return;
                                }
                                else if (oldWidth !== window.innerWidth) {
                                    oldWidth = window.innerWidth;
                                }
                                if (eventHandler.handleEvent) {
                                    eventHandler.handleEvent.call(this, event);
                                }
                                else {
                                    eventHandler.call(this, event);
                                };
                            }, useCapture);
                        }
                        else {
                            originalAddEventListener.call(this, eventName, eventHandler, useCapture);
                        };
                    };
                };
            </script>
            <title>MERCH | The Licking</title>
    <script>window._wca = window._wca || [];</script>

    <!-- All In One SEO Pack 3.6.2ob_start_detected [-1,-1] -->
    <script type="application/ld+json" class="aioseop-schema">{"@context":"https://schema.org","@graph":[{"@type":"Organization","@id":"https://www.thelicking.com/#organization","url":"https://www.thelicking.com/","name":"The Licking","sameAs":[]},{"@type":"WebSite","@id":"https://www.thelicking.com/#website","url":"https://www.thelicking.com/","name":"The Licking","publisher":{"@id":"https://www.thelicking.com/#organization"},"potentialAction":{"@type":"SearchAction","target":"https://www.thelicking.com/?s={search_term_string}","query-input":"required name=search_term_string"}},{"@type":"WebPage","@id":"https://www.thelicking.com/merch/#webpage","url":"https://www.thelicking.com/merch/","inLanguage":"en","name":"MERCH","isPartOf":{"@id":"https://www.thelicking.com/#website"},"breadcrumb":{"@id":"https://www.thelicking.com/merch/#breadcrumblist"},"datePublished":"2020-10-12T17:06:43+00:00","dateModified":"2020-10-13T15:49:42+00:00"},{"@type":"BreadcrumbList","@id":"https://www.thelicking.com/merch/#breadcrumblist","itemListElement":[{"@type":"ListItem","position":1,"item":{"@type":"WebPage","@id":"https://www.thelicking.com/","url":"https://www.thelicking.com/","name":"The Licking"}},{"@type":"ListItem","position":2,"item":{"@type":"WebPage","@id":"https://www.thelicking.com/merch/","url":"https://www.thelicking.com/merch/","name":"MERCH"}}]}]}</script>
    {{-- <link rel="canonical" href="index.html" /> --}}
    <!-- All In One SEO Pack -->
    <link rel='dns-prefetch' href='https://stats.wp.com' />
    <link rel='dns-prefetch' href='https://fonts.googleapis.com' />
    <link rel='dns-prefetch' href='https://s.w.org' />
    <link rel='dns-prefetch' href='https://c0.wp.com' />
    <link rel='dns-prefetch' href='https://i0.wp.com' />
    <link rel='dns-prefetch' href='https://i1.wp.com' />
    <link rel='dns-prefetch' href='https://i2.wp.com' />
    {{-- <link rel="alternate" type="application/rss+xml" title="The Licking &raquo; Feed" href="../feed/index.rss" />
    <link rel="alternate" type="application/rss+xml" title="The Licking &raquo; Comments Feed" href="../comments/feed/index.rss" /> --}}
            <script type="text/javascript">
                window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.0\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.0\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/www.thelicking.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.5.3"}};
                !function(e,a,t){var r,n,o,i,p=a.createElement("canvas"),s=p.getContext&&p.getContext("2d");function c(e,t){var a=String.fromCharCode;s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,e),0,0);var r=p.toDataURL();return s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,t),0,0),r===p.toDataURL()}function l(e){if(!s||!s.fillText)return!1;switch(s.textBaseline="top",s.font="600 32px Arial",e){case"flag":return!c([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])&&(!c([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!c([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]));case"emoji":return!c([55357,56424,8205,55356,57212],[55357,56424,8203,55356,57212])}return!1}function d(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(i=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},o=0;o<i.length;o++)t.supports[i[o]]=l(i[o]),t.supports.everything=t.supports.everything&&t.supports[i[o]],"flag"!==i[o]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[i[o]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(r=t.source||{}).concatemoji?d(r.concatemoji):r.wpemoji&&r.twemoji&&(d(r.twemoji),d(r.wpemoji)))}(window,document,window._wpemojiSettings);
            </script>
            <style type="text/css">img.wp-smiley,
    img.emoji {
        display: inline !important;
        border: none !important;
        box-shadow: none !important;
        height: 1em !important;
        width: 1em !important;
        margin: 0 .07em !important;
        vertical-align: -0.1em !important;
        background: none !important;
        padding: 0 !important;
    }
    </style>
        <link rel='stylesheet' id='wp-block-library-css'  href='https://c0.wp.com/c/5.5.3/wp-includes/css/dist/block-library/style.min.css' type='text/css' media='all' />
    <style id='wp-block-library-inline-css' type='text/css'>.has-text-align-justify{text-align:justify;}
    </style>
    <link rel='stylesheet' id='wp-block-library-theme-css'  href='https://c0.wp.com/c/5.5.3/wp-includes/css/dist/block-library/theme.min.css' type='text/css' media='all' />
    <link rel='stylesheet' id='wc-block-vendors-style-css'  href='https://c0.wp.com/p/woocommerce/4.5.2/packages/woocommerce-blocks/build/vendors-style.css' type='text/css' media='all' />
    <link rel='stylesheet' id='wc-block-style-css'  href='https://c0.wp.com/p/woocommerce/4.5.2/packages/woocommerce-blocks/build/style.css' type='text/css' media='all' />
    <link rel='stylesheet' id='rs-plugin-settings-css'  href='{{asset("wp-content/plugins/revslider/public/assets/css/rs6%EF%B9%96ver=6.2.22.css")}}' type='text/css' media='all' />
    <style id='rs-plugin-settings-inline-css' type='text/css'>#rs-demo-id {}
    </style>
    <style id='woocommerce-inline-inline-css' type='text/css'>.woocommerce form .form-row .required { visibility: visible; }
    </style>
    <link rel='stylesheet' id='the7-Defaults-css'  href='{{asset("wp-content/uploads/smile_fonts/Defaults/Defaults%EF%B9%96ver=5.5.3.css")}}' type='text/css' media='all' />
    <link rel='stylesheet' id='the7-icomoon-font-awesome-14x14-css'  href='{{asset("wp-content/uploads/smile_fonts/icomoon-font-awesome-14x14/icomoon-font-awesome-14x14%EF%B9%96ver=5.5.3.css")}}' type='text/css' media='all' />
    <link rel='stylesheet' id='js_composer_front-css'  href='{{asset("wp-content/uploads/js_composer/js_composer_front_custom%EF%B9%96ver=6.3.0.css")}}' type='text/css' media='all' />
    <link rel='stylesheet' id='dt-web-fonts-css'  href='https://fonts.googleapis.com/css?family=Roboto:300,400,500,600,700%7CRoboto+Condensed:300,400,600,700' type='text/css' media='all' />
    <link rel='stylesheet' id='dt-main-css'  href='{{asset("wp-content/themes/dt-the7/css/main.min%EF%B9%96ver=9.2.0.css")}}' type='text/css' media='all' />
    <style id='dt-main-inline-css' type='text/css'>body #load {
      display: block;
      height: 100%;
      overflow: hidden;
      position: fixed;
      width: 100%;
      z-index: 9901;
      opacity: 1;
      visibility: visible;
      transition: all .35s ease-out;
    }
    .load-wrap {
      width: 100%;
      height: 100%;
      background-position: center center;
      background-repeat: no-repeat;
      text-align: center;
    }
    .load-wrap > svg {
      position: absolute;
      top: 50%;
      left: 50%;
      transform: translate(-50%,-50%);
    }
    #load {
      background-color: #ffffff;
    }
    .uil-default rect:not(.bk) {
      fill: rgba(97,98,103,0.4);
    }
    .uil-ring > path {
      fill: rgba(97,98,103,0.4);
    }
    .ring-loader .circle {
      fill: rgba(97,98,103,0.4);
    }
    .ring-loader .moving-circle {
      fill: #616267;
    }
    .uil-hourglass .glass {
      stroke: #616267;
    }
    .uil-hourglass .sand {
      fill: rgba(97,98,103,0.4);
    }
    .spinner-loader .load-wrap {
      background-image: url("data:image/svg+xml,%3Csvg width='75px' height='75px' xmlns='http://www.w3.org/2000/svg' viewBox='0 0 100 100' preserveAspectRatio='xMidYMid' class='uil-default'%3E%3Crect x='0' y='0' width='100' height='100' fill='none' class='bk'%3E%3C/rect%3E%3Crect  x='46.5' y='40' width='7' height='20' rx='5' ry='5' fill='rgba%2897%2C98%2C103%2C0.4%29' transform='rotate(0 50 50) translate(0 -30)'%3E  %3Canimate attributeName='opacity' from='1' to='0' dur='1s' begin='0s' repeatCount='indefinite'/%3E%3C/rect%3E%3Crect  x='46.5' y='40' width='7' height='20' rx='5' ry='5' fill='rgba%2897%2C98%2C103%2C0.4%29' transform='rotate(30 50 50) translate(0 -30)'%3E  %3Canimate attributeName='opacity' from='1' to='0' dur='1s' begin='0.08333333333333333s' repeatCount='indefinite'/%3E%3C/rect%3E%3Crect  x='46.5' y='40' width='7' height='20' rx='5' ry='5' fill='rgba%2897%2C98%2C103%2C0.4%29' transform='rotate(60 50 50) translate(0 -30)'%3E  %3Canimate attributeName='opacity' from='1' to='0' dur='1s' begin='0.16666666666666666s' repeatCount='indefinite'/%3E%3C/rect%3E%3Crect  x='46.5' y='40' width='7' height='20' rx='5' ry='5' fill='rgba%2897%2C98%2C103%2C0.4%29' transform='rotate(90 50 50) translate(0 -30)'%3E  %3Canimate attributeName='opacity' from='1' to='0' dur='1s' begin='0.25s' repeatCount='indefinite'/%3E%3C/rect%3E%3Crect  x='46.5' y='40' width='7' height='20' rx='5' ry='5' fill='rgba%2897%2C98%2C103%2C0.4%29' transform='rotate(120 50 50) translate(0 -30)'%3E  %3Canimate attributeName='opacity' from='1' to='0' dur='1s' begin='0.3333333333333333s' repeatCount='indefinite'/%3E%3C/rect%3E%3Crect  x='46.5' y='40' width='7' height='20' rx='5' ry='5' fill='rgba%2897%2C98%2C103%2C0.4%29' transform='rotate(150 50 50) translate(0 -30)'%3E  %3Canimate attributeName='opacity' from='1' to='0' dur='1s' begin='0.4166666666666667s' repeatCount='indefinite'/%3E%3C/rect%3E%3Crect  x='46.5' y='40' width='7' height='20' rx='5' ry='5' fill='rgba%2897%2C98%2C103%2C0.4%29' transform='rotate(180 50 50) translate(0 -30)'%3E  %3Canimate attributeName='opacity' from='1' to='0' dur='1s' begin='0.5s' repeatCount='indefinite'/%3E%3C/rect%3E%3Crect  x='46.5' y='40' width='7' height='20' rx='5' ry='5' fill='rgba%2897%2C98%2C103%2C0.4%29' transform='rotate(210 50 50) translate(0 -30)'%3E  %3Canimate attributeName='opacity' from='1' to='0' dur='1s' begin='0.5833333333333334s' repeatCount='indefinite'/%3E%3C/rect%3E%3Crect  x='46.5' y='40' width='7' height='20' rx='5' ry='5' fill='rgba%2897%2C98%2C103%2C0.4%29' transform='rotate(240 50 50) translate(0 -30)'%3E  %3Canimate attributeName='opacity' from='1' to='0' dur='1s' begin='0.6666666666666666s' repeatCount='indefinite'/%3E%3C/rect%3E%3Crect  x='46.5' y='40' width='7' height='20' rx='5' ry='5' fill='rgba%2897%2C98%2C103%2C0.4%29' transform='rotate(270 50 50) translate(0 -30)'%3E  %3Canimate attributeName='opacity' from='1' to='0' dur='1s' begin='0.75s' repeatCount='indefinite'/%3E%3C/rect%3E%3Crect  x='46.5' y='40' width='7' height='20' rx='5' ry='5' fill='rgba%2897%2C98%2C103%2C0.4%29' transform='rotate(300 50 50) translate(0 -30)'%3E  %3Canimate attributeName='opacity' from='1' to='0' dur='1s' begin='0.8333333333333334s' repeatCount='indefinite'/%3E%3C/rect%3E%3Crect  x='46.5' y='40' width='7' height='20' rx='5' ry='5' fill='rgba%2897%2C98%2C103%2C0.4%29' transform='rotate(330 50 50) translate(0 -30)'%3E  %3Canimate attributeName='opacity' from='1' to='0' dur='1s' begin='0.9166666666666666s' repeatCount='indefinite'/%3E%3C/rect%3E%3C/svg%3E");
    }
    .ring-loader .load-wrap {
      background-image: url("data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 32 32' width='72' height='72' fill='rgba%2897%2C98%2C103%2C0.4%29'%3E   %3Cpath opacity='.25' d='M16 0 A16 16 0 0 0 16 32 A16 16 0 0 0 16 0 M16 4 A12 12 0 0 1 16 28 A12 12 0 0 1 16 4'/%3E   %3Cpath d='M16 0 A16 16 0 0 1 32 16 L28 16 A12 12 0 0 0 16 4z'%3E     %3CanimateTransform attributeName='transform' type='rotate' from='0 16 16' to='360 16 16' dur='0.8s' repeatCount='indefinite' /%3E   %3C/path%3E %3C/svg%3E");
    }
    .hourglass-loader .load-wrap {
      background-image: url("data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 32 32' width='72' height='72' fill='rgba%2897%2C98%2C103%2C0.4%29'%3E   %3Cpath transform='translate(2)' d='M0 12 V20 H4 V12z'%3E      %3Canimate attributeName='d' values='M0 12 V20 H4 V12z; M0 4 V28 H4 V4z; M0 12 V20 H4 V12z; M0 12 V20 H4 V12z' dur='1.2s' repeatCount='indefinite' begin='0' keytimes='0;.2;.5;1' keySplines='0.2 0.2 0.4 0.8;0.2 0.6 0.4 0.8;0.2 0.8 0.4 0.8' calcMode='spline'  /%3E   %3C/path%3E   %3Cpath transform='translate(8)' d='M0 12 V20 H4 V12z'%3E     %3Canimate attributeName='d' values='M0 12 V20 H4 V12z; M0 4 V28 H4 V4z; M0 12 V20 H4 V12z; M0 12 V20 H4 V12z' dur='1.2s' repeatCount='indefinite' begin='0.2' keytimes='0;.2;.5;1' keySplines='0.2 0.2 0.4 0.8;0.2 0.6 0.4 0.8;0.2 0.8 0.4 0.8' calcMode='spline'  /%3E   %3C/path%3E   %3Cpath transform='translate(14)' d='M0 12 V20 H4 V12z'%3E     %3Canimate attributeName='d' values='M0 12 V20 H4 V12z; M0 4 V28 H4 V4z; M0 12 V20 H4 V12z; M0 12 V20 H4 V12z' dur='1.2s' repeatCount='indefinite' begin='0.4' keytimes='0;.2;.5;1' keySplines='0.2 0.2 0.4 0.8;0.2 0.6 0.4 0.8;0.2 0.8 0.4 0.8' calcMode='spline' /%3E   %3C/path%3E   %3Cpath transform='translate(20)' d='M0 12 V20 H4 V12z'%3E     %3Canimate attributeName='d' values='M0 12 V20 H4 V12z; M0 4 V28 H4 V4z; M0 12 V20 H4 V12z; M0 12 V20 H4 V12z' dur='1.2s' repeatCount='indefinite' begin='0.6' keytimes='0;.2;.5;1' keySplines='0.2 0.2 0.4 0.8;0.2 0.6 0.4 0.8;0.2 0.8 0.4 0.8' calcMode='spline' /%3E   %3C/path%3E   %3Cpath transform='translate(26)' d='M0 12 V20 H4 V12z'%3E     %3Canimate attributeName='d' values='M0 12 V20 H4 V12z; M0 4 V28 H4 V4z; M0 12 V20 H4 V12z; M0 12 V20 H4 V12z' dur='1.2s' repeatCount='indefinite' begin='0.8' keytimes='0;.2;.5;1' keySplines='0.2 0.2 0.4 0.8;0.2 0.6 0.4 0.8;0.2 0.8 0.4 0.8' calcMode='spline' /%3E   %3C/path%3E %3C/svg%3E");
    }

    </style>
    <link rel='stylesheet' id='the7-font-css'  href='{{asset("wp-content/themes/dt-the7/fonts/icomoon-the7-font/icomoon-the7-font.min%EF%B9%96ver=9.2.0.css")}}' type='text/css' media='all' />
    <link rel='stylesheet' id='the7-awesome-fonts-css'  href='{{asset("wp-content/themes/dt-the7/fonts/FontAwesome/css/all.min%EF%B9%96ver=9.2.0.css")}}' type='text/css' media='all' />
    <link rel='stylesheet' id='the7-awesome-fonts-back-css'  href='{{asset("wp-content/themes/dt-the7/fonts/FontAwesome/back-compat.min%EF%B9%96ver=9.2.0.css")}}' type='text/css' media='all' />
    <link rel='stylesheet' id='the7-core-css'  href='{{asset("wp-content/plugins/dt-the7-core/assets/css/post-type.min%EF%B9%96ver=2.5.1.css")}}' type='text/css' media='all' />
    <link rel='stylesheet' id='dt-custom-css'  href='{{asset("wp-content/uploads/the7-css/custom%EF%B9%96ver=d5a604162a9b.css")}}' type='text/css' media='all' />
    <link rel='stylesheet' id='wc-dt-custom-css'  href='{{asset("wp-content/uploads/the7-css/compatibility/wc-dt-custom%EF%B9%96ver=d5a604162a9b.css")}}' type='text/css' media='all' />
    <link rel='stylesheet' id='dt-media-css'  href='{{asset("wp-content/uploads/the7-css/media%EF%B9%96ver=d5a604162a9b.css")}}' type='text/css' media='all' />
    <link rel='stylesheet' id='the7-mega-menu-css'  href='{{asset("wp-content/uploads/the7-css/mega-menu%EF%B9%96ver=d5a604162a9b.css")}}' type='text/css' media='all' />
    <link rel='stylesheet' id='the7-elements-albums-portfolio-css'  href='{{asset("wp-content/uploads/the7-css/the7-elements-albums-portfolio%EF%B9%96ver=d5a604162a9b.css")}}' type='text/css' media='all' />
    <link rel='stylesheet' id='the7-elements-css'  href='{{asset("wp-content/uploads/the7-css/post-type-dynamic%EF%B9%96ver=d5a604162a9b.css")}}' type='text/css' media='all' />
    <link rel='stylesheet' id='style-css'  href='{{asset("wp-content/themes/dt-the7/style%EF%B9%96ver=9.2.0.css")}}' type='text/css' media='all' />
    <link rel='stylesheet' id='jetpack_css-css'  href='https://c0.wp.com/p/jetpack/8.9.1/css/jetpack.css' type='text/css' media='all' />
    <script type='text/javascript' src='https://c0.wp.com/c/5.5.3/wp-includes/js/jquery/jquery.js' id='jquery-core-js'></script>
    <script type='text/javascript' src='{{asset("wp-content/plugins/revslider/public/assets/js/rbtools.min%EF%B9%96ver=6.2.22.js")}}' id='tp-tools-js'></script>
    <script type='text/javascript' src='{{asset("wp-content/plugins/revslider/public/assets/js/rs6.min%EF%B9%96ver=6.2.22.js")}}' id='revmin-js'></script>
    <script type='text/javascript' src='https://c0.wp.com/p/woocommerce/4.5.2/assets/js/jquery-blockui/jquery.blockUI.min.js' id='jquery-blockui-js'></script>
    <script type='text/javascript' id='wc-add-to-cart-js-extra'>
    /* <![CDATA[ */
    var wc_add_to_cart_params = {"ajax_url":"\/wp-admin\/admin-ajax.php","wc_ajax_url":"\/?wc-ajax=%%endpoint%%","i18n_view_cart":"View cart","cart_url":"https:\/\/www.thelicking.com\/cart\/","is_cart":"","cart_redirect_after_add":"no"};
    /* ]]> */
    </script>
    <script type='text/javascript' src='https://c0.wp.com/p/woocommerce/4.5.2/assets/js/frontend/add-to-cart.min.js' id='wc-add-to-cart-js'></script>
    <script type='text/javascript' src='{{asset("wp-content/plugins/js_composer/assets/js/vendors/woocommerce-add-to-cart%EF%B9%96ver=6.3.0.js")}}' id='vc_woocommerce-add-to-cart-js-js'></script>
    <script async defer type='text/javascript' src='https://stats.wp.com/s-202116.js' id='woocommerce-analytics-js'></script>
    <script type='text/javascript' id='dt-above-fold-js-extra'>
    /* <![CDATA[ */
    var dtLocal = {"themeUrl":"https:\/\/www.thelicking.com\/wp-content\/themes\/dt-the7","passText":"To view this protected post, enter the password below:","moreButtonText":{"loading":"Loading...","loadMore":"Load more"},"postID":"2198","ajaxurl":"https:\/\/www.thelicking.com\/wp-admin\/admin-ajax.php","REST":{"baseUrl":"https:\/\/www.thelicking.com\/wp-json\/the7\/v1","endpoints":{"sendMail":"\/send-mail"}},"contactMessages":{"required":"One or more fields have an error. Please check and try again.","terms":"Please accept the privacy policy.","fillTheCaptchaError":"Please, fill the captcha."},"captchaSiteKey":"","ajaxNonce":"8b0027a7fd","pageData":{"type":"page","template":"page","layout":null},"themeSettings":{"smoothScroll":"off","lazyLoading":false,"accentColor":{"mode":"solid","color":"#f40000"},"desktopHeader":{"height":102},"ToggleCaptionEnabled":"disabled","ToggleCaption":"Navigation","floatingHeader":{"showAfter":150,"showMenu":true,"height":70,"logo":{"showLogo":true,"html":"<img class=\" preload-me\" src=\"https:\/\/www.thelicking.com\/wp-content\/uploads\/2020\/10\/thelickinglogonew250.png\" srcset=\"https:\/\/www.thelicking.com\/wp-content\/uploads\/2020\/10\/thelickinglogonew250.png 250w, https:\/\/www.thelicking.com\/wp-content\/uploads\/2020\/10\/thelickinglogonew250.png 250w\" width=\"250\" height=\"39\"   sizes=\"250px\" alt=\"The Licking\" \/>","url":"https:\/\/www.thelicking.com\/"}},"topLine":{"floatingTopLine":{"logo":{"showLogo":false,"html":""}}},"mobileHeader":{"firstSwitchPoint":1150,"secondSwitchPoint":990,"firstSwitchPointHeight":70,"secondSwitchPointHeight":70,"mobileToggleCaptionEnabled":"disabled","mobileToggleCaption":"Menu"},"stickyMobileHeaderFirstSwitch":{"logo":{"html":"<img class=\" preload-me\" src=\"https:\/\/www.thelicking.com\/wp-content\/uploads\/2020\/09\/LickingsmallLOGO50.png\" srcset=\"https:\/\/www.thelicking.com\/wp-content\/uploads\/2020\/09\/LickingsmallLOGO50.png 50w, https:\/\/www.thelicking.com\/wp-content\/uploads\/2020\/09\/LickingsmallLOGO50.png 50w\" width=\"50\" height=\"50\"   sizes=\"50px\" alt=\"The Licking\" \/>"}},"stickyMobileHeaderSecondSwitch":{"logo":{"html":"<img class=\" preload-me\" src=\"https:\/\/www.thelicking.com\/wp-content\/uploads\/2020\/09\/LickingsmallLOGO50.png\" srcset=\"https:\/\/www.thelicking.com\/wp-content\/uploads\/2020\/09\/LickingsmallLOGO50.png 50w, https:\/\/www.thelicking.com\/wp-content\/uploads\/2020\/09\/LickingsmallLOGO50.png 50w\" width=\"50\" height=\"50\"   sizes=\"50px\" alt=\"The Licking\" \/>"}},"content":{"textColor":"#0f0000","headerColor":"#333333"},"sidebar":{"switchPoint":990},"boxedWidth":"1340px","stripes":{"stripe1":{"textColor":"#72777d","headerColor":"#3c3e45"},"stripe2":{"textColor":"#f4f4f5","headerColor":"#f4f4f5"},"stripe3":{"textColor":"#ffffff","headerColor":"#ffffff"}}},"VCMobileScreenWidth":"778","wcCartFragmentHash":"7cd609e96583c7fced2baec63cf2df5e"};
    var dtShare = {"shareButtonText":{"facebook":"Share on Facebook","twitter":"Tweet","pinterest":"Pin it","linkedin":"Share on Linkedin","whatsapp":"Share on Whatsapp"},"overlayOpacity":"85"};
    /* ]]> */
    </script>
    <script type='text/javascript' src='{{asset("wp-content/themes/dt-the7/js/above-the-fold.min%EF%B9%96ver=9.2.0.js")}}' id='dt-above-fold-js'></script>
    <link rel="https://api.w.org/" href="../wp-json/index.json" /><link rel="alternate" type="application/json" href="../wp-json/wp/v2/pages/2198.json" /><link rel="EditURI" type="application/rsd+xml" title="RSD" href="../xmlrpc.php%EF%B9%96rsd.xml" />
    <link rel="wlwmanifest" type="application/wlwmanifest+xml" href="../wp-includes/wlwmanifest.xml" />
    <meta name="generator" content="WordPress 5.5.3" />
    <meta name="generator" content="WooCommerce 4.5.2" />
    <link rel='shortlink' href='index.html' />
    <link rel="alternate" type="application/json+oembed" href="../wp-json/oembed/1.0/embed%EF%B9%96url=https%EF%B9%95%EA%A4%B7%EA%A4%B7www.thelicking.com%EA%A4%B7merch%EA%A4%B7.json" />
    <link rel="alternate" type="text/xml+oembed" href="../wp-json/oembed/1.0/embed%EF%B9%96url=https%EF%B9%95%EA%A4%B7%EA%A4%B7www.thelicking.com%EA%A4%B7merch%EA%A4%B7&amp;format=xml.xml" />
    <style type='text/css'>img#wpstats{display:none}</style><meta property="og:site_name" content="The Licking" />
    <meta property="og:title" content="MERCH" />
    <meta property="og:url" content="https://www.thelicking.com/merch/" />
    <meta property="og:type" content="article" />
        <noscript><style>.woocommerce-product-gallery{ opacity: 1 !important; }</style></noscript>
        <meta name="generator" content="Powered by WPBakery Page Builder - drag and drop page builder for WordPress."/>
    <meta name="generator" content="Powered by Slider Revolution 6.2.22 - responsive, Mobile-Friendly Slider Plugin for WordPress with comfortable drag and drop interface." />
    <script type="text/javascript">
    document.addEventListener("DOMContentLoaded", function(event) {
        var load = document.getElementById("load");
        if(!load.classList.contains('loader-removed')){
            var removeLoading = setTimeout(function() {
                load.className += " loader-removed";
            }, 300);
        }
    });
    </script>
            <link rel="icon" href='{{asset("wp-content/uploads/2020/09/LickingsmallLOGO16.png")}}' type="image/png" sizes="16x16"/><link rel="icon" href='{{asset("wp-content/uploads/2020/09/LickingsmallLOGO32.png")}}' type="image/png" sizes="32x32"/><script type="text/javascript">function setREVStartSize(e){
                //window.requestAnimationFrame(function() {
                    window.RSIW = window.RSIW===undefined ? window.innerWidth : window.RSIW;
                    window.RSIH = window.RSIH===undefined ? window.innerHeight : window.RSIH;
                    try {
                        var pw = document.getElementById(e.c).parentNode.offsetWidth,
                            newh;
                        pw = pw===0 || isNaN(pw) ? window.RSIW : pw;
                        e.tabw = e.tabw===undefined ? 0 : parseInt(e.tabw);
                        e.thumbw = e.thumbw===undefined ? 0 : parseInt(e.thumbw);
                        e.tabh = e.tabh===undefined ? 0 : parseInt(e.tabh);
                        e.thumbh = e.thumbh===undefined ? 0 : parseInt(e.thumbh);
                        e.tabhide = e.tabhide===undefined ? 0 : parseInt(e.tabhide);
                        e.thumbhide = e.thumbhide===undefined ? 0 : parseInt(e.thumbhide);
                        e.mh = e.mh===undefined || e.mh=="" || e.mh==="auto" ? 0 : parseInt(e.mh,0);
                        if(e.layout==="fullscreen" || e.l==="fullscreen")
                            newh = Math.max(e.mh,window.RSIH);
                        else{
                            e.gw = Array.isArray(e.gw) ? e.gw : [e.gw];
                            for (var i in e.rl) if (e.gw[i]===undefined || e.gw[i]===0) e.gw[i] = e.gw[i-1];
                            e.gh = e.el===undefined || e.el==="" || (Array.isArray(e.el) && e.el.length==0)? e.gh : e.el;
                            e.gh = Array.isArray(e.gh) ? e.gh : [e.gh];
                            for (var i in e.rl) if (e.gh[i]===undefined || e.gh[i]===0) e.gh[i] = e.gh[i-1];

                            var nl = new Array(e.rl.length),
                                ix = 0,
                                sl;
                            e.tabw = e.tabhide>=pw ? 0 : e.tabw;
                            e.thumbw = e.thumbhide>=pw ? 0 : e.thumbw;
                            e.tabh = e.tabhide>=pw ? 0 : e.tabh;
                            e.thumbh = e.thumbhide>=pw ? 0 : e.thumbh;
                            for (var i in e.rl) nl[i] = e.rl[i]<window.RSIW ? 0 : e.rl[i];
                            sl = nl[0];
                            for (var i in nl) if (sl>nl[i] && nl[i]>0) { sl = nl[i]; ix=i;}
                            var m = pw>(e.gw[ix]+e.tabw+e.thumbw) ? 1 : (pw-(e.tabw+e.thumbw)) / (e.gw[ix]);
                            newh =  (e.gh[ix] * m) + (e.tabh + e.thumbh);
                        }
                        if(window.rs_init_css===undefined) window.rs_init_css = document.head.appendChild(document.createElement("style"));
                        document.getElementById(e.c).height = newh+"px";
                        window.rs_init_css.innerHTML += "#"+e.c+"_wrapper { height: "+newh+"px }";
                    } catch(e){
                        console.log("Failure at Presize of Slider:" + e)
                    }
                //});
              };</script>
            <style type="text/css" id="wp-custom-css">@media (max-width: 767px) {
    .vc_video-bg-container .vc_hidden-xs {    display: block !important;
    }
    }		</style>
            <noscript><style>.wpb_animate_when_almost_visible { opacity: 1; }</style></noscript>
            @stack('style')

        </head>
<body data-rsssl=1 class="home page-template page-template-template-microsite page-template-template-microsite-php page page-id-1353 wp-embed-responsive theme-dt-the7 the7-core-ver-2.5.1 woocommerce-no-js one-page-row title-off dt-responsive-on right-mobile-menu-close-icon ouside-menu-close-icon mobile-hamburger-close-bg-enable mobile-hamburger-close-bg-hover-enable  fade-medium-mobile-menu-close-icon fade-medium-menu-close-icon srcset-enabled btn-flat custom-btn-color custom-btn-hover-color phantom-fade phantom-disable-decoration phantom-main-logo-on floating-mobile-menu-icon top-header first-switch-logo-left first-switch-menu-right second-switch-logo-left second-switch-menu-right right-mobile-menu layzr-loading-on popup-message-style dt-fa-compatibility the7-ver-9.2.0 wpb-js-composer js-comp-ver-6.3.0 vc_responsive">
    <!--loader-->
    <div id="load" class="ring-loader">
        <div class="load-wrap"></div>
    </div>
    <div id="page">


        <!--loader end-->

        <!-- main start  -->
        {{-- <div  id="loaderDiv">
            <button onclick="topFunction()" id="myBtn" title="Go to top"><i class="fa fa-arrow-up"></i></button> --}}

            @include('frontend.partials.header')

            @yield('content')

            @include('frontend.partials.footer')

        {{-- </div> --}}



    </div>


    <script type="text/html" id="wpb-modifications"></script>
    <link href="https://fonts.googleapis.com/css?family=Roboto:400" rel="stylesheet" property="stylesheet" media="all" type="text/css">

    <script type="text/javascript">
        var c = document.body.className;
        c = c.replace(/woocommerce-no-js/, 'woocommerce-js');
        document.body.className = c;
    </script>
    <script type="text/javascript">
        if (typeof revslider_showDoubleJqueryError === "undefined") {
            function revslider_showDoubleJqueryError(sliderID) {
                var err = "<div class='rs_error_message_box'>";
                err += "<div class='rs_error_message_oops'>Oops...</div>";
                err += "<div class='rs_error_message_content'>";
                err += "You have some jquery.js library include that comes after the Slider Revolution files js inclusion.<br>";
                err += "To fix this, you can:<br>&nbsp;&nbsp;&nbsp; 1. Set 'Module General Options' -> 'Advanced' -> 'jQuery & OutPut Filters' -> 'Put JS to Body' to on";
                err += "<br>&nbsp;&nbsp;&nbsp; 2. Find the double jQuery.js inclusion and remove it";
                err += "</div>";
                err += "</div>";
                var slider = document.getElementById(sliderID);
                slider.innerHTML = err;
                slider.style.display = "block";
            }
        }
    </script>
    <link rel='stylesheet' id='vc_animate-css-css' href='wp-content/plugins/js_composer/assets/lib/bower/animate-css/animate.min%EF%B9%96ver=6.3.0.css' type='text/css' media='all' />
    <link rel='stylesheet' id='ult-background-style-css' href='wp-content/plugins/Ultimate_VC_Addons/assets/min-css/background-style.min%EF%B9%96ver=3.19.6.css' type='text/css' media='all' />
    <script type='text/javascript' src='wp-content/themes/dt-the7/js/main.min%EF%B9%96ver=9.2.0.js' id='dt-main-js'></script>
    <script type='text/javascript' src='https://c0.wp.com/p/jetpack/8.9.1/_inc/build/photon/photon.min.js' id='jetpack-photon-js'></script>
    <script type='text/javascript' src='https://c0.wp.com/p/woocommerce/4.5.2/assets/js/js-cookie/js.cookie.min.js' id='js-cookie-js'></script>
    <script type='text/javascript' id='woocommerce-js-extra'>
        /* <![CDATA[ */
        var woocommerce_params = {
            "ajax_url": "\/wp-admin\/admin-ajax.php",
            "wc_ajax_url": "\/?wc-ajax=%%endpoint%%"
        };
        /* ]]> */
    </script>
    <script type='text/javascript' src='https://c0.wp.com/p/woocommerce/4.5.2/assets/js/frontend/woocommerce.min.js' id='woocommerce-js'></script>
    <script type='text/javascript' id='wc-cart-fragments-js-extra'>
        /* <![CDATA[ */
        var wc_cart_fragments_params = {
            "ajax_url": "\/wp-admin\/admin-ajax.php",
            "wc_ajax_url": "\/?wc-ajax=%%endpoint%%",
            "cart_hash_key": "wc_cart_hash_63df7a177787068da70e85c1cd799018",
            "fragment_name": "wc_fragments_63df7a177787068da70e85c1cd799018",
            "request_timeout": "5000"
        };
        /* ]]> */
    </script>
    <script type='text/javascript' src='https://c0.wp.com/p/woocommerce/4.5.2/assets/js/frontend/cart-fragments.min.js' id='wc-cart-fragments-js'></script>
    <script type='text/javascript' id='wc-cart-fragments-js-after'>
        jQuery('body').bind('wc_fragments_refreshed', function() {
            var jetpackLazyImagesLoadEvent;
            try {
                jetpackLazyImagesLoadEvent = new Event('jetpack-lazy-images-load', {
                    bubbles: true,
                    cancelable: true
                });
            } catch (e) {
                jetpackLazyImagesLoadEvent = document.createEvent('Event')
                jetpackLazyImagesLoadEvent.initEvent('jetpack-lazy-images-load', true, true);
            }
            jQuery('body').get(0).dispatchEvent(jetpackLazyImagesLoadEvent);
        });
    </script>
    <script type='text/javascript' src='wp-content/plugins/dt-the7-core/assets/js/post-type.min%EF%B9%96ver=2.5.1.js")}}' id='the7-core-js'></script>
    <script type='text/javascript' src='https://c0.wp.com/c/5.5.3/wp-includes/js/wp-embed.min.js' id='wp-embed-js'></script>
    <script type='text/javascript' src='{{asset("wp-content/plugins/js_composer/assets/js/dist/js_composer_front.min%EF%B9%96ver=6.3.0.js")}}' id='wpb_composer_front_js-js'></script>
    <script type='text/javascript' src='{{asset("wp-content/plugins/js_composer/assets/lib/vc_waypoints/vc-waypoints.min%EF%B9%96ver=6.3.0.js")}}' id='vc_waypoints-js'></script>
    <script type='text/javascript' src='{{asset("wp-content/plugins/js_composer/assets/lib/bower/skrollr/dist/skrollr.min%EF%B9%96ver=6.3.0.js")}}' id='vc_jquery_skrollr_js-js'></script>
    <script type='text/javascript' src='{{asset("wp-content/plugins/Ultimate_VC_Addons/assets/min-js/jquery-appear.min%EF%B9%96ver=3.19.6.js")}}' id='ultimate-appear-js'></script>
    <script type='text/javascript' src='{{asset("wp-content/plugins/Ultimate_VC_Addons/assets/min-js/ultimate_bg.min%EF%B9%96ver=3.19.6.js")}}' id='ultimate-row-bg-js'></script>
    <script type='text/javascript' src='{{asset("wp-content/plugins/Ultimate_VC_Addons/assets/min-js/custom.min%EF%B9%96ver=3.19.6.js")}}' id='ultimate-custom-js'></script>
    <script type='text/javascript' src='https://stats.wp.com/e-202116.js' async='async' defer='defer'></script>
    <script type='text/javascript'>
        _stq = window._stq || [];
        _stq.push(['view', {
            v: 'ext',
            j: '1:8.9.1',
            blog: '183431069',
            post: '1353',
            tz: '0',
            srv: 'www.thelicking.com'
        }]);
        _stq.push(['clickTrackerInit', '183431069', '1353']);
    </script>

    <div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="pswp__bg"></div>
        <div class="pswp__scroll-wrap">
            <div class="pswp__container">
                <div class="pswp__item"></div>
                <div class="pswp__item"></div>
                <div class="pswp__item"></div>
            </div>
            <div class="pswp__ui pswp__ui--hidden">
                <div class="pswp__top-bar">
                    <div class="pswp__counter"></div>
                    <button class="pswp__button pswp__button--close" title="Close (Esc)" aria-label="Close (Esc)"></button>
                    <button class="pswp__button pswp__button--share" title="Share" aria-label="Share"></button>
                    <button class="pswp__button pswp__button--fs" title="Toggle fullscreen" aria-label="Toggle fullscreen"></button>
                    <button class="pswp__button pswp__button--zoom" title="Zoom in/out" aria-label="Zoom in/out"></button>
                    <div class="pswp__preloader">
                        <div class="pswp__preloader__icn">
                            <div class="pswp__preloader__cut">
                                <div class="pswp__preloader__donut"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
                    <div class="pswp__share-tooltip"></div>
                </div>
                <button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)" aria-label="Previous (arrow left)">
			</button>
                <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)" aria-label="Next (arrow right)">
			</button>
                <div class="pswp__caption">
                    <div class="pswp__caption__center"></div>
                </div>
            </div>
        </div>
    </div>

  @stack('scripts')

</body>
</html>
