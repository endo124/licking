@extends('frontend.partials.profile.app')


        <!--  section  -->
        <section class="gray-bg main-dashboard-sec" id="sec1">
            <div class="container">
                @section('profile-content')

                    <!-- dashboard content-->
                    <div class="col-md-9">
                        <div class="dashboard-title   fl-wrap">
                            <h3>Your Reviews </h3>
                        </div>
                        <!-- profile-edit-container-->
                        <div class="profile-edit-container fl-wrap block_box">
                            <!-- reviews-comments-item -->
                            <div class="reviews-comments-item">
                                <div class="review-comments-avatar">
                                    <img src="{{asset(Auth::guard('customer')->user()->images ??  'backend/img/user.jpeg')  }}" alt="">                                </div>
                                <div class="reviews-comments-item-text fl-wrap">
                                    <div class="reviews-comments-header fl-wrap">
                                        <h4><a href="#">Liza Rose</a></h4>
                                        <div class="review-score-user">
                                            <span class="review-score-user_item">4.2</span>
                                            <div class="listing-rating card-popup-rainingvis" data-starrating2="4"></div>
                                        </div>
                                    </div>
                                    <p>" Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. "</p>
                                    <div class="reviews-comments-item-footer fl-wrap">
                                        <div class="reviews-comments-item-date"><span><i class="far fa-calendar-check"></i>12 April 2018</span></div>
                                        <a href="#" class="rate-review"><i class="fal fa-reply"></i>  Reply </a>
                                    </div>
                                </div>
                            </div>
                            <!--reviews-comments-item end-->

                        </div>
                        <!-- profile-edit-container end-->
                        <div class="pagination">
                            <a href="#" class="prevposts-link" style="display: flex;align-items:center"><i class="fas fa-caret-left"></i><span>Prev</span></a>
                            <a href="#">1</a>
                            <a href="#" class="current-page">2</a>
                            <a href="#">3</a>
                            <a href="#">...</a>
                            <a href="#">7</a>
                            <a href="#" class="nextposts-link"  style="display: flex;align-items:center"><span>Next</span><i class="fas fa-caret-right"></i></a>
                        </div>
                    </div>
                    <!-- dashboard content end-->

                @endsection
            </div>
        </section>
        <!--  section  end-->
        <div class="limit-box fl-wrap"></div>
    </div>
    <!--content end-->
</div>
<!-- wrapper end-->

