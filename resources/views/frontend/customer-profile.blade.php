@extends('frontend.partials.profile.app')


        <!--  section  -->
        <section class="gray-bg main-dashboard-sec" id="sec1">
            <div class="container">

                @section('profile-content')

                     <!-- dashboard content-->
                     <form action="{{ route('profile.update',auth()->guard('customer')->user()->id) }}" method="post" enctype="multipart/form-data">
                         @csrf
                         @method('put')
                        <div class="col-md-9 my-5">


                            <div class="dashboard-title fl-wrap">
                                <h3>Your Profile</h3>
                            </div>
                            <!-- profile-edit-container-->
                            <div class="profile-edit-container fl-wrap block_box">
                                <div class="custom-form">
                                    @include('frontend.partials.errors')
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <label>Full Name <i class="fal fa-user"></i></label>
                                            <input type="text" name="name"  value="{{ auth()->guard('customer')->user()->name }}"/>
                                        </div>
                                        {{-- <div class="col-sm-6">
                                            <label>Second Name <i class="fal fa-user"></i></label>
                                            <input type="text"value=""/>
                                        </div> --}}
                                        <div class="col-sm-6">
                                            <label>Email Address<i class="far fa-envelope"></i>  </label>
                                            <input type="text" name="email" value="{{ auth()->guard('customer')->user()->email }}" />
                                        </div>
                                        <div class="col-sm-6">
                                            <label>Phone<i class="far fa-phone"></i>  </label>
                                            <input type="text" name="phone"  value="{{ auth()->guard('customer')->user()->phone }}"/>
                                        </div>
                                        {{-- <div class="col-sm-6">
                                            <label> Adress <i class="fas fa-map-marker"></i>  </label>
                                            <input type="text" placeholder="USA 27TH Brooklyn NY" value=""/>
                                        </div> --}}
                                        {{-- <div class="col-sm-6">
                                            <label> Website <i class="far fa-globe"></i>  </label>
                                            <input type="text" placeholder="themeforest.net" value=""/>
                                        </div> --}}
                                    </div>
                                    {{-- <label> Notes</label>
                                    <textarea cols="40" rows="3" placeholder="About Me" style="margin-bottom:20px;"></textarea> --}}
                                    <label>Change Avatar</label>
                                    <div class="photoUpload mt-5">
                                        <span><i class="fal fa-image"></i> <strong>Add Photos</strong></span>
                                        <input type="file" name="image"  class="upload">
                                    </div>
                                </div>
                            </div>
                            <!-- profile-edit-container end-->
                            {{-- <div class="dashboard-title dt-inbox fl-wrap">
                                <h3>Your  Socials</h3>
                            </div> --}}
                            <!-- profile-edit-container-->
                            <div class="profile-edit-container fl-wrap block_box">
                                {{-- <div class="custom-form">
                                    <label>Facebook <i class="fab fa-facebook"></i></label>
                                    <input type="text" placeholder="https://www.facebook.com/" value=""/>
                                    <label>Twitter<i class="fab fa-twitter"></i>  </label>
                                    <input type="text" placeholder="https://twitter.com/" value=""/>
                                    <label>Vkontakte<i class="fab fa-vk"></i>  </label>
                                    <input type="text" placeholder="https://vk.com" value=""/>
                                    <label> Instagram <i class="fab fa-instagram"></i>  </label>
                                    <input type="text" placeholder="https://www.instagram.com/" value=""/> --}}
                                    <button type="submit" class="btn    color2-bg  float-btn">Save Changes<i class="fal fa-save"></i></button>
                                {{-- </div> --}}
                            </div>
                            <!-- profile-edit-container end-->
                        </div>
                     </form>

                    <!-- dashboard content end-->
                @endsection

            </div>
        </section>
        <!--  section  end-->
        <div class="limit-box fl-wrap"></div>
    </div>
    <!--content end-->
</div>
<!-- wrapper end-->

