<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Dublicate(thelicking)</title>
  <!-- Owl Carousele -->
  <link rel="stylesheet" href="{{ asset('assets/css/OwlCarousel/dist/assets/owl.carousel.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/css/OwlCarousel/dist/assets/owl.theme.default.min.css') }}">
  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>


  <link rel="stylesheet" href="{{ asset('assets/css/thelicking.css') }}">
  <link type="text/css" rel="stylesheet" href="{{asset('frontend/css/reset.css')}}">
  <link type="text/css" rel="stylesheet" href="{{asset('frontend/css/plugins.css')}}">
  <link type="text/css" rel="stylesheet" href="{!!asset('frontend/css/style.css')!!}">
  <link type="text/css" rel="stylesheet" href="{{asset('frontend/css/color.css')}}">
  <style>
    #owl-demo .item{
      background: #3fbf79;
      padding: 30px 0px;
      margin: 10px;
      color: #FFF;
      -webkit-border-radius: 3px;
      -moz-border-radius: 3px;
      border-radius: 3px;
      text-align: center;
    }
    .customNavigation{
      text-align: center;
    }
    .customNavigation a{
      -webkit-user-select: none;
      -khtml-user-select: none;
      -moz-user-select: none;
      -ms-user-select: none;
      user-select: none;
      -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
    }

    #sections .scroll-to-fixed-fixed{
        top: 80px !important
    }
    #sections .team-box{
        width: 50%;
        display: flex;
        justify-content: space-between;
        align-items: center;
        padding: 0 0 0 5px;
        border: 1px solid #eee;
        margin-bottom: 5px;
        cursor:pointer

    }
    .modal-backdrop{
        z-index: 1 !important;
    }
    .modal {
      display: none; /* Hidden by default */
      position: fixed; /* Stay in place */
      z-index: 1; /* Sit on top */
      left: 0;
      top: 0;
      width: 100%; /* Full width */
      height: 100%; /* Full height */
      overflow: auto; /* Enable scroll if needed */
      background-color: rgb(0,0,0); /* Fallback color */
      background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
      margin-top: 70px !important
    }

    /* Modal Content/Box */
    .modal-content {
      background-color: #fefefe;
      margin: 15% auto; /* 15% from the top and centered */
      padding: 20px;
      border: 1px solid #888;
      width: 80%; /* Could be more or less, depending on screen size */
    }

    /* The Close Button */
    .close {
      color: #aaa;
      float: right;
      font-size: 28px;
      font-weight: bold;
    }

    .close:hover,
    .close:focus {
      color: black;
      text-decoration: none;
      cursor: pointer;
    }
    .dish-img{
        max-width: 150px;
        max-height: 160px;
    }
    @media screen (min-width: 700px) {
        .dish-img{
        width: 100px;
    }
    }
    @media screen  (min-width: 700px) {
        #sections .team-box{
        width: 100%;
        }


    }
    .no-list-style li a{
        padding-right: 0
    }

    .geodir-js-favorite_btn i{
        float: left;
        width: 20px !important;
        height: 21px !important;
        line-height: 24px !important;
        border-radius: 100%;
        background: #B32D2B;
        position: relative;
        z-index: 1 !important;
    }
    .geodir-js-favorite_btn{
        height: 22px !important;
        z-index: 0 !important;

    }
    .geodir-js-favorite_btn span{
        height: 100% !important;
        line-height: 22px !important;
        z-index: 0 !important;

    }
    .addons{
        max-height: 250px;
        overflow-y: scroll
    }


    .auth{

    }

    .owl-carousel{
        display: block !important
    }
    </style>

</head>


<body onload="loadFunction()" style="margin:0;">
    <!--loader-->
    <div id="loader"></div>

    <!--loader end-->

    <!-- main start  -->
    <div  id="loaderDiv">
        <button onclick="topFunction()" id="myBtn" title="Go to top"><i class="fa fa-arrow-up"></i></button>

        <!-- Start Header -->
        <div class="header">

            <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <div class="container-fluid">
                <a class="navbar-brand" href="#">
                    <img src="{{ asset('assets/img/thelickinglogonew250.png') }}" alt="The Licking">
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <!-- <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button> -->
                <div class="collapse navbar-collapse  justify-content-end" id="navbarSupportedContent">
                    <ul class="navbar-nav mb-2 mb-lg-0">
                    <li class="nav-item ml-3 mr-3">
                        <a class="nav-link  {{ Request::segment(1) == 'home'?  'active' : '' }}" aria-current="page" href="{{ url('/home') }}" > HOME </a>
                    </li>
                    <li class="nav-item ml-3 mr-3">
                        <a class="nav-link" href="#"> ORDER NOW </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link {{ Request::segment(1) == 'vendors'?  'active' : '' }}" href="{{ route('vendors.show',$cooks->id) }}"> MENU </a>
                    </li>
                    <li class="nav-items ml-3 mr-3">
                        <div class="dropdown">
                        <button class="dropbtn"> <a href="{{ url('/merch') }}"></a> MERCH </button>
                        <div class="dropdown-content">
                            <div class="Cart">
                            <a href="#">CART</a>
                            </div>
                            <div class="Checkout">
                            <a href="#">CHECKOUT</a>
                            </div>
                        </div>
                        </div>
                    </li>
                    </ul>
                </div>
                </div>
            </nav>
        </div>
        <!-- End of Header -->
        <!-- wrapper-->
        <div id="wrapper">
            <!-- content-->
            <div class="content">
                {{-- <section class="listing-hero-section hidden-section" data-scrollax-parent="true" id="sec1">
                    <div class="bg-parallax-wrap">
                        <div class="slideshow-container" data-scrollax="properties: { translateY: '300px' }" >
                            <div class="swiper-container">
                                <div class="swiper-wrapper">
                                    <div class="swiper-slide">
                                        <div class="ms-item_fs fl-wrap full-height">
                                            <div class="bg" data-bg="{{ url(asset('backend/img/cook/'.$cook->images)) }}"></div>
                                            <div class="overlay"></div>
                                        </div>
                                    </div>

                                    <div class="swiper-slide ">
                                        <div class="ms-item_fs fl-wrap full-height">
                                            <div class="bg" data-bg="{{ url(asset('backend/img/cook/'.$cook->images)) }}"></div>
                                            <div class="overlay"></div>
                                        </div>
                                    </div>

                                    <div class="swiper-slide">
                                        <div class="ms-item_fs fl-wrap full-height">
                                            <div class="bg" data-bg="{{ url(asset('backend/img/cook/'.$cook->images)) }}"></div>
                                            <div class="overlay"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="overlay"></div>
                    </div>



                    <div class="container">
                        <div class="list-single-header-item  fl-wrap">
                            <div class="row">

                            </div>
                        </div>
                        <div class="list-single-header_bottom fl-wrap">

                            <div class="geodir_status_date gsd_{{ $cook->availability == 1 ? 'open' : 'close' }}"><i class="fal fa-{{ $cook->availability == 1 ? 'unlock' : 'lock' }}"></i>{{ $cook->availability == 1 ? 'Open' : 'Close' }} Now</div>
                            <div class="list-single-stats">

                            </div>
                        </div>
                    </div>
                </section> --}}
                <!-- scroll-nav-wrapper-->
                {{-- <div class="scroll-nav-wrapper fl-wrap" style="z-index: 0">
                    <div class="container">
                        <div class="alert alert-danger auth" style="display: none" auth="{{ auth('customer')->user()->name ?? 'guest' }}">

                        </div>
                        <nav class="scroll-nav scroll-init">
                            <ul class="no-list-style">

                                <li><a class="act-scrlink" href="#sec1"><i class="fal fa-images"></i> Top</a></li>
                                <li><a href="#sections"><i class="fas fa-align-center"></i>Full Menu </a></li>

                                <li><a href="#sec3"><i class="fal fa-info"></i>Details</a></li>

                                <li><a href="#sec6"><i class="fal fa-comments-alt"></i>Reviews</a></li>
                            </ul>
                        </nav>
                        <div class="scroll-nav-wrapper-opt">
                            <a href="#" class="scroll-nav-wrapper-opt-btn"> <i class="fas fa-heart"></i> Save </a>
                            <a href="#" class="scroll-nav-wrapper-opt-btn showshare"> <i class="fas fa-share"></i> Share </a>
                            <div class="share-holder hid-share">
                                <div class="share-container  isShare"></div>
                            </div>

                        </div>
                    </div>
                </div> --}}
                <!-- scroll-nav-wrapper end-->
                <section class="gray-bg no-top-padding">
                    <div class="container">
                        <div class="breadcrumbs inline-breadcrumbs fl-wrap">
                            <a href="#">Home</a><a href="{{ route('vendors.index') }}">Restaurants</a><span>{{ $cook->name }}</span>
                        </div>
                        <div class="clearfix"></div>
                        <div class="row">
                            <!-- list-single-main-wrapper-col -->
                            <div class="col-md-8">
                                <!-- list-single-main-wrapper -->
                                <div class="list-single-main-wrapper fl-wrap">
                                    <div class="list-single-main-item fl-wrap block_box" id="sections">
                                        <div class="list-single-main-item-title">
                                            <h3>Full Menu</h3>
                                        </div>
                                        <div class="scroll-nav-wrapper fl-wrap" >
                                            <div class="container">

                                                <div class="list-single-main-item_content " >
                                                    <nav class="scroll-nav scroll-init" style="width: 100%">
                                                        <ul class="no-list-style owl-carousel owl-theme" id="owl-demo" >
                                                            <li><a class="act-scrlink" href="#sections">Top</a></li>
                                                            @foreach ($cook->sections as $section)
                                                                @php
                                                                    $from=(strtotime($section->available_from));
                                                                    $to=(strtotime($section->available_to));
                                                                    $current=((time()));


                                                                    if ($to < $from) {
                                                                    $to=strtotime('+1 day', $to);
                                                                    }
                                                                @endphp
                                                                {{-- @if ($current >= $from && $current <= $to) --}}
                                                                    <li class="mx-2"><a   href="#section{{ $section->id }}">{{ $section->name }}</a></li>
                                                                {{-- @endif --}}
                                                            @endforeach

                                                        </ul>
                                                    </nav>
                                                </div>
                                            </div>
                                        </div>
                                        @foreach ($cook->sections as $section)
                                            @php
                                                $dishes=\App\Models\Dish::where('section_id',$section->id)->get();
                                                $from=(strtotime($section->available_from));
                                                $to=(strtotime($section->available_to));
                                                $current=((time()));
                                                if ($to < $from) {
                                                                    $to=strtotime('+1 day', $to);
                                                }
                                            @endphp
                                                {{-- @dump( $from,$to,$current) --}}
                                                {{-- @if ($current >= $from && $current <= $to) --}}

                                                <div class="list-single-main-item fl-wrap block_box" id="section{{ $section->id }}">
                                                    <div class="list-single-main-item-title">
                                                        <h3>{{ $section->name }} </h3>
                                                        @if ($current >= $from && $current <= $to)
                                                        <div class="geodir_status_date gsd_open" style="z-index:0"><i class="fal fa-lock-open" style="z-index:0"></i>Open Now</div>

                                                        @else
                                                        <div class="geodir_status_date gsd_close" style="z-index:0"><i class="fal fa-lock-close" style="z-index:0"></i>Close Now</div>
                                                        @endif

                                                    </div>
                                                    <div class="list-single-main-item_content fl-wrap">
                                                        <div class="team-holder fl-wrap row">
                                                            @foreach ($dishes as $dish)
                                                            @php
                                                                $price=$dish->portions_price;
                                                                $min=$price[0];
                                                                foreach ($price as $pr) {
                                                                    if($pr !=null){
                                                                        if ($min > $pr) {
                                                                        $min =$pr;
                                                                        }
                                                                    }
                                                                }
                                                            @endphp
                                                            <!-- team-item -->
                                                            <div class="team-box col-md-6 col-sm-12 col-12 " style="height: 110px" >
                                                                <div class="row w-100" style="justify-content: space-between; height:100%">
                                                                    <div class="col-8 pt-2 dish" id="{{ $dish->id }}" >
                                                                        <h3 style="text-align:left">{{ $dish->name }}</h3>
                                                                        <p style="margin-top: 5px ;text-align:left">start from <span id="">{{ $min }}</span>  $</p>
                                                                        {!! $dish->calories  > 0 ? '<p  style="text-align:left">calories:'.$dish->calories  : ''    !!} </p>
                                                                    </div>

                                                                    <div class="col-4" style="padding:0;position: relative" style="background-image: url({{ asset('frontend/images/LOGO 3.png') }})">
                                                                        {{-- <div  title="{{ $dish->id }}" class="geodir-js-favorite_btn whishlist" style="overflow: hidden;position: absolute;top:initial;top:3;left:3"><i class="fal fa-heart" style="float: left;"></i><span>Save</span></div> --}}
                                                                        <div title="{{ $dish->id }}" id={{ $dish->id }} class="geodir-js-favorite_btn dish" style="overflow: hidden;position: absolute;top:3px;left:3px"><i class="fas fa-shopping-cart"></i><span>Save</span></div>
                                                                        <img class="dish-img w-100 h-100" src="{{asset('backend/img/dishes/'.$dish->images) }}"   alt="">
                                                                        {{-- <div title="{{ $dish->id }}" id={{ $dish->id }} class="geodir-js-favorite_btn dish" style="overflow: hidden;position: absolute;top:unset;left:unset;bottom:3;right:3"><i class="fas fa-shopping-cart"></i></div> --}}

                                                                    </div>
                                                                </div>

                                                            </div>
                                                            <!-- team-item  end-->
                                                            @endforeach
                                                            <!-- team-item -->


                                                        </div>
                                                    </div>
                                                </div>
                                                {{-- @endif --}}

                                        @endforeach
                                        <div class="modal fade" id="add_order" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="margin-top: 143px;z-index: 99999999;">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div id="addonsPrice" hidden></div>
                                                    <div id="regqty" hidden ></div>
                                                    <div id="dish" hidden></div>
                                                    <!-- Modal Header -->
                                                            <div class="modal-header "style="border:0">
                                                                <div class="right">
                                                                    <div class="flex" >
                                                                        <h5 class="modal-title dish-title text-left" style="font-size: 14px;"> </h5>

                                                                    </div>

                                                                    <div class="my-3">
                                                                        <img class="w-100 dish-image" src="" alt="">
                                                                    </div>
                                                                </div>
                                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                            </div>

                                                            <!-- Modal body -->
                                                    <div class="modal-body mx-3">

                                                        <form action="{{ url('dish/order') }}" method="post">
                                                            @csrf
                                                            <input id="available_count" type="text" value="5" hidden>
                                                            <div class="md-form">
                                                                <input name="dish_id"  type="text"  class="form-control validate" value="5" hidden>
                                                            </div>
                                                            <div class="PortionDetails">
                                                                <div class="row">
                                                                    {{-- <div class="d-flex justify-content-between align-items-center w-100  mb-2" style="padding: 10px 10px;">
                                                                        <span class="portion-details" id="portion-details-title">Portion Details</span>
                                                                        <span class="special-option-label">4 available</span>
                                                                    </div> --}}
                                                                    <div class="dish-detail__section dish-data-portion">
                                                                        <div class="dish-data-portion__container">

                                                                            <div class="d-flex justify-content-start align-items-center">
                                                                                <div class="dish-portion-image">
                                                                                    <img src="{{ asset('/backend/img/dishes/1605710362.jpg') }}" style="width: 50px">
                                                                                </div>
                                                                                <div class="dish-portion-description ">
                                                                                    <p class="mx-2 dish-data-portion_description  dish-description"></p>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            {{-- <hr> --}}
                                                            <div class="Portions" style="display: none">
                                                                <div class="row">
                                                                    <div class="d-flex justify-content-between align-items-center w-100  mb-2" style="padding: 10px 10px;">
                                                                        <span class="portion-details" id="portion-details-title">Size </span>
                                                                    </div>
                                                                    <div class="dish-detail__section dish-data-portion">
                                                                        @php
                                                                            $size=['small' , 'medium', 'large'];
                                                                        @endphp
                                                                        @for ($i = 0; $i < count($size); $i++)
                                                                            <div class="custom-control custom-radio" id="{{ $size[$i] }}" style="padding-left: 10px; ">
                                                                                <input onclick="changePrice(this)" type="radio" class="custom-control-input" id="{{ $i }}" name="addon_price" value="" >
                                                                                <label class="custom-control-label" for="{{ $i }}">
                                                                                    {{-- @if ($i==0)
                                                                                        <span id="allow_small"></span>
                                                                                        @elseif($i==1)
                                                                                        <span id="allow_medium"></span>
                                                                                        @else
                                                                                        <span id="allow_large"></span>
                                                                                    @endif --}}
                                                                                    <span id="allow_{{ $size[$i] }}"></span>

                                                                                </label>
                                                                            </div>
                                                                        @endfor
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            {{-- <hr> --}}


                                                            <div class="addons">



                                                            </div>
                                                            <textarea id="myTextarea" style="width: 100%;padding:3px;border: 1px solid #c2c1c1;" class="br-5 mt-3 mb-4 ng-untouched ng-pristine ng-valid" cols="30" name="notes" placeholder="Notes for Restaurant(i.e. sauce on the side, etc)" rows="3"></textarea>
                                                            {{-- <p class="doordash-logo-container">
                                                                <span>Delivery powered by our partner</span>
                                                                <img style="height: auto; max-width: 15%;" src="{{ asset('/backend/img/logista.jpg') }}">
                                                            </p> --}}
                                                            <!-- Modal footer -->
                                                            <div class="modal-footer">
                                                                <br>
                                                                <div class='main' style="margin:5px auto ">
                                                                    <a onclick="remove(this)" class='  btn-info' id="down" title='Down' style="display: inline;color:#fff;padding:3px"><i class="fa fa-minus-circle"></i></a>
                                                                    <input style="width: 30px;border: #fff;text-align: center;" class='counter disapled' id="qty" name="qty" type="text" placeholder="value..." value='1'  readonly/>
                                                                    <a onclick="add(this)" class=' btn-info' dish="50"  id="up" title='Up'  style="display: inline;color:#fff;padding:3px"><i class="fa fa-plus-circle"></i></a>
                                                                </div>
                                                                <a class="btn" onclick="checkmodalvallidation()" style="cursor: pointer;width: 100%; display: flex;justify-content: space-between; width: 100%; border: 0px;color:#fff; background-color: #fff; border-radius: 5px;" class="modal-footer portions-modal__footer-button">
                                                                    <div>
                                                                        <p  style="color:#fff;display:inline-block" id="total_price" class="select-portion__add-to-cart">  </p>
                                                                        <span> $</span>
                                                                    </div>

                                                                    <p style="color:#fff;"  class="select-portion__add-to-cart">Continue</p>
                                                                </a>
                                                            </div>
                                                        </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- list-single-main-item-->
                                    {{-- <div class="list-single-main-item fl-wrap block_box" id="sec2">
                                        <div class="list-single-main-item-title">
                                            <h3>Our Trainers</h3>
                                        </div>
                                        <div class="list-single-main-item_content fl-wrap">
                                            <div class="team-holder fl-wrap">
                                                <!-- team-item -->
                                                <div class="team-box">
                                                    <div class="team-photo">
                                                        <img src="images/team/1.jpg" alt="" class="respimg">
                                                    </div>
                                                    <div class="team-info fl-wrap">
                                                        <h3><a href="#">Alisa Gray</a></h3>
                                                        <h4>Crossfit / Fitness</h4>
                                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.  </p>
                                                        <div class="team-social">
                                                            <ul class="no-list-style">
                                                                <li><a href="#" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
                                                                <li><a href="#" target="_blank"><i class="fab fa-twitter"></i></a></li>
                                                                <li><a href="#" target="_blank"><i class="fab fa-instagram"></i></a></li>
                                                                <li><a href="#" target="_blank"><i class="fab fa-vk"></i></a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- team-item  end-->
                                                <!-- team-item -->
                                                <div class="team-box">
                                                    <div class="team-photo">
                                                        <img src="images/team/1.jpg" alt="" class="respimg">
                                                    </div>
                                                    <div class="team-info fl-wrap">
                                                        <h3><a href="#">Martin Gably</a></h3>
                                                        <h4>Weight room / Fitness </h4>
                                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.  </p>
                                                        <div class="team-social">
                                                            <ul class="no-list-style">
                                                                <li><a href="#" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
                                                                <li><a href="#" target="_blank"><i class="fab fa-twitter"></i></a></li>
                                                                <li><a href="#" target="_blank"><i class="fab fa-instagram"></i></a></li>
                                                                <li><a href="#" target="_blank"><i class="fab fa-vk"></i></a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- team-item end  -->
                                                <!-- team-item -->
                                                <div class="team-box">
                                                    <div class="team-photo">
                                                        <img src="images/team/1.jpg" alt="" class="respimg">
                                                    </div>
                                                    <div class="team-info fl-wrap">
                                                        <h3><a href="#">Taylor Roberts</a></h3>
                                                        <h4>TRX / Crossfit </h4>
                                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.  </p>
                                                        <div class="team-social">
                                                            <ul class="no-list-style">
                                                                <li><a href="#" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
                                                                <li><a href="#" target="_blank"><i class="fab fa-twitter"></i></a></li>
                                                                <li><a href="#" target="_blank"><i class="fab fa-instagram"></i></a></li>
                                                                <li><a href="#" target="_blank"><i class="fab fa-vk"></i></a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- team-item end  -->
                                            </div>
                                        </div>
                                    </div> --}}
                                    <!-- list-single-main-item end -->
                                    <!-- list-single-facts -->
                                    {{-- <div class="list-single-facts fl-wrap">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <!-- inline-facts -->
                                                <div class="inline-facts-wrap gradient-bg ">
                                                    <div class="inline-facts">
                                                        <i class="fal fa-smile-plus"></i>
                                                        <div class="milestone-counter">
                                                            <div class="stats animaper">
                                                                <div class="num" data-content="0" data-num="245">0</div>
                                                            </div>
                                                        </div>
                                                        <h6>New Visiters Every Week</h6>
                                                    </div>
                                                    <div class="stat-wave">
                                                        <svg viewbox="0 0 100 25">
                                                            <path fill="#fff" d="M0 30 V12 Q30 17 55 2 T100 11 V30z" />
                                                        </svg>
                                                    </div>
                                                </div>
                                                <!-- inline-facts end -->
                                            </div>
                                            <div class="col-md-4">
                                                <!-- inline-facts  -->
                                                <div class="inline-facts-wrap gradient-bg ">
                                                    <div class="inline-facts">
                                                        <i class="fal fa-users"></i>
                                                        <div class="milestone-counter">
                                                            <div class="stats animaper">
                                                                <div class="num" data-content="0" data-num="2557">0</div>
                                                            </div>
                                                        </div>
                                                        <h6>Happy customers every year</h6>
                                                    </div>
                                                    <div class="stat-wave">
                                                        <svg viewbox="0 0 100 25">
                                                            <path fill="#fff" d="M0 30 V12 Q30 17 55 12 T100 11 V30z" />
                                                        </svg>
                                                    </div>
                                                </div>
                                                <!-- inline-facts end -->
                                            </div>
                                            <div class="col-md-4">
                                                <!-- inline-facts  -->
                                                <div class="inline-facts-wrap gradient-bg ">
                                                    <div class="inline-facts">
                                                        <i class="fal fa-award"></i>
                                                        <div class="milestone-counter">
                                                            <div class="stats animaper">
                                                                <div class="num" data-content="0" data-num="25">0</div>
                                                            </div>
                                                        </div>
                                                        <h6>Won Awards</h6>
                                                    </div>
                                                    <div class="stat-wave">
                                                        <svg viewbox="0 0 100 25">
                                                            <path fill="#fff" d="M0 30 V12 Q30 12 55 5 T100 11 V30z" />
                                                        </svg>
                                                    </div>
                                                </div>
                                                <!-- inline-facts end -->
                                            </div>
                                        </div>
                                    </div> --}}
                                    <!-- list-single-facts end -->
                                    <!-- list-single-main-item -->
                                    <div class="list-single-main-item fl-wrap block_box" id="sec3">
                                        <div class="list-single-main-item-title">
                                            <h3>Description</h3>
                                        </div>
                                        <div class="list-single-main-item_content fl-wrap">
                                            <p>{{ $cook->info }}</p>
                                            {{-- <p>Praesent eros turpis, commodo vel justo at, pulvinar mollis eros. Mauris aliquet eu quam id ornare. Morbi ac quam enim. Cras vitae nulla condimentum, semper dolor non, faucibus dolor. Vivamus adipiscing eros quis orci fringilla, sed pretium lectus viverra. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Donec nec velit non odio aliquam suscipit. Sed non neque faucibus, condimentum lectus at, accumsan enim.   </p>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas in pulvinar neque. Nulla finibus lobortis pulvinar. Donec a consectetur nulla. Nulla posuere sapien vitae lectus suscipit, et pulvinar nisi tincidunt. Aliquam erat volutpat. Curabitur convallis fringilla diam sed aliquam. Sed tempor iaculis massa faucibus feugiat. In fermentum facilisis massa, a consequat purus viverra.</p> --}}
                                            {{-- <a href="#" class="btn color2-bg float-btn">Visit Website<i class="fal fa-chevron-right"></i></a> --}}
                                        </div>
                                    </div>

                                    <!-- list-single-main-item end -->
                                    <!-- list-single-main-item -->
                                    {{-- <div class="list-single-main-item fl-wrap block_box">
                                        <div class="list-single-main-item-title">
                                            <h3>Listing Features</h3>
                                        </div>
                                        <div class="list-single-main-item_content fl-wrap">
                                            <div class="listing-features fl-wrap">
                                                <ul class="no-list-style">
                                                    <li><a href="#"><i class="fa fa-rocket"></i> Elevator in building</a></li>
                                                    <li><a href="#"><i class="fa fa-wifi"></i> Free Wi Fi</a></li>
                                                    <li><a href="#"><i class="fa fa-motorcycle"></i> Free Parking</a></li>
                                                    <li><a href="#"><i class="fa fa-cloud"></i> Air Conditioned</a></li>
                                                    <li><a href="#"><i class="fa fa-shopping-cart"></i> Online Ordering</a></li>
                                                    <li><a href="#"><i class="fa fa-paw"></i> Pet Friendly</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div> --}}
                                    <!-- list-single-main-item end -->
                                    <!-- list-single-main-item-->
                                    {{-- <div class="list-single-main-item fl-wrap block_box" id="sec4">
                                        <div class="list-single-main-item-title">
                                            <h3>Gallery / Photos</h3>
                                        </div>
                                        <div class="list-single-main-item_content fl-wrap">
                                            <div class="single-carousel-wrap fl-wrap lightgallery">
                                                <div class="sc-next sc-btn color2-bg"><i class="fas fa-caret-right"></i></div>
                                                <div class="sc-prev sc-btn color2-bg"><i class="fas fa-caret-left"></i></div>
                                                <div class="single-carousel fl-wrap full-height">
                                                    <div class="swiper-container">
                                                        <div class="swiper-wrapper">
                                                            <!-- swiper-slide-->
                                                            <div class="swiper-slide">
                                                                <div class="box-item">
                                                                    <img  src="images/all/1.jpg"   alt="">
                                                                    <a href="images/all/1.jpg" class="gal-link popup-image"><i class="fa fa-search"  ></i></a>
                                                                </div>
                                                            </div>
                                                            <!-- swiper-slide end-->
                                                            <!-- swiper-slide-->
                                                            <div class="swiper-slide">
                                                                <div class="box-item">
                                                                    <img  src="images/all/1.jpg"   alt="">
                                                                    <a href="images/all/1.jpg" class="gal-link popup-image"><i class="fa fa-search"  ></i></a>
                                                                </div>
                                                            </div>
                                                            <!-- swiper-slide end-->
                                                            <!-- swiper-slide-->
                                                            <div class="swiper-slide">
                                                                <div class="box-item">
                                                                    <img  src="images/all/1.jpg"   alt="">
                                                                    <a href="images/all/1.jpg" class="gal-link popup-image"><i class="fa fa-search"  ></i></a>
                                                                </div>
                                                            </div>
                                                            <!-- swiper-slide end-->
                                                            <!-- swiper-slide-->
                                                            <div class="swiper-slide">
                                                                <div class="box-item">
                                                                    <img  src="images/all/1.jpg"   alt="">
                                                                    <a href="images/all/1.jpg" class="gal-link popup-image"><i class="fa fa-search"  ></i></a>
                                                                </div>
                                                            </div>
                                                            <!-- swiper-slide end-->
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div> --}}
                                    <!-- list-single-main-item end -->
                                    <!-- accordion-->
                                    {{-- <div class="accordion mar-top" id="sec5">
                                        <a class="toggle act-accordion" href="#"> Crossfit<span></span></a>
                                        <div class="accordion-inner visible">
                                            <h4 class="simple-title">The Trend in Web Design <span>By David Gray</span></h4>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas in pulvinar neque. Nulla finibus lobortis pulvinar. Donec a consectetur nulla. Nulla posuere sapien vitae lectus suscipit, et pulvinar nisi tincidunt. Aliquam erat volutpat. Curabitur convallis fringilla diam sed aliquam. Sed tempor iaculis massa faucibus feugiat. In fermentum facilisis massa, a consequat purus viverra.</p>
                                        </div>
                                        <a class="toggle" href="#"> Fitness  <span></span></a>
                                        <div class="accordion-inner">
                                            <h4 class="simple-title">Successful Marketing Strategy <span>By Austin Evon</span></h4>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas in pulvinar neque. Nulla finibus lobortis pulvinar. Donec a consectetur nulla. Nulla posuere sapien vitae lectus suscipit, et pulvinar nisi tincidunt. Aliquam erat volutpat. Curabitur convallis fringilla diam sed aliquam. Sed tempor iaculis massa faucibus feugiat. In fermentum facilisis massa, a consequat purus viverra.</p>
                                        </div>
                                        <a class="toggle" href="#"> TRX  <span></span></a>
                                        <div class="accordion-inner">
                                            <h4 class="simple-title">The Results - Closing <span>By Martin Kowalsky</span></h4>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas in pulvinar neque. Nulla finibus lobortis pulvinar. Donec a consectetur nulla. Nulla posuere sapien vitae lectus suscipit, et pulvinar nisi tincidunt. Aliquam erat volutpat. Curabitur convallis fringilla diam sed aliquam. Sed tempor iaculis massa faucibus feugiat. In fermentum facilisis massa, a consequat purus viverra.</p>
                                        </div>
                                    </div> --}}
                                    <!-- accordion end -->
                                    <!-- list-single-main-item -->
                                    <div class="list-single-main-item fl-wrap block_box" id="sec6">
                                        <div class="list-single-main-item-title">
                                            <h3>Item Reviews -  <span>0 </span></h3>
                                        </div>
                                        {{-- <div class="reviews-score-wrap fl-wrap">
                                            <div class="review-score-total">
                                                <span class="review-score-total-item">
                                                4.1
                                                </span>
                                                <div class="listing-rating card-popup-rainingvis" data-starrating2="5"></div>
                                            </div>
                                            <div class="review-score-detail">
                                                <div class="review-score-detail-list">
                                                    <div class="rate-item">
                                                        <div class="rate-item-title"><span>Quality</span></div>
                                                        <div class="rate-item-bg" data-percent="100%">
                                                            <div class="rate-item-line gradient-bg"></div>
                                                        </div>
                                                        <div class="rate-item-percent">5.0</div>
                                                    </div>
                                                    <div class="rate-item">
                                                        <div class="rate-item-title"><span>Location</span></div>
                                                        <div class="rate-item-bg" data-percent="90%">
                                                            <div class="rate-item-line gradient-bg"></div>
                                                        </div>
                                                        <div class="rate-item-percent">4.0</div>
                                                    </div>

                                                    <div class="rate-item">
                                                        <div class="rate-item-title"><span>Price</span></div>
                                                        <div class="rate-item-bg" data-percent="60%">
                                                            <div class="rate-item-line gradient-bg"></div>
                                                        </div>
                                                        <div class="rate-item-percent">3.0</div>
                                                    </div>

                                                    <div class="rate-item">
                                                        <div class="rate-item-title"><span>Service</span></div>
                                                        <div class="rate-item-bg" data-percent="80%">
                                                            <div class="rate-item-line gradient-bg"></div>
                                                        </div>
                                                        <div class="rate-item-percent">4.0</div>
                                                    </div>

                                                </div>

                                            </div>
                                        </div> --}}

                                        <div class="list-single-main-item_content fl-wrap">
                                            <div class="reviews-comments-wrap">

                                                {{-- <div class="reviews-comments-item">
                                                    <div class="review-comments-avatar">
                                                        <img src="{{ asset('frontend/images/avatar/1.jpg') }}" alt="">
                                                    </div>
                                                    <div class="reviews-comments-item-text fl-wrap">
                                                        <div class="reviews-comments-header fl-wrap">
                                                            <h4><a href="#">Liza Rose</a></h4>
                                                            <div class="review-score-user">
                                                                <span class="review-score-user_item">4.2</span>
                                                                <div class="listing-rating card-popup-rainingvis" data-starrating2="4"></div>
                                                            </div>
                                                        </div>
                                                        <p>" Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. "</p>
                                                        <div class="reviews-comments-item-footer fl-wrap">
                                                            <div class="reviews-comments-item-date"><span><i class="far fa-calendar-check"></i>12 April 2018</span></div>
                                                            <a href="#" class="rate-review"><i class="fal fa-thumbs-up"></i>  Helpful Review  <span>2</span> </a>
                                                        </div>
                                                    </div>
                                                </div> --}}
        {{--
                                                <div class="reviews-comments-item">
                                                    <div class="review-comments-avatar">
                                                        <img src="{{ asset('frontend/images/avatar/1.jpg') }}" alt="">
                                                    </div>
                                                    <div class="reviews-comments-item-text fl-wrap">
                                                        <div class="reviews-comments-header fl-wrap">
                                                            <h4><a href="#">Adam Koncy</a></h4>
                                                            <div class="review-score-user">
                                                                <span class="review-score-user_item">5.0</span>
                                                                <div class="listing-rating card-popup-rainingvis" data-starrating2="5"></div>
                                                            </div>
                                                        </div>
                                                        <p>" Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc posuere convallis purus non cursus. Cras metus neque, gravida sodales massa ut. "</p>
                                                        <div class="review-images ">
                                                            <a href="images/all/1.jpg" class="image-popup"><img src="images/all/1.jpg" alt=""></a>
                                                            <a href="images/all/1.jpg" class="image-popup"><img src="images/all/1.jpg" alt=""></a>
                                                        </div>
                                                        <div class="reviews-comments-item-footer fl-wrap">
                                                            <div class="reviews-comments-item-date"><span><i class="far fa-calendar-check"></i>03 December 2017</span></div>
                                                            <a href="#" class="rate-review"><i class="fal fa-thumbs-up"></i>  Helpful Review  <span>4</span> </a>
                                                        </div>
                                                    </div>
                                                </div> --}}
                                                <!--reviews-comments-item end-->
                                            </div>
                                        </div>

                                    </div>
                                    <!-- list-single-main-item end -->
                                    <!-- list-single-main-item -->

                                </div>
                            </div>
                            <!-- list-single-main-wrapper-col end -->

                        </div>
                        <!-- list-single-sidebar -->
                        <div class="col-md-4">
                            <!--box-widget-item -->

                            <!--box-widget-item end -->
                            <!--box-widget-item -->
                            {{-- <div class="box-widget-item fl-wrap">
                                <div class="banner-wdget fl-wrap">
                                    <div class="overlay"></div>
                                    <div class="bg"  data-bg="images/bg/1.jpg"></div>
                                    <div class="banner-wdget-content fl-wrap">
                                        <h4>Get two months free when you purchase a subscription.</h4>
                                        <a href="#" class="color-bg">Book Now</a>
                                    </div>
                                </div>
                            </div>
                            <!--box-widget-item end -->
                            <!--box-widget-item -->
                            <div class="box-widget-item fl-wrap block_box">
                                <div class="box-widget-item-header">
                                    <h3>Book a Season Ticket</h3>
                                </div>
                                <div class="box-widget">
                                    <div class="box-widget-content">
                                        <form   class="add-comment custom-form">
                                            <fieldset>
                                                <label><i class="fal fa-user"></i></label>
                                                <input type="text" placeholder="Your Name *" value=""/>
                                                <div class="clearfix"></div>
                                                <label><i class="fal fa-envelope"></i>  </label>
                                                <input type="text" placeholder="Email Address*" value=""/>
                                                <div class="quantity fl-wrap">
                                                    <span><i class="fal fa-user-plus"></i>Persons : </span>
                                                    <div class="quantity-item">
                                                        <input type="button" value="-" class="minus">
                                                        <input type="text"    name="quantity"   title="Qty" class="qty color-bg" data-min="1" data-max="3" data-step="1" value="1">
                                                        <input type="button" value="+" class="plus">
                                                    </div>
                                                </div>
                                                <div class="listsearch-input-item clact">
                                                    <select data-placeholder="Ticket Type" class="chosen-select no-search-select" >
                                                        <option value="Ticket Type">Pass Type</option>
                                                        <option value="Standard Pass">Standard Pass</option>
                                                        <option value="Silver Pass">Silver Pass</option>
                                                        <option value="Gold Pass">Gold Pass</option>
                                                        <option value="Platinum Pass">Platinum Pass</option>
                                                    </select>
                                                </div>
                                                <textarea cols="40" rows="3" placeholder="Additional Information:"></textarea>
                                            </fieldset>
                                            <button class="btn color2-bg url_btn float-btn" onclick="window.location.href='booking.html'">Book   Now <i class="fal fa-bookmark"></i></button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <!--box-widget-item end -->
                            <!--box-widget-item -->
                            <div class="box-widget-item fl-wrap block_box">
                                <div class="box-widget-item-header">
                                    <h3>Location / Contacts  </h3>
                                </div>
                                <div class="box-widget">
                                    <div class="map-container">
                                        <div id="singleMap" data-latitude="40.7427837" data-longitude="-73.11445617675781" data-mapTitle="Our Location"></div>
                                    </div>
                                    <div class="box-widget-content bwc-nopad">
                                        <div class="list-author-widget-contacts list-item-widget-contacts bwc-padside">
                                            <ul class="no-list-style">
                                                <li><span><i class="fal fa-map-marker"></i> Adress :</span> <a href="#">USA 27TH Brooklyn NY</a></li>
                                                <li><span><i class="fal fa-phone"></i> Phone :</span> <a href="#">+7(123)987654</a></li>
                                                <li><span><i class="fal fa-envelope"></i> Mail :</span> <a href="#">AlisaNoory@domain.com</a></li>
                                                <li><span><i class="fal fa-browser"></i> Website :</span> <a href="#">themeforest.net</a></li>
                                            </ul>
                                        </div>
                                        <div class="list-widget-social bottom-bcw-box  fl-wrap">
                                            <ul class="no-list-style">
                                                <li><a href="#" target="_blank" ><i class="fab fa-facebook-f"></i></a></li>
                                                <li><a href="#" target="_blank"><i class="fab fa-twitter"></i></a></li>
                                                <li><a href="#" target="_blank" ><i class="fab fa-vk"></i></a></li>
                                                <li><a href="#" target="_blank" ><i class="fab fa-instagram"></i></a></li>
                                            </ul>
                                            <div class="bottom-bcw-box_link"><a href="#" class="show-single-contactform tolt" data-microtip-position="top" data-tooltip="Write Message"><i class="fal fa-envelope"></i></a></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--box-widget-item end -->
                            <!--box-widget-item -->
                            <div class="box-widget-item fl-wrap block_box">
                                <div class="box-widget-item-header">
                                    <h3> Price Range </h3>
                                </div>
                                <div class="box-widget">
                                    <div class="box-widget-content">
                                        <div class="claim-price-wdget fl-wrap">
                                            <div class="claim-price-wdget-content fl-wrap">
                                                <div class="pricerange fl-wrap"><span>Price : </span> 81$ - 320$ </div>
                                                <div class="claim-widget-link fl-wrap"><span>Own or work here?</span><a href="#">Claim Now!</a></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--box-widget-item end -->                                           <!--box-widget-item -->
                            <div class="box-widget-item fl-wrap block_box">
                                <div class="box-widget-item-header">
                                    <h3> Instagram  </h3>
                                </div>
                                <div class="box-widget">
                                    <div class="box-widget-content">
                                        <div class='jr-insta-thumb' id="insta-content" data-instatoken="3075034521.5d9aa6a.284ff8339f694dbfac8f265bf3e93c8a"></div>
                                    </div>
                                </div>
                            </div>
                            <!--box-widget-item end -->
                            <!--box-widget-item -->
                            <div class="box-widget-item fl-wrap block_box">
                                <div class="box-widget-item-header">
                                    <h3>Hosted by : </h3>
                                </div>
                                <div class="box-widget">
                                    <div class="box-widget-author fl-wrap">
                                        <div class="box-widget-author-title">
                                            <div class="box-widget-author-title-img">
                                                <img src="images/avatar/1.jpg" alt="">
                                            </div>
                                            <div class="box-widget-author-title_content">
                                                <a href="user-single.html">Alisa Noory</a>
                                                <span>4 Places Hosted</span>
                                            </div>
                                            <div class="box-widget-author-title_opt">
                                                <a href="user-single.html" class="tolt green-bg" data-microtip-position="top" data-tooltip="View Profile"><i class="fas fa-user"></i></a>
                                                <a href="#" class="tolt color-bg cwb" data-microtip-position="top" data-tooltip="Chat With Owner"><i class="fas fa-comments-alt"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--box-widget-item end -->
                            <!--box-widget-item -->
                            <div class="box-widget-item fl-wrap block_box">
                                <div class="box-widget-item-header">
                                    <h3>Similar listings :</h3>
                                </div>
                                <div class="box-widget  fl-wrap">
                                    <div class="box-widget-content">
                                        <!--widget-posts-->
                                        <div class="widget-posts  fl-wrap">
                                            <ul class="no-list-style">
                                                <li>
                                                    <div class="widget-posts-img"><a href="listing-single.html"><img src="images/gallery/thumbnail/1.png" alt=""></a>
                                                    </div>
                                                    <div class="widget-posts-descr">
                                                        <h4><a href="listing-single.html">Iconic Cafe</a></h4>
                                                        <div class="geodir-category-location fl-wrap"><a href="#"><i class="fas fa-map-marker-alt"></i> 40 Journal Square Plaza, NJ, USA</a></div>
                                                        <div class="widget-posts-descr-link"><a href="listing.html" >Restaurants </a>   <a href="listing.html">Cafe</a></div>
                                                        <div class="widget-posts-descr-score">4.1</div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="widget-posts-img"><a href="listing-single.html"><img src="images/gallery/thumbnail/1.png" alt=""></a>
                                                    </div>
                                                    <div class="widget-posts-descr">
                                                        <h4><a href="listing-single.html">MontePlaza Hotel</a></h4>
                                                        <div class="geodir-category-location fl-wrap"><a href="#"><i class="fas fa-map-marker-alt"></i> 70 Bright St New York, USA </a></div>
                                                        <div class="widget-posts-descr-link"><a href="listing.html" >Hotels </a>  </div>
                                                        <div class="widget-posts-descr-score">5.0</div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="widget-posts-img"><a href="listing-single.html"><img src="images/gallery/thumbnail/1.png" alt=""></a>
                                                    </div>
                                                    <div class="widget-posts-descr">
                                                        <h4><a href="listing-single.html">Rocko Band in Marquee Club</a></h4>
                                                        <div class="geodir-category-location fl-wrap"><a href="#"><i class="fas fa-map-marker-alt"></i>75 Prince St, NY, USA</a></div>
                                                        <div class="widget-posts-descr-link"><a href="listing.html" >Events</a> </div>
                                                        <div class="widget-posts-descr-score">4.2</div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="widget-posts-img"><a href="listing-single.html"><img src="images/gallery/thumbnail/1.png" alt=""></a>
                                                    </div>
                                                    <div class="widget-posts-descr">
                                                        <h4><a href="listing-single.html">Premium Fitness Gym</a></h4>
                                                        <div class="geodir-category-location fl-wrap"><a href="#"><i class="fas fa-map-marker-alt"></i> W 85th St, New York, USA</a></div>
                                                        <div class="widget-posts-descr-link"><a href="listing.html" >Fitness</a> <a href="listing.html" >Gym</a> </div>
                                                        <div class="widget-posts-descr-score">5.0</div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                        <!-- widget-posts end-->
                                    </div>
                                </div>
                            </div>
                            <!--box-widget-item end -->
                            <!--box-widget-item -->
                            <div class="box-widget-item fl-wrap block_box">
                                <div class="box-widget-item-header">
                                    <h3>Tags</h3>
                                </div>
                                <div class="box-widget opening-hours fl-wrap">
                                    <div class="box-widget-content">
                                        <div class="list-single-tags tags-stylwrap">
                                            <a href="#">Hotel</a>
                                            <a href="#">Hostel</a>
                                            <a href="#">Room</a>
                                            <a href="#">Spa</a>
                                            <a href="#">Restourant</a>
                                            <a href="#">Parking</a>
                                        </div>
                                    </div>
                                </div>
                            </div> --}}
                            <!--box-widget-item end -->
                        </div>
                        <!-- list-single-sidebar end -->
                    </div>
                </section>
                <div class="limit-box fl-wrap"></div>
            </div>
            <!--content end-->

        </div>
        <!-- wrapper end-->


        <footer class="footer">
            <div class="container-fluid pl-5 pr-5">
              <div class="row align-items-cente">
                <div class="col-lg-3 col-md-3 col-sm-5 text-left align-items-center pt-2 pb-2 align-self-center pl-5"> <!-- Start Img-Logo-link-column -->
                  <a href="#">
                    <img src="asset('assets/img/thelickinglogonew150.png') }}" alt="thelickinglogonew150.pngg">
                  </a>
                </div> <!-- End Img-Logo-link-column -->
                <div class="col-lg-9 col-md-9 col-sm-7 text-right pt-3 pb-3 pr-5"> <!-- Start Email-column -->
                  <p class="mb-0 email"><span class="email-span">EMAIL:</span> HR@THELICKING.COM</p>
                </div> <!-- END Email-column -->
              </div>
            </div>
        </footer>
    </div>
    <!-- Main end -->
    <!--=============== scripts  ===============-->

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<!-- jQuery and Bootstrap Bundle (includes Popper) -->
{{-- <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script> --}}
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>
<!-- Owl Carousele JS -->
<script src="{{ asset('assets/js/OwlCarousel/dist/owl.carousel.min.js') }}"></script>

<script src="{{ asset('assets/js/thelicking.js') }}"></script>
{{-- <script src="{{asset('frontend/js/jquery.min.js')}}"></script> --}}
{{-- <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script> --}}
<script src="{{asset('frontend/js/plugins.js')}}"></script>
<script src="{{asset('frontend/js/scripts.js')}}"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<script>
    $(document).ready(function() {

        var owl = $("#owl-demo");

        owl.owlCarousel({
           // items : 5, 10 items above 1000px browser width
            itemsDesktop : [1000,5.5], //5 items between 1000px and 901px
            itemsDesktopSmall : [900,3.5], // betweem 900px and 601px
            itemsTablet: [600,2.5], //2 items between 600 and 0
            itemsMobile : false // itemsMobile disabled - inherit from itemsTablet option
        });

        // Custom Navigation Events
        // $(".next").click(function(){
        // owl.trigger('owl.next');
        // })
        // $(".prev").click(function(){
        // owl.trigger('owl.prev');
        // })
        // $(".play").click(function(){
        // owl.trigger('owl.play',1000); //owl.play event accept autoPlay speed as second parameter
        // })
        // $(".stop").click(function(){
        // owl.trigger('owl.stop');
        // })
        $('.owl-controls').hide();
        $('.owl-item').css('width','auto');
        $('.owl-stage').css('width','300000px')

    });

    //==================dish modal================//

    $('.dish').click(function(){
        $('.addons').empty();
        $('.Portions').css('display','none');
        var id=$(this).attr('id');
        // $(this).attr('data-toggle', 'modal');
        //     $(this).attr('data-target', 'add_order');
        $.ajax({
            type: 'GET',
            url: 'https://thelicking.momentum-host.com/api/dish/{{ Config::get('app.locale') }}/'+id,
            dataType: 'json',
            success: function (data) {

                console.log(data.dish[0].portions_price)
                var price=data.dish[0].portions_price;
                var pricewithnull=data.dish[0].portions_price;
                var min=price[0];
                var value;
                $.each(price, function( i, value ) {
                    if(value !=null){
                        if (min > value) {
                        min =value;
                        }
                    }
                });
                $('#dish').html(data.dish[0].id)
                $('#regqty').html(min)
                $('#add_order').find('.dish-title').html(data.dish[0].name+ '<p class="mx-4 dish-calories" style="display: inline-block;">  Calories  : '+data.dish[0].calories+'</p><p class="mx-4 dish-calories" style="display: inline-block;">  Price  : <span id=reg_price>'+min +'</span> $')
                var dish_image= '{{ URL::asset('/backend/img/dishes/') }}/'+data.dish[0].images;
                $('#add_order .dish-image').attr("src",dish_image);
                $('#add_order .dish-description').html(data.dish[0].info);
                var desc=$('#add_order .dish-description').html();
                var arr=[]
                arr=price
                var emptyElems = 0;
                    for(var i=0, l = arr.length; i < l; i++){
                        emptyElems += (arr[i] === null) ? 1 : 0;
                    }

                if(emptyElems <=1){

                    $('.Portions').css('display','block');
                    var small=pricewithnull[0];
                    var medium=pricewithnull[1];
                    var large=pricewithnull[2];
                    var prices=[];


                    // alert( Math.min(filtered))

                    if(small == null){
                        $('#small').hide();
                    }else{
                        $('#allow_small').html('small ' +small+ ' $');
                        prices.push(parseFloat(small));
                    }
                    if(medium == null){
                        $('#medium').hide();

                    }else{
                        $('#allow_medium').html('medium ' +medium+ ' $')
                        prices.push(parseFloat(medium));

                    }
                    if(large == null){
                        $('#large').hide();

                    }else{
                        $('#allow_large').html('large ' +large+ ' $')
                        prices.push(parseFloat(large));

                    }
                    var filtered = prices.filter(function (el) {//remove null value from portion price
                        return el != null;
                    });
                    var min=Math.min.apply(Math,filtered); //get the min value
                    jQuery("input:radio[name=addon_price]").first().click()//checked the min value
                    jQuery("input:radio[name=addon_price]").first().val(min)//set value to the min radio input

                }

                 if(desc == ''){
                    // $('#add_order .dish-description').html(' there is no information')
                    $('.PortionDetails').hide();
                 }
                $('#add_order #total_price').html(min);
                //==================addon sections================//

                // console.log(data.dish[0].addonsections[0]);
                    var x=1;
                    data.dish[0].addonsections.forEach(element => {
                    var i=1;
                    var addons=element.addons;
                    var condition;
                    var textcondition;
                    if(element.conditions == "0"){
                        condition ="Optional";
                    }else{
                        condition="Required";
                    }

                    if(element.max ==null){
                        textcondition="Select at least " +element.min;

                    }else if(element.max !=null && element.min !=null){
                        textcondition="Select from "+element.min +" to "+element.max;

                    }else{
                        textcondition="Select max "+element.max;
                    }
                    var section= '<div class="first mt-2 sectiongroup" condition="'+condition+'" min="'+element.min+'" max="'+element.max+'" style="border: 1px solid #eee;padding:20px">'+
                                    '<div class="errors"></div>'+
                                    '<div class="head" style="display: flex;justify-content:space-between" >'+
                                        '<div>'+
                                            '<h3 class="">'+element.name+'</h3>'+
                                            '<p style="text-align: left; font-weight: 800;">'+condition+'</p>'+
                                        '</div>'+
                                        '<p class="conditioncounter">'+ textcondition +' </p>'+
                                    '</div>'+
                                    '<div id="addonGroup'+x+'">'+
                                    '</div>'+
                                '</div>';
                                $('.addons').append(section);

                            //==================addon ================//

                        addons.forEach(eleAddon => {
                        var group=  '<div class="listaddon mt-3" style="display: flex;justify-content:space-between">'+
                                        '<div class="addon">'+
                                            '<input onclick="add_addon_price(this)" class="addon'+i+'"  type="checkbox" name="addons[]" id="'+eleAddon.id+'"  value="'+eleAddon.price+'">'+
                                            '<label for="addon" style="    padding-left: 6px"> ' +eleAddon.name+'</label>'+
                                        '</div>'+
                                        '<p>'+eleAddon.calories+' Calories</p>'+
                                        '<p>+'+eleAddon.price+' $</p>'+
                                    '</div>';

                        $("#addonGroup"+x).append(group);
                    });
                    x++;
                    });

                    var desc=$('#add_order .dish-description').html();

                $('#add_order').modal('toggle');
            }

        });

    });
            //==================change price when check dish size ================//

    function changePrice(elem){
        var checked_price =$(elem).next('label').find('span').html();
        checked_price=parseFloat(checked_price.replace( /\D+/g, ''));
        var reg_price=$('#reg_price').html(checked_price);
       $(elem).val(checked_price);

    //    var qty=$('.counter').val();
    //    $('#total_price').html(qty*checked_price);

        regqty();

    }
        //==================increase counter of item when check dish size ================//

     function add(elem){
        var reg_price=$('#reg_price').html();

		var qty=$(elem).prev("input").val();
		qty++;
		$(elem).prev("input").val(qty);
		$('.remove').prop("disabled", false);
        // var total= $('#total_price').html();
        // $('#total_price').html(qty*reg_price);
        regqty();
	}

        //==================decrease counter of item  when check dish size ================//

    function remove(elem){
        var reg_price=$('#reg_price').html();

		var qty=$(elem).next("input").val();
		qty-=1;
        if (qty == 0) {
            qty=1;
        }
        $(elem).next("input").val(qty);
        // alert(qty)
		$('.remove').prop("disabled", true);
        // var total= $('#total_price').html();
        // alert(total);
        // $('#total_price').html(qty*reg_price);
        regqty();
	}



    //==================adoons checked ================//

    function add_addon_price(elem){
        //     var addon_price=$(elem).val();
        //     var qty=$('.counter').val();
        //     var reg_price=$('#reg_price').html();
        // if($(elem).prop('checked')){
        //     $('#total_price').html(Number(qty*reg_price)+Number(addon_price));
        // }else{
        //     console.log(Number(qty*reg_price) ,Number(addon_price),Number(qty*reg_price)-Number(addon_price) ,qty,reg_price,addon_price)
        //     $('#total_price').html(Number(qty*reg_price)-Number(addon_price));

        // }
        addonsPrice();
    }

    function regqty(){
        var reg_price=$('#reg_price').html();
        var qty=$('.counter').val();
        var regqty=reg_price * qty;
       $('#regqty').html(regqty);
       total();

    }
    var selectedaddons=[];
    function addonsPrice(){
        selectedaddons=[];
        var values = $("input[name='addons[]']").map(function(){
            if($(this).prop('checked')){
                selectedaddons.push($(this).attr('id'));
                return parseFloat($(this).val());
            }
            }).get();
           var addonsPrice = values.reduce((a, b) => a + b, 0)
        $('#addonsPrice').html(addonsPrice);
        total();

    }
        //==================total price regprice + addons price================//

        // $( document ).ready(function() {
            function total(){

            var addonsPrice = $('#addonsPrice').html();
            var regqty  =  $('#regqty').html();
            $('#total_price').html(parseFloat(addonsPrice)+parseFloat(regqty))
        }
        // });

        //==================add to modal validation================//

        function checkmodalvallidation(){
            $('.auth').css('display','none');
            var guest=$('.auth').attr('auth');
            if(guest =='guest'){
                $('.modal').modal('hide');
                $('.auth').html('You  Must Login First');
                $('.auth').css('display','block');

                setTimeout(function() {
				$('.auth').fadeOut('fast');
			        }, 2000);
            }
            var errots_counter =[];

            $('.sectiongroup').each(function(i, obj) {
                var condition =$(this).attr('condition');
                var min =$(this).attr('min');
                var max =$(this).attr('max');
                var sectiongrouperrors=$(this).find('.head').find('.conditioncounter');
                // sectiongrouperrors.empty();
                sectiongrouperrors.css('color','#878C9F')
                var selected = [];
                $(this).find('input:checked').each(function() {
                    selected.push($(this).attr('value'));
                });
            //    if(condition == 'Required'){
                   if(min == null || max == null){//check min or max is null

                        if(min != null){//check min not null
                            if(selected.length < min){//check if selected less than min
                                // var error='<span style="color:red">you must select up to '+min+'</span>';
                                // sectiongrouperrors.append(error);
                                sectiongrouperrors.css('color','red');
                                errots_counter.push('1');

                            }
                        }
                        if(max != null){
                            if(selected.length > mix){//check if selected more than max
                                sectiongrouperrors.css('color','red');
                                errots_counter.push('1');

                            }
                        }
                   }else{
                        if(selected.length < min ){//check if selected less than min
                                // var error='<span style="color:red">you must select up to '+min+'</span> <br>';
                                // sectiongrouperrors.append(error);
                                sectiongrouperrors.css('color','red');
                                errots_counter.push('1');


                        }if( selected.length > max){
                            // var error='<span style="color:red">you must select less than '+max+'</span> <br>';
                            //     sectiongrouperrors.append(error);
                            sectiongrouperrors.css('color','red');
                            errots_counter.push('1');

                        }
                   }

            //    }else{

            //    }
            });

            if(errots_counter.length == []) {
                $.ajaxSetup({

                    headers: {

                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

                    }

                });

                var dish_id=$('#dish').html();
                var size = $("input[name='addon_price']").attr('id');//index of portion size and portion price of dish
                var price=$('#reg_price').html();
                var qty=$('#regqty').html()/price;
                var addons=selectedaddons;
                var notes= document.getElementById("myTextarea").value;
                _token = document.getElementsByName("_token")[0].value
                // console.log(qty,dish_id,size,price,addons);
            $.ajax({
                url: "/addDish/en",
                type:"POST",
                data:{
                qty:qty,
                dish_id:dish_id,
                notes:notes,
                size:size,
                price:price,
                addons:addons,
                _token:_token
                },
                success:function(response){
                console.log(response);
                if(response) {

                }
                $('.modal').modal('hide');

                },
            });


            }

        }
            //==================add to cart================//

    // $(".cart").click(function(e){
	// 		e.preventDefault();

	// 		var id = $(this).attr("title");


	// 		$.ajax({
	// 		type:'POST',
	// 		url:'/addDish',
	// 		data:{ id:id },

	// 		success:function(data){

	// 				// var res=jQuery.inArray(id,localStorage);
	// 				// var key='thejet_product_'+data.id;
	// 				// var storage=localStorage['cart'];
	// 				// if(storage.includes(key)){
	// 				// 	$('#alert-cart').show();
	// 				// 	$('#alert-cart').html(data.name+' already in cart ');
	// 				// 	setTimeout(function() {
	// 				// 			$('#alert-cart').fadeOut('fast');
	// 				// 	}, 2000);
	// 				// }else{
	// 				// 	addToCart(data);
	// 				// 	$('#alert-cart').show();
	// 				// 	$('#alert-cart').html('added to cart succssfully');
	// 				// 	setTimeout(function() {
	// 				// 			$('#alert-cart').fadeOut('fast');
	// 				// 	}, 2000);
	// 				// 	$('.total-count').html('');
	// 				// 	var total=JSON.parse(localStorage.getItem("cart"));
	// 				// 	$('.total-count').html(JSON.parse(total.length));
	// 				// }
	// 			}
	// 		});

	// 	});
                //==================add to wishlist================//

    $(".whishlist").click(function(e){
			e.preventDefault();

			var id = $(this).attr("title");

			$.ajax({
			type:'POST',
			url:'/fav/{{ Config::get('app.locale') }}',
			data:{
                "_token": "{{ csrf_token() }}",

                id:id },

			success:function(data){
                console.log(data);
					// var res=jQuery.inArray(id,localStorage);
					// var key='thejet_product_'+data.id;
					// var storage=localStorage['cart'];
					// if(storage.includes(key)){
					// 	$('#alert-cart').show();
					// 	$('#alert-cart').html(data.name+' already in cart ');
					// 	setTimeout(function() {
					// 			$('#alert-cart').fadeOut('fast');
					// 	}, 2000);
					// }else{
					// 	addToCart(data);
					// 	$('#alert-cart').show();
					// 	$('#alert-cart').html('added to cart succssfully');
					// 	setTimeout(function() {
					// 			$('#alert-cart').fadeOut('fast');
					// 	}, 2000);
					// 	$('.total-count').html('');
					// 	var total=JSON.parse(localStorage.getItem("cart"));
					// 	$('.total-count').html(JSON.parse(total.length));
					// }
				}
			});

		});

</script>

</body>
</html>
