@extends('frontend.partials.profile.app')

    @section('profile-content')

        <!--  section  -->
        <section class="gray-bg main-dashboard-sec" id="sec1">
            <div class="container">
                @section('profile-content')
                     <!-- dashboard content-->
                    <div class="col-md-9 my-5">
                        <!-- dashboard-list-box-->
                        <div class="dashboard-list-box fl-wrap">
                            <div class="dashboard-header color-bg fl-wrap">
                                <h3>Indox</h3>
                            </div>
                            <div class="chat-wrapper fl-wrap">
                                <div class="row">
                                    <!-- chat-box-->
                                    <div class="col-sm-8">
                                        <div class="chat-box fl-wrap">
                                            <!-- message-->
                                            <div class="chat-message chat-message_guest fl-wrap">
                                                <div class="dashboard-message-avatar">
                                                    <img src="{{asset('backend/img/user.jpeg')}}" alt="">
                                                    <span class="chat-message-user-name cmun_sm">Andy</span>
                                                </div>
                                                <span class="massage-date">25 may 2018  <span>7.51 PM</span></span>
                                                <p>Vivamus lobortis vel nibh nec maximus. Donec dolor erat, rutrum ut feugiat sed, ornare vitae nunc. Donec massa nisl, bibendum id ultrices sed, accumsan sed dolor.</p>
                                            </div>
                                            <!-- message end-->
                                            <!-- message-->
                                            <div class="chat-message chat-message_user fl-wrap">
                                                <div class="dashboard-message-avatar">
                                                    <img src="{{asset('backend/img/user.jpeg')}}" alt="">
                                                    <span class="chat-message-user-name cmun_sm">Jessie</span>
                                                </div>
                                                <span class="massage-date">25 may 2018  <span>7.51 PM</span></span>
                                                <p>Nulla eget erat consequat quam feugiat dapibus eget sed mauris.</p>
                                            </div>
                                            <!-- message end-->
                                            <!-- message-->
                                            <div class="chat-message chat-message_guest fl-wrap">
                                                <div class="dashboard-message-avatar">
                                                    <img src="{{asset('backend/img/user.jpeg')}}" alt="">
                                                    <span class="chat-message-user-name cmun_sm">Andy</span>
                                                </div>
                                                <span class="massage-date">25 may 2018  <span>7.51 PM</span></span>
                                                <p>Sed non neque faucibus, condimentum lectus at, accumsan enim. Fusce pretium egestas cursus..</p>
                                            </div>
                                            <!-- message end-->
                                            <!-- message-->
                                            <div class="chat-message chat-message_user fl-wrap">
                                                <div class="dashboard-message-avatar">
                                                    <img src="{{asset('backend/img/user.jpeg')}}" alt="">
                                                    <span class="chat-message-user-name cmun_sm">Jessie</span>
                                                </div>
                                                <span class="massage-date">25 may 2018  <span>7.51 PM</span></span>
                                                <p>Donec a consectetur nulla. Nulla posuere sapien vitae lectus suscipit, et pulvinar nisi tincidunt. Aliquam erat volutpat. Curabitur convallis fringilla diam sed aliquam. Sed tempor iaculis massa faucibus feugiat. In fermentum facilisis massa, a consequat .</p>
                                            </div>
                                            <!-- message end-->
                                        </div>
                                        <div class="chat-widget_input fl-wrap">
                                            <textarea placeholder="Type Message"></textarea>
                                            <button type="submit"><i class="fal fa-paper-plane"></i></button>
                                        </div>
                                    </div>
                                    <!-- chat-box end-->
                                    <!-- chat-contacts-->
                                    <div class="col-sm-4">
                                        <div class="chat-contacts fl-wrap">
                                            <!-- chat-contacts-item-->
                                            <a class="chat-contacts-item" href="#">
                                                <div class="dashboard-message-avatar">
                                                    <img src="{{asset('backend/img/user.jpeg')}}" alt="">
                                                    <div class="message-counter">2</div>
                                                </div>
                                                <div class="chat-contacts-item-text">
                                                    <h4>Mark Rose</h4>
                                                    <span>27 Dec 2018 </span>
                                                    <p>Vivamus lobortis vel nibh nec maximus. Donec dolor erat, rutrum ut feugiat sed, ornare vitae nunc. Donec massa nisl, bibendum id ultrices sed, accumsan sed dolor.</p>
                                                </div>
                                            </a>
                                            <!-- chat-contacts-item -->
                                            <!-- chat-contacts-item-->
                                            <a class="chat-contacts-item" href="#">
                                                <div class="dashboard-message-avatar">
                                                    <img src="{{asset('backend/img/user.jpeg')}}" alt="">
                                                    <div class="message-counter">1</div>
                                                </div>
                                                <div class="chat-contacts-item-text">
                                                    <h4>Adam Koncy</h4>
                                                    <span>27 Dec 2018 </span>
                                                    <p>Vivamus lobortis vel nibh nec maximus. Donec dolor erat, rutrum ut feugiat sed, ornare vitae nunc. Donec massa nisl, bibendum id ultrices sed, accumsan sed dolor.</p>
                                                </div>
                                            </a>
                                            <!-- chat-contacts-item -->
                                            <!-- chat-contacts-item-->
                                            <a class="chat-contacts-item chat-contacts-item_active" href="#">
                                                <div class="dashboard-message-avatar">
                                                    <img src="{{asset('backend/img/user.jpeg')}}" alt="">
                                                    <div class="message-counter">3</div>
                                                </div>
                                                <div class="chat-contacts-item-text">
                                                    <h4>Andy Smith</h4>
                                                    <span>27 Dec 2018 </span>
                                                    <p>Vivamus lobortis vel nibh nec maximus. Donec dolor erat, rutrum ut feugiat sed, ornare vitae nunc. Donec massa nisl, bibendum id ultrices sed, accumsan sed dolor.</p>
                                                </div>
                                            </a>
                                            <!-- chat-contacts-item -->
                                            <!-- chat-contacts-item-->
                                            <a class="chat-contacts-item" href="#">
                                                <div class="dashboard-message-avatar">
                                                    <img src="{{asset('backend/img/user.jpeg')}}" alt="">
                                                    <div class="message-counter">4</div>
                                                </div>
                                                <div class="chat-contacts-item-text">
                                                    <h4>Joe Frick</h4>
                                                    <span>27 Dec 2018 </span>
                                                    <p>Vivamus lobortis vel nibh nec maximus. Donec dolor erat, rutrum ut feugiat sed, ornare vitae nunc. Donec massa nisl, bibendum id ultrices sed, accumsan sed dolor.</p>
                                                </div>
                                            </a>
                                            <!-- chat-contacts-item -->
                                            <!-- chat-contacts-item-->
                                            <a class="chat-contacts-item" href="#">
                                                <div class="dashboard-message-avatar">
                                                    <img src="{{asset('backend/img/user.jpeg')}}" alt="">
                                                </div>
                                                <div class="chat-contacts-item-text">
                                                    <h4>Alise Goovy</h4>
                                                    <span>27 Dec 2018 </span>
                                                    <p>Vivamus lobortis vel nibh nec maximus. Donec dolor erat, rutrum ut feugiat sed, ornare vitae nunc. Donec massa nisl, bibendum id ultrices sed, accumsan sed dolor.</p>
                                                </div>
                                            </a>
                                            <!-- chat-contacts-item -->
                                            <!-- chat-contacts-item-->
                                            <a class="chat-contacts-item" href="#">
                                                <div class="dashboard-message-avatar">
                                                    <img src="{{asset('backend/img/user.jpeg')}}" alt="">
                                                </div>
                                                <div class="chat-contacts-item-text">
                                                    <h4>Freddy Kovalsky</h4>
                                                    <span>27 Dec 2018 </span>
                                                    <p>Vivamus lobortis vel nibh nec maximus. Donec dolor erat, rutrum ut feugiat sed, ornare vitae nunc. Donec massa nisl, bibendum id ultrices sed, accumsan sed dolor.</p>
                                                </div>
                                            </a>
                                            <!-- chat-contacts-item -->
                                            <!-- chat-contacts-item-->
                                            <a class="chat-contacts-item" href="#">
                                                <div class="dashboard-message-avatar">
                                                    <img src="{{asset('backend/img/user.jpeg')}}" alt="">
                                                </div>
                                                <div class="chat-contacts-item-text">
                                                    <h4>Cristiano Olando</h4>
                                                    <span>27 Dec 2018 </span>
                                                    <p>Vivamus lobortis vel nibh nec maximus. Donec dolor erat, rutrum ut feugiat sed, ornare vitae nunc. Donec massa nisl, bibendum id ultrices sed, accumsan sed dolor.</p>
                                                </div>
                                            </a>
                                            <!-- chat-contacts-item -->
                                        </div>
                                    </div>
                                    <!-- chat-contacts end-->
                                </div>
                            </div>
                            <!-- dashboard-list-box end-->
                        </div>
                    </div>
                    <!-- dashboard content end-->
                @endsection

            </div>
        </section>
        <!--  section  end-->
        <div class="limit-box fl-wrap"></div>
    </div>
    <!--content end-->
</div>
<!-- wrapper end-->

@endsection
