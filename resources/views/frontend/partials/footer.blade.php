 <!-- !Footer -->
 <footer id="footer" class="footer solid-bg">


    <!-- !Bottom-bar -->
    <div id="bottom-bar" class="logo-left" role="contentinfo">
        <div class="wf-wrap">
            <div class="wf-container-bottom">

                <div id="branding-bottom">
                    <a class="" href="#!/up"><img class=" preload-me" src="wp-content/uploads/2020/10/thelickinglogonew150.png" srcset="wp-content/uploads/2020/10/thelickinglogonew150.png 150w, wp-content/uploads/2020/10/thelickinglogonew150.png 150w" width="150" height="23"
                            sizes="150px" alt="The Licking" /></a>
                </div>
                <div class="wf-float-right">

                    <div class="bottom-text-block">
                        <p><span class="paint-accent-color">EMAIL:</span> HR@THELICKING.COM</p>
                    </div>
                </div>

            </div>
            <!-- .wf-container-bottom -->
        </div>
        <!-- .wf-wrap -->
    </div>
    <!-- #bottom-bar -->
</footer>
<!-- #footer -->
<a href="#" class="scroll-top"><span class="screen-reader-text">Go to Top</span></a>
