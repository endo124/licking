@extends('frontend.layouts.app')

<link rel="stylesheet" href="{{ asset('frontend/css/dashboard-style.css') }}">

@section('content')
        @include('frontend.partials.profile.header')
        <div class="container">

            @include('frontend.partials.profile.sidebar')

            @yield('profile-content')
        </div>


@endsection
