  <a class="skip-link screen-reader-text" href="#content">Skip to content</a>

        <div class="masthead inline-header right widgets shadow-mobile-header-decoration small-mobile-menu-icon dt-parent-menu-clickable show-sub-menu-on-hover show-device-logo show-mobile-logo" role="banner">

            <div class="top-bar top-bar-empty top-bar-line-hide">
                <div class="top-bar-bg"></div>
                <div class="mini-widgets left-widgets"></div>
                <div class="mini-widgets right-widgets"></div>
            </div>

            <header class="header-bar">

                <div class="branding">
                    <div id="site-title" class="assistive-text">The Licking</div>
                    <div id="site-description" class="assistive-text">Soul Food to the Stars</div>
                    <a class="same-logo" href="#!/up"><img class=" preload-me" src="wp-content/uploads/2020/10/thelickinglogonew250.png" srcset="wp-content/uploads/2020/10/thelickinglogonew250.png 250w, wp-content/uploads/2020/10/thelickinglogonew250.png 250w" width="250" height="39"
                            sizes="250px" alt="The Licking" /><img class="mobile-logo preload-me" src="wp-content/uploads/2020/09/LickingsmallLOGO50.png" srcset="wp-content/uploads/2020/09/LickingsmallLOGO50.png 50w, wp-content/uploads/2020/09/LickingsmallLOGO50.png 50w"
                            width="50" height="50" sizes="50px" alt="The Licking" /></a>
                </div>

                <ul id="primary-menu" class="main-nav underline-decoration l-to-r-line" role="menubar">
                    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-1353 current_page_item menu-item-1919 {{ Request::segment(1) == 'home'?  'act' : '' }} first" role="presentation"><a href='{{ url('/home') }}' data-level='1' role="menuitem"><span class="menu-item-text"><span class="menu-text">HOME</span></span></a></li>
                    <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1376" role="presentation"><a href='https://order.online/store/the-licking-174663' data-level='1' role="menuitem"><span class="menu-item-text"><span class="menu-text">ORDER NOW</span></span></a></li>
                    <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2195  {{ Request::segment(1) == 'vendors'?  'act' : '' }}" role="presentation"><a href='{{ route('vendors.show',$cooks->id) }}' data-level='1' role="menuitem"><span class="menu-item-text"><span class="menu-text">MENU</span></span></a></li>
                    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-2488 has-children" role="presentation"><a href='{{ url('/merch') }}' data-level='1' role="menuitem"><span class="menu-item-text"><span class="menu-text">MERCH</span></span></a>
                        <ul class="sub-nav level-arrows-on" role="menubar">
                            <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2494 first" role="presentation"><a href='cart/index.html' data-level='2' role="menuitem"><span class="menu-item-text"><span class="menu-text">Cart</span></span></a></li>
                            <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2493" role="presentation"><a href='cart/index.html' data-level='2' role="menuitem"><span class="menu-item-text"><span class="menu-text">Checkout</span></span></a></li>
                        </ul>
                    </li>
                </ul>
                <div class="mini-widgets">
                    <div class="soc-ico show-on-desktop in-top-bar-right near-logo-second-switch custom-bg disabled-border border-off hover-accent-bg hover-disabled-border  hover-border-off"><a title="Facebook page opens in new window" href="https://www.facebook.com/pages/category/Soul-Food-Restaurant/MiamiFingalicking-609256772427176/" target="_blank" class="facebook"><span class="soc-font-icon"></span><span class="screen-reader-text">Facebook page opens in new window</span></a>
                        <a title="Twitter page opens in new window" href="https://twitter.com/thelicking" target="_blank" class="twitter"><span class="soc-font-icon"></span><span class="screen-reader-text">Twitter page opens in new window</span></a>
                        <a title="Instagram page opens in new window" href="https://www.instagram.com/thelickingmiamigardens/" target="_blank" class="instagram"><span class="soc-font-icon"></span><span class="screen-reader-text">Instagram page opens in new window</span></a>
                    </div>
                </div>
            </header>

        </div>
        <div class="dt-mobile-header mobile-menu-show-divider">
            <div class="dt-close-mobile-menu-icon">
                <div class="close-line-wrap"><span class="close-line"></span><span class="close-line"></span><span class="close-line"></span></div>
            </div>
            <ul id="mobile-menu" class="mobile-main-nav" role="menubar">
                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-1353 current_page_item menu-item-1919 {{ Request::segment(1) == 'home'?  'act' : '' }}  first" role="presentation"><a href='{{ url('/home') }}' data-level='1' role="menuitem"><span class="menu-item-text"><span class="menu-text">HOME</span></span></a></li>
                <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1376" role="presentation"><a href='https://order.online/store/the-licking-174663' data-level='1' role="menuitem"><span class="menu-item-text"><span class="menu-text">ORDER NOW</span></span></a></li>
                <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2195  {{ Request::segment(1) == 'vendors'?  'act' : '' }}" role="presentation"><a href='{{ route('vendors.show',$cooks->id) }}' data-level='1' role="menuitem"><span class="menu-item-text"><span class="menu-text">MENU</span></span></a></li>
                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-2488 has-children {{ Request::segment(1) == 'merch'?  'act' : '' }}" role="presentation"><a href='{{ url('/merch') }}' data-level='1' role="menuitem"><span class="menu-item-text"><span class="menu-text">MERCH</span></span></a>
                    <ul class="sub-nav level-arrows-on" role="menubar">
                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2494 first" role="presentation"><a href='cart/index.html' data-level='2' role="menuitem"><span class="menu-item-text"><span class="menu-text">Cart</span></span></a></li>
                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2493" role="presentation"><a href='cart/index.html' data-level='2' role="menuitem"><span class="menu-item-text"><span class="menu-text">Checkout</span></span></a></li>
                    </ul>
                </li>
            </ul>
            <div class='mobile-mini-widgets-in-menu'></div>
        </div>

        <style id="the7-page-content-style">
            #main {
                padding-top: 0px;
                padding-bottom: 0px;
            }

            @media screen and (max-width: 778px) {
                #main {
                    padding-top: 0px;
                    padding-bottom: 0px;
                }
            }
        </style>
