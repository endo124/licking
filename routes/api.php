<?php

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([

  'middleware' => 'api',
  'prefix' => 'auth'

], function ($router) {

  Route::post('login', 'AuthController@login');
  Route::post('logout', 'AuthController@logout');
  Route::post('refresh', 'AuthController@refresh');
  Route::post('me', 'AuthController@me');

});



Route::post('payment', 'PaymentApiController@payment')->middleware(['api','auth:api']);
Route::post('paymentIntent', 'PaymentApiController@paymentIntent')->middleware(['api','auth:api']);

Route::group(['middleware' => ['api','lang'], 'namespace' => 'API'], function () {






    Route::get('/dishes/{alng}','DishesNearYouController@index');
    Route::get('/cities/{lang}', 'CityController@index');
    Route::get('/cusines/{lang}', 'CusineController@index');
    Route::get('/categories', 'CategoryController@index');
    Route::get('/categories/{id}/{items}', 'CategoryController@show');
    Route::get('/cusines/{id}/{lang}/{items}', 'CusineController@show');
    Route::get('/cook/{lang}/{id}', 'CookController@show');
    // Route::get('/cooks/{lang}/{items}', 'CookController@index');
    Route::get('/cooks', 'CookController@index');
    Route::get('/dish/{lang}/{id}', 'DishController@show');
    Route::get('/cooksnearyou/{items}/{lang}/{location}', 'CookNearYouController@getaddress');
    Route::post('/search/{lang}','DishController@search');
    Route::get('/filter/{lang}','FilterController@index');
    Route::post('/filter/{lang}','FilterController@show');
    Route::get('/vipcooks/{lang}','VIPCooksController@index');
    Route::get('/offers','AdvertisementController@index');
    Route::get('/cookadv','CookAdvController@index');

    Route::get('/sections', 'SectionController@index');

    Route::get('/privacy/{lang}','TermsController@index');



  //Cook APIs
  Route::post('cook/login','Cook\LoginController@login');
  Route::post('cook/logout', 'Cook\LoginController@logout');


Route::get('order/delivered/{logista_id}','Cook\OrderController@logistadelivered');

Route::post('/token','OrderController@updateFcnToken');



});

Route::post('register', 'API\AuthController@register');
Route::post('login', 'API\AuthController@login');
Route::post('logout', 'API\AuthController@logout');
Route::post('refresh', 'API\AuthController@refresh');
Route::post('forgetpassword', 'API\AuthController@forgetpassword');






Route::group(['middleware' => ['api','lang','auth:api'], 'namespace' => 'API'], function () {
    Route::get('/profile/{lang}/{id}','ProfileController@edit');
    Route::post('/customer-profile/{lang}','ProfileController@update');
    Route::post('/changePass','AuthController@changePass');
    Route::post('/addTocart/{lang}','CartController@store');
    Route::put('/editcart/{lang}/{id}','CartController@update');
    Route::get('/cart/{lang}','CartController@index');
    Route::post('/cart/editqty','CartController@updateqty');
    Route::post('/checkcoupon','ApplyCouponController@check');
    Route::post('/checkout','OrderController@store');
    Route::get('/orders/{lang}','OrderController@index');
    // Route::get('/pastorders/{lang}','OrderController@pastorders');
    Route::post('/get_paid','OrderController@get_paid');
    Route::post('/feast/{lang}','FeastController@store');
    Route::post('/fav/{lang}','FavController@store');
    Route::get('/fav/{lang}','FavController@index');
    Route::post('/address','AddressController@store');
    Route::post('/address/{id}','AddressController@update');
    Route::get('/address/{id}','AddressController@destroy');
    Route::get('/address','AddressController@index');
    Route::get('/branches','CookController@branches');





    Route::get('/paid/{id}','OrderController@paid');





    //Cook APIs
    Route::post('profile/{lang}/','Cook\ProfileController@update');
    Route::get('availability','Cook\ProfileController@availability');
    Route::get('order/accepted/','Cook\OrderController@accepted');
    Route::get('order/delivered/','Cook\OrderController@delivered');
    Route::get('orders/','Cook\OrderController@index');
    Route::get('order/current/','Cook\OrderController@currentorder');
    Route::get('order/accept/{id}','Cook\OrderController@accept');
    Route::get('order/inprogress/{id}','Cook\OrderController@inprogress');
    Route::get('order/shipped/{id}','Cook\OrderController@shipped');
    Route::get('order/reject/{id}','Cook\OrderController@reject');
    Route::get('cookdishes/{lang}','Cook\DishController@index');
    Route::post('dish/','Cook\DishController@status');
    // Route::put('dish/{id}','Cook\DishController@update');
    Route::post('withdraw/{lang}','Cook\WithdrawController@store');
    Route::get('withdraw/','Cook\WithdrawController@index');
    Route::post('withdraw/cancel/{id}/{lang}','Cook\WithdrawController@cancel');
    Route::post('withdraw/delete/{id}/{lang}','Cook\WithdrawController@destroy');

    Route::get('wallet/','Cook\WithdrawController@wallet');




});




