<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

 Route::get('/','Front\HomeController@index')->name('home');
 Route::get('/home','Front\HomeController@home');
 Route::get('/merch','Front\HomeController@merch');
//  Route::get('contact','Front\HomeController@contact')->name('contact');
//  Route::get('faq','Front\HomeController@faq')->name('faq');
//  Route::get('overview','Front\HomeController@overview')->name('overview');
//  Route::get('vendor','Front\VendorController@index')->name('vendor');

// /*******************frontend routes**************************/

Route::group(['namespace'=>'Front'], function () {

    Route::resource('vendors', 'VendorController');
//     // Route::resource('dishes', 'HomeController');

//     // Route::group(['middleware' => ['customer']], function () {
//             // Route::resource('dish', 'DishController');
//             // Route::resource('cooker', 'CookerController');
//             Route::resource('profile', 'CustomerFrontController');
//             Route::get('orders', 'CustomerFrontController@orders');
//             Route::get('wishlist', 'CustomerFrontController@wishlist');
//             Route::get('messages', 'CustomerFrontController@messages');
//             Route::get('change-password', 'CustomerFrontController@changePassword');
//             Route::put('change-password', 'LoginController@changePass')->name('change-password');
//             Route::get('reviews', 'CustomerFrontController@reviews');
//             // Route::resource('orders', 'OrderController');
//             // Route::post('/filter', 'FilterController@filter')->name('filter');

//     // });
});

// // Route::get('/login', 'Front\LoginController@login')->name('login');
// Route::post('/login', 'Front\LoginController@checklogin');
// Route::get('/logout/{id}', 'Front\LoginController@logout');
// Route::get('/register', 'Front\LoginController@register');
// Route::post('/checkregister', 'Front\LoginController@checkregister')->name('checkregister');


// // Route::get('FAQS',function(){
// //     return view('front.faqs');
// // });
// // Route::get('Safety&Trust',function(){
// //     return view('front.safetyandtrust');
// });


// // Route::resource('dish/order', 'Backend\OrderController');


