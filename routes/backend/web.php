<?php

use GuzzleHttp\Middleware;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('backend.login
//     ');
// });

/*******************frontend routes**************************/

Route::get('dashboard/login','Backend\LoginController@login');
Route::post('dashboard/checklogin','Backend\LoginController@checklogin');
Route::post('dashboard/logout','Backend\LoginController@logout');
Route::group(
    [
        'prefix' => LaravelLocalization::setLocale(),
        'middleware' => [ 'localeSessionRedirect', 'localizationRedirect', 'localeViewPath' ,'admin']
    ], function(){



        Route::group(['namespace'=>'Backend','prefix'=>'dashboard'], function () {


            Route::resource('/', 'HomeController');

            Route::resource('/admin', 'AdminController');

            Route::get('/changepass', 'AdminController@changepass');
            Route::put('/changepass', 'AdminController@changePassword')->name('changePassword');

            Route::resource('/cusine', 'CusineController');

            Route::resource('/section', 'SectionController');

            Route::resource('/addons', 'AddonsController');
            Route::resource('/addons_section', 'AddonSectionController');

            Route::resource('/category', 'CategoryController');

            Route::resource('/cook', 'CookController');
            Route::get('/cookaccount/{id}', 'CookController@editcook');

            Route::resource('/dish', 'DishController');

            Route::resource('/discount', 'DiscountController');

            Route::resource('/allergen', 'AllergenController');

            Route::resource('/order', 'OrderController');
            Route::get('/order/assign/{id}', 'OrderController@assign');
            Route::get('/order/orderdetails/{id}','OrderController@orderdetails');

            Route::resource('/customer', 'CustomerController');

            Route::resource('/city', 'CityController');

            Route::resource('/setting', 'SettingController');

            Route::resource('/advertisement', 'AdvertisementController');
            Route::get('/advertisement/details/{name}', 'AdvertisementController@details');

            Route::resource('/cookAdv', 'CookAdvController');
            Route::resource('/withdraw', 'WithdrawController');
            Route::get('/withdraw/cancel/{id}', 'WithdrawController@cancel')->name('withdraw.cancel');
            Route::get('/withdraw/decline/{id}', 'WithdrawController@decline')->name('withdraw.decline');
            Route::get('/withdraw/accept/{id}', 'WithdrawController@accept')->name('withdraw.accept');


            Route::get('/customer/vip/{id}', 'CustomerController@vip_customer');
            Route::get('/vipCustomers', 'CustomerController@vip');
            Route::get('/customer/reset/{id}', 'CustomerController@vip_reset');

            Route::get('/cook/reset/{id}', 'CookController@vip_reset');
            Route::get('/cook/vip/{id}', 'CookController@vip_cook');
            Route::get('/vipCooks', 'CookController@vip');


            Route::resource('/drivers', 'DriverController');
            Route::resource('/tracking-drivers', 'TrackingDriverController');

            Route::post('/getDataDriver', 'DriverController@getDataDriver');

            Route::get('/order/accept/{id}', 'OrderController@accept');
            Route::get('/order/reject/{id}', 'OrderController@reject');
            Route::get('/order/cancel/{id}', 'OrderController@cancel');
            Route::put('/order/updatedata/{id}', 'OrderController@updatedata')->name('order.updatedata');

            Route::get('/cook/active/{id}', 'CookController@active');
            Route::get('/cook/deactive/{id}', 'CookController@deactive');
            Route::resource('account', 'AccountController');
            Route::resource('reports', 'ReportController');


            Route::resource('branches', 'BranchController');

            Route::resource('delivery', 'DeliveryController');
            Route::resource('feast', 'FeastController');

            Route::resource('support', 'SupportController');

            Route::get('/order/customerorders/{id}', 'OrderController@customerorders');


        });

    });





