<?php

namespace App\Http\Middleware;

use Closure;

class admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {


        // if (!$request->secure() && \App::environment() === 'production') {
        //     return redirect()->secure($request->getRequestUri());
        // }
        if(auth()->guard('admin')->user()){
                return $next($request);

        }
        else{
            return redirect()->to('dashboard/login');
        }
    }
}
