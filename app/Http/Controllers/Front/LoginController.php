<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\City;
use App\Models\Role;
use App\Models\User;
use App\Models\UserTranslation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use GuzzleHttp\Client;

class LoginController extends Controller
{


        // public function login()
        // {
        //     return view('front/login');
        // }


    public function checklogin(Request $request)
    {
        $validator=\Validator::make($request->all(), [
            'email' => 'required',
            'password' => 'required',
         ]);
        if ($validator->fails()) {
        // return response()->json(['error'=>$validator->errors()], 401);
        return back();

        }

        //     $validatedData = $request->validate([
        //         'email' => 'required',
        //         'password' => 'required',
        //     ]);
        //    if(Auth::guard('customer')->attempt(['email' =>  $request->email, 'password' => $request->password])){
        //          if(auth()->guard('customer')->user()->roles[0]->name == 'customer'){
        //             // return redirect()->route('frontend.index');
        //             return back();

        //         }else{
        //             auth()->guard('customer')->logout();
        //             // return redirect()->route('login');
        //             return back();

        //         }
        //     }

                // $client = new Client();
                // $login_request = $client->request('POST', 'http://thejetter.com/api/login', [
                //     'headers' => [
                //         'Content-Type' => 'application/json'
                //     ],

                //     'json' => [
                //         "email"=>$request->email,
                //         "password" =>  $request->password,
                //     ],
                // ]);
                // $customer=json_decode($login_request);

                $body=[
                    "email"=>$request->email,
                    "password" =>  $request->password,
                ];
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, 'http://thejetter.com/api/login');
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($body));
                curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                $server_output = curl_exec($ch);
                $customer=json_decode($server_output);

                $user= Auth::guard('customer')->attempt(['email' => $request->email, 'password' => $request->password]);
                // $user=\Auth::guard('customer')->login( $customer->customer);

                $user=User::find($customer->customer->id);

                $user->update([
                    'token'=>$customer->access_token
                ]);
            // }



            return back();


    }

    public function logout($id){

            // $user=User::find($id);

        auth()->guard('customer')->logout();

        return redirect()->to('/home');
    }


    // public function register()
    // {
    //     $cities=City::all();
    //     return view('front.register',compact('cities'));
    // }



    public function checkregister(Request $request)
    {

        $validator=\Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|unique:users',
            // 'phone' => 'required|unique:users|regex:/(01)[0-9]{9}/',
            'phone' => 'required|unique:users',
            'password' => 'required|confirmed',
         ]);
        if ($validator->fails()) {
        return back();

        // return response()->json(['error'=>$validator->errors()], 401);
        }
        // try{
//           $client = new Client();
//             $login_request = $client->request('POST', 'http://thejetter.com/api/register', [
//                 'headers' => [
//                     'Content-Type' => 'application/json'
//                 ],

//                 'json' => [
//                     "name"=>$request->name,
//                     "email"=>$request->email,
//                     "phone"=>$request->phone,
//                     "password" =>  $request->password,
//                     "password_confirmation" =>  $request->password_confirmation,
//                 ],
//             ]);

        $body = [

            "name"=>$request->name,
            "email"=>$request->email,
            "phone"=>$request->phone,
            "password" =>  $request->password,
            "password_confirmation" =>  $request->password_confirmation,
        ];

        $ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, 'http://thejetter.com/api/register');
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($body));
		curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec($ch);
        $customer=json_decode($server_output);
        if(isset($customer->customer)){
            $user= Auth::guard('customer')->attempt(['email' => $request->email, 'password' => $request->password]);
            $user=User::find($customer->customer->id);
            $user->update([
                'token'=>$customer->access_token
            ]);
        }


        // }

        // catch(\Exception $e)
        // {
        //     return back()->with(['errors' => $e]);
        // }

        return back();



        }


    public function changePass( Request $request)
    {

            $validator=\Validator::make($request->all(), [
                'current_password' => 'required',
                'password' => 'required|confirmed',
            ]);
            if ($validator->fails()) {
                return back();

            // return response()->json(['error'=>$validator->errors()], 401);
            }
            // try{
            $customer=User::find(auth('customer')->user()->id);

            $headers = [
                'Authorization' => 'Bearer '.$customer->token
            ];
            $data=[
                'current_password' => $request->current_password,
                'password' => $request->password,
                'password_confirmation' => $request->password_confirmation,
            ];
            $client = new Client();
            $res = $client->request('POST', 'http://thejetter.com/api/changePass',[
                    'json'=>$data,
                    'headers'=>$headers,
            ]);


            $data=json_decode($res->getBody());
        // }

        // catch(\Exception $e)
        // {
        //     return back()->with(['errors' => $e]);
        // }

        return back();

        // $request->validate([
        //     'current_password' => 'required',
        //     'password' => 'required|confirmed',
        //  ]);
        // // if ($validator->fails()) {
        // // return response()->json(['error'=>$validator->errors()], 401);

        // // }


        // $customer=User::find(auth('customer')->user()->id);
        // $current_password=$customer->password;
        // if(Hash::check($request->current_password, $current_password)){
        //     $customer->update([
        //         'password'=>Hash::make($request->password)
        //     ]);
        //     // return $this->returnSuccessMessage('password edit successfully');
        // return back();


        // }
        // else{
        //     $error = array('current_password' => 'Please enter correct current password');
        //     // return response()->json(array('error' => $error), 400);
        // return back();

        // }
    }
}



