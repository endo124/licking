<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\UserTranslation;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class CustomerFrontController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('frontend.customer-profile');

    }

    public function messages(){

        return view('frontend.messages');
    }

    public function changePassword(){

        return view('frontend.change-password');
    }

    public function reviews(){

        return view('frontend.reviews');
    }

    public function orders()
    {
        return view('frontend.customer-profile');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $customer=User::find(auth('customer')->user()->id);

        $validator=\Validator::make($request->all(), [
            'name'=>'required',
            // 'name_en'=>'required',
            'email'=>'required|unique:users,id,'.$customer->id,
            'phone'=>'required|unique:users,id,'.$customer->id,
            // 'password'=>'required|confirmed'
          ]);
        if ($validator->fails()) {
            // return response()->json(['error'=>$validator->errors()], 401);
            return back();

        }
    //     $customer=User::find($id);

    //     $validate=$request->validate([
    //         'name'=>'required',
    //         'email'=>'required|unique:users,email,'.$id,
    //         'phone'=>'required|regex:/(01)[0-9]{9}/|unique:users,id,'.$customer->id,
    //         // 'password'=>'required|confirmed'
    //     ]);
    //     $customer=User::find($id);


    //     if ($request->file('image')) {

    //         $ext=$request->file('image')->getClientOriginalExtension();
    //         $image_name=time().'.'.$ext;
    //         $path='backend/images/customers';
    //         $request->file('image')->move($path,$image_name);
    //         if ($customer->images) {
    //             \Storage::disk('customers')->delete($customer->images);
    //         }
    //     }
    //     else{
    //         $image_name=$customer->images;
    //     }
    //     $customer->update([
    //         'phone'=>$request->phone,
    //         'email'=>$request->email,
    //         'images'=>$image_name,

    //     ]);

    //     $customer_en=UserTranslation::where('user_id',auth()->guard('customer')->user()->id)->where('locale','en')->first();
    //     $customer_en->update([
    //         'name'=>$request->name
    //     ]);
    //     $customer_ar=UserTranslation::where('user_id',auth()->guard('customer')->user()->id)->where('locale','ar')->update([
    //         'name'=>$request->name
    //     ]);

    //     return back();
    // try{

        $headers = [
            'Authorization' => 'Bearer '.$customer->token
        ];
        $data=[
            [
                'name'     => 'phone',
                'contents' => $request->phone,
            ],
            [
                'name'     => 'email',
                'contents' =>$request->email,
            ],
            [
                'name'     => 'name',
                'contents' =>$request->name,
            ]
        ];

        if(isset($request->image)){
            $img=[
                'name'     => 'image',
                'contents' => fopen($request->image ,'r'),
            ];
            array_push($data,$img);
        }
        $client = new Client();
        $res = $client->request('POST', 'http://thejetter.com/api/customer-profile/en',[

                'multipart' =>$data,
                'headers'=>$headers,
        ]);

        $data=json_decode($res->getBody());
    // }

    // catch(\Exception $e)
    // {
    //     return back()->with(['errors' => $e]);
    // }

    return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
