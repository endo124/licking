<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\Addons;
use App\Models\Cusine;
use App\Models\Dish;
use App\Models\User;
use Illuminate\Http\Request;
use GuzzleHttp\Client;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $client = new Client();

        // $alldishes = $client->request('GET', 'http://127.0.0.1:8000/api/dishes/ar/30.099269,31.3681253' );
        // dd($alldishes);
        // $users=User::whereHas('roles',function($q){
        //          $q->where('name','cook');
        // })->get();
        // $dishes=Dish::all();
        // $cusines=Cusine::all();
        // $addons=Addons::all();
        return view('frontend.index');
    }

    public function home(){
        $client = new Client();
        $cooks = $client->request('GET', 'https://thelicking.momentum-host.com/api/cooks' );
        $cooks = json_decode($cooks->getBody());
        $cooks=array_slice($cooks->cooks, 0, 8);
        // foreach($cooks as $cook){
        //     foreach($cook->dishes as $dish){
        //         dd($dish->cusines[0]->name);
        //         $cusine=$dish->cusines->name;
        //     }
        // }
        $cusines = $client->request('GET', 'https://thelicking.momentum-host.com/api/cusines/en' );
        $cusines = json_decode($cusines->getBody());
        $cooks=$cooks[0];
        return view('frontend.home',compact('cooks','cusines'));

    }

    public function contact()
    {
        return view('frontend.contact');
    }
    public function faq()
    {
        return view('frontend.faq');
    }
    public function overview()
    {
        return view('frontend.overview');
    }

    public function merch(){
        $client = new Client();
        $cooks = $client->request('GET', 'https://thelicking.momentum-host.com/api/cooks' );
        $cooks = json_decode($cooks->getBody());
        $cooks=array_slice($cooks->cooks, 0, 8);
        // foreach($cooks as $cook){
        //     foreach($cook->dishes as $dish){
        //         dd($dish->cusines[0]->name);
        //         $cusine=$dish->cusines->name;
        //     }
        // }
        $cusines = $client->request('GET', 'https://thelicking.momentum-host.com/api/cusines/en' );
        $cusines = json_decode($cusines->getBody());
        $cooks=$cooks[0];
        // return view('frontend.home',compact('cooks','cusines'));
        return view('frontend.merch',compact('cooks','cusines'));

    }
    // public function vendor()
    // {
    // }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
