<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Dish;
use App\Models\User;
use Illuminate\Http\Request;
use App\Traits\GeneralTrait;

class CookController extends Controller
{
    use GeneralTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cooks=User::with('dishes.cusines')->with('address')
        ->whereRoleIs('cook')->get();
        return $this->returnData('cooks',$cooks);

    }
    public function branches()
    {
        $cooks=User::with('address')
        ->whereRoleIs('cook')->whereNotNull('parent')->get();
        return $this->returnData('cooks',$cooks);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($lang,$id)
    {
        $cook=User::whereRoleIs('cook')
        ->where('id',$id)
        ->with('dishes','dishes.sections','sections','address')
        ->first();


        // sections = \DB::table('addonsection')
        // ->rightJoin('addonsection_translations' ,'addonsection.id' , '=' , 'addonsection_translations.addon_section_id')
        // ->where('addonsection_translations.locale',$lang)

        // ->rightJoin('addons' ,'addonsection.id' , '=' , 'addons.addonsection_id')
        // // ->whereIn('addonsection.id',$section)
        // ->rightJoin('addon_cook' ,'addons.id' , '=' , 'addon_cook.addon_id')

        // ->rightJoin('addons_translations' ,'addons_translations.addon_id' , '=' , 'addons.id')
        // ->where('addons_translations.locale',$lang)

        // ->rightJoin('addon_dish' ,'addons.id' , '=' , 'addon_dish.addon_id')
        // ->select('addon_dish.min','addon_dish.max','addon_dish.condition','addon_cook.price As price ','addons.addonsection_id','addons.id','addonsection_translations.name As section_name','addons_translations.name As addon_name')

        // ->get();
        // return $sections;





        return response()->json($cook,200);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
