<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Addons;
use App\Models\Dish;
use App\Models\Order;
use App\Models\User;
use Validator;
use Illuminate\Http\Request;
use App\Traits\GeneralTrait;

class CartController extends Controller
{
    use GeneralTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cart=[];
        $order=Order::where('user_id',auth('api_customer')->user()->id)
        ->whereNull('status')
        ->first();
        if(isset($order)){
            $orderEntry=[];
            $total_price=0;

                    foreach($order['orderEntry'] as $item){

                        $addons=[];


                        $dish=Dish::find($item['id']);
                        $dish['size']=$item['size'];
                        $dish['price']=$item['price'];
                        $dish['qty']=$item['qty'];
                        $total_price+=$item['price'];
                        foreach ($item['addons'] as $addon) {

                            $addon=Addons::find($addon['id']);

                            array_push($addons,$addon);
                        }
                        $dish['addons'] = $addons;

                        array_push($orderEntry,$dish);

                    }

                    $order['orderEntry']=$orderEntry;
                    $order['total_price']=$total_price;
                    array_push($cart,$order);


        }



        if(isset($order['cook_id'])){


            $cook=User::find($order['cook_id']);
            // dd($cook);
            $address=$cook->address[0];
            return  response()->json([
                'status' => true,
                'errNum' => "S000",
                'msg' => "",
                'cart' => $cart,
                'cook'=>['taxes_type'=>$cook->taxes_type,'amount'=>$cook->taxes,'address'=>$address]
            ]);
        }else{
            return  response()->json([
                'status' => true,
                'errNum' => "S000",
                'msg' => "",
                'cart' => $cart,
                'cook'=>null
            ]);
        }


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $data=array();
        $item=$request->data;

        // foreach ($request->data as  $item) {

            // $validator=Validator::make($item, [
            //     'qty'=>'required',
            //     'size'=>'required'
            //   ]);
            // if ($validator->fails()) {
            // return response()->json(['error'=>$validator->errors()], 401);
            // }
            $orderEntry=array();
            $dish=Dish::find($item['id']);
            $orderEntry['id']=$item['id'];
            $orderEntry['qty']=$item['qty'];
            $orderEntry['size']=$item['size'];

            if(isset($item['notes'])){
                $orderEntry['notes']=$item['notes'];
            }
            $orderEntry['addons']=$item['addons'];

            $orderEntry['name']=$dish->name;
            $orderEntry['price']=$item['price']*$item['qty'];
            $orderEntry['section']=$dish->sections->name;
            $orderEntry['images']=$dish->images;
            array_push($data,$orderEntry);


        // }

        $cart=Order::where(['user_id'=>auth()->guard('api_customer')->user()->id,'status'=>null])->first();

        if(isset($cart)){
            if($dish->user_id == $cart->cook_id){
                $cartEntry=$cart->orderEntry;
                array_push( $cartEntry,$orderEntry);
                $cart->update([
                    'orderEntry'=>$cartEntry,
                ]);
            }elseif ($dish->user_id != $cart->cook_id && $request->force == 1) {
                $cart->update([
                    'cook_id'=>$dish->user_id,
                    // 'date'=>$request->date,
                    // 'time'=>$request->time,
                    'orderEntry'=>$data,
                    // 'address'=>$request->address,
                ]);
                return $this->returnSuccessMessage('cart updated successfully');

            }else{

                return $this->returnSuccessMessage('you can not add to cart from different Restaurant');

            }

        }else{
            $order=Order::create([
                'user_id'=>auth()->guard('api_customer')->user()->id,
                'cook_id'=>$dish->user_id,
                // 'date'=>$request->date,
                // 'time'=>$request->time,
                'orderEntry'=>$data,
                // 'address'=>$request->address,
                'soldout'=>$request->soldout

            ]);

        }

        return $this->returnSuccessMessage('successfully added to cart');
    }


    public function updateqty(Request $request){

        $cart=Order::where(['user_id'=>auth()->guard('api_customer')->user()->id,'status'=>null])->first();
            if(isset($cart)){
                $orderEntry=[];

                foreach($cart['orderEntry'] as $index=>$item){
                    if($item['id'] != $request->id){
                        array_push($orderEntry,$item);
                    }else{
                        if($request->qty != 0 && $request->index == $index){
                            $dish=Dish::find($item['id']);

                            if( $item['size'] == 'small'){
                                $item['price']=$request->qty*$dish['portions_price'][0];

                            }elseif($item['size'] == 'medium'){
                                // dd($request->qty,$dish['portions_price'][1]);
                                $item['price']=$request->qty*$dish['portions_price'][1];

                            }else{
                                $item['price']=$request->qty*$dish['portions_price'][2];

                            }
                            $item['qty']=$request->qty;

                            array_push($orderEntry,$item);
                        }elseif($request->qty == 0 && $request->index == $index){
                            continue;
                        }
                        else{
                            array_push($orderEntry,$item);
                        }

                    }

                }
            }
            if( $orderEntry==[]){
                $cart->delete();
            }else{
                $cart->update([
                    'orderEntry'=>$orderEntry
                ]);
            }


            // $cart->save();
            return $this->returnSuccessMessage('cart updated successfully');


        }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $lang,$id)
    {
    //     $data=array();

    //     foreach ($request->all() as  $item) {
    //     $validator=Validator::make($item, [
    //         'qty'=>'required',
    //         'size'=>'required'
    //       ]);
    //     dd('gjg');

    //     if ($validator->fails()) {
    //     return response()->json(['error'=>$validator->errors()], 401);
    //     }
    //     $orderEntry=array();
    //     $dish=Dish::find($id);
    //     $orderEntry['qty']=$item->qty;
    //     $orderEntry['size']=$item->size;
    //     if($item->notes){
    //         $orderEntry['notes']=$item->notes;
    //     }
    //     $orderEntry['name']=$dish->name;
    //     $orderEntry['price']=$dish->price*$item->qty;
    //     $orderEntry['section']=$dish->sections->name;
    //     $orderEntry['images']=$dish->images;
    //     array_push($data,$orderEntry);
    // }
    // dd($id);

    // $order=Order::find($id);
    // $order->update([
    //     // 'user_id'=>auth()->guard('api_customer')->user()->id,
    //     // 'date'=>$request->date,
    //     // 'time'=>$request->time,
    //     'orderEntry'=>json_encode($data),
    //     // 'address'=>$request->address,
    // ]);
    //     return $this->returnSuccessMessage('successfully edit  cart');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
