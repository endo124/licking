<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Dish;
use App\Models\DishTranslation;
use App\Models\AddonSection;
use App\Models\Section;
use Illuminate\Http\Request;
use App\Traits\GeneralTrait;

class DishController extends Controller
{
    use GeneralTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function search(Request $request ,$lang)
    {
        $dishes_id=DishTranslation::where('name', 'LIKE', "%$request->dish%")->pluck('dish_id');
        $dishes=Dish::whereIn('id',$dishes_id)
        ->with('cusines')
        ->with('sections')
        ->with('addons.users')
        ->with('addonsections')
        ->get();
        return $this->returnData('dishes',$dishes);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($lang,$id)
    {
        $section=[];

        $dish=Dish::where('id',$id)
        ->with('sections')
        ->with('cusines')
        ->with('addonsections')
        ->with('addonsections.addons')
        ->first();
        // foreach ($dish->addons as  $addon) {
        //     $section_data=AddonSection::where('id',$addon->addonsection_id)->first();
        //     $section_name=$section_data->name;
        //     array_push($section,$section_name);
        // }
        // $section= array_unique($section);

        // $sections = \DB::table('addonsection')
        // ->rightJoin('addonsection_translations' ,'addonsection.id' , '=' , 'addonsection_translations.addon_section_id')
        // ->where('addonsection_translations.locale',$lang)

        // ->rightJoin('addons' ,'addonsection.id' , '=' , 'addons.addonsection_id')
        // // ->whereIn('addonsection.id',$section)
        // ->rightJoin('addon_cook' ,'addons.id' , '=' , 'addon_cook.addon_id')

        // ->rightJoin('addons_translations' ,'addons_translations.addon_id' , '=' , 'addons.id')
        // ->where('addons_translations.locale',$lang)

        // ->rightJoin('addon_dish' ,'addons.id' , '=' , 'addon_dish.addon_id')
        // ->select('addon_dish.min','addon_dish.max','addon_dish.condition','addon_cook.price As price','addons.addonsection_id','addons.id','addonsection_translations.name As section_name','addons_translations.name As addon_name')

        // ->get();

        // $dish['addonsection']=$sections;

        return response()->json(["dish"=>[$dish ]],200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
