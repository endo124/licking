<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\CityTranslation;
use App\Models\Cusine;
use App\Models\Dish;
use App\Models\User;
use Illuminate\Http\Request;
use App\Traits\GeneralTrait;

class DishesNearYouController extends Controller
{
    use GeneralTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    // public function index($lang,$governorate)
    public function index($lang)
    {
        // $city=CityTranslation::where('name',$governorate)->first();
        // if(isset($city)){
            $cooks=User::with('dishes')
            ->whereRoleIs('cook')
            // ->where('city_id',$city->city_id)
            ->where('active',1)
            ->pluck('id');
            $dishes=Dish::with('users')
            ->with('cusines')
            ->with('sections')
            // ->with('addons.users')
            ->with('addonsections')
            ->with('addonsections.addons')
            ->whereIn('user_id',$cooks)
            ->get();

        // $sections = \DB::table('addonsection')
        // ->rightJoin('addonsection_translations' ,'addonsection.id' , '=' , 'addonsection_translations.addon_section_id')
        // ->where('addonsection_translations.locale',$lang)

        // ->rightJoin('addons' ,'addonsection.id' , '=' , 'addons.addonsection_id')
        // // ->whereIn('addonsection.id',$section)
        // ->rightJoin('addon_cook' ,'addons.id' , '=' , 'addon_cook.addon_id')

        // ->rightJoin('addons_translations' ,'addons_translations.addon_id' , '=' , 'addons.id')
        // ->where('addons_translations.locale',$lang)

        // ->rightJoin('addon_dish' ,'addons.id' , '=' , 'addon_dish.addon_id')
        // ->select('addon_dish.min','addon_dish.max','addon_dish.condition','addon_cook.price As price ','addons.addonsection_id','addons.id','addonsection_translations.name As section_name','addons_translations.name As addon_name')

        // ->get();
        // return $sections;


            return $this->returnData('dishes',$dishes);
        // }else{
        //     return $this->returnData('dishes',[]);
        // }

    }

    // function getaddress($lang,$location)
    // {

    //    $url = 'https://maps.google.com/maps/api/geocode/json?latlng='.$location.'&key=AIzaSyArUJhAM_MY5imJh7g6l6-rgcBOMyOS63s';
    //    $json = @file_get_contents($url);
    //    $data=json_decode($json);
    //    $status = $data->status;

    //    if($status=="OK")
    //    {
    //         $address= $data->results;

    //         $address_components= $address[0]->address_components;
    //         for ($i = 0; $i < count($address_components); $i++) {
    //             $address= $data->results;
    //             $address_components= $address[0]->address_components;
    //             $addressType = $address_components[$i]->types[0];
    //             if ($addressType == 'administrative_area_level_1') {
    //                 $governorate= $address[0]->address_components[$i]->short_name;
    //             }
    //         }
    //     }
    //    return $this->index($lang,$governorate);
    // }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
