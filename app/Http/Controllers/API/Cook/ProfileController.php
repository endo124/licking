<?php

namespace App\Http\Controllers\API\Cook;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule as Rule;
use App\Models\User;
use App\Models\Address;
use App\Models\UserTranslation;
use Validator;
use App\Traits\GeneralTrait;

class ProfileController extends Controller
{
    use GeneralTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$en)
    {
        $id=auth()->guard('api_cook')->user()->id;
        $cook=User::find($id);
        if(auth()->guard('api_cook')->user()->roles[0]->name =='cook'){//update from cook profile

            $validator=Validator::make($request->all(), [
                'phone'=>'required|regex:/(01)[0-9]{9}/|unique:users,id,'.$cook->id,
                'email'=>'required|unique:users,email,'.$cook->id,
                'address'=>'required',
                // 'work_from'=>'required',
                // 'work_to'=>'required',
                'name_ar'=>'required',
                'name_en'=>'required',
                // 'birth_date'=>'required',
                // 'info'=>'required',
            ]);
            if(isset($request->work_from)){
                $cook->update([
                'work_from'=>$request->work_from,
                'work_to'=>$request->work_to,
                ]);
            }
            if ($validator->fails()) {

            return response()->json(['error'=>$validator->errors()], 401);

            }


            $cook=User::find($id);
            if ($request->file('profile_image')) {

                $ext=$request->file('profile_image')->getClientOriginalExtension();
                $image_name=time().'.'.$ext;
                $path='backend/img/cook';
                $request->file('profile_image')->move($path,$image_name);
                if ($cook->images != '1606647695.jpg') {
                    Storage::disk('cooks')->delete($cook->images);
                }
            }
            else{
                $image_name=$cook->images;
            }

            if($request->availability == null){
                $request->availability = 0;
            }else{
                $request->availability = 1;
            }

            // dd($request->work_from);
            // dd($request->email,$cook->email);
            // $from=json_encode($request->work_from);
            // $to=json_encode($request->work_to);
            // dd($request->work_from);

            $cook->update([
                'availability'=>$request->availability,
                'name'=>$request->name,
                'phone'=>$request->phone,
                'email'=>$request->email,
                'info'=>$request->info,
                'images'=>$image_name,
                'date_of_birth'=>$request->birth_date,
                'work_from'=>$request->work_from,
                'work_to'=>$request->work_to,
            ]);

            $cook_name_en=UserTranslation::where('user_id',auth()->guard('api_cook')->user()->id)->where('locale','en')->first();
            $cook_name_en->update([
                'name'=>$request->name_en
            ]);
            $trans=UserTranslation::where('user_id',auth()->guard('api_cook')->user()->id)->where('locale','ar')->update([
                'name'=>$request->name_ar
            ]);

            $cook=User::find(auth()->guard('api_cook')->user()->id);
            $address=$cook->address;
            if($request->coordinates == null){
                $request->coordinates='30.099269,31.3681253';
            }
            if(count($address) > 0 ){
                $address[0]->update([
                'user_id'=>$cook->id,
                'address'=>$request->address,
                'coordinates'=>$request->coordinates,
                ]);
            } else{

                $address=Address::create([
                    'user_id'=>$cook->id,
                    'address'=>$request->address,
                    'coordinates'=>$request->coordinates,
                ]);
            }
             return $this->returnData('cook',$cook);

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
