<?php

namespace App\Http\Controllers\API\Cook;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Order;
use App\Models\User;
use App\Traits\GeneralTrait;

class OrderController extends Controller
{
    use GeneralTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $orders=Order::where('cook_id',auth()->guard('api_cook')->user()->id)
            ->whereNotNull('status')
            ->where('status', '!=', 1)
            ->orderBy('id', 'DESC')
            ->get();
        return $this->returnData('orders',$orders);
    }

    public function currentorder(){

        $currentActivities=Order::where('cook_id',auth()->guard('api_cook')->user()->id)
        ->where('status',1)
        ->orderBy('id', 'DESC')
        ->get();
        return $this->returnData('currentorder',$currentActivities);

    }

    public function accept($id){

        $order= Order::where('id',$id)->first();
       if($order->status == 1){
        $order->update([
            'status'=>2
        ]);
        $admin=User::first();
        $admin->update(['revenue'=>doubleval($admin->revenue) + $order->total_price + doubleval($order->delivery_fees) ]);
        return $this->returnSuccessMessage('order accepted successfully');

       }

    }

    public function inprogress($id){

        $order= Order::where('id',$id)->first();
       if($order->status == 2){
        $order->update([
            'status'=>3
        ]);
        return $this->returnSuccessMessage('order now in progress ');

       }

    }
    public function shipped($id){
        $order= Order::where('id',$id)->first();
        $order->update([
            'status'=>4
        ]);
        return $this->returnSuccessMessage('order rejected successfully');

    }

    public function reject($id){
        $order= Order::where('id',$id)->first();
        $order->update([
            'status'=>0
        ]);
        return $this->returnSuccessMessage('order rejected successfully');

    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
