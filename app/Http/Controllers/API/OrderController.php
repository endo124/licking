<?php

namespace App\Http\Controllers\API;

use App\Events\NewOrder;
use App\Http\Controllers\Controller;
use App\Models\Addons;
use App\Models\AddonsTranslation;
use App\Models\Address;
use App\Models\DeliveryCharges;
use App\Models\Discount;
use App\Models\Dish;
use App\Models\SectionTranslation;
use App\Models\DishTranslation;
use App\Models\Order;
use App\Models\Setting;
use App\Models\User;
use Illuminate\Http\Request;
use Validator;
use App\Traits\GeneralTrait;
use DateTime;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
use Kreait\Firebase\Database;
use GuzzleHttp\Client;
use Illuminate\Console\Scheduling\Event;

class OrderController extends Controller
{
    use GeneralTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($lang)// current orders
    {
        // $factory = (new Factory)
        // ->withServiceAccount(__DIR__.'/logista-b796f05ac902.json');
        // $database = $factory->createDatabase();


        // $getUser = $database
        // ->getReference('users')
        // ->orderByChild('email')
        // ->equalTo('hesham.zaki.99@hotmail.com')
        // ->getValue();
        // $user=array_keys($getUser);
        // $orders = $database
        // ->getReference('/users/'.$user[0].'/tasks')
        // ->getSnapshot()
        // ->getValue();
        // $logistaorders=Order::whereIn('logista_order_id',array_keys($orders))->get();
        // dd($logistaorders);

        $orders=Order::where('orders.user_id',auth('api_customer')->user()->id)
        ->WhereNotNull('status')
        // ->where('status' , '!=' , '5')
        ->rightJoin('user_translations' ,'orders.cook_id' , '=' , 'user_translations.user_id')
        ->where('locale',$lang)
        ->rightJoin('users' ,'orders.cook_id' , '=' , 'users.id')
        ->select('orders.id','orders.user_id','orders.cook_id','date','time','orderEntry','total_price','address','payment_method','payment_status','delivery_type','status','assign','name','images','paid')
        // ->select('orders.id','orders.user_id','orders.cook_id','date','time','orderEntry','total_price','delivery_fees','address','payment_method','payment_status','delivery_type','status','assign','name','images','taxes_type','taxes')
        ->get();

        return $this->returnData('orders',$orders);

    }

    // public function pastorders($lang){
    //     $orders=Order::where('orders.user_id',auth('api_customer')->user()->id)
    //     ->WhereNotNull('status')
    //     ->where('status' , '==' , '5')
    //     ->rightJoin('user_translations' ,'orders.cook_id' , '=' , 'user_translations.user_id')
    //     ->where('locale',$lang)
    //     ->rightJoin('users' ,'orders.cook_id' , '=' , 'users.id')
    //     ->select('orders.id','orders.user_id','orders.cook_id','date','time','orderEntry','total_price','delivery_fees','address','payment_method','payment_status','delivery_type','status','assign','name','images','taxes_type','taxes')
    //     ->get();

    //     return $this->returnData('orders',$orders);
    // }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $customer=User::where('id',auth('api_customer')->user()->id)->first();
        if($customer->active !=1 ){
            return response()->json(['error' => "sorry you can't order your account is inactive"], 401);
        }

        $allow = 0;
        $time= date("H:i", strtotime($request->time));
        $date=date("Y-m-d", strtotime($request->date));

        $cook=User::where('id',$request->cook_id)->first();//cook work from sat and sat is index (4)date('w')+1

        $from=(json_decode($cook->work_from));
        $from=strtotime($from[date('w')+1]);
        $to=(json_decode($cook->work_to));
        $to=strtotime($to[date('w')+1]);
        $current=((time()));
        if ($to < $from) {
           $to=strtotime('+1 day', $to);
        }
        if($current >= $from && $current <= $to){
            $allow = 1;

        }

        if($cook->availability != 1 && $allow != 1){
            return response()->json(['error' => true, 'msg' => 'Vendor not available now']);
       }
        //    dd($cook->availability != 1 , $allow != 1);
        $address=Address::find($request->address);
        if(isset($address->phone)){
            $customer_phone=$address->phone;
        }else{
            $customer_phone=auth('api_customer')->user()->phone;
        }
        // return($address);



        $coordinates=explode(",",$cook->address[0]->coordinates);


        if($coordinates[0] == ''){
            return response()->json(['error' => true, 'msg' => 'Vendor not assign his location yet']);
       }

        $order=Order::where(['user_id'=>auth()->guard('api_customer')->user()->id,'status'=>null])->first();
        if( ! isset($order)){
            return response()->json(['error' => true, 'msg' => 'there is no dishes in cart']);
        }
        $details=$order->orderEntry;
        $orders=[];
        foreach ( $details as $or) {
            $item=[];
            $item['id']=(string)$or['id'];
            // $item['name_ar']=DishTranslation::where(['dish_id'=>$item['id'],'locale'=>'ar'])->first()->name;
            $item['name']=DishTranslation::where(['dish_id'=>$item['id'],'locale'=>'en'])->first()->name;
            $item['price']=$or['price'];
            $item['quantity']=(int)$or['qty'];
            $item['addons']=[];

            $section_id=Dish::find($item['id'])->sections->id;
            // $item['section_ar']=SectionTranslation::where(['section_id'=>$section_id,'locale'=>'ar'])->first()->name;
            $item['section']=SectionTranslation::where(['section_id'=>$section_id,'locale'=>'en'])->first()->name;
            $item['size']=$or['size'];
            // if($or['size'] =='small'){
            //     $item['size_ar']='صغير';
            // }elseif ($or['size'] =='large') {
            //     $item['size_ar']='كبير';
            // }else{
            //     $item['size_ar']='متوسط';
            // }
            if(isset($or['addons'])){
                $adds=[];

                foreach ($or['addons'] as  $addon) {
                    $add=[];
                    // $add['add_ar']=AddonsTranslation::where( ['addon_id'=>$addon['id'] ,'locale'=>'ar'])->first()->name;
                    $add['add']=AddonsTranslation::where( ['addon_id'=>$addon['id'] ,'locale'=>'en'])->first()->name;
                    $add['add_price']=Addons::find($addon['id'])->price;
                    array_push($adds,$add);
                }
                $item['addons']=$adds;
            }
            array_push($orders, $item);
        }

        if($request->delivery_type =='pickup'){
            $order->update([
                'cook_id'=>$request->cook_id,
                // 'delivery_fees'=>$request->delivery_fees,
                'total_price'=>(string)$request->total_price,
                'time'=>$time,
                'date'=>$date,
                // 'address'=>$address['address'],
                'customer_address_id'=>$request->address,
                'orderEntry'=>$orders,
                'status'=>'1',
                'payment_method'=>0,//0 for cash and 1 for credit
                'delivery_type'=>0,
                'note'=>$request->note,
                'discount_id'=>$request->discount_id,
                'discount_price'=>$request->discount_price
            ]);
            if($request->coupon_id != null){
                $coupon=Discount::find($request->coupon_id);
                $order = $order->coupon()->associate($coupon);
                $order->save();

            }

         $this->sendWebNotification($order);


         return response()->json([
            'status' => true,
            'errNum' => "S000",
            'msg' => 'success',
            'orderId' => $order->id
        ]);

        }









        $customer_coordinates=explode(",",$address->coordinates);
        $validator=\Validator::make($request->all(), [
            // 'time' => 'required',
            // 'date' => 'required',
            'address' => 'required',
         ]);
        if ($validator->fails()) {
        return response()->json(['error'=>$validator->errors()], 401);
        }
        // $orders['delivery_fees']=$request->delivery_fees;
        $admin=User::first();
        // $admin->update(['revenue'=>doubleval($admin->revenue) + $request->total_price + doubleval($request->delivery_fees) ]);
        $order->update([
            'delivery_fees'=>$request->delivery_fees,
            'total_price'=>$request->total_price,
            'time'=>$time,
            'date'=>$date,
            'address'=>$address['address'],
            'customer_address_id'=>$request->address,
            'orderEntry'=>$orders,
            'status'=>'1',
            'payment_method'=>0,
            'delivery_type'=>1,
            'change'=>$request->change,
            'note'=>$request->note

        ]);
        if($request->coupon_id != null){
            $coupon=Discount::find($request->coupon_id);
            $order = $order->coupon()->associate($coupon);
            $order->save();


        }


        $details=$order->orderEntry;
        // dd($details);
        $orders=[];
        foreach ( $details as $ord) {
            $item=[];
            $item['id']=(string)$ord['id'];
            $item['name']=DishTranslation::where(['dish_id'=>$item['id'],'locale'=>'ar'])->first()->name;
            // $item['name_en']=DishTranslation::where(['dish_id'=>$item['id'],'locale'=>'en'])->first()->name;
            $item['price']=$ord['price'];
            $item['quantity']=(int)$ord['quantity'];
            // $item['addons']=[];
            // if(isset($order['addons'])){
            //     $adds=[];

            //     foreach ($order['addons'] as  $addon) {
            //         return $addon;
            //         $add=[];
            //         $add['add_ar']=AddonsTranslation::where( ['addon_id'=>$addon['id'] ,'locale'=>'ar'])->first()->name;
            //         $add['add_en']=AddonsTranslation::where( ['addon_id'=>$addon['id'] ,'locale'=>'en'])->first()->name;
            //         $add['add_price']=Addons::find($addon['id'])->price;
            //         array_push($adds,$add);
            //     }
            //     array_push($item['addons'],$adds);
            // }
            $item['note']=$request->change;
            array_push($orders, $item);
        }
        $date=strtotime($date . " " . $time);
        $customer=User::find(auth()->guard('api_customer')->user()->id);
        // dd($customer_coordinates[0] ,$cook->address[0]->address, $customer_coordinates[1],$coordinates[0] ,$coordinates[1], (string)strtotime(date("Y-m-d H:i:s",$date)))

        // $factory = (new Factory)
        // ->withServiceAccount(__DIR__.'/logista-b796f05ac902.json');
        // $database = $factory->createDatabase();

        // $getUser = $database
        // ->getReference('users')
        // ->orderByChild('email')
        // ->equalTo('hesham.zaki.99@hotmail.com')
        // ->getValue();
        // $user=array_keys($getUser);
        // $newOrder = $database
        // ->getReference('/users/'.$user[0].'/tasks')
        // ->push(
        //     [
        // 'deliver'=>['address'=>['lat'=>$customer_coordinates[0],'lng'=>$customer_coordinates[1],
        // 'name'=>$address['address']],
        //     'date' => (string)strtotime(date("Y-m-d H:i:s",$date)),
        //     'description' => 'change : '.$request->change,
        //     'email'=>$customer->email,
        //     'name' => $customer->name,
        //     'orderId' => (string)$order['id'],
        //     'phone' =>  $customer_phone],
        //     'distance'=>'',
        //     'driverId'=>'',
        //     'estTime'=>'',
        //     'pickup'=>['address'=>['lat'=>$coordinates[0],'lng'=>$coordinates[1],'name'=>$cook->address[0]->address],
        //     'date' => (string)strtotime(date("Y-m-d H:i:s",$date)),
        //     'description' => 'change : '.$request->change,
        //     'email'=>$customer->email,
        //     'name' => $customer->name,
        //     'orderId' => (string)$order['id'],
        //     'phone' =>  $customer_phone],
        //     'status'=>-1,
        //     'orderItems' => $orders,
        //     'type'=>'pickupOnDelivery',
        //     'taken'=>false,
        // ]);

        // $logista_tasks = $database
        // ->getReference('/users/'.$user[0].'/tasks')
        // ->getSnapshot()->getValue();

        // $order->update([
        //     'logista_order_id'=>array_key_last($logista_tasks)
        // ]);


        // event(new NewOrder($order->id));
        // $this->sendWebNotification($order);
        // dd('aaaaa');
        return $this->returnData('msg','success');
    }



    // public function get_paid(Request $request)
    // {
    //     // $address=Address::find($request->address);
    //     // $coordinates=explode(",", $address->coordinates);
    //     // $cook=User::find($request->cook_id);


    //     // $cook_coordinates=explode(",", $cook->address[0]['coordinates']);
    //     // $distance =$this->haversineGreatCircleDistance($coordinates[0],$coordinates[1],$cook_coordinates[0],$cook_coordinates[1]);


    //     // dd($distance ,$address->coordinates ,$cook->address[0]['coordinates']);
    //     $validator=\Validator::make($request->all(), [
    //         'cook_id' => 'required',
    //         'address' => 'required',
    //      ]);
    //     if ($validator->fails()) {
    //     return response()->json(['error'=>$validator->errors()], 401);
    //     }
    //         $factory = (new Factory)
    //         ->withServiceAccount(__DIR__.'/logista-b796f05ac902.json')
    //         ;
    //         $database = $factory->createDatabase();


    //         //get value of all the unique ID
    //         $getUser = $database
    //                ->getReference('users')
    //                ->orderByChild('email')
    //                ->equalTo('hesham.zaki.99@hotmail.com')
    //                ->getValue();
    //                $user=array_keys($getUser);
    //                $getZones = $database
    //                ->getReference('/users/'.$user[0].'/geoFences')

    //                ->getValue();
    //         $zones=(array_values($getZones));
    //         $arr_check=[];
    //         foreach($zones as $zo)
    //         {

    //             $arr_x=[];
    //             $arr_y=[];
    //             foreach($zo['cords'] as $zone)
    //             {
    //                 array_push($arr_x,$zone['lat']);
    //                 array_push($arr_y,$zone['lng']);
    //             }

    //             $points_polygon = count($arr_x) - 1;  // number vertices - zero-based array

    //             // $cords=explode(",", str_replace('"', '', $request->coordinates_new));
    //              // dd('fdhh');
    //             //  dd($cords);
    //             $address=Address::find($request->address);
    //             $coordinates=explode(",", $address->coordinates);
    //             // dd($coordinates);
    //             if($coordinates[0] !=''){
    //                 $check=$this->is_in_polygon($points_polygon, $arr_x, $arr_y,$coordinates[0],$coordinates[1]);

    //             }else{
    //                 return response()->json(['error' => true, 'msg' => 'Vendor not assign his location yet']);
    //             }
    //             // dd($coordinates[0],$coordinates[1]);
    //             if ($check){
    //                 array_push($arr_check,$zo);
    //             }

    //         }
    //         if($arr_check==[])
    //         {
    //             return response()->json(['error' => true, 'msg' => 'Out Of Zones']);
    //         }
    //         $deliveryCharges=DeliveryCharges::where('cook_id',$request->cook_id)->first();
    //         $cook=User::find($request->cook_id);
    //         // return($cook);
    //         // dd($cook->default_address);
    //         if(isset($cook->default_address)){
    //             // dd('dd');
    //             $cook_address=Address::find( $cook->default_address);
    //             $cook_coordinates=explode(",", $cook_address->coordinates);
    //             // dd($cook_address);
    //             $distance =$this->haversineGreatCircleDistance($coordinates[0],$coordinates[1],$cook_coordinates[0],$cook_coordinates[1]);
    //             $distance =round($distance,1);
    //             // dd($distance);
    //         }elseif (isset($cook->address)) {
    //             $cook_address=Address::find( $cook->address);
    //             // dd($cook_address);
    //             $cook_coordinates=explode(",", $cook->address[0]['coordinates']);
    //             if($cook_coordinates[0] == ''){
    //                  return response()->json(['error' => true, 'msg' => 'Vendor not assign his location yet']);
    //             }
    //             $distance =$this->haversineGreatCircleDistance($coordinates[0],$coordinates[1],$cook_coordinates[0],$cook_coordinates[1]);
    //             // dd('dd');
    //             $distance =round($distance,1);
    //         }else{
    //             return response()->json(['error' => true, 'msg' => 'Vendor not assign his location yet']);
    //         }
    //         $initialfees=Setting::first()->fees;
    //         // dd($initialfees);
    //         if(isset($deliveryCharges)){//check if this cook in delivery charge [time or order or both]

    //             // if(isset($deliveryCharges->from) && isset($deliveryCharges->orders)){

    //             // }else{
    //                 $allow =0;
    //                 if(isset($deliveryCharges->from)){

    //                     $deliveryFees=0;

    //                     $current = date('Y-m-d');
    //                     $current=date('Y-m-d', strtotime($current));
    //                     //echo $paymentDate; // echos today!
    //                     $from = date('Y-m-d', strtotime($deliveryCharges->from));
    //                     $to = date('Y-m-d', strtotime($deliveryCharges->to));

    //                     if (($current >= $from) && ($current <= $to)){
    //                        $allow =1;
    //                     }
    //                 }

    //                 if( ($deliveryCharges->orders != null   && $deliveryCharges->orders >= 1) || $allow == 1 ){
    //                     if($deliveryCharges->delivery_type == 0){
    //                         $data=['cost'=>0 ,'flag' =>'Free' ];
    //                     }else{
    //                         $data=['cost'=>$deliveryCharges->amount ,'flag' =>'Fixed Rate' ];
    //                     }
    //                     return response()->json([
    //                         'success' => true,
    //                         'msg' => 'Get Orders successfully.',
    //                         'data' => [$data],
    //                     ], 200);
    //                 }else{
    //                     $distance =round($distance,1);
    //                     return response()->json([
    //                         'success' => true,
    //                         'msg' => 'Get Orders successfully.',
    //                         'data' => array_values($arr_check[0]['flags']),
    //                         'distance'=>$distance

    //                     ], 200);
    //                 }

    //             // }


    //         }else{
    //             $distance =round($distance,1);

    //             return response()->json([
    //                 'success' => true,
    //                 'msg' => 'Get Orders successfully.',
    //                 'data' => array_values($arr_check[0]['flags']),
    //                 'distance'=>$distance,
    //                 'initial delivery fees'=>$initialfees

    //             ], 200);
    //         }

    // }

    // public function is_in_polygon($points_polygon, $vertices_x, $vertices_y, $longitude_x, $latitude_y)
	// {
	//   $i = $j = $c = 0;
	//   for ($i = 0, $j = $points_polygon ; $i < $points_polygon; $j = $i++) {
	// 	if ( (($vertices_y[$i]  >  $latitude_y != ($vertices_y[$j] > $latitude_y)) &&
	// 	 ($longitude_x < ($vertices_x[$j] - $vertices_x[$i]) * ($latitude_y - $vertices_y[$i]) / ($vertices_y[$j] - $vertices_y[$i]) + $vertices_x[$i]) ) )
	// 	   $c = !$c;
    //   }
	//   return $c;
	// }

    // function haversineGreatCircleDistance($latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo)
    //   {
    //     if (($latitudeFrom == $latitudeTo) && ($longitudeFrom == $longitudeTo)) {
    //         return 0;
    //     }else{
    //         $customer=User::find(auth('api_customer')->user()->id);
    //         $client = new Client();
    //         $data = $client->request('GET', 'https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins='.$latitudeFrom.','.$longitudeFrom.'&destinations='.$latitudeTo.','.$longitudeTo.'&key=AIzaSyArUJhAM_MY5imJh7g6l6-rgcBOMyOS63s' );
    //         $data = json_decode($data->getBody());
    //         // if( collect(collect($data->rows[0])['elements'][0])['status'] == 'ZERO_RESULTS'){
    //         //     return
    //         // }
    //         $miles =floatval(collect(collect(collect($data->rows[0])['elements'][0])['distance'])['text']);

    //     // $theta = $longitudeFrom - $longitudeTo;
    //     // $dist = sin(deg2rad($latitudeFrom)) * sin(deg2rad($latitudeTo)) +  cos(deg2rad($latitudeFrom)) * cos(deg2rad($latitudeTo)) * cos(deg2rad($theta));
    //     // $dist = acos($dist);
    //     // $dist = rad2deg($dist);
    //     // $miles = $dist * 60 * 1.1515;
    //     $km = $miles * 1.609344;
    //     // dd($km);
    //     return $km;
    //     }
    //   }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // $currency=Setting::first();
        // if(isset($currency->currency)){
        //     $currency=$currency->currency;
        // }else{
        //     $currency='EGP';
        // }

        // if(auth()->guard('admin')->user()->roles[0]->name !='cook'){//super admin
        //     $orders=Order::where('user_id',$id)->whereNotNull('status')
        //     ->where('status', '!=', 1)
        //     ->orderBy('id', 'DESC')
        //     ->get();

        //     $currentActivities=Order::where('user_id',$id)->where('status',1)->get();
        // }elseif(auth()->guard('admin')->user()->roles[0]->name =='cook'){//single vendor or branch

        //     $orders=Order::where('user_id',$id)->where('cook_id',auth()->guard('admin')->user()->id)
        //     ->whereNotNull('status')
        //     ->where('status', '!=', 1)
        //     ->orderBy('id', 'DESC')
        //     ->get();

        //     $currentActivities=Order::where('cook_id',auth()->guard('admin')->user()->id)
        //     ->where('user_id',$id)->
        //     ->where('status',1)
        //     ->orderBy('id', 'DESC')
        //     ->get();

        // }


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    public function paid($id){ // paied 1
        $order= Order::find($id);
        $order->update(['paid'=>1]);

        return $this->returnData('msg','success');


    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function updateFcnToken(Request $request){



        $user=auth('api_customer')->user();
        $user->update(['device_key'=>$request->token]);
        return $this->returnData('msg','token updated successfully');

    }




    public function sendWebNotification($order)
    {
        $url = 'https://fcm.googleapis.com/fcm/send';
        $serverKey='AAAAxJAyh0Q:APA91bEWjkR_pj_iYBdOAyu4xvFFWKsfwMPm7LoD9c_gqcIGRCH7lSPLfMKt97Zjkb2xBiZPuo4jK-ByumcTaGlSIVhAaoUNLUyo0oBysz3wf4LIue36onIZTM76U4bXX2UwGQtbkK19';

        $user=User::find($order->cook_id);
        $data = [
            "to" => $user->device_key,
            "notification" => [
                // "id" => $order->id,
                // "body" => $order->body,
                // "time" =>$order->updated_at,
                // "status"=> $order->status
                "body" => $order->status,
                "title" =>$order->id,
                "sound" => "notify.mp3",
            ]
        ];
        $encodedData = json_encode($data);

        $headers = [
            'Authorization:key=' . $serverKey,
            'Content-Type: application/json',
        ];

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        // Disabling SSL Certificate support temporarly
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $encodedData);

        // Execute post
        $result = curl_exec($ch);

        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }

        // Close connection
        curl_close($ch);

        // FCM response
        return($result);
    }


}
