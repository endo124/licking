<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Advertisement;
use App\Models\Discount;
use App\Models\User;
use App\Models\UserTranslation;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class AdvertisementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $coupons=Discount::whereHas('discountCooks')->get();
        $ads=Advertisement::all();
        return view('backend.advertisement_list',compact('coupons','ads'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules=[
                'coupon'=>'required',
                'files'=>'required'
            ];
        $validator=\Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return \Redirect::back()->with(['message_add'=>$validator->errors()]);
        }
        else{
            if ($request->file('files')) {
                    $ext=$request->file('files')->getClientOriginalExtension();
                    $image_name=time().'.'.$ext;

                    $path='backend/img/offers';
                    $request->file('files')->move($path,$image_name);
            }
            $ad=Advertisement::create([
                'advertisement'=>$image_name,
                'coupon'=>$request->coupon
            ]);

            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $ad=Advertisement::find($id);
        return response()->json(['success'=>$ad]);
    }

    public function details($name){
        $registeredcook=Discount::where('name',$name)->first()->cooks;
        if($registeredcook != 'all'){
            $cooks=[];
            foreach(json_decode($registeredcook) as $index=>$cook_id){
                $cook=UserTranslation::find($cook_id);
                // dd($cook->name,json_decode($registeredcook));
                if(isset($cook)){
                    array_push($cooks,$cook['name']);
                }
            }
            // dd($cooks);
        }else{
            $cooks=['all cooks'];
        }
        return response()->json(['success'=>$cooks]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $ad=Advertisement::find($id);
        $rules=[
                'coupon_name'=>'required',
                // 'files'=>'required'
            ];
        $validator=\Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return \Redirect::back()->with(['message_update'=>$validator->errors(),'ad_id'=>$ad->id]);
        }
        else{
            if ($request->file('files')) {
                $ext=$request->file('files')->getClientOriginalExtension();
                $image_name=time().'.'.$ext;

                $path='backend/img/offers';
                $request->file('files')->move($path,$image_name);
                $ad->update(
                    [
                        'advertisement'=>$image_name,
                    ]);
            $ad->save();

            }

            $ad->update(
                [
                    'coupon'=>$request->coupon_name
                ]
            );
            $ad->save();

            return  redirect()->route('advertisement.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $ad=Advertisement::find($id);
        $ad->delete();

        return back();
    }
}
