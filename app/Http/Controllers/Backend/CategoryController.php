<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\User;
use App\Models\CategoryTranslation;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule as Rule;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (auth()->guard('admin')->user()->roles[0]->name != 'cook'){
            $categories=Category::with('users')->get();
            $cooks=User::whereHas('roles',function($q){
                $q->where('name','cook');
            })->where('active',1)->get();
            return view('backend.category_list',compact('categories','cooks'));
        }else{
            if (auth()->guard('admin')->user()->parent != null) {
                $cook=User::find(auth()->guard('admin')->user()->parent);
                $categories=$cook->categories;
            }else{
                $cook=User::find(auth()->guard('admin')->user()->id);
                $categories=$cook->categories;

            }

            return view('backend.category_list',compact('categories'));

        }


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules=[];
        foreach (config('translatable.locales') as $locale) {
            $rules += [$locale.'.name'=>['required'
            // ,Rule::unique('category_translations','name')
            ]];
        }
        $validator=\Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return \Redirect::back()->with(['message_add'=>$validator->errors()]);
        }
        else{
            $data=$request->except('Vendor');
            $category=Category::create($data);
            $cook=User::find($request->Vendor);
            $category->users()->attach($cook);
            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $category=Category::find($id);
        $rules = [];
        foreach (config('translatable.locales') as $locale) {
            $rules += [$locale . '.name' => ['required',
            // Rule::unique('category_translations', 'name')->ignore($category->id, 'category_id')
            ]];
        }//end of for each

        $validator=\Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return \Redirect::back()->with(['message_update'=>$validator->errors(),'category_id'=>$category->id]);
        }
        else{
            $data=$request->except('Vendor');
            $category->update($data);
            $cook=User::find($request->Vendor);
            $category->users()->sync($cook);

            $category->save();
            return  redirect()->route('category.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category_trans=CategoryTranslation::where('category_id',$id)->delete();

        $category=Category::find($id);
        if( ! empty($category->addons[0])){
        $category->addons()->detach();
        }
        if( ! empty($category->users[0])){
        $category->users()->detach();
        }
        if(! empty($category->dishes[0])){
            $category->dishes()->detach();
        }
        $category->delete();
        return back();
    }
}
