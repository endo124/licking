<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Dish;
use App\Models\Order;
use App\Models\Setting;
use App\Models\User;
use Illuminate\Http\Request;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
use Kreait\Firebase\Database;
class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {


        // if(auth()->guard('admin')->user()->roles[0]->name !='cook'){
        //     $orders=Order::where('status','!=',-1)->get();
        //     $currentActivities=Order::where('status',-1)->get();
        // }elseif(auth()->guard('admin')->user()->roles[0]->name =='cook'){

        //     $orders=Order::where('cook_id',auth()->guard('admin')->user()->id)
        //     ->where('status','!=',-1)->get();

        //     $currentActivities=Order::where('cook_id',auth()->guard('admin')->user()->id)
        //     ->where('status',-1)->get();

        // }

        $currency=Setting::first();
        if(isset($currency->currency)){
            $currency=$currency->currency;
        }else{
            $currency='EGP';
        }
        if(auth()->guard('admin')->user()->roles[0]->name =='cook' &&auth()->guard('admin')->user()->parent == null){//super admin
            $orders=Order::whereNotNull('status')
            ->where('status', '!=', 1)
            ->orderBy('id', 'DESC')
            ->get();
            $currentActivities=Order::where('status',1)->get();
        }elseif(auth()->guard('admin')->user()->roles[0]->name =='cook'){//single vendor or branch

            $orders=Order::where('cook_id',auth()->guard('admin')->user()->id)
            ->whereNotNull('status')
            ->where('status', '!=', 1)
            ->orderBy('id', 'DESC')
            ->get();

            $currentActivities=Order::where('cook_id',auth()->guard('admin')->user()->id)
            ->where('status',1)
            ->orderBy('id', 'DESC')
            ->get();

        }
        $factory = (new Factory)
        ->withServiceAccount(__DIR__.'/logista-b796f05ac902.json');
        $database = $factory->createDatabase();


        $available=[];
        $onduty=[];

        $getUser = $database
        ->getReference('users')
        ->orderByChild('email')
        ->equalTo("hesham.zaki.99@hotmail.com")
        ->getValue();

        if(isset($getUser)){
            $user=array_keys($getUser);
            if($user != []){

                if (isset($getUser[$user[0]]['drivers'])) {
                    $drivers= $getUser[$user[0]]['drivers'];
                    if (isset($drivers)) {
                        foreach (array_keys($drivers) as $driver) {
                            if ($drivers[$driver]['driverStatus'] == -1) {
                                array_push($available, $driver);
                            } elseif (['driverStatus'] == 3) {
                                array_push($onduty, $driver);
                            }
                        }
                    }
                }
            }
        }


        $accepted_orders=Order::where('status','!=',-1)
        ->where('status','!=',1)
        ->where('status','!=',0)->get();
        // $cooks=User::whereHas('roles',function($q){
        //     return $q->where('name','cook');
        // })->get();

        // $cook=User::find($orders->cook_id);
        return view('backend.order-list',compact('orders','currentActivities','available','onduty','accepted_orders','currency'));
    }

    public function orderdetails($id){
        $order= Order::where('id',$id)->first();
        return response()->json(['success' => true,'order'=>$order]);

     }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
    public function assign($id){

        $order=Order::find($id);
        $order->update([
            'assign'=>auth()->guard('admin')->user()->name
        ]);
        return response()->json(['name'=>auth()->guard('admin')->user()->name]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $orders=[];
        $validation=$request->validate([
            'qty'=>'required',
            'size'=>'required'
        ]);

       $orderEntry=array();
       $dish=Dish::find($request->dish_id);
        if($request->notes){
            $orderEntry['notes']=$request->notes;
        }
       $orderEntry['qty']=$request->qty;
       $orderEntry['notes']=$request->notes;
       $orderEntry['size']=$request->size;
       $orderEntry['name']=$dish->name;
       $orderEntry['price']=$request->price*$request->qty;
       $orderEntry['section']=$dish->sections->name;
       $orderEntry['images']=$dish->images;

       array_push($orders,$orderEntry);
       $order=Order::create([
            'user_id'=>auth()->guard('customer')->user()->id,
            'cook_id'=>$dish->user_id,
            // 'date'=>$request->date,
            // 'time'=>$request->time,
            // 'status'=>'0',
            'orderEntry'=>$orders,
            // 'address'=>$request->address,
        ]);
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       $order= Order::where('id',$id)->first();
       if($order->status == 1){
        $order->update([
            'status'=>2
        ]);
        $admin=User::first();
        $admin->update(['revenue'=>doubleval($admin->revenue) + $order->total_price + doubleval($order->delivery_fees) ]);

       }else if($order->status == 2){
        $order->update([
            'status'=>3
        ]);
       }else if($order->status == 3){
        $order->update([
            'status'=>4
        ]);
       }


        return response()->json(['success' => true,'status'=>$order->status]);
    }
    public function reject($id)
    {
       $order= Order::where('id',$id)->first();
        $order->update([
            'status'=>0
        ]);
        return response()->json(['success' => true,'status'=>$order->status]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }
    // public function accept($id){
    //     $order=Order::find($id);
    //     $order->update([
    //         'status'=>1
    //     ]);
    //     return back();
    // }
    // public function reject($id){
    //     $order=Order::find($id);
    //     $order->update([
    //         'status'=>0
    //     ]);
    //     return back();
    // }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $order= Order::where('id',$id)->first();
        if($order->status ==null){
         $order->update([
             'status'=>'Accepted'
         ]);
        }else if($order->status == 'Accepted'){
         $order->update([
             'status'=>'In progress'
         ]);
        }
         return response()->json(['success' => true,'status'=>$order->status]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function customerorders($id){
        $currency=Setting::first();
        if(isset($currency->currency)){
            $currency=$currency->currency;
        }else{
            $currency='EGP';
        }

        if(auth()->guard('admin')->user()->roles[0]->name !='cook'){//super admin
            $orders=Order::where('user_id',$id)->whereNotNull('status')
            ->where('status', '!=', 1)
            ->orderBy('id', 'DESC')
            ->get();

            $currentActivities=Order::where('user_id',$id)->where('status',1)->get();
        }elseif(auth()->guard('admin')->user()->roles[0]->name =='cook'){//single vendor or branch

            $orders=Order::where('user_id',$id)->where('cook_id',auth()->guard('admin')->user()->id)
            ->whereNotNull('status')
            ->where('status', '!=', 1)
            ->orderBy('id', 'DESC')
            ->get();

            $currentActivities=Order::where('cook_id',auth()->guard('admin')->user()->id)
            ->where('user_id',$id)
            ->where('status',1)
            ->orderBy('id', 'DESC')
            ->get();
        }

            return view('backend.customer-order-list',compact('orders','currentActivities','currency'));

    }
}
