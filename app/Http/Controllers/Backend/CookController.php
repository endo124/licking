<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Address;
use App\Models\City;
use App\Models\Setting;
use App\Models\User;
use Facade\FlareClient\Http\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule as Rule;

class CookController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */




    public function index()
    {
        $currency=Setting::first();
        if(isset($currency->currency)){
            $currency=$currency->currency;
        }else{
            $currency='EGP';
        }
        $cities=City::all();
        $user=User::where('id',auth()->guard('admin')->user()->id)->first();
        if($user->hasRole('admin')){
            $cooks=User::whereRoleIs('cook')
            ->where('city_id',auth()->guard('admin')->user()->city_id)
            ->get();
        }else{
            $cooks=User::whereRoleIs('cook')
            ->where('active','!=',-1)
            ->get();
        }

        return view('backend.cooks-list',compact('cooks','cities','currency'));
    }




    public function vip_cook( $id)
    {

       $cook=User::where('id',$id)->update([
            'VIP'=>1
        ]);
        return response()->json(['success' => $id,]);
    }

    public function vip_reset( $id)
    {

        User::where('id',$id)->update([
            'VIP'=>0
        ]);

       return response()->json(['success' => $id]);
    }

    public function vip()
    {
        $cooks=User::where('VIP','1')->whereHas('roles',function($q){
            $q->where('name','cook');
         })->get();
         return view('backend.vip-cook-list',compact('cooks'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $rules=[
            // 'taxes'=>'required',
            'pickup_commission'=>'required',
            'phone'=>'required',
            'email'=>'required|email|unique:users',
            'password'=>'required',
            'city'=>'required',
            'contract'=>'required|unique:users',
            'commission'=>'required',
            'commission_type'=>'required',
            'bank'=>'required',
            'credit_number'=>'required',
            'card_holder_name'=>'required',
        ];
        foreach (config('translatable.locales') as $locale) {
            $rules += [$locale.'.name'=>['required',Rule::unique('user_translations','name')]];
        }
        $validator=\Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                return \Redirect::back()->with(['message_add'=>$validator->errors()]);
            }
            else{
                $credit_number =json_encode($request->credit_number,true);
                $bank =json_encode($request->bank,true);
                $card_holder_name =json_encode($request->card_holder_name,true);
                $cook_trans=$request->only(['en','ar']);
                $cook= User::create([
                    'images'=>'1606647695.jpg',
                    'email'=>$request->email,
                    'phone'=>$request->phone,
                    'card_holder_name'=>$card_holder_name,
                    'contract'=>$request->contract,
                    'commission'=>$request->commission,
                    'commission_type'=>$request->commission_type,
                    'pickup_commission'=>$request->pickup_commission,
                    'pickup_commission_type'=>$request->pickup_commission_type,
                    'taxes'=>$request->taxes,
                    'taxes_type'=>$request->taxes_type,
                    'city_id'=>$request->city,
                    'bank'=>$bank,
                    'credit_number'=>$credit_number,
                    'password'=>Hash::make($request->password),
                ]);
                $cook->update($cook_trans);
                $cook->roles()->attach(3);
            }
        return redirect('dashboard/cook');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        // $cook=User::find(auth()->guard('admin')->user()->id);
        // $addons=$cook->addons;

        // return response()->json($addons);


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $currency=Setting::first();
        if(isset($currency->currency)){
            $currency=$currency->currency;
        }else{
            $currency='EGP';
        }
        $cook=User::where('id',auth()->guard('admin')->user()->id)->first();

        $setting=Setting::first();
        if(isset($setting->terms)){
            $terms=$setting->terms;
        }
        return view('backend.cook.profile',compact('cook','terms','currency'));
    }


    public function editcook($id){
        $currency=Setting::first();
        if(isset($currency->currency)){
            $currency=$currency->currency;
        }else{
            $currency='EGP';
        }

        $cities=City::all();
        $cook=User::find($id);
        return view('backend.edit-cooks-list',compact('cook','cities','currency'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $cook=User::find($id);

        if(auth()->guard('admin')->user()->roles[0]->name =='cook' && $cook->parent == null){//update from cook profile
                $validate=[
                    // 'phone'=>'required',
                    'birth_date'=>'required ',
                    // 'state'=>'required',
                    // 'city'=>'required',
                    // 'area'=>'required',
                    // 'title'=>'required',
                    'address'=>'required',
                    'info'=>'required',
                    'work_from'=>'required',
                    'work_to'=>'required',
                    'lat'=>'required',
                    'lng'=>'required',
                ];
                $request->validate($validate);
                // $validator=\Validator::make($request->all(), $validate);
                // if ($validator->fails()) {
                //     return \Redirect::back()->with(['message_add'=>$validator->errors(),'cook_id'=>$cook->id]);
                // }
                // else{

                    $cook=User::find($id);
                    if ($request->file('profile_image')) {

                        $request->validate(['profile_image'=>'required|image|max:2048|mimes:jpeg,png,jpg,gif,svg']);

                        $ext=$request->file('profile_image')->getClientOriginalExtension();
                        $image_name=time().'.'.$ext;
                        $path='backend/img/cook';
                        $request->file('profile_image')->move($path,$image_name);
                        if ($cook->images != '1606647695.jpg') {
                            Storage::disk('cooks')->delete($cook->images);
                        }
                    }
                    else{
                        $image_name=$cook->images;
                    }

                    if($request->availability == null){
                        $request->availability = 0;
                    }else{
                        $request->availability = 1;
                    }

                    $from=json_encode($request->work_from);
                    $to=json_encode($request->work_to);

                    $from2=json_encode($request->work_from2);
                    $to2=json_encode($request->work_to2);
                    $cook->update([
                        'availability'=>$request->availability,
                        'name'=>$request->name,
                        'phone'=>$request->phone,
                        'email'=>$request->email,

                        'info'=>$request->info,
                        'images'=>$image_name,
                        'date_of_birth'=>$request->birth_date,
                        'work_from'=>$from,
                        'work_to'=>$to,
                        'work_from2'=>$from2,
                        'work_to2'=>$to2,
                        'terms'=>1

                    ]);

                    if($request->lat){
                        $coordinates=$request->lat.','.$request->lng;
                    }
                    if(isset($cook->address[0])){

                        $address=Address::find($cook->address[0]->id);
                        $address->update([
                        'address'=>$request->address,
                        'coordinates'=>$coordinates,
                        ]);
                    }
                    else{
                        $address=Address::create([
                            'user_id'=>$cook->id,
                            'address'=>$request->address,
                            'coordinates'=>$coordinates,
                        ]);
                    }
                // }


        }elseif(auth()->guard('admin')->user()->roles[0]->name !='cook'){//update from admin dashboard

            $cook=User::find($id);
                $rules=[
                    'phone'=>'required|unique:users,id,'.$cook->id,
                    'email'=>'required|email|unique:users,id,'.$cook->id,
                    // 'password'=>'required',
                    'city'=>'required',
                    'contract'=>'required|unique:users,id,'.$cook->id,
                    'commission'=>'required',
                    'commission_type'=>'required',
                    'pickup_commission'=>'required',
                    'pickup_commission_type'=>'required',
                    // 'taxes'=>'required',
                    'bankedit'=>'required',
                    'credit_numberedit'=>'required|unique:users,id,'.$cook->id,
                    'card_holder_name'=>'required|unique:users,id,'.$cook->id,

                ];
                foreach (config('translatable.locales') as $locale) {
                    $rules += [$locale.'.name'=>['required',Rule::unique('user_translations','name')->ignore($cook->id,'user_id')]];
                }
              $validator=\Validator::make($request->all(),$rules);

            if ($validator->fails()) {
                return \Redirect::back()->with(['message_update'=>$validator->errors(),'cook_id'=>$cook->id]);
            }
            else{
                    $cook_trans=$request->only(['en','ar']);
                    $credit_numberedit=json_encode($request->credit_numberedit,true);
                    $bankedit=json_encode($request->bankedit,true);
                    $card_holder_name =json_encode($request->card_holder_name,true);

                    $cook->update([
                            'email'=>$request->email,
                            'phone'=>$request->phone,
                            'bank'=>$bankedit,
                            'credit_number'=>$credit_numberedit,
                            'card_holder_name'=>$card_holder_name,
                            'contract'=>$request->contract,
                            'commission'=>$request->commission,
                            'commission_type'=>$request->commission_type,
                            'pickup_commission'=>$request->pickup_commission,
                            'pickup_commission_type'=>$request->pickup_commission_type,
                            'taxes'=>$request->taxes,
                            'taxes_type'=>$request->taxes_type,
                            'city_id'=>$request->city,
                            'password'=>Hash::make($request->password)
                        ]);
                        $cook->update($cook_trans);
                }
                return redirect()->to('/dashboard/cook');
        }else{
            $cook_trans=$request->only(['en','ar']);

            $cook->update([
                'email'=>$request->email,
                'phone'=>$request->phone,
                'password'=>Hash::make($request->password)
            ]);
            $cook->update($cook_trans);

        }
        return back();
    }



    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function destroy($id)
    // {
    //     $cook=User::find($id);
    //     if($cook->address){
    //         $cook->address()->delete();
    //     }
    //     if($cook->dishes){
    //         $cook->dishes()->delete();
    //     }
    //     $cook->delete();
    //     return back();
    // }

    public function deactive($id)
    {
        $cook=User::find($id);
        $cook->update(['active'=>0]);
        return redirect()->back();
    }
    public function active($id)
    {

        $cook=User::find($id);
        $cook->update(['active'=>1]);
        return redirect()->back();
    }


    public function destroy($id)
    {

        $cook=User::find($id);
        $cook->update(['active'=>-1]);
        return back();

    }
}
