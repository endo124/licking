<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Notification;
use App\Models\Order;
use App\Models\Setting;
use App\Models\User;
use App\Models\Withdraw;
use Illuminate\Http\Request;

class WithdrawController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $currency=Setting::first();
        if(isset($currency->currency)){
            $currency=$currency->currency;
        }else{
            $currency='EGP';
        }
        $user=User::where('id',auth()->guard('admin')->user()->id)->first();

        $total_profit=$user->profit;

        $withdraws=Withdraw::with('cooks')
        ->get();
        // dd($withdraws);
        if(auth()->guard('admin')->user()->roles[0]->name == 'cook'){
            $withdraws= $withdraws->where('user_id',$user->id);
        }
        if(auth()->guard('admin')->user()->roles[0]->name == 'admin'){
            $cooks=User::where('city_id',$user->city_id)->pluck('id');
            $withdraws=$withdraws->whereIn('user_id',$cooks);

        }
        return view('backend.withdraw_list',compact('total_profit','withdraws','currency'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate=$request->validate(['amount']);

        $cook=User::find(auth()->guard('admin')->user()->id);
        if( $cook->profit <=0){
            return back();

        }
        $withdraw=Withdraw::create([
            'user_id'=>$cook->id,
            'amount'=>$request->amount,
            'status'=>0
        ]);
        $totalProfit=$cook->profit-$request->amount;
        $cook->update(['profit'=>$totalProfit]);
        $notify=['customer_id'=>$cook->id,'notify'=>'withdraw'];
        // $notify=json_encode($notify);
        // $notification=Notification::create([
        //     'notify'=>$notify
        // ]);
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)//update profit
    {


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)//status null make withdraw request , 0 pending , -1 decline , 1 accept
    {
    }

    public function cancel($id)
    {
        $withdraw=Withdraw::find($id);
        $amount=$withdraw->amount;
        $cook=User::find(auth()->guard('admin')->user()->id);
        $profit_past=$cook->profit;
        $cook->update(['profit'=>$profit_past+$amount]);
        $withdraw->delete();
        return back();
    }

    public function decline($id)
    {

        $withdraw=Withdraw::find($id);
        $amount=$withdraw->amount;
        $cook=User::where('id', $withdraw->user_id)->first();
        $profit_past=$cook->profit;
        // dd($profit_past,$amount, ($profit_past+$amount));
        $cook->update(['profit'=>($profit_past + $amount)]);
        $withdraw->update(['status'=>-1]);

        return back();
    }

    public function accept($id)//addmin revenue - withdraw->amount
    {

        $withdraw=Withdraw::find($id);
        // $amount=$withdraw->amount;
        // $cook=User::where('id', $withdraw->user_id)->first();
        // $profit_past=$cook->profit;
        // $cook->update(['profit'=>$profit_past+$amount]);
        $admin=User::find(auth()->guard('admin')->user()->id);
        $revenue=$admin->revenue;
        $admin->update(['revenue'=>doubleval($revenue) - $withdraw->amount] );
        $withdraw->update(['status'=>1]);
        return back();
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $withdraw=Withdraw::find($id);

        $withdraw->delete();
        return back();

    }
}
