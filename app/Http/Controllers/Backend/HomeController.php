<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\Setting;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $currency=Setting::first();
        if(isset($currency->currency)){
            $currency=$currency->currency;
        }else{
            $currency='EGP';
        }
        if(auth()->guard('admin')->user()->roles[0]->name != 'cook'){
            $users=User::all();
            $cooks=User::whereHas('roles',function($q){
                $q->where('name','cook');
            })->whereNotNull('parent')->get();
            $customers=User::whereHas('roles',function($q){
                $q->where('name','customer');
            })->where('active','!=',-1)->get();

            $orders=Order::whereNotNull('status')
            ->where('status', '!=', 1)
            ->get();
            $orders_accepted=$orders->where('status', '!=', 0)->where('status', '!=', 9);
            $orders_rejected=$orders->where('status', '==', 0);

            $currentActivities=Order::where('status',1)->get();
            $total_profit=0;
            $orders_count=0;
            $total_revenue=0;
            $delivery_fees=0;
            $discount=0;
            // if(auth()->guard('admin')->user()->roles[0]->name != 'super_admin'){ //profit
                // $orders=$orders->where('status', '!=', 0)->get();
                $admin=User::find(auth()->guard('admin')->user()->id);


            // }else
            if (auth()->guard('admin')->user()->roles[0]->name == 'admin') {

                $admin=User::find(auth()->guard('admin')->user()->id);

                $city=$admin->city_id;
                $customers=User::whereHas('roles',function($q){
                    $q->where('name','customer');
                })
                ->where('active','!=',-1)
                ->where('city_id' ,$city)
                ->get();

                $cooks=User::
                whereHas('roles',function($q){
                    $q->where('name','cook');
                })
                ->where('city_id' ,$city)
                ->where('active',1)
                ->get();

                $cooks_id= $cooks->pluck('id');
                $orders= $orders->whereIn('cook_id',$cooks_id);


            }
            // $orders_accepted=$orders->where('status', '!=', 0)->where('status', '!=', 9);
                foreach($orders_accepted as $order){
                    if($order->discount_id != null){
                        if ($order->coupon['discount_type'] =="%") {
                            $discount+= ($order->coupon['total']*$order->total_price)/100;
                        }
                        else if ($order->coupon['discount_type'] =="EGP") {
                            $discount+= $order->coupon['total'];
                        }
                    }
                    $delivery_fees+=(int)$order['delivery_fees'];
                    $orders_count+=1;
                    $cook=User::find($order->cook_id);
                    if($cook->commission_type == '%'){

                        $total_profit+= ($order->total_price * $cook->commission) / 100;
                    }
                    else{
                        $total_profit+=( $cook->commission ) ;
                    }
                    if (auth()->guard('admin')->user()->roles[0]->name == 'admin') {
                    $total_revenue+=$order->total_price + $order->delivery_fees;
                    }
                }
                $total_profit=round($total_profit,2)- $discount/2;
                if (auth()->guard('admin')->user()->roles[0]->name == 'admin') {

                $total_revenue=round($total_revenue,2);
                $admin->update(['revenue'=>$total_revenue]);
                }

                $admin->update([
                    'profit'=>$total_profit,
                    // 'revenue'=>$total_revenue
                    ]);
                $total_revenue =$admin->revenue;
                // dd($total_revenue ,$admin);
            return view('backend.index',compact('orders','discount','delivery_fees','orders_accepted','orders_rejected','currency','currentActivities','users','cooks','customers','total_profit','total_revenue'));
        }

        else{
            $users=User::all();

            $user=User::where('id',auth()->guard('admin')->user()->id)->first();

            $orders=Order::where('cook_id',auth()->guard('admin')->user()->id)
            ->whereNotNull('status')
            ->where('status', '!=', 1)
            ->get();

            $currentActivities=Order::where('cook_id',auth()->guard('admin')->user()->id)
            ->where('status',1)
            ->get();

            $total_profit=0;
            $orders_count=0;
            $total_revenue=0;

            foreach($orders as $order){
                $total_profit+=$order->total_price;
                $orders_count+=1;
                $total_revenue+=$order->total_price;
            }
            if($user->commission_type == '%'){
                $total_profit=$total_profit - ($total_profit * $user->commission) / 100;
            }
            else{
                $total_profit=$total_profit -($user->commission *$orders_count) ;
            }
            $user->update(['profit'=>$total_profit]);

            $customers_id=Order::pluck('user_id');
            // $customers = DB::table('users')
            // ->select('users.*')
            // ->join('orders', 'users.id', '=', 'orders.user_id')
            // ->where('orders.cook_id',auth()->guard('admin')->user()->id)
            // ->groupBy('users.id')
            // ->get();
            $orders_accepted=$orders->where('status', '!=', 0);

            $customers=User::whereIn('id',$customers_id)->where('active','!=',-1)->get();
            return view('backend.index',compact('orders','orders_accepted','currentActivities','users','currency','customers','total_profit','total_revenue'));
        }


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
