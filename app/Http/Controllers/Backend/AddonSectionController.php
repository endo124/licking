<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\AddonSection;
use App\Models\User;
use App\Models\AddonSectionTranslation;
use Illuminate\Validation\Rule;

class AddonSectionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (auth()->guard('admin')->user()->parent != null) {
            $cook=User::find(auth()->guard('admin')->user()->parent);
            $sections=$cook->addonsection;
        }else{
            $cook=User::find(auth()->guard('admin')->user()->id);
            $sections=$cook->addonsection;
        }

        // $sections=AddonSection::all();
        // dd($sections);
        return view('backend.addon_section_list',compact('sections'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules=[];
        foreach (config('translatable.locales') as $locale) {
            $rules += [$locale.'.name'=>['required',Rule::unique('addonsection_translations','name')],
                'min'=>'required',
                'max'=>'required',
                'conditions'=>'required',
            ];
        }
        $validator=\Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return \Redirect::back()->with(['message_add'=>$validator->errors()]);
        }
        else{
            $section=AddonSection::create($request->all());
            $cook=User::find(auth()->guard('admin')->user()->id);
            $cook->addonsection()->attach($section);
            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $cook=User::find($id);
        $addonsections=$cook->addonsection;
        return response()->json(['success'=>$addonsections]);


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {


        $section=AddonSection::find($id);
        $rules = [];
        foreach (config('translatable.locales') as $locale) {
            $rules += [$locale . '.name' => ['required', Rule::unique('addonsection_translations', 'name')->ignore($section->id, 'addon_section_id')],
            'min'=>'required',
            'max'=>'required',
            'conditions'=>'required',
            ];
        }//end of for each
        $validator=\Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return \Redirect::back()->with(['message_update'=>$validator->errors(),'section_id'=>$section->id]);
        }
        else{
            $section->update($request->all());
            $section->save();
            $cook=User::find(auth()->guard('admin')->user()->id);
            // $cook->addonsection()->sync($section);
            return redirect()->route('addons_section.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $section_trans=AddonSectionTranslation::where('addon_section_id',$id)->delete();
        $section=AddonSection::find($id);
        // $cook=User::find(auth()->guard('admin')->user()->id);
        $section->cooks()->detach();
        $section->delete();
        return back();
    }
}
