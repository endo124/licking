<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Dish;
use App\Models\DishTranslation;
use App\Models\Section;
use App\Models\User;
use App\Models\SectionTranslation;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule as Rule;

class SectionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(auth()->guard('admin')->user()->roles[0]->name != 'cook'){
        $sections=Section::with('users')->get();
        $cooks=User::whereHas('roles',function($q){
            $q->where('name','cook');
        })->where('active',1)->get();
        return view('backend.section_list',compact('sections','cooks'));
        }else{

            if (auth()->guard('admin')->user()->parent != null) {
                $cook=User::find(auth()->guard('admin')->user()->parent );
            $sections=$cook->sections;
            }else{
                $cook=User::find(auth()->guard('admin')->user()->id);
                $sections=$cook->sections;
             }


            return view('backend.section_list',compact('sections'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $rules=[
            'from'=>'required',
            'to'=>'required',
            'Vendor'=>'required'

        ];
        foreach (config('translatable.locales') as $locale) {
            $rules += [$locale.'.name'=>['required'
            // ,Rule::unique('section_translations','name')
            ]];
        }
        $validator=\Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return \Redirect::back()->with(['message_add'=>$validator->errors()]);
        }
        else{

            $section=Section::create($request->all());
            $section->update([
                'available_from'=>$request->from,
                'available_to'=>$request->to
            ]);
            $cook=User::find($request->Vendor);
            $section->users()->attach($cook);
            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $section=Section::find($id);

        $rules=[
            // 'name'=>'unique:section_translations,id,'.$id,
            'from'=>'required',
            'to'=>'required',
            'Vendor'=>'required'
        ];
        foreach (config('translatable.locales') as $locale) {
            $rules += [$locale.'.name'=>['required']];
        }

        $section=Section::find($id);
        foreach (config('translatable.locales') as $locale) {
            $rules += [$locale.'.name'=>['required',
            // Rule::unique('section_translations','name')->ignore($section->id,'section_id')
            ]];
        }

        $validator=\Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return \Redirect::back()->with(['message_update'=>$validator->errors(),'section_id'=>$section->id]);
        }
        else{
            $data=$request->except('available_from','available_to');
            $section->update($data);
            $section->update([
                'available_from'=>$request->from,
                'available_to'=>$request->to
            ]);
            $cook=User::find($request->Vendor);
            $section->users()->sync($cook);
            $section->save();
            return  redirect()->route('section.index');}
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $section_trans=SectionTranslation::where('section_id',$id)->delete();
        $section=Section::find($id);

        $dishes=Dish::where('section_id',$section->id)->get();
        foreach ($dishes as $dish) {
            if($dish){
                $dish->update([
                    'section_id'=>null,
                ]);
                if( ! empty( $dish->cusines[0])){
                    $dish->cusines()->detach();

                }
                if( ! empty( $dish->allergens[0])){
                    $dish->allergens()->detach();
                }
                if( ! empty( $dish->addons[0])){
                    $dish->addons()->detach();

                }
                $dish_trans=DishTranslation::where('dish_id',$dish->id)->delete();
                $section->users()->detach();
                $dish->forceDelete();
            }

        }
        $section->delete();
        return back();
    }
}
