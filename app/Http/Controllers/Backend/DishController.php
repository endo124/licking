<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Addons;
use App\Models\Allergen;
use App\Models\Availability;
use App\Models\Category;
use App\Models\Cusine;
use App\Models\Dish;
use App\Models\DishTranslation;
use App\Models\Section;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class DishController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $allergens=Allergen::all();
        $addons=Addons::all();
        $sections=Section::all();
        $cusines=Cusine::all();
        if (auth()->guard('admin')->user()->roles[0]->name != 'cook'){
            $categories=Category::all();
            $sections=Section::all();
            $dishes=Dish::paginate(10);
            $cooks=User::whereHas('roles',function($q){
                $q->where('name','cook');
            })->where('active',1)->whereNull('parent')->get();
        return  view('backend.dish-list',compact('dishes','cusines','sections','addons','categories','allergens','cooks'));
        }
        else{
            if (auth()->guard('admin')->user()->parent != null) {
                $dishes=Dish::where('user_id',auth()->guard('admin')->user()->parent)->with('availabilities')->paginate(20);
            }else{
                $dishes=Dish::where('user_id',auth()->guard('admin')->user()->id)->paginate(20);

            }
            $cook=User::find(auth()->guard('admin')->user()->id);
            $addons=$cook->addons;
            $sections=$cook->sections;
            $categories=$cook->categories;
            return view('backend.dish-list',compact('dishes','cusines','sections','addons','categories','allergens'));
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $rules=[
            'files'=>'required',
            'cusine'=>'required',
            'category'=>'required',
            'section'=>'required',
            'portions_available'=>'required',
            'dish_available'=>'required',
            'portions_price	'=>'requires|numeric|regex:/^\d*\.?\d*$/',
            'calories'=>'required'

        ];

        if(isset($request->min[0])){
            $rules += [
                'max'=>'numeric|regex:/^\d*\.?\d*$/',
                'min'=>'numeric|regex:/^\d*\.?\d*$/',
            ];
        }
        if(auth()->guard('admin')->user()->roles[0]->name =='cook'){
            foreach (config('translatable.locales') as $locale) {
                $rules += [
                    $locale.'.name'=>['required'],

                ];
            }
            $request->validate( $rules);

            $cook_id=auth()->guard('admin')->user()->id;
        }
        else{
            $rules+=['Vendor_id'=>'required',];

                foreach (config('translatable.locales') as $locale) {
                    $rules += [
                        $locale.'.name'=>['required'],

                    ];
                }
                $request->validate( $rules);
                $cook_id=$request->Vendor_id;
            }
        if ($request->file('files')) {
            $dishes_images=array();
            foreach($request->file('files') as $index=>$img){
                $ext=$img->getClientOriginalExtension();
                $image_name=time().$index.'.'.$ext;
                $path='backend/img/dishes';
                $img->move($path,$image_name);
                array_push($dishes_images,$image_name);
                }
                $dishes_images=implode('__',$dishes_images);
        }
        if($request->dish_type == 'main'){
            $request->dish_type =1;
        }else{
            $request->dish_type=0;
        }
        $dish_trans=$request->only(['en','ar']);



        $dish=Dish::create([
            // 'name'=>$request->name,
            'images'=>$dishes_images,
            'user_id'=>$cook_id,
            'calories'=>$request->calories,
'portions_available'=>json_encode($request->portions_available, true),
            'portions_price'=>json_encode($request->portions_price,
true),
            'available'=>$request->dish_available,
            'category_id'=>$request->category,
            'section_id'=>$request->section,
            // 'main_ingredients'=>$request->main_ingredients,
            // 'info'=>$request->info,
            // 'available_count'=>$request->available_count,
        ]);
        if(isset($request->info)){
            $dish->update(['info'=>$request->info]);
        }
        // dd($dish_trans);
        $dish->update($dish_trans);

        foreach($request->cusine as $cusine){
            $dish->cusines()->attach($cusine);
        }

        if($request->addonsection){
            $dish->addonsections()->attach($request->addonsection);
        }
        // if($request->addons){
        //     for ($i=0; $i <count($request->addons) ; $i++) {
        //         if($request->addons[$i] != null){
        //             $dish->addons()->attach($request->addons[$i],['min' =>$request->min[$i],'max'=>$request->max[$i],'condition'=>$request->addon_conditions[$i]]);
        //         }
        //     }
        // }


        // foreach($request->addons as $addon){
        //     $dish->addons()->attach($addon);
        // }

        if(isset($request->allergens) > 0){
            foreach($request->allergens as $allergen){
                $dish->allergens()->attach($allergen);
            }
        }



        return back();

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $avalability=Availability::where(['user_id'=>auth()->guard('admin')->user()->id,'dish_id'=>$id])->first();
        if (is_null($avalability)) {

            $availability=Availability::create([
                'dish_id'=>$id,
                'user_id'=>auth()->guard('admin')->user()->id,
                'available'=>0
            ]);
            return response()->json(['success' => 0]);

        }else{

            if($avalability->available == 0){
                $avalability->delete();
                return response()->json(['success' => 1]);
            }
        }
        // dd($availability['available']);
        // return response()->json(['success' => $avalability['available']]);


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {


        $allergens=Allergen::all();
        $addons=Addons::all();
        $sections=Section::all();
        $cusines=Cusine::all();
        $categories=Category::all();
        if (auth()->guard('admin')->user()->roles[0]->name != 'cook'){
            $dish=Dish::find($id);
            $cooks=User::whereHas('roles',function($q){
                $q->where('name','cook');
            })->where('active',1)->get();
            $dish=Dish::find($id);
            $cook=User::find($dish->user_id);

            $addonsections=$cook->addonsection;
        return view('backend.edit-dish-list',compact('dish','addonsections','cusines','sections','addons','categories','allergens','cooks'));
        }
        else{
            $dish=Dish::find($id);
            $cook=User::find(auth()->guard('admin')->user()->id);
            $addons=$cook->addons;
            $addonsections=$cook->addonsection;
            $sections=$cook->sections;
            $categories=$cook->categories;

            return view('backend.edit-dish-list',compact('dish','addonsections','cusines','sections','addons','categories','allergens'));
        }



    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $dish=Dish::find($id);
        $rules=[
            'cusine'=>'required',
            'category'=>'required',
            // 'addons'=>'required',
            'calories'=>'required',
            'section'=>'required',
            'portions_available'=>'required',
            'dish_available'=>'required',
            // 'allergens'=>'required',
            // 'available_count'=>'required',
            'portions_price	'=>'requires|numeric|regex:/^\d*\.?\d*$/'
        ];
        if(isset($request->min[0])){
            $rules += [
                'max'=>'numeric|regex:/^\d*\.?\d*$/',
                'min'=>'numeric|regex:/^\d*\.?\d*$/',
            ];
        }
        if(auth()->guard('admin')->user()->roles[0]->name =='cook'){
            foreach (config('translatable.locales') as $locale) {
                $rules += [
                    $locale.'.name'=>['required'],
                    // $locale.'.main_ingredients'=>['required'],
                    // $locale.'.main_ingredients'=>['required'],
                    // $locale.'.info'=>['required'],
                ];
            }
            $request->validate( $rules);

            $cook_id=auth()->guard('admin')->user()->id;
        }
        else{
        $rules+=['cook_name'=>'required',];

            foreach (config('translatable.locales') as $locale) {
                $rules += [
                    $locale.'.name'=>['required'],
                    // $locale.'.main_ingredients'=>['required'],
                    // $locale.'.info'=>['required'],
                ];
            }
            $request->validate( $rules);
            $cook_id=$request->cook_name;
        }


        if($request->dish_type == 'main'){
            $request->dish_type =1;
        }else{
            $request->dish_type=0;
        }
        $dish_trans=$request->only(['en','ar']);



        $dish->update([
            // 'name'=>$request->name,
            // 'images'=>$dishes_images,
            'user_id'=>$cook_id,
            'calories'=>$request->calories,
'portions_available'=>json_encode($request->portions_available, true),
            'portions_price'=>json_encode($request->portions_price,
true),
            'available'=>$request->dish_available,
            'category_id'=>$request->category,
            'section_id'=>$request->section,
            'main_ingredients'=>$request->time_of_preparation,
            // 'info'=>$request->info,
            // 'available_count'=>$request->available_count,
        ]);

        if ($request->file('files')) {
            $dishes_images=array();
            foreach($request->file('files') as $index=>$img){
                $ext=$img->getClientOriginalExtension();
                $image_name=time().$index.'.'.$ext;
                $path='backend/img/dishes';
                $img->move($path,$image_name);
                array_push($dishes_images,$image_name);
                }
                $dishes_images=implode('__',$dishes_images);
        $dish->update([
             'images'=>$dishes_images,

        ]);

        }

        $dish->update($dish_trans);

        $dish->cusines()->sync($request->cusine);
        $dish->addonsections()->sync($request->addonsection);
        // if($request->addons){
        //     $dish->addons()->detach();

        //     for ($i=0; $i <count($request->addons) ; $i++) {
        //         if($request->addons[$i] != null && $request->min[$i] != null && $request->max[$i] != null){
        //             $dish->addons()->attach($request->addons[$i],['min' =>$request->min[$i],'max'=>$request->max[$i],'condition'=>$request->addon_conditions[$i]]);
        //         }
        //         elseif($request->addons[$i] != null){
        //             $dish->addons()->attach($request->addons[$i],['min' =>1,'max'=>1,'condition'=>0]);

        //         }

        //     }

        // }else{
        //     $dish->addons()->detach();
        // }
        // foreach($request->addons as $addon){
        //     $dish->addons()->attach($addon);
        // }

        // foreach($request->allergens as $allergen){
        //     $dish->allergens()->attach($allergen);
        // }


        return redirect()->to('/dashboard/dish');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $dish_trans=DishTranslation::where('dish_id',$id)->delete();
        $dish=Dish::find($id);


            $dish->update([
                'section_id'=>null,
            ]);
            if( ! empty( $dish->cusines[0])){
                $dish->cusines()->detach();

            }
            if( ! empty( $dish->allergens[0])){
                $dish->allergens()->detach();
            }
            if( ! empty( $dish->addonsections[0])){
                $dish->addonsections()->detach();
            }
            // if( ! empty( $dish->addons[0])){
            //     $dish->addons()->detach();

            // }

$dish_trans=DishTranslation::where('dish_id',$dish->id)->delete();
        $dish->delete();
        return back();
    }
}

