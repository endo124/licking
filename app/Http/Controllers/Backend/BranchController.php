<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\City;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule as Rule;

class BranchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $cities=City::all();
        $cooks=User::whereRoleIs('cook')->where('parent',auth()->guard('admin')->user()->id)->where('active' ,'!=',-1)
        ->get();


    return view('backend.branches-list',compact('cooks'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules=[
            'phone'=>'required',
            'email'=>'required|unique:users',
            'password'=>'required',
            // 'city'=>'required',
            // 'contract'=>'required|unique:users',
            // 'commission'=>'required',
            // 'commission_type'=>'required',
            // 'bank'=>'required',
            // 'credit_number'=>'required',
            // 'card_holder_name'=>'required',
        ];
        foreach (config('translatable.locales') as $locale) {
            $rules += [$locale.'.name'=>['required',Rule::unique('user_translations','name')]];
        }
        $validator=\Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return \Redirect::back()->with(['message_add'=>$validator->errors()]);
        }
        else{
            $cook_trans=$request->only(['en','ar']);
                $cook= User::create([
                    'images'=>'1606647695.jpg',
                    'email'=>$request->email,
                    'phone'=>$request->phone,
                    // 'card_holder_name'=>$card_holder_name,
                    // 'contract'=>$request->contract,
                    // 'commission'=>$request->commission,
                    // 'commission_type'=>$request->commission_type,
                    // 'city_id'=>$request->city,
                    // 'bank'=>$bank,
                    // 'credit_number'=>$credit_number,
                    'password'=>Hash::make($request->password),
                    'parent'=>auth()->guard('admin')->user()->id
                ]);
                $cook->update($cook_trans);
                $cook->roles()->attach(3);
            }
        return redirect('dashboard/branches');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
