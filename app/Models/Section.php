<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
class Section extends Model implements TranslatableContract
{

    use Translatable;

    protected $guarded = [];
    protected $fillable=['available_from','available_to'];
    public $translatedAttributes = ['name'];

    public function dishes(){
        return $this->hasMany('App\Models\Dish','section_id');
    }
    public function users(){
        return $this->belongsToMany('App\Models\User','cook_section','section_id','user_id');
    }

}
