<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable=['user_id','vendor','assign','cook_id','payment_method','delivery_type','discount_price','discount_id','date','time','orderEntry','total_price','address','status','soldout','paid'];


    public function users(){
        return $this->belongsTo('App\Models\User','user_id');

    }


    public function coupon(){
        return $this->belongsTo('App\Models\Discount','discount_id');
    }

    public $append=['vendor'];
    public $attribute=['vendor'];

    protected $casts = [
        'orderEntry' => 'array',
    ];


    public function getVendorAttribute () {

        // dd('dsgfdg');
        $user=UserTranslation::find($this->cook_id);
        return $user->name ;

    }


}
