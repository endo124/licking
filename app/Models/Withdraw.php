<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Withdraw extends Model
{

    public $table='withdraws';

    protected $fillable=['user_id','amount','status'];

    public function cooks(){
        return $this->belongsTo('App\Models\User','user_id');

    }

}
