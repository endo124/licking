<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Chat extends Model
{
    public $fillable=['sender_id','image','sender_name','message'];
    public $timestamps = true;

}
