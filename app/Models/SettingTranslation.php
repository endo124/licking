<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SettingTranslation extends Model
{
    public $table="setting_translations";

    public $timestamps = false;
    protected $fillable = ['terms'];
}
