<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DeliveryCharges extends Model
{
   public $table="delivery_charges";

   public $fillable=['cook_id','delivery_type','amount','from','to','orders'];

   public $timestamps = true;

   public function cooks()
   {
        return $this->belongsTo('App\Models\User','cook_id');
   }
}
