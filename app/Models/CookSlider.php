<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CookSlider extends Model
{
    public $table='cookslider';

    protected $fillable=['image','user_id'];

    public function cooks(){
        return $this->belongsTo('App\Models\User','user_id');
    }
}
